<?php
require_once("../../../database/pdo.mysql.connection.php");
require_once("../../../library/general.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

/*if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete-records") {

	$code = escapeString($_POST['selected'][0]);

	$q = mysql_query("SELECT code FROM _category WHERE parent_code = '$code'");
	if(mysql_num_rows($q) > 0) {
		$status  = "error";
		 $message = popupError001(); // INVALID ACTION, DEPENDENCY FOUND
	} else {
		$q = mysql_query("SELECT code FROM _program WHERE category_code = '$code'") or die(mysql_error());
		if(mysql_num_rows($q) > 0) {
			$status  = "error";
			$message = popupError001(); // INVALID ACTION, DEPENDENCY FOUND
        } else {
			mysql_query("DELETE FROM _category WHERE code = '$code'") or die(mysql_error());
		}
	}
}
*/

$stmt = $conn->prepare("SELECT * FROM _app ORDER BY name");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $install = ($row['install']) ? "Yes" : "No";
		$active  = ($row['active']) ? "Yes" : "No";
		
		echo '{ 
			"recid"       : "' . $row['app_id'] . '", 
			"app_id"      : "' . $row['app_id'] . '", 
			"name"        : "' . $SYSTEM_FUNCTIONS->cleanString($row['name']) . '",
			"description" : "' . $SYSTEM_FUNCTIONS->cleanString($row['description']) . '",
			"install"     : "' . $install . '",
			"active"      : "' . $active . '"
		}';

   }

echo ']}';

?>
