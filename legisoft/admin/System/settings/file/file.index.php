<div id="app_<?php echo $p_menu_id; ?>" style="width:100%; height:100%;"></div>
<script>
$(function () {	
	$().w2destroy('grid_<?php echo $p_menu_id; ?>');
	$('#app_<?php echo $p_menu_id; ?>').w2grid({ 
		name        : 'grid_<?php echo $p_menu_id; ?>',
		header      : '<?php echo $p_menu_name; ?>',
		multiSelect : false,
		url         : 'System/settings/file/json.file.php',
		show: {
			header        : true,
			toolbar       : true,
			footer        : true,
			lineNumbers   : true,
			toolbarAdd    : true,
			toolbarDelete : true,
			toolbarEdit   : true
		},      
		columns: [
			{ field: 'file_id',    size: '10%', caption: 'ID' },
			{ field: 'filename',   size: '20%', caption: 'Filename' },
			{ field: 'name',       size: '20%', caption: 'Name' },
			{ field: 'keyword',    size: '20%', caption: 'Keyword' },
			{ field: 'module_id',  hidden: true },			
			{ field: 'module',     size: '20%', caption: 'Module' },
			{ field: 'active_id',  hidden: true },
			{ field: 'active',     size: '10%', caption: 'Active' },
			
		],
		onAdd: function (event) {
			formAdd();
		},
		onEdit: function (event) {
			formEdit();
		},
		onDelete: function (event) {},
		multiSearch: false,
		searches: [
			{ field: 'name', caption: 'Name', type: 'text' }
		]
	});
	
	function openPopupForm(action) {
		$().w2destroy('form_<?php echo $p_menu_id; ?>');
		$().w2form({
			name  : 'form_<?php echo $p_menu_id; ?>',
			style : 'border: 0px; background-color: transparent;',
			url   : 'System/settings/file/save.file.php',
			formHTML: 
				'<div class="w2ui-page page-0" style="padding-top:25px;">' +
				'    <div class="w2ui-field">' +
				'        <label>File ID:</label>' +
				'        <div>' +
				'           <input name="file_id" type="text" maxlength="20" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Filename:</label>' +
				'        <div>' +
				'           <input name="filename" type="text" maxlength="255" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Name:</label>' +
				'        <div>' +
				'           <input name="name" type="text" maxlength="50" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Keyword:</label>' +
				'        <div>' +
				'           <input name="keyword" type="text" maxlength="30" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Module:</label>' +
				'        <div>' +
				'            <input name="module_id" type="list" style="width: 250px"/>' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Active:</label>' +
				'        <div>' +
				'            <input name="active" type="list" style="width: 250px"/>' +
				'        </div>' +
				'    </div>' +
				'</div>' +
				'<div class="w2ui-buttons">' +
				'    <button class="w2ui-btn" name="reset">Reset</button>' +
				'    <button class="w2ui-btn" name="save">Save</button>' +
				'</div>',
			fields: [
				{ field: 'file_id',   type: 'text', required: true },
				{ field: 'filename',  type: 'text', required: true },
				{ field: 'name',      type: 'text', required: true },
				{ field: 'keyword',   type: 'text' },
				{ field: 'module_id', type: 'list', required: true,
					options: { 
						url:     'System/settings/module/json.module.php', 
						recId:   'module_id',
						recText: 'name', 
						minLength: 0
					} 
				},
				{ field: 'active', type: 'list', required: true, 
					options: {
						items: [
							{ id: 1, text: 'Yes' },
							{ id: 0, text: 'No' }
						]
					} 
				}
			],
			actions: {
				"save": function() { 
					this.save(function(data){ 
						w2ui['grid_<?php echo $p_menu_id; ?>'].reload(); 
						w2popup.close();
					}); 
				},
				"reset": function() { this.clear(); }
			},
			onSave: function(event) {}, 
			onError: function(event) {},
			postData: {
        		'action' : action
			}
		});
	}

	function formAdd() {
		openPopupForm('add');
		$().w2popup('open', {
			title   : 'Add New <?php echo $p_menu_name; ?>',
			body    : '<div id="popup_form_<?php echo $p_menu_id; ?>" style="width: 100%; height: 100%;"></div>',
			style   : 'padding: 0px; border-radius:0;',
			width   : 530,
			height  : 350, 
			onOpen: function (event) {
				event.onComplete = function () {
					$('#w2ui-popup #popup_form_<?php echo $p_menu_id; ?>').w2render('form_<?php echo $p_menu_id; ?>');
					$('input[name=active]').w2field().set({ id: 1, text: 'Yes' });
					w2ui['form_<?php echo $p_menu_id; ?>'].refresh();
				}
			},
			onClose: function () {
				w2ui['form_<?php echo $p_menu_id; ?>'].clear();
			}
		});
	}
	
	function formEdit() {
		openPopupForm('edit');
		$().w2popup('open', {
			title   : 'Edit <?php echo $p_menu_name; ?>',
			body    : '<div id="popup_form_<?php echo $p_menu_id; ?>" style="width: 100%; height: 100%;"></div>',
			style   : 'padding: 0px; border-radius:0;',
			width   : 530,
			height  : 350, 
			onOpen: function (event) {
				event.onComplete = function () {
					$('#w2ui-popup #popup_form_<?php echo $p_menu_id; ?>').w2render('form_<?php echo $p_menu_id; ?>');

					var sel    = w2ui['grid_<?php echo $p_menu_id; ?>'].getSelection();
					var record = w2ui['grid_<?php echo $p_menu_id; ?>'].get(sel[0]);
					
					w2ui['form_<?php echo $p_menu_id; ?>'].record['module_id'] = record['module_id']; ;
					w2ui['form_<?php echo $p_menu_id; ?>'].record['name'] = record['name']; 
					w2ui['form_<?php echo $p_menu_id; ?>'].record['keyword'] = record['keyword']; 
					
					$('input[name=app_id]').w2field().set({ id: record['app_id'], text: record['app'] });
					$('input[name=parent_id]').w2field().set({ id: record['parent_id'], text: record['parent'] });
					$('input[name=active]').w2field().set({ id: record['active_id'], text: record['active'] });
					$('input[name=module_id]').prop("readonly", true);
					w2ui['form_<?php echo $p_menu_id; ?>'].refresh();	
				}
			},
			onClose: function () {
				w2ui['form_<?php echo $p_menu_id; ?>'].clear();
			}
		});
	}
	
	 
});
</script>
