<?php
require_once("../../../database/pdo.mysql.connection.php");

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);

	$file_id   = $data['record']['file_id'];
	$filename  = $data['record']['filename'];
	$name      = $data['record']['name'];
	$keyword   = $data['record']['keyword'];
	$module_id = $data['record']['module_id']['id'];
	$active    = $data['record']['active']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {	
		$stmt = $conn->prepare("SELECT file_id FROM _app_module_file WHERE file_id = :file_id");
		$stmt->bindParam(':file_id', $file_id, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO _app_module_file SET file_id = :file_id, keyword = :keyword, filename = :filename, name = :name, module_id = :module_id, active = :active, created_date = NOW(), created_by = ''");
			$stmt->bindParam(':file_id',    $file_id,   PDO::PARAM_STR);
			$stmt->bindParam(':keyword',    $keyword,   PDO::PARAM_STR);
			$stmt->bindParam(':filename',   $filename,  PDO::PARAM_STR);
			$stmt->bindParam(':name',       $name,      PDO::PARAM_STR);
			$stmt->bindParam(':module_id',  $module_id, PDO::PARAM_STR);
			$stmt->bindParam(':active',     $active,    PDO::PARAM_STR);
			//$stmt->bindParam(':created_by', '', PDO::PARAM_STR);
			$stmt->execute();
		
		} else {
			$status  = 'error';
			$message = 'Duplicate File ID!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$stmt = $conn->prepare("UPDATE _app_module_file SET keyword = :keyword, filename = :filename, name = :name, module_id = :module_id, active = :active, created_date = NOW(), created_by = '' WHERE file_id = :file_id");
		$stmt->bindParam(':file_id',    $file_id,   PDO::PARAM_STR);
		$stmt->bindParam(':keyword',    $keyword,   PDO::PARAM_STR);
		$stmt->bindParam(':filename',   $filename,  PDO::PARAM_STR);
		$stmt->bindParam(':name',       $name,      PDO::PARAM_STR);
		$stmt->bindParam(':module_id',  $module_id, PDO::PARAM_STR);
		$stmt->bindParam(':active',     $active,    PDO::PARAM_STR);
		//$stmt->bindParam(':created_by', '', PDO::PARAM_STR);
		$stmt->execute();
	
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>