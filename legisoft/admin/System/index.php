<?php
require_once("../database/pdo.mysql.connection.php");
require_once("../library/general.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

?>
<div id="app_system" style="width: 100%; height: 100%;"></div>
<script>
	$(function () {
		var pstyle = 'border: 1px solid #dfdfdf; ';
		$('#app_system').w2layout({
			name: 'app_system_layout',
			padding: 4,
			panels: [
				{ type: 'left', size: 200, style: pstyle },
				{ type: 'main' }
			]
		});
		
		
		w2ui['app_system_layout'].content('left', $().w2sidebar({
				name : 'app_system_sidebar',
				flatButton: true,
				nodes: [
					{ id: 'level-1', text: 'Application', img: 'icon-folder', expanded: true, group: true, groupShowHide: false,
						nodes : [
							<?php
								//if(getSessionVar('usercode') == getRootCode()) {
									$stmt = $conn->prepare("SELECT module_id, name FROM _app_module WHERE parent_id = '0' AND active = '1' ORDER BY name");
									$stmt->execute();
								//} else {
								//	$q = mysql_query("SELECT DISTINCT a.code, a.name FROM _category a INNER JOIN _program b ON a.code = b.category_code INNER JOIN _userprogram c ON b.code = c.program_code WHERE a.parent_code = '0' AND a.popup_menu = '0' AND a.active = '1' AND c.usercode = '".getSessionVar('usercode')."' ORDER BY a.name") or die(mysql_error());
								//}
				
								while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
									echo "{ id: '$row[module_id]', text: '$row[name]', img: 'icon-folder', expanded: false,";
									echo "nodes: [";
									$SYSTEM_FUNCTIONS->displayMenu($row['module_id']);
									echo "]";
									echo "},";
								}
							?>
							{ id: '99', text: 'Help', img: 'icon-page' },
						]
					}
				],
				onClick: function (event) {
					var file_id = event.target;
					
					// check if click on files
					if(file_id.length > 7) {
						w2ui['app_system_layout'].load('main', 'router.php?file_id=' + file_id);
					}
				},
				onFlat: function (event) {
					if(event.goFlat) {
						w2ui['app_system_layout'].set('left', { size: 50 });
					} else {
						w2ui['app_system_layout'].set('left', { size: 200 });
					}
				}
			})
		);
		

	});
</script>
