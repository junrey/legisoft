<?php
class SystemFunctions {

function getDefaultApp() {
	global $conn;
	$app = array();
	$app['url']  = '';
	$app['name'] = '';
	
	if($this->getSessionVar('user_id') == $this->getRootID()) {
		$app['url']  = 'System/index.php';
		$app['name'] = 'System Settings';
		
	} else {
		$stmt = $conn->prepare("SELECT a.index_file, a.name FROM _app AS a INNER JOIN _app_module AS b ON a.app_id = b.app_id INNER JOIN _app_module_file AS c ON b.module_id = c.module_id INNER JOIN _user_file AS d ON c.file_id = d.file_id WHERE user_id = :user_id");
		$stmt->bindParam(':user_id', $this->getSessionVar('user_id'), PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$app['url']  = $row['index_file'];
			$app['name'] = $row['name'];
		}
	}
	
	return $app;
}

function displayMenu($parent_id) {
	global $conn;
	
	$stmt = $conn->prepare("SELECT module_id, name FROM _app_module WHERE parent_id = :parent_id AND active = '1' ORDER BY name");
	$stmt->bindParam(':parent_id', $parent_id, PDO::PARAM_STR);
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "{ id: '$row[module_id]', text: '" . $this->cleanString($row['name']) . "', img: 'icon-folder', expanded: false,";
                    
        echo "nodes: [";

        $this->displayMenu($row['module_id']);
        
        echo "]";

        echo "},";

	}
	
	if($this->getSessionVar('user_id') == $this->getRootID()) {
		$stmt2 = $conn->prepare("SELECT file_id, name FROM _app_module_file WHERE module_id = :parent_id AND active = '1' ORDER BY name");
		$stmt2->bindParam(':parent_id', $parent_id, PDO::PARAM_STR);
	} else {
		$stmt2 = $conn->prepare("SELECT DISTINCT a.file_id, a.name FROM _app_module_file a INNER JOIN _user_file b ON a.file_id = b.file_id WHERE a.module_id = :parent_id AND a.active = '1' AND b.user_id = :user_id ORDER BY a.name");
		$stmt2->bindParam(':parent_id', $parent_id, PDO::PARAM_STR);
		$stmt2->bindParam(':user_id',   $this->getSessionVar('user_id'), PDO::PARAM_STR);
	}
	
	$stmt2->execute();
	
	while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
        echo "{ id: '$row2[file_id]', text: '" . $this->cleanString($row2['name']) . "', img: 'icon-page' },";
	}

}

function filterString($s) {
	return trim($s);
}
function filterInput($s) {
	return filter_var(trim($s), FILTER_SANITIZE_STRING);
}
function cleanString($s) {
	$s = preg_replace('/[^A-Za-z0-9\. -]/', '', $s);
	$s = htmlentities($s, ENT_QUOTES);

	return $this->setUTF8String($s);
}
function setUTF8String($s) {
	return mb_convert_encoding(trim($s), 'utf-8');
}
function generateCode($prx) {
    return strtoupper(uniqid($prx));
}

function passwordHash($s) {
	return password_hash($s, PASSWORD_DEFAULT);
}

function getSystemName() {
	return 'core';
}

function getRootID() {
	return 'root';
}
function getRootUsername() {
	return 'root';
}
function getSessionVar($var) {
	return $_SESSION[$this->getSystemName()][$var];
}


}
?>