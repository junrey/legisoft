<?php
class GeneralFunctions {

function filterString($s) {
	return trim($s);
}
function filterInput($s) {
	return filter_var(trim($s), FILTER_SANITIZE_STRING);
}
function cleanString($s) {
	$s = preg_replace('/[^A-Za-z0-9\. -]/', '', $s);
	$s = htmlentities($s, ENT_QUOTES);

	return $this->setUTF8String($s);
}
function setUTF8String($s) {
	return mb_convert_encoding(trim($s), 'utf-8');
}
function generateCode($prx) {
    return strtoupper(uniqid($prx));
}

function passwordHash($s) {
	return password_hash($s, PASSWORD_DEFAULT);
}

function getSystemName() {
	return 'core';
}

function getRootID() {
	return 'root';
}
function getRootUsername() {
	return 'root';
}
function getSessionVar($var) {
	return $_SESSION[$this->getSystemName()][$var];
}

function getSystemPath() {
	return "http://" . $_SERVER['SERVER_NAME'] . "/legisoft";
}

function getWeeks($date, $rollover) {
	$cut = substr($date, 0, 8);
	$daylen = 86400;

	$timestamp = strtotime($date);
	$first = strtotime($cut . "00");
	$elapsed = ($timestamp - $first) / $daylen;

	$weeks = 1;

	for ($i = 1; $i <= $elapsed; $i++) {
		$dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
		$daytimestamp = strtotime($dayfind);

		$day = strtolower(date("l", $daytimestamp));

		if($day == strtolower($rollover))  $weeks ++;
	}

	return $weeks;
}


}
?>