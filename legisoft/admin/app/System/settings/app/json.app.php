<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	$code = $data['selected'];
	
	if($data['cmd'] == 'delete') {
		$code = $code[0] ;
		
		$stmt = $conn->prepare("SELECT app_id FROM _app_module WHERE app_id = :app_id");
		$stmt->bindParam(':app_id', $code, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("DELETE FROM _app WHERE app_id = :app_id");
			$stmt->bindParam(':app_id', $code, PDO::PARAM_STR);
			$stmt->execute();
		
		} else {
			$status = "error";
			$message = ''; // INVALID ACTION, DEPENDENCY FOUND
		}
	}
}

$stmt = $conn->prepare("SELECT * FROM _app ORDER BY name");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$install = ($row['install']) ? "Yes" : "No";
		$active  = ($row['active']) ? "Yes" : "No";
		 
		echo '{ 
			"recid"       : "' . $row['app_id'] . '", 
			"app_id"      : "' . $row['app_id'] . '", 
			"name"        : "' . htmlspecialchars($row['name']) . '", 
			"description" : "' . htmlspecialchars($row['description']) . '", 
			"icon"        : "' . htmlspecialchars($row['icon']) . '", 
			"install_id"  : "' . $row['install'] . '",
			"install"     : "' . $install . '",
			"active_id"   : "' . $row['active'] . '",
			"active"      : "' . $active . '"
		}';

   }

echo ']}';

?>
