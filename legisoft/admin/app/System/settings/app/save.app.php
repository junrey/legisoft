<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);

	$app_id      = $data['record']['app_id'];
	$name        = $data['record']['name'];
	$description = $data['record']['description'];
	$icon        = $data['record']['icon'];
	$install     = $data['record']['install']['id'];
	$active      = $data['record']['active']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {	
		$stmt = $conn->prepare("SELECT app_id FROM _app WHERE app_id = :app_id");
		$stmt->bindParam(':app_id', $app_id, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO _app SET app_id = :app_id, name = :name, description = :description, icon = :icon, install = :install, active = :active, created_date = NOW(), created_by = :created_by");
			$stmt->bindParam(':app_id',      $app_id,      PDO::PARAM_STR);
			$stmt->bindParam(':name',        $name,        PDO::PARAM_STR);
			$stmt->bindParam(':description', $description, PDO::PARAM_STR);
			$stmt->bindParam(':icon',        $icon,        PDO::PARAM_STR);
			$stmt->bindParam(':install',     $install,     PDO::PARAM_STR);
			$stmt->bindParam(':active',      $active,      PDO::PARAM_STR);
			$stmt->bindParam(':created_by',  $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
			$stmt->execute();
			//print_r($conn->errorInfo());
		} else {
			$status  = 'error';
			$message = 'Duplicate ID!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$stmt = $conn->prepare("UPDATE _app SET name = :name, description = :description, icon = :icon, install = :install, active = :active, updated_date = NOW(), updated_by = :updated_by WHERE app_id = :app_id");
		$stmt->bindParam(':app_id',      $app_id,      PDO::PARAM_STR);
		$stmt->bindParam(':name',        $name,        PDO::PARAM_STR);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);
		$stmt->bindParam(':icon',        $icon,        PDO::PARAM_STR);
		$stmt->bindParam(':install',     $install,     PDO::PARAM_STR);
		$stmt->bindParam(':active',      $active,      PDO::PARAM_STR);
		$stmt->bindParam(':updated_by',  $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
		$stmt->execute();
		
		//print_r($stmt->errorInfo()); 
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>