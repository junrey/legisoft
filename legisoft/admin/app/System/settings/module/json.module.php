<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	$code = $data['selected'];
	
	if($data['cmd'] == 'delete') {
		$code = $code[0] ;
		
		$stmt = $conn->prepare("SELECT module_id FROM _app_module WHERE parent_code = :parent_code");
		$stmt->bindParam(':parent_code', $code, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt2 = $conn->prepare("SELECT file_id FROM _app_module_file WHERE module_id = :module_id");
			$stmt2->bindParam(':module_id', $code, PDO::PARAM_STR);
			$stmt2->execute();
			
			if($stmt2->rowCount() == 0) {
				$stmt3 = $conn->prepare("DELETE FROM _app_module WHERE module_id = :module_id");
				$stmt3->bindParam(':module_id', $code, PDO::PARAM_STR);
				$stmt3->execute();
			
			} else {
				$status  = 'error';
				$message = ''; // INVALID ACTION, DEPENDENCY FOUND
			} 
		} else {
			$status  = 'error';
			$message = ''; // INVALID ACTION, DEPENDENCY FOUND
		}
	}

}


$array_app = array();
$stmt = $conn->prepare("SELECT app_id, name FROM _app");
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$array_app[$row["app_id"]] = $row["name"];
}
function getAppDesc($app_id) {
	if($app_id != "") {
		global $array_app;
		if(array_key_exists($app_id, $array_app)) {
			return $array_app[$app_id];
		} else {
			return $app_id;
		}
	} else {
		return $app_id;
	}
}	

$array_module = array();
$stmt = $conn->prepare("SELECT module_id, name FROM _app_module");
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$array_module[$row["module_id"]] = $row["name"];
}
function getModuleDesc($module_id) {
	if($module_id != "") {
		global $array_module;
		if(array_key_exists($module_id, $array_module)) {
			return $array_module[$module_id];
		} else {
			return $module_id;
		}
	} else {
		return $module_id;
	}
}

$stmt = $conn->prepare("SELECT * FROM _app_module ORDER BY name");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$active = ($row['active']) ? "Yes" : "No";
		 
		echo '{ 
			"recid"     : "' . $row['module_id'] . '", 
			"module_id" : "' . $row['module_id'] . '", 
			"app_id"    : "' . $row['app_id'] . '", 
			"app"       : "' . htmlspecialchars(getAppDesc($row['app_id'])) . '", 
			"name"      : "' . htmlspecialchars($row['name']) . '",
			"keyword"   : "' . htmlspecialchars($row['keyword']) . '", 
			"parent_id" : "' . $row['parent_id'] . '", 
			"parent"    : "' . htmlspecialchars(getModuleDesc($row['parent_id'])) . '", 
			"active_id" : "' . $row['active'] . '",
			"active"    : "' . $active . '"
		}';

   }

echo ']}';

?>
