<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$module_id = $SYSTEM_FUNCTIONS->filterString($data['record']['module_id']);
	$app_id    = $SYSTEM_FUNCTIONS->filterString($data['record']['app_id']['id']);
	$name      = $SYSTEM_FUNCTIONS->filterString($data['record']['name']);
	$keyword   = $SYSTEM_FUNCTIONS->filterString($data['record']['keyword']);

	$parent_id = 0;
	if(isset($data['record']['parent_id']['id'])) {
		$parent_id = $SYSTEM_FUNCTIONS->filterString($data['record']['parent_id']['id']);
	}
	
	$active = $data['record']['active']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {
		$stmt = $conn->prepare("SELECT module_id FROM _app_module WHERE module_id = :module_id");
		$stmt->bindParam(':module_id',  $module_id,  PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO _app_module SET module_id = :module_id, app_id = :app_id, name = :name, keyword = :keyword, parent_id = :parent_id, active = :active, created_date = NOW(), created_by = :created_by");
			$stmt->bindParam(':module_id',  $module_id,  PDO::PARAM_STR);
			$stmt->bindParam(':app_id',     $app_id,     PDO::PARAM_STR);
			$stmt->bindParam(':name',       $name,       PDO::PARAM_STR);
			$stmt->bindParam(':keyword',    $keyword,    PDO::PARAM_STR);
			$stmt->bindParam(':parent_id',  $parent_id,  PDO::PARAM_STR);
			$stmt->bindParam(':active',     $active,     PDO::PARAM_STR);
			$stmt->bindParam(':created_by', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
			$stmt->execute();
		
		} else {
			$status  = 'error';
			$message = 'Duplicate ID!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$stmt = $conn->prepare("UPDATE _app_module SET app_id = :app_id, name = :name, keyword = :keyword, parent_id = :parent_id, active = :active, updated_date = NOW(), updated_by = :updated_by WHERE module_id = :module_id");
		$stmt->bindParam(':module_id',  $module_id,  PDO::PARAM_STR);
		$stmt->bindParam(':app_id',     $app_id,     PDO::PARAM_STR);
		$stmt->bindParam(':name',       $name,       PDO::PARAM_STR);
		$stmt->bindParam(':keyword',    $keyword,    PDO::PARAM_STR);
		$stmt->bindParam(':parent_id',  $parent_id,  PDO::PARAM_STR);
		$stmt->bindParam(':active',     $active,     PDO::PARAM_STR);
		$stmt->bindParam(':updated_by', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
		$stmt->execute();
	
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>