<?php session_start();
require_once("../../database/pdo.mysql.connection.php");
require_once("../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

?>
<div id="app_system" style="width: 100%; height: 100%;"></div>
<script>
	$(function () {
		var pstyle = 'border-right: 1px solid #b9cee9;';
		$('#app_system').w2layout({
			name: 'app_system_layout',
			padding: 0,
			panels: [
				{ type: 'left', size: 200, style: pstyle },
				{ type: 'main' }
			]
		});
		
		
		w2ui['app_system_layout'].content('left', $().w2sidebar({
				name : 'app_system_sidebar',
				flatButton: true,
				nodes: [
					{ id: 'level-1', text: 'Application', img: 'icon-folder', expanded: true, group: true, groupShowHide: false,
						nodes : [
							<?php
								if($SYSTEM_FUNCTIONS->getSessionVar('user_id') == $SYSTEM_FUNCTIONS->getRootID()) {
									$stmt = $conn->prepare("SELECT module_id, name FROM _app_module WHERE parent_id = '0' AND active = '1' ORDER BY name");
								} else {
									$stmt = $conn->prepare("SELECT DISTINCT a.module_id, a.name FROM _app_module a INNER JOIN _app_module_file b ON a.module_id = b.module_id INNER JOIN _user_file c ON b.file_id = c.file_id WHERE a.parent_id = '0' AND a.active = '1' AND c.user_id = :user_id ORDER BY a.name");
									$stmt->bindParam(':user_id', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR);
								}
								
								$stmt->execute();
								while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
									echo "{ id: '$row[module_id]', text: '" . $SYSTEM_FUNCTIONS->cleanString($row['name']) . "', img: 'icon-folder', expanded: false,";
									echo "nodes: [";
									$SYSTEM_FUNCTIONS->displayMenu($row['module_id']);
									echo "]";
									echo "},";
								}
							?>
							{ id: '99', text: 'Help', img: 'icon-page' },
						]
					}
				],
				onClick: function (event) {
					var file_id = event.target;
					
					// check if click on files
					if(file_id.length > 7) {
						w2ui['app_system_layout'].load('main', 'router.php?file_id=' + file_id);
					}
				},
				onFlat: function (event) {
					if(event.goFlat) {
						w2ui['app_system_layout'].set('left', { size: 50 });
					} else {
						w2ui['app_system_layout'].set('left', { size: 200 });
					}
				}
			})
		);
		

	});
</script>
