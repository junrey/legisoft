<div id="app_<?php echo $p_menu_id; ?>" style="width:100%; height:100%;"></div>
<script>
$(function () {
	var user_id = '';
	$().w2destroy('layout_grid');
	$().w2destroy('grid_<?php echo $p_menu_id; ?>');
	
	$('#app_<?php echo $p_menu_id; ?>').w2grid({ 
		name        : 'grid_<?php echo $p_menu_id; ?>',
		header      : '<?php echo $p_menu_name; ?>',
		multiSelect : false,
		url         : 'app/System/user/account/json.account.php',
		show: {
			header        : true,
			toolbar       : true,
			footer        : true,
			lineNumbers   : true,
			toolbarAdd    : false,
			toolbarDelete : false,
			toolbarEdit   : false
		},
		toolbar: {
			items: [
				{ type: 'button', id: 'add_access', caption: 'Add Access Rights', icon: 'w2ui-icon-plus', disabled: true }
			],
			onClick: function (target, data) {
				if (target == 'add_access') {
					w2confirm('Do you want to assign access rights?', function (btn) {
						if(btn.toUpperCase() == "YES") {
							var sel = w2ui['grid_<?php echo $p_menu_id; ?>'].getSelection();
							var recid = sel[0];
							
							user_id = recid;

							if(recid == "<?php echo $SYSTEM_FUNCTIONS->getSessionVar('user_id'); ?>") {
								w2alert('Editing your own rights is invalid!');
							} else {
								 w2popup.open({
									title   : '<?php echo $p_menu_name; ?> For ' + (w2ui['grid_<?php echo $p_menu_id; ?>'].get(recid)).firstname,
									style   : 'padding:5px; overflow: hidden',
									height  : '500',
									width   : '900',
									showMax : true,
									body:  '<div id="popup_rights" style="width:100%; height:100%; padding:4px;"></div>',
									onOpen  : function (event) {
										event.onComplete = function () {  
											$('#w2ui-popup #popup_rights').w2render('layout_grid');
											$().w2destroy('grid1');
											w2ui['layout_grid'].content('left',
												$().w2grid({ 
													name: 'grid1',
													header: 'Available Rights',
													url: 'app/System/user/rights/json.user.file.php?user_id=' + user_id,
													show: {
														header        : true,
														toolbar       : true,
														footer        : true,
														selectColumn  : true,
														lineNumbers   : true,
														toolbarSearch : false,
														toolbarInput  : false,
														toolbarReload : false,
														toolbarColumns: false
													},
													toolbar: {
														items: [
															{ type: 'button', id: 'add_access', caption: 'Add Access', icon: 'w2ui-icon-plus', disabled: true }
														],
														onClick: function (target, data) {
															if (target == 'add_access') {
																w2confirm('Do you want to assign?', function (btn) {
																	if(btn.toUpperCase() == "YES") {
																		var sel = w2ui['grid1'].getSelection();
																		w2ui['grid2'].load( 'app/System/user/rights/json.user.file.assign.php?user_id=' + user_id + '&addaccess=' + sel);
																		w2ui['grid1'].reload();
																	}
																})			
																
															}
														}
													},
													columns: [
														{ field: 'module', caption: 'Module', size: '30%' },
														{ field: 'file',   caption: 'File', size: '70%' },			
													],
													onSelect: function(event) {
														w2ui['grid1'].toolbar.enable('add_access');
													},
													onUnselect: function(event) {
														w2ui['grid1'].toolbar.disable('add_access');
													}, 
												})
											
											);
											
											$().w2destroy('grid2');
                							w2ui['layout_grid'].content('main',
												$().w2grid({ 
													name: 'grid2',
													header: 'Assigned Rights',
													url: 'app/System/user/rights/json.user.file.assign.php?user_id=' + user_id,
													show: {
														header        : true,
														toolbar       : true,
														footer        : true,
														selectColumn  : true,
														lineNumbers   : true,
														toolbarSearch : false,
														toolbarInput  : false,
														toolbarReload : false,
														toolbarColumns: false
													},
													toolbar: {
														items: [
															{ type: 'button', id: 'remove_access', caption: 'Remove Access', icon: 'w2ui-icon-cross', disabled: true }
														],
														onClick: function (target, data) {
															if (target == 'remove_access') {
																w2confirm('Do you want to remove?', function (btn) {
																	if(btn.toUpperCase() == "YES") {	
																		var sel = w2ui['grid2'].getSelection();
																		w2ui['grid1'].load( 'app/System/user/rights/json.user.file.php?user_id=' + user_id + '&removeaccess=' + sel);
																		w2ui['grid2'].reload();
																	}
																})                   
															}
														}
													},
													columns: [
														{ field: 'module', caption: 'Module', size: '30%' },
														{ field: 'file',   caption: 'File', size: '70%' },			
													],
													onSelect: function(event) {
														w2ui['grid2'].toolbar.enable('remove_access');
													},
													onUnselect: function(event) {
														w2ui['grid2'].toolbar.disable('remove_access');
													}, 
												})
											
											);
										}
									},
									onToggle: function (event) { 
										event.onComplete = function () {
											w2ui['layout_grid'].resize();
										}
									}      
								});
							}
						}
					})			
					
				}
			}
		},   
		columns: [
			{ field: 'username',   size: '10%', caption: 'Username' },
			{ field: 'firstname',  size: '20%', caption: 'First Name' },
			{ field: 'lastname',   size: '20%', caption: 'Last Name' },
			{ field: 'middlename', size: '20%', caption: 'Middle Name' },
			{ field: 'email',      size: '20%', caption: 'Email' },
			{ field: 'active_id',  hidden: true },
			{ field: 'active',     size: '10%', caption: 'Active' },
			
		],
		 onSelect: function(event) {
			w2ui['grid_<?php echo $p_menu_id; ?>'].toolbar.enable('add_access');
		},
		onUnselect: function(event) {
			w2ui['grid_<?php echo $p_menu_id; ?>'].toolbar.disable('add_access');
		}, 
		multiSearch: false,
		searches: [
			{ field: 'username', caption: 'Username', type: 'text' },
			{ field: 'firstname', caption: 'First Name', type: 'text' },
			{ field: 'lastname', caption: 'Last Name', type: 'text' },
			{ field: 'middlename', caption: 'Middle Name', type: 'text' }
		]
	});
	
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	
	$().w2layout({
		name: 'layout_grid',
		padding: 4,
		panels: [
			{ type: 'left', size: '50%', resizable: true, content: '<div id="grid1" style="width:100%; height:100%;"></div>' },
			{ type: 'main', size: '50%', resizable: true, content: '<div id="grid2" style="width:100%; height:100%;"></div>' }
		]
	});
	
	 
});
</script>
