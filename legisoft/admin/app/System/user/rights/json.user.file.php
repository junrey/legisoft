<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$user_id = '';
if(isset($_GET['user_id'])) {
	$user_id = $_GET['user_id'];
}

$status  = 'success';
$message = '';

if(isset($_GET['removeaccess'])) {
	$removeaccess = $_GET['removeaccess'];
    $array        = explode(',', $removeaccess);

    foreach ($array as $value) {
		$stmt = $conn->prepare("DELETE FROM _user_file WHERE file_id = :file_id AND user_id = :user_id");
		$stmt->bindParam(':file_id', $value,   PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
		$stmt->execute();
    }
}

if($SYSTEM_FUNCTIONS->getSessionVar('user_id') == $SYSTEM_FUNCTIONS->getRootID()) {
	$stmt = $conn->prepare("SELECT module_id, name FROM _app_module WHERE active = '1'");
} else {
	$stmt = $conn->prepare("SELECT DISTINCT a.module_id, a.name FROM _app_module AS a INNER JOIN _app_module_file AS b ON a.module_id = b.module_id INNER JOIN _user_file AS c ON b.file_id = c.file_id WHERE a.active = '1' AND c.user_id = :user_id");
	$stmt->bindParam(':user_id', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR);
}
$stmt->execute();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		if($SYSTEM_FUNCTIONS->getSessionVar('user_id') == $SYSTEM_FUNCTIONS->getRootID()) {
           	$stmt2 = $conn->prepare("SELECT file_id, name FROM _app_module_file WHERE module_id = :module_id AND active = '1'");
			$stmt2->bindParam(':module_id', $row['module_id'], PDO::PARAM_STR);
			
        } else {
        	$stmt2 = $conn->prepare("SELECT DISTINCT p.file_id, p.name FROM _app_module_file p INNER JOIN _user_file u ON p.file_id = u.file_id WHERE p.module_id = :module_id AND p.active = '1' AND u.user_id = :user_id");
        	$stmt2->bindParam(':module_id', $row['module_id'], PDO::PARAM_STR);
			$stmt2->bindParam(':user_id',   $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR);
			
		}

		$stmt2->execute();
		
		while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {

            $stmt3 = $conn->prepare("SELECT file_id FROM _user_file WHERE user_id = :user_id AND file_id = :file_id") or die(mysql_error());
			$stmt3->bindParam(':user_id', $user_id, PDO::PARAM_STR);
			$stmt3->bindParam(':file_id', $row2['file_id'], PDO::PARAM_STR);
			$stmt3->execute();
			
			if($stmt3->rowCount() == 0) {
				$cnt++; if($cnt > 1) { echo ","; }
				
				echo '{ 
					"recid"  : "' . $row2['file_id'] . '",  
					"module" : "' . htmlspecialchars($row['name']) . '", 
					"file"   : "' . htmlspecialchars($row2['name']) . '"
				}';
				
            }
        }

   }

echo '], "total": "' . $cnt . '" }';

?>

