<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	$code = $data['selected'];
	
	if($data['cmd'] == 'delete') {
		$code = $code[0] ;
		
		$stmt = $conn->prepare("UPDATE _user SET deleted = '1' WHERE user_id = :user_id");
		$stmt->bindParam(':user_id', $code, PDO::PARAM_STR);
		$stmt->execute();
		
	}
}

$stmt = $conn->prepare("SELECT * FROM _user WHERE deleted = '0' ORDER BY lastname, firstname");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$active  = ($row['active']) ? "Yes" : "No";
		 
		echo '{ 
			"recid"      : "' . $row['user_id'] . '", 
			"user_id"    : "' . $row['user_id'] . '", 
			"username"   : "' . $row['username'] . '", 
			"firstname"  : "' . htmlspecialchars($row['firstname']) . '", 
			"lastname"   : "' . htmlspecialchars($row['lastname']) . '", 
			"middlename" : "' . htmlspecialchars($row['middlename']) . '", 
			"email"      : "' . $row['email'] . '",
			"active_id"  : "' . $row['active'] . '",
			"active"     : "' . $active . '"
		}';

   }

echo ']}';

?>
