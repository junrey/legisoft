<div id="app_<?php echo $p_menu_id; ?>" style="width:100%; height:100%;"></div>
<script>
$(function () {	
	$().w2destroy('grid_<?php echo $p_menu_id; ?>');
	$('#app_<?php echo $p_menu_id; ?>').w2grid({ 
		name        : 'grid_<?php echo $p_menu_id; ?>',
		header      : '<?php echo $p_menu_name; ?>',
		multiSelect : false,
		url         : 'app/System/user/account/json.account.php',
		show: {
			header        : true,
			toolbar       : true,
			footer        : true,
			lineNumbers   : true,
			toolbarAdd    : true,
			toolbarDelete : true,
			toolbarEdit   : true
		},      
		columns: [
			{ field: 'username',   size: '10%', caption: 'Username' },
			{ field: 'firstname',  size: '20%', caption: 'First Name' },
			{ field: 'lastname',   size: '20%', caption: 'Last Name' },
			{ field: 'middlename', size: '20%', caption: 'Middle Name' },
			{ field: 'email',      size: '20%', caption: 'Email' },
			{ field: 'active_id',  hidden: true },
			{ field: 'active',     size: '10%', caption: 'Active' },
			
		],
		onAdd: function (event) {
			formAdd();
		},
		onEdit: function (event) {
			formEdit();
		},
		onDelete: function (event) {},
		multiSearch: false,
		searches: [
			{ field: 'username', caption: 'Username', type: 'text' },
			{ field: 'firstname', caption: 'First Name', type: 'text' },
			{ field: 'lastname', caption: 'Last Name', type: 'text' },
			{ field: 'middlename', caption: 'Middle Name', type: 'text' }
		]
	});
	
	function openPopupForm(action) {
		$().w2destroy('form_<?php echo $p_menu_id; ?>');
		$().w2form({
			name  : 'form_<?php echo $p_menu_id; ?>',
			style : 'border: 0px; background-color: transparent;',
			url   : 'app/System/user/account/save.account.php',
			formHTML: 
				'<div class="w2ui-page page-0" style="padding-top:25px;">' +
				'    <div class="w2ui-field">' +
				'        <label>Username:</label>' +
				'        <div>' +
				'           <input name="username" type="text" maxlength="20" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>First Name:</label>' +
				'        <div>' +
				'           <input name="firstname" type="text" maxlength="50" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Last Name:</label>' +
				'        <div>' +
				'           <input name="lastname" type="text" maxlength="50" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Middle Name:</label>' +
				'        <div>' +
				'           <input name="middlename" type="text" maxlength="50" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Email:</label>' +
				'        <div>' +
				'           <input name="email" type="email" maxlength="50" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Password:</label>' +
				'        <div>' +
				'           <input name="password" type="password" maxlength="200" style="width: 250px" />' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Active:</label>' +
				'        <div>' +
				'            <input name="active" type="list" style="width: 250px"/>' +
				'        </div>' +
				'    </div>' +
				'</div>' +
				'<div class="w2ui-buttons">' +
				'    <button class="w2ui-btn" name="reset">Reset</button>' +
				'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
				'</div>',
			fields: [
				{ field: 'user_id',    hidden: true },	
				{ field: 'username',   type: 'text', required: true },
				{ field: 'firstname',  type: 'text', required: true },
				{ field: 'lastname',   type: 'text', required: true },
				{ field: 'middlename', type: 'middlename' },
				{ field: 'email',      type: 'email', required: true },
				{ field: 'password',   type: 'password', required: true },
				{ field: 'active',     type: 'list', required: true, 
					options: {
						items: [
							{ id: 1, text: 'Yes' },
							{ id: 0, text: 'No' }
						]
					} 
				}
			],
			actions: {
				"save": function() { 
					this.save(function(data){ 
						w2ui['grid_<?php echo $p_menu_id; ?>'].reload(); 
						w2popup.close();
					}); 
				},
				"reset": function() { this.clear(); }
			},
			onSave: function(event) {}, 
			onError: function(event) {},
			postData: {
        		'action' : action
			}
		});
	}

	function formAdd() {
		openPopupForm('add');
		$().w2popup('open', {
			title   : 'Add New <?php echo $p_menu_name; ?>',
			body    : '<div id="popup_form_<?php echo $p_menu_id; ?>" style="width: 100%; height: 100%;"></div>',
			style   : 'padding: 0px; border-radius:0;',
			width   : 530,
			height  : 350, 
			onOpen: function (event) {
				event.onComplete = function () {
					$('#w2ui-popup #popup_form_<?php echo $p_menu_id; ?>').w2render('form_<?php echo $p_menu_id; ?>');
					$('input[name=active]').w2field().set({ id: 1, text: 'Yes' });
					w2ui['form_<?php echo $p_menu_id; ?>'].refresh();
				}
			},
			onClose: function () {
				w2ui['form_<?php echo $p_menu_id; ?>'].clear();
			}
		});
	}
	
	function formEdit() {
		openPopupForm('edit');
		$().w2popup('open', {
			title   : 'Edit <?php echo $p_menu_name; ?>',
			body    : '<div id="popup_form_<?php echo $p_menu_id; ?>" style="width: 100%; height: 100%;"></div>',
			style   : 'padding: 0px; border-radius:0;',
			width   : 530,
			height  : 350, 
			onOpen: function (event) {
				event.onComplete = function () {
					$('#w2ui-popup #popup_form_<?php echo $p_menu_id; ?>').w2render('form_<?php echo $p_menu_id; ?>');

					var sel    = w2ui['grid_<?php echo $p_menu_id; ?>'].getSelection();
					var record = w2ui['grid_<?php echo $p_menu_id; ?>'].get(sel[0]);
					
					w2ui['form_<?php echo $p_menu_id; ?>'].record['user_id']  = record['user_id'];
					w2ui['form_<?php echo $p_menu_id; ?>'].record['username'] = record['username']; 
					w2ui['form_<?php echo $p_menu_id; ?>'].record['firstname'] = record['firstname']; 
					w2ui['form_<?php echo $p_menu_id; ?>'].record['lastname'] = record['lastname']; 
					w2ui['form_<?php echo $p_menu_id; ?>'].record['middlename'] = record['middlename']; 
					w2ui['form_<?php echo $p_menu_id; ?>'].record['email'] = record['email']; 
					$('input[name=active]').w2field().set({ id: record['active_id'], text: record['active'] });
					$('input[name=password]').prop("placeholder", '*******');
					w2ui['form_<?php echo $p_menu_id; ?>'].set('password', { required: false }); 
					$('button[name=reset]').prop('disabled', true);
					w2ui['form_<?php echo $p_menu_id; ?>'].refresh();	
				}
			},
			onClose: function () {
				w2ui['form_<?php echo $p_menu_id; ?>'].clear();
			}
		});
	}
	
	 
});
</script>
