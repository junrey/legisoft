<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$username   = $SYSTEM_FUNCTIONS->filterString($data['record']['username']);
	$firstname  = $SYSTEM_FUNCTIONS->filterString($data['record']['firstname']);
	$lastname   = $SYSTEM_FUNCTIONS->filterString($data['record']['lastname']);
	$middlename = $SYSTEM_FUNCTIONS->filterString($data['record']['middlename']);
	$email      = $SYSTEM_FUNCTIONS->filterString($data['record']['email']);
	$password   = $SYSTEM_FUNCTIONS->filterString($data['record']['password']);

	$active = $data['record']['active']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {
		$user_id  = $SYSTEM_FUNCTIONS->generateCode('A');
		$password = $SYSTEM_FUNCTIONS->passwordHash($password);
		
		$stmt = $conn->prepare("SELECT user_id FROM _user WHERE user_id = :user_id OR username = :username OR email = :email");
		$stmt->bindParam(':user_id',  $module_id, PDO::PARAM_STR);
		$stmt->bindParam(':username', $username,  PDO::PARAM_STR);
		$stmt->bindParam(':email',    $email,     PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO _user SET user_id = :user_id, username = :username, firstname = :firstname, lastname = :lastname, middlename = :middlename, email = :email, password = :password, active = :active, created_date = NOW(), created_by = :created_by");
			$stmt->bindParam(':user_id',    $user_id,    PDO::PARAM_STR);
			$stmt->bindParam(':username',   $username,   PDO::PARAM_STR);
			$stmt->bindParam(':firstname',  $firstname,  PDO::PARAM_STR);
			$stmt->bindParam(':lastname',   $lastname,   PDO::PARAM_STR);
			$stmt->bindParam(':middlename', $middlename, PDO::PARAM_STR);
			$stmt->bindParam(':email',      $email,      PDO::PARAM_STR);
			$stmt->bindParam(':password',   $password,   PDO::PARAM_STR);
			$stmt->bindParam(':active',     $active,     PDO::PARAM_STR);
			$stmt->bindParam(':created_by', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
			$stmt->execute();
		
		} else {
			$status  = 'error';
			$message = 'Duplicate ID OR Email!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$user_id = $SYSTEM_FUNCTIONS->filterString($data['record']['user_id']);
		
		$stmt = $conn->prepare("UPDATE _user SET username = :username, firstname = :firstname, lastname = :lastname, middlename = :middlename, email = :email, active = :active, updated_date = NOW(), updated_by = :updated_by WHERE user_id = :user_id");
		$stmt->bindParam(':user_id',    $user_id,  PDO::PARAM_STR);
		$stmt->bindParam(':username',   $username,   PDO::PARAM_STR);
		$stmt->bindParam(':firstname',  $firstname,  PDO::PARAM_STR);
		$stmt->bindParam(':lastname',   $lastname,   PDO::PARAM_STR);
		$stmt->bindParam(':middlename', $middlename, PDO::PARAM_STR);
		$stmt->bindParam(':email',      $email,      PDO::PARAM_STR);
		$stmt->bindParam(':active',     $active,     PDO::PARAM_STR);
		$stmt->bindParam(':updated_by', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR); 
		$stmt->execute();
		
		if($password != '') {
			$password = $SYSTEM_FUNCTIONS->passwordHash($password);
			
			$stmt = $conn->prepare("UPDATE _user SET password = :password WHERE user_id = :user_id");
			$stmt->bindParam(':user_id',  $user_id,  PDO::PARAM_STR);
			$stmt->bindParam(':password', $password, PDO::PARAM_STR);
			$stmt->execute();
		}
	
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>