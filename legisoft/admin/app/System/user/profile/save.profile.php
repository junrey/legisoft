<?php session_start();
require_once("../../../../database/pdo.mysql.connection.php");
require_once("../../../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$firstname  = $SYSTEM_FUNCTIONS->filterString($data['record']['firstname']);
	$lastname   = $SYSTEM_FUNCTIONS->filterString($data['record']['lastname']);
	$middlename = $SYSTEM_FUNCTIONS->filterString($data['record']['middlename']);
	$email      = $SYSTEM_FUNCTIONS->filterString($data['record']['email']);
	$password   = $SYSTEM_FUNCTIONS->filterString($data['record']['password']);

	$user_id = $SYSTEM_FUNCTIONS->getSessionVar('user_id');
		
	$stmt = $conn->prepare("UPDATE _user SET firstname = :firstname, lastname = :lastname, middlename = :middlename, email = :email, updated_date = NOW(), updated_by = :updated_by WHERE user_id = :user_id");
	$stmt->bindParam(':user_id',    $user_id,    PDO::PARAM_STR);
	$stmt->bindParam(':firstname',  $firstname,  PDO::PARAM_STR);
	$stmt->bindParam(':lastname',   $lastname,   PDO::PARAM_STR);
	$stmt->bindParam(':middlename', $middlename, PDO::PARAM_STR);
	$stmt->bindParam(':email',      $email,      PDO::PARAM_STR);
	$stmt->bindParam(':updated_by', $user_id,    PDO::PARAM_STR);
	$stmt->execute();
	
	if($password != '') {
		$password = $SYSTEM_FUNCTIONS->passwordHash($password);
		
		$stmt = $conn->prepare("UPDATE _user SET password = :password WHERE user_id = :user_id");
		$stmt->bindParam(':user_id',  $user_id,  PDO::PARAM_STR);
		$stmt->bindParam(':password', $password, PDO::PARAM_STR);
		$stmt->execute();
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>