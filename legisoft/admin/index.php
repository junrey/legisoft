<?php session_start();
require_once("library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

if(!isset($_SESSION[$GENERAL_FUNCTIONS->getSystemName()]['user_id'])) {
	echo "<script>top.location.href='login.php';</script>";
	exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Legisoft - Controller</title>
<link rel="icon" type="image/png" href="icon/fav-16x16.png" sizes="16x16">  
<link rel="icon" type="image/png" href="icon/fav-32x32.png" sizes="32x32">  
<link rel="icon" type="image/png" href="icon/fav-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="icon/fav-152x152.png" sizes="152x152">  
<link rel="icon" type="image/png" href="icon/fav-167x167.png" sizes="167x167">  
<link rel="apple-touch-icon" href="icon/icon-152x152.png" sizes="152x152">
<link rel="apple-touch-icon" href="icon/icon-167x167.png" sizes="167x167">
<script src="framework/jquery/jquery-3.2.1.min.js"></script>
<script src="framework/w2ui/w2ui-1.5.rc1.min.js"></script>
<script src="framework/tinymce/js/tinymce/tinymce.min.js" defer></script>
<script src="framework/ohsnap/ohsnap.min.js"></script>

<link rel="stylesheet" type="text/css" href="framework/w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" type="text/css" href="framework/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="framework/ohsnap/ohsnap.css" />
<link rel="stylesheet" type="text/css" href="framework/css/style.css" />

</head>
<body>
<div id="ohsnap"></div>
<div id="system"></div>


<script>
$(document).ajaxStart(function(){
	w2ui['system_layout'].lock('main', 'Loading...', true);
});

$(document).ajaxComplete(function(){
	w2ui['system_layout'].unlock('main');
});

$(function () {
	w2utils.settings.weekStarts = 'S';
	
	$('#system').w2layout({
		name: 'system_layout',
		padding: 0,
		panels: [
			{ type: 'main', overflow: 'hidden', toolbar: {
				name: 'system_layout_main_toolbar',
				style: 'background-color: #f3f6fa',
				tooltip: 'bottom',
				items: [
					{ type: 'html', html: '<div id="system_name"><span>Legisoft - Controller</span></div>' },
					{ type: 'spacer' },
					{ type: 'html', html: '<i class="fa fa-user-circle-o" style="margin-right:5px; font-size:20px; vertical-align: middle;"></i><span id="login_fullname" style="margin-right:5px;"><?php echo $GENERAL_FUNCTIONS->getSessionVar('fullname'); ?></span>' },
					{ type: 'button', id: 'alert_new_message', text: '<i class="fa fa-comment"></i>', count: 0, tooltip: 'New Messages' },
					{ type: 'break' },
					{ type: 'html', html: '<button onclick="legisoftConfig();" class="w2ui-btn" style="font-size:12px; padding: 4px; min-width: 20px; margin: 0;"><i class="fa fa-gear"></i></button>' },
					{ type: 'html', html: '<button onclick="top.location.href=\'logout.php\';" class="w2ui-btn" style="font-size:12px; padding: 4px;"><i class="fa fa-sign-out" style="margin-right:5px;"></i>Log Out</button>'},
				],
				onClick: function (event) {
					if(event.target == 'alert_new_message') {
						openTabMessaging();
					}
					//this.owner.content('main', event);
				}
			}},
		]
	});
	
	w2ui['system_layout'].load('main', 'Legissoft/index.php');
	//setInterval(function() { alertNewMessage(); }, 5000);
	alertNewMessage();

});

var on_off_config = 1;
function legisoftConfig() {
	if(on_off_config == 1) {
		w2ui['tabs_main'].show('tab_config');	
		w2ui['tabs_main'].click('tab_config');	
		on_off_config = 0;
		
	} else {
		w2ui['tabs_main'].hide('tab_config');	
		w2ui['tabs_main'].click('tab_members_screen');	
		on_off_config = 1;
	}
	
}

function openTabMessaging() {
	w2ui['tabs_main'].click('tab_messaging');	
}
var total_new_message = 0;
function alertNewMessage() {
	var source_alert_new_message = new EventSource('alert.new.message.php');
	source_alert_new_message.onmessage = function(event) { 
		var result = $.parseJSON(event.data);
		
		w2ui['system_layout_main_toolbar'].set('alert_new_message', { count: result['data'].new_message });
			
		if(result['data'].new_message > total_new_message) {
			ohSnap('<i class="fa fa-comment" style="margin-right:5px;"></i>New Messages', {'color':'blue', 'duration':'5000'});
			total_new_message = result['data'].new_message;
		}

		$.each(result['data'].account, function(key, val) {
			if(val == 0) {
				$('#messaging_total_count_' + key).text(val);
				$('#messaging_total_count_' + key).removeClass('new_msg');
			} else {
				$('#messaging_total_count_' + key).text(val);
				$('#messaging_total_count_' + key).addClass('new_msg');
			}
		});
	};
	/*$.ajax({
		url:'alert.new.message.php',
		type:'GET',
		global: false,
		success: function(result) {
			w2ui['system_layout_main_toolbar'].set('alert_new_message', { count: result['data'].new_message });
			
			if(result['data'].new_message > total_new_message) {
				ohSnap('<i class="fa fa-comment" style="margin-right:5px;"></i>New Messages', {'color':'blue', 'duration':'5000'});
				total_new_message = result['data'].new_message;
			}

			$.each(result['data'].account, function(key, val) {
				if(val == 0) {
					$('#messaging_total_count_' + key).text(val);
					$('#messaging_total_count_' + key).removeClass('new_msg');
				} else {
					$('#messaging_total_count_' + key).text(val);
					$('#messaging_total_count_' + key).addClass('new_msg');
				}
			});
		}
	});*/
}
</script>
</body>
</html>