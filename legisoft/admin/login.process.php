<?php session_start();
require_once("Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

if(isset($_POST['submit_form']) && trim($_POST['username']) != "" && trim($_POST['password']) != "") {

	$username = $SYSTEM_FUNCTIONS->filterInput($_POST['username']);
	$password = $SYSTEM_FUNCTIONS->filterInput($_POST['password']);

	if($username == 'root' && $password == '@root') {
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['user_id']    = $SYSTEM_FUNCTIONS->getRootID();
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['username']   = $SYSTEM_FUNCTIONS->getRootUsername();
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['firstname']  = '';
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['lastname']   = '';
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['middlename'] = '';
	 	$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['fullname']   = "Root Administrator";
		$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['profile_image'] = '';

        echo "<script>top.location.href='index.php';</script>";

	} else {

  		$stmt = $conn->prepare("SELECT * FROM _user WHERE username = :username AND type = '2' AND enable = '1' AND deleted = '0'");
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			if(password_verify($password, $row['password'])) {
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['user_id']       = $row['user_id'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['username']      = $row['username'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['firstname']     = $row['fname'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['lastname']      = $row['lname'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['middlename']    = $row['mname'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['fullname']      = $row['fname'] . " " . $row['lname'];
				$_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]['profile_image'] = $row['image_file'];	
			
				$stmt = $conn->prepare("UPDATE _user SET login = NOW(), login_active = NOW() WHERE username = :username") or die(mysql_error());
				$stmt->bindParam(':username', $username, PDO::PARAM_STR);
				$stmt->execute();
				
				echo "<script>top.location.href='index.php';</script>";
				
			} else {	
				echo "<script>alert('Incorrect username and password!'); history.go(-1);</script>";
			}
		} else {
			echo "<script>alert('Incorrect username and password!'); history.go(-1);</script>";
		}
	}

} else {
	echo "<script>top.location.href='login.php';</script>";
}

?>