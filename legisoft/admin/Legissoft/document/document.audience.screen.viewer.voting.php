<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();
	
$stmt = $conn->prepare("SELECT vote_audience_view FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$vote_code = $row['vote_audience_view'];

if($vote_code != "") {
	$stmt2 = $conn->prepare("SELECT title FROM vote_header WHERE code = :vote_code");
	$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

	
?>
<input type="hidden" name="screen_vote_code" value="<?php echo $vote_code; ?>">
<input type="hidden" name="screen_vote_title" value="<?php echo $row2['title']; ?>">
    
<?php
	$stmt = $conn->prepare("SELECT detail, value, place FROM vote_item WHERE enable = '1' ORDER BY place");  
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
?>
	<div class="screen_vote_panel" style="width:calc((100% / <?php echo $stmt->rowCount(); ?>) - 10px);">
    	<div class="screen_vote_panel_title <?php echo strtolower($row['detail']); ?>"><?php echo $row['detail']; ?></div>
        <div class="screen_vote_panel_content">
        	<?php
				$stmt2 = $conn->prepare("SELECT b.username, b.lname, b.fname, b.image_file FROM vote a INNER JOIN _user b ON a.username = b.username WHERE a.code = :vote_code AND a.vote = :vote AND a.enable = '1' AND a.deleted = '0' ORDER BY b.lname, b.fname");  
				$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
				$stmt2->bindParam(':vote', $row['value'], PDO::PARAM_STR);
				$stmt2->execute();
				while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { 
			?>
            	<div class="screen_vote_panel_members">
                    <table>
                        <tr>
                            <th><img class="uk-border-circle" width="60" height="60" src="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/user_image/profile/<?php echo $row2['image_file']; ?>" onerror="if(this.src != '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/images/no_profile_pic.gif') this.src = '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/images/no_profile_pic.gif';"></th>
                            <td><?php echo $row2['fname'] . " " . $row2['lname']; ?></td>
                        </tr>
                    </table>
                </div>
            <?php
				}
			?>
        </div>
    </div>
<?php	
	}
} 
?>