<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
html {
	height:100%; 
	width:100%; 
	overflow:hidden;
	padding:0; 
	margin:0;
}
body { 
	height:100%; 
	width:100%; 
	overflow-y:auto; 
	overflow-x:hidden;
	padding:0; 
	margin:0; 
}

#paper_gutter {
	display:none;
	width:615px;
	min-height:11in;
	background-color:#FFFFFF;
	border:2px solid #999999;
	margin:40px auto;
	padding:78px 95px;
}

.pdf_window {
	display:none;
	height: 100%;
	width: 100%;
	overflow: hidden;
	z-index:1; 
}
.action_window {
	display:none;
	z-index:1; 
	width:1024px;
	min-height:11in;
	margin:auto;
	background: #fff;
}
.action_window #action_wrapper {
	height:calc(100% - 75px);
	background-color:#fff;
	overflow-x:hdden;
	overflow-y:auto;
	padding-bottom:40px;
}
.action_window #action_content {
	padding:20px;
}
.action_window #reference {
	background-color:#3e7ab4;
	padding:5px 10px;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center;
	color:#fff;
	font-size:25px;
}
.action_window #action_item {
	background-color:#fbfbfb;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center; 
	padding:10px;
	letter-spacing: 2px;
	border-radius:5px;
}
.action_window #movant {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#08f4ad;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #second {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#ff8000;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#f9ca02;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee .label {
	width:100%;
	float:left;
	font-size:18px;
}
.action_window #movant .label, .action_window #second .label {
	width:370px;
	float:left;
	font-size:18px;
}
.action_window #movant .title, .action_window #second .title, .action_window #commitee .title {
	padding:10px;
	width:150px;
	vertical-align:top;
	font-size:20px;
	text-align:right;
	line-height:25px;
}
.action_window #movant .title span, .action_window #second .title span, .action_window #commitee .title span {
	float:right;
	color:#fff;
	font-size:30px;
	line-height:18px;
	margin-left:10px;
}

.voting_window {
	display:none;
	position:absolute; 
	z-index:1; 
	left:0; 
	top:0;
	width:100%;
	height:100%;
	background: #fff;
	overflow: auto;
}

.screen_vote_content {
	border-top: 5px solid #3e7ab4;
	width:100%;
	background-color: #fff;
}
.screen_vote_content td {
	padding:0 5px;
}
.screen_vote_panel {
	border-collapse: collapse;
	width:100%;
	height:100%;
	background-color:#f2f2f2;
	border-top: 4px solid #fff;
	border-left: 4px solid #fff;
	border-right: 4px solid #fff;
}

.screen_vote_panel_title {
	padding:5px;
	text-align:center;
	font-family:Verdana,Arial,sans-serif;
	color:#fff;
	font-size:25px;
	width:130px;
}

.screen_vote_panel_content {
	width: calc(100% - 130px);
	padding:5px;
}

.screen_vote_panel_title.yes {
	background-color:#32d296;
}

.screen_vote_panel_title.no {
	background-color:#f0506e;
}

.screen_vote_panel_title.abstain {
	background-color:#aaa;
	
}

.screen_vote_panel_members {
	float: left;
}
.screen_vote_panel_members table {
	width:100%;	
	font-family:Verdana,Arial,sans-serif;
}

.screen_vote_panel_members table th {
	width:60px;
}
.screen_vote_panel_members table td {
	font-size:20px;	
	width:220px;
}
.screen_vote_panel_members table th img {
	border-radius: 50%;
}

.image_window {
	display:none;
	position:absolute; 
	z-index:1; 
	left:0; 
	top:0;
	width:100%;
	height:100%;
	background: #fff;
	overflow:auto;
}

.image_window img {
	max-width:100%;
}

.announcement_window {
	display:none;
	position:absolute; 
	z-index:1; 
	left:0; 
	top:0;
	width:100%;
	height:100%;
	background: #fff;
	overflow:auto;
}

</style>
</head>
<body>
<div id="page_current">
    <div id="paper_gutter" class="screen_window">
        <div id="paper_main"></div>
    </div>
</div>
<div class="screen_window pdf_window"></div>
<div class="screen_window action_window"></div>
<div class="screen_window voting_window"></div>
<div class="screen_window image_window"></div>
<div class="screen_window announcement_window"></div>
<script>
    parent.$(function() {
		getAudienceAnnouncementAlert();
    });
	
	function getAudienceAnnouncementAlert() {
        parent.$.ajax({
            method: 'GET',
            url: 'Legissoft/document/document.audience.screen.viewer.announcement.php', 	
            success: function(result){
				if(result != '') {
					parent.$(document).contents().find('.screen_window').hide();
					parent.$(document).contents().find('.announcement_window').empty().html(result);
					parent.$(document).contents().find('.announcement_window').show();
					
					parent.$('#audience_screen_title').html('Announcement<button class="tools w2ui-btn" style="margin-right: 228px; min-width: 20px;" onclick="reloadScreenAudience();"><i class="fa fa-refresh"></i></button><button class="tools w2ui-btn w2ui-btn-green" onclick="removeScreenAudienceAnnouncement();">Hide Announcement From Audience</button>');

				} else {
					parent.$('#audience_screen_title').html('&nbsp;');
					parent.$(document).contents().find('.announcement_window').hide();
					getAudienceImageAlert();
					
				}
			},
            error: function(error) {

            }
        });
    }
    
	function getAudienceImageAlert() {
        parent.$.ajax({
            method: 'GET',
            url: 'Legissoft/document/document.audience.screen.viewer.image.php', 	
            success: function(result){
                if(result != '') {
                    parent.$(document).contents().find('.screen_window').hide();
                    parent.$(document).contents().find('.image_window').empty().html(result);
                    parent.$(document).contents().find('.image_window').show();
                    
                    parent.$('#audience_screen_title').html(parent.$(document).contents().find('input[name=screen_image_title]').val() + '<button class="tools w2ui-btn" style="margin-right: 182px; min-width: 20px;" onclick="reloadScreenAudience();"><i class="fa fa-refresh"></i></button><button class="tools w2ui-btn w2ui-btn-green" onclick="removeScreenAudienceImage(\'' + parent.$(document).contents().find('input[name=screen_image_code]').val() + '\');">Hide Result From Members</button>');

                } else {
                    parent.$(document).contents().find('.image_window').hide();
					getAudienceVoteAlert();
                    
                }
            },
            error: function(error) {

            }
        });
    }
	
	function getAudienceVoteAlert() {
        parent.$.ajax({
            method: 'GET',
            url: 'Legissoft/document/document.audience.screen.viewer.voting.v2.php', 	
            success: function(result){
            	if(result.data['voting'] != '') {
                    parent.$(document).contents().find('.screen_window').hide();
                    parent.$(document).contents().find('.voting_window').empty().html(result.data['voting']);
                    parent.$(document).contents().find('.voting_window').show();
                    
                    parent.$('#audience_screen_title').html(result.data['title'] + '<button class="tools w2ui-btn" style="margin-right: 182px; min-width: 20px;" onclick="reloadScreenAudience();"><i class="fa fa-refresh"></i></button><button class="tools w2ui-btn w2ui-btn-green" onclick="removeScreenAudienceVoting(\'' + result.data['code'] + '\');">Hide Result From Audience</button>');

                } else {
                    parent.$(document).contents().find('.voting_window').hide();
					getAudienceActionAlert();
                    
                }
            },
            error: function(error) {

            }
        });
    }
    
	 function getAudienceActionAlert() {
        parent.$.ajax({
            method: 'GET',
            url: 'Legissoft/document/document.audience.screen.viewer.actions.php', 	
            success: function(result){
                if(result != '') {
					parent.$(document).contents().find('.screen_window').hide();
					parent.$(document).contents().find('.action_window').empty().html(result);
					parent.$(document).contents().find('.action_window').show();
                    
                    parent.$('#audience_screen_title').html(parent.$(document).contents().find('input[name=screen_action_title]').val() + '<button class="tools w2ui-btn" style="margin-right: 145px; min-width: 20px;" onclick="reloadScreenAudience();"><i class="fa fa-refresh"></i></button><button class="tools w2ui-btn w2ui-btn-green" onclick="removeScreenAudienceAction(\'' + parent.$(document).contents().find('input[name=screen_action_code]').val() + '\');">Hide From Audience</button>');
                    
                } else {
                    parent.$(document).contents().find('.action_window').hide();
                    getContent();
                }
            },
            error: function(error) {

            }
        });
    }
    
    function getContent() {
        parent.$.ajax({
            method: 'GET',
            url: 'Legissoft/document/document.audience.screen.viewer.ongoing.php', 	
            success: function(result){
				parent.$(document).contents().find('.screen_window').hide();
				
                if(result.data['title'] != '') {
                    parent.$('#audience_screen_title').html(result.data['title'] + '<button class="tools w2ui-btn" style="margin-right: 145px; min-width: 20px;" onclick="reloadScreenAudience();"><i class="fa fa-refresh"></i></button><button class="tools w2ui-btn w2ui-btn-green" onclick="removeScreenAudienceDocument(\'' + result.data['code'] + '\');">Hide From Audience</button>');
			    } else {
                    parent.$('#audience_screen_title').html('&nbsp;');
                }
				
				if(result.data['source'] == 'pdf') {
					parent.$(document).contents().find('.pdf_window').show();
					parent.$(document).contents().find('.pdf_window').empty().html('<embed src="../../../upload/' + result.data['filename'] + '" style="height: 100%; width: 100%;" type="application/pdf">');
				} else {             
                	parent.$(document).contents().find('#paper_main').empty().html(result.data['document']);
					parent.$(document).contents().find('#paper_gutter').show();
				}
				
            },
            error: function(error) {

            }
        });
    }
    
</script>
</body>
</html>