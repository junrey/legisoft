<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$code = "";

if(isset($_POST['code']) && trim($_POST['code']) != "") {
	$code = trim($_POST['code']);
}

if(isset($_POST['member_code']) && trim($_POST['member_code']) != "" && $code != "") {
	$member_code = trim($_POST['member_code']);
	$status      = trim($_POST['status']);
	
	if($status == "hide") {
		$stmt = $conn->prepare("UPDATE _user SET view_document = '', mod_by = :mod_by, mod_date = NOW() WHERE username = :username");
		$stmt->bindParam(':username', $member_code, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',   $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();

	} else {
		$stmt = $conn->prepare("UPDATE _user SET view_document = :view_document, mod_by = :mod_by, mod_date = NOW() WHERE username = :username");
		$stmt->bindParam(':view_document', $code,        PDO::PARAM_STR);
		$stmt->bindParam(':username',      $member_code, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',        $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}
}

$stmt = $conn->prepare("SELECT username, fname, lname, view_document FROM _user WHERE enable = '1' AND deleted = '0' AND type = '1' ORDER BY FIELD(view_document, :view_document) DESC, lname, fname");
$stmt->bindParam(':view_document', $code, PDO::PARAM_STR);
$stmt->execute();

echo '{ 
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$status = "show";
		$select = "";
		if($code ==	$row['view_document']) {
			$status = "hide";
			$select = "w2ui-btn-green";
		}

		echo '{ 
			"recid"    : "' . $row['username'] . '", 
			"code"     : "' . $row['username'] . '", 
			"fullname" : "' . htmlspecialchars($row['lname']) . ', ' . htmlspecialchars($row['fname']) . '",
			"button"   : "<button onclick=\"getDocumentMembers(\'' . $row['username'] . '\', \'' . $status . '\');\" class=\"w2ui-btn ' . $select . '\" style=\"font-size:10px; padding: 2px; min-width: 20px;\"><i class=\"fa fa-eye\"></i></button>",
			"status"   : "' . $status . '"
		}';

	}

echo '], "status"  : "' . $status . '", 
	     "message" : "' . $message . '", 
	     "total"   : "' . $cnt . '"}';

?>
