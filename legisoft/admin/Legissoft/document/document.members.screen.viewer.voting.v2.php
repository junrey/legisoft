<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();
$rows['code']     = '';
$rows['title']    = '';
$rows['voting']   = '';
$rows['date_log'] = '';
	
$stmt = $conn->prepare("SELECT vote_members_view FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$vote_code = $row['vote_members_view'];

if($vote_code != "") {
	$stmt2 = $conn->prepare("SELECT title, content, mod_date FROM vote_header WHERE code = :vote_code");
	$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

	$rows['code']     = $vote_code;
	$rows['title']    = $row2['title'];
	$rows['date_log'] = $row2['mod_date'];
	

	$stmt = $conn->prepare("SELECT detail, value, place FROM vote_item WHERE enable = '1' ORDER BY place");  
	$stmt->execute();
	
	$voting = '';
	$voting .= '<table class="screen_vote_content"><tr><td>' . $row2['content'] . '</td></tr></table>';
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$voting .= '<table class="screen_vote_panel" style="height:calc((100% / ' . $stmt->rowCount() . ') - 20px);">';
			$voting .= '<tr><th class="screen_vote_panel_title ' . strtolower($row['detail']) . '">' . $row['detail']. '</th>';
			$voting .= '<td class="screen_vote_panel_content">';
        	
				$stmt2 = $conn->prepare("SELECT b.username, b.lname, b.fname, b.image_file FROM vote a INNER JOIN _user b ON a.username = b.username WHERE a.code = :vote_code AND a.vote = :vote AND a.enable = '1' AND a.deleted = '0' ORDER BY b.lname, b.fname");  
				$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
				$stmt2->bindParam(':vote', $row['value'], PDO::PARAM_STR);
				$stmt2->execute();
				
				$voting_detail = '';
				while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { 
					$voting_detail .= '<div class="screen_vote_panel_members">';
						$voting_detail .= '<table>';
							$voting_detail .= '<tr>';
								$voting_detail .= '<th><img class="uk-border-circle" width="50" height="50" src="'. $GENERAL_FUNCTIONS->getSystemPath(). '/admin/Legissoft/user_image/profile/' . $row2['image_file'] . '" onerror="if(this.src != \''.$GENERAL_FUNCTIONS->getSystemPath() . '/images/no_profile_pic.gif\') this.src = \'' . $GENERAL_FUNCTIONS->getSystemPath() . '/images/no_profile_pic.gif\';"></th>';
								$voting_detail .= '<td>' . $row2['fname'] . " " . $row2['lname'] . '</td>';
							$voting_detail .= '</tr>';
						$voting_detail .= '</table>';
					$voting_detail .= '</div>';
				}
					
				$voting .= $voting_detail;
        	$voting .= '</td></tr>';
    	$voting .= '</table>';
	}
		
	$rows['voting'] = $voting;
} 

echo json_encode(array('data' => $rows));
?>