<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$search = "";
if(isset($_GET['request'])) {
	$data = json_decode($_GET['request'], true);
	if(isset($data["search"])) {
		$search = $data["search"][0]["value"];
	}
}

$document_type = array();
$document_type[""] = "";
$stmt4 = $conn->prepare("SELECT code, detail FROM document_type WHERE isdelete = '0'");
$stmt4->execute();
while($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
	$document_type[$row4['code']] = $row4['detail'];
}

$stmt = $conn->prepare("SELECT code, title, document_type, document_date, created_by, mod_by, mod_date FROM document WHERE CONCAT(title, ' ', document) LIKE :search AND isdelete = '0' ORDER BY YEAR(document_date) DESC, MONTH(document_date) DESC, WEEK(document_date) ASC, title");
$stmt->bindValue(':search', '%' . $search . '%');
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		 
		echo '{ 
			"recid"         : "' . $row['code'] . '", 
			"document_code" : "' . $row['code'] . '", 
			"title"         : "' . $GENERAL_FUNCTIONS->cleanString($row['title']) . '",
			"document_type" : "' . $document_type[$row['document_type']] . '", 
			"created_by"    : "' . $GENERAL_FUNCTIONS->cleanString($row['created_by']) . '",
			"created_date"  : "' . date('m/d/Y', strtotime($row['document_date'])) . '", 
			"mod_by"        : "' . $GENERAL_FUNCTIONS->cleanString($row['mod_by']) . '",
			"mod_date"      : "' . date('m/d/Y', strtotime($row['mod_date'])) . '"

		}';

   }

echo ']}';

?>
