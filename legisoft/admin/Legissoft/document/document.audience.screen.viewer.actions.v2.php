<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();
$rows['title']    = '';
$rows['action']   = '';
$rows['date_log'] = '';

$stmt = $conn->prepare("SELECT audience_action_view FROM config"); 
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$audience_action_view = $row['audience_action_view'];

$stmt2 = $conn->prepare("SELECT action, title, status_code, reference, mod_date FROM action WHERE code = :audience_action_view"); 
$stmt2->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

$title       = $row2['title'];
$action      = $row2['action'];
$status_code = $row2['status_code'];
$reference   = $row2['reference'];
$date_log    = $row2['mod_date'];

$stmt3 = $conn->prepare("SELECT detail FROM action_reference WHERE code = :reference"); 
$stmt3->bindParam(':reference', $reference, PDO::PARAM_STR);
$stmt3->execute();
$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);

if($action != '') {
	$rows['title']    = $title;
	$rows['date_log'] = $date_log;
	
	$rows['action'] = '<div id="reference">' . trim($row3['detail']) . '</div>';
	$rows['action'] .= '<div id="action_wrapper">';
		$rows['action'] .= '<div id="action_content">';
		
		$stmt4 = $conn->prepare("SELECT detail FROM action_item WHERE code = :status_code");
		$stmt4->bindParam(':status_code', $status_code, PDO::PARAM_STR);
		$stmt4->execute();
		$row4 = $stmt4->fetch(PDO::FETCH_ASSOC);
		
		
		if($stmt4->rowCount() > 0) {
			$rows['action'] .= '<div id="action_item" style="font-size:35px;">' . trim($row4['detail']) . '</div>';
		}
		
		$rows['action'] .= $action;
		$rows['action'] .= '</div>';
		
		
		$stmt5 = $conn->prepare("SELECT b.fname, b.lname FROM action_movant AS a INNER JOIN _user AS b ON a.movant_code = b.username WHERE a.action_code = :audience_action_view ORDER BY b.lname, b.fname"); 
		$stmt5->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
		$stmt5->execute();
		
		
		if($stmt5->rowCount() > 0) {
			$rows['action'] .= '<div id="movant">';
				$rows['action'] .= '<table>';
					$rows['action'] .= '<tr>';
						$rows['action'] .= '<td class="title">MOVANT <span>&rsaquo;</span></td>';
						$rows['action'] .= '<td>';
						
							$movant = '';
							 while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) { 
								
								$movant .= '<div class="label">';
								$movant .= '<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>';
								$movant .= $row5['fname'] . ' ' . $row5['lname'];
								$movant .= '</div>';
							}
						
							$rows['action'] .= $movant;
						$rows['action'] .= '</td>';
					$rows['action'] .= '</tr>';
				$rows['action'] .= '</table>';
			$rows['action'] .= '</div>';
		 }
		
		
		$stmt6 = $conn->prepare("SELECT b.fname, b.lname FROM action_second AS a INNER JOIN _user AS b ON a.second_code = b.username WHERE a.action_code = :audience_action_view ORDER BY b.lname, b.fname"); 
		$stmt6->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
		$stmt6->execute();
		
		if($stmt6->rowCount() > 0) {
			$rows['action'] .= '<div id="second">';
				$rows['action'] .= '<table>';
					$rows['action'] .= '<tr>';
						$rows['action'] .= '<td class="title">SECOND <span>&rsaquo;</span></td>';
						$rows['action'] .= '<td>';
							$second = '';
							while($row6 = $stmt6->fetch(PDO::FETCH_ASSOC)) {
								$second .= '<div class="label">';
								$second .= '<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>';
								$second .= $row6['fname'] . ' ' . $row6['lname'];
								$second .= '</div>';
							}
							$rows['action'] .= $second;
						$rows['action'] .= '</td>';
					$rows['action'] .= '</tr>';
				$rows['action'] .= '</table>';
			$rows['action'] .= '</div>';
		}
		
		$stmt7 = $conn->prepare("SELECT b.detail FROM action_committee AS a INNER JOIN committee AS b ON a.committee_code = b.code WHERE a.action_code = :audience_action_view ORDER BY b.detail"); 
		$stmt7->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
		$stmt7->execute();
		
		if($stmt7->rowCount() > 0) {
			$rows['action'] .= '<div id="commitee">';
				$rows['action'] .= '<table>';
					$rows['action'] .= '<tr>';
						$rows['action'] .= '<td class="title">COMMITTEE <span>&rsaquo;</span></td>';
						$rows['action'] .= '<td>';
							$committee = '';
							while($row7 = $stmt7->fetch(PDO::FETCH_ASSOC)) {
								$committee .= '<div class="label">';
								$committee .= '<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>';
								$committee .= $row7['detail'];
								$committee .= '</div>';
							}
							$rows['action'] .= $committee;
						$rows['action'] .= '</td>';
					$rows['action'] .= '</tr>';
				$rows['action'] .= '</table>';
			$rows['action'] .= '</div>';
		 }
	$rows['action'] .= '</div>';
} 


echo json_encode(array('data' => $rows));
?>