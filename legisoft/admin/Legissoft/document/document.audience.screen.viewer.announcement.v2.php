<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();
$rows['announcement'] = '';
$rows['date_log']     = '';

$stmt = $conn->prepare("SELECT audience_alert_message, audience_alert_message_date FROM config WHERE audience_alert_message_show = '1'"); 
$stmt->execute();	
$row = $stmt->fetch(PDO::FETCH_ASSOC);

if($row['audience_alert_message'] != "") {
	$rows['announcement'] = $row['audience_alert_message'];
	$rows['date_log']     = $row['audience_alert_message_date'];
}

echo json_encode(array('data' => $rows));
?>