<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");
include "../../framework/autoload.php";

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();

if(isset($_POST['code']) && trim($_POST['title']) != "") {
	$document      = trim($_POST['content']);
	$filename      = trim($_POST['filename']);
	$title         = trim($_POST['title']);
	$document_type = trim($_POST['type']);
	$document_date = date("Y-m-d", strtotime(trim($_POST['date'])));
	$source        = trim($_POST['source']);
	
	if($source == "pdf") {
		// Parse pdf file and build necessary objects.
		$parser   = new \Smalot\PdfParser\Parser();
		$pdf      = $parser->parseFile("../../../upload/" . $filename);		 
		$document = $pdf->getText();
	}

	if(trim($_POST['code']) == "") {
		$today = date("mdY") . date("His");
		$code  = "JDX" . rand(0,9) . rand(0,9) . $today; 
				
		$stmt = $conn->prepare("INSERT INTO document SET code = :code, title = :title, document = :document, document_type = :document_type, document_date = :document_date, filename = :filename, source = :source, created_by = :created_by, created_date = NOW(), mod_by = :mod_by, mod_date = NOW(), save = '1'");
		$stmt->bindParam(':code',          $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',         $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':document',      $document,                                     PDO::PARAM_STR);
		$stmt->bindParam(':document_type', $document_type, 								  PDO::PARAM_STR);
		$stmt->bindParam(':document_date', $document_date, 								  PDO::PARAM_STR);
		$stmt->bindParam(':filename',      $filename,                                     PDO::PARAM_STR);
		$stmt->bindParam(':source',        $source,                                       PDO::PARAM_STR);
		$stmt->bindParam(':created_by',    $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',        $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
				
	} else { 
		$code = trim($_POST['code']);
		
		$stmt = $conn->prepare("UPDATE document SET title = :title, document = :document, document_type = :document_type, document_date = :document_date, filename = :filename, source = :source, mod_by = :mod_by, mod_date = NOW(), save = '1' WHERE code = :code");
		$stmt->bindParam(':code',          $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',         $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':document',      $document,                                     PDO::PARAM_STR);
		$stmt->bindParam(':document_type', $document_type, 								  PDO::PARAM_STR);
		$stmt->bindParam(':document_date', $document_date, 								  PDO::PARAM_STR);
		$stmt->bindParam(':filename',      $filename,                                     PDO::PARAM_STR);
		$stmt->bindParam(':source',        $source,                                       PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',        $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}

	$rows['code'] = $code;

} else if(isset($_POST['code_delete']) && trim($_POST['code_delete']) != "") {
	$code_delete = trim($_POST['code_delete']);
	
	$stmt = $conn->prepare("UPDATE document SET isdelete = '1', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code', $code_delete, PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET members_view = '' WHERE members_view = :members_view");
	$stmt->bindParam(':members_view', $code_delete, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET audience_view = '' WHERE audience_view = :audience_view");
	$stmt->bindParam(':audience_view', $code_delete, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE _user SET view_document = '' WHERE view_document = :view_document");
	$stmt->bindParam(':view_document', $code_delete, PDO::PARAM_STR);
	$stmt->execute();

	$rows['code'] = "";
	
} else if(isset($_POST['code_members']) && trim($_POST['code_members']) != "") {
	$members_view = trim($_POST['code_members']);
	
	$stmt = $conn->prepare("SELECT members_view FROM config WHERE members_view = :members_view");
	$stmt->bindParam(':members_view', $members_view, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET members_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
	
	} else {
		$stmt = $conn->prepare("UPDATE config SET members_view = :members_view");
		$stmt->bindParam(':members_view', $members_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}

	$rows['code'] = $members_view;
	
} else if(isset($_POST['code_audience']) && trim($_POST['code_audience']) != "") {
	$audience_view = trim($_POST['code_audience']);
	
	$stmt = $conn->prepare("SELECT audience_view FROM config WHERE audience_view = :audience_view");
	$stmt->bindParam(':audience_view', $audience_view, PDO::PARAM_STR);
	$stmt->execute();

	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET audience_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
		
	} else {
		$stmt = $conn->prepare("UPDATE config SET audience_view = :audience_view");
		$stmt->bindParam(':audience_view', $audience_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}
	
	$rows['code'] = $audience_view;
}



echo json_encode(array('data' => $rows));
?>