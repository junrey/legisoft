<style>
#members_screen_title {
	padding: 5px 10px; 
	font-size:14px; 
	font-weight: bold;
	text-transform:capitalize;
}
#members_screen_title .tools {
    position: absolute;
    top: 2px;
    right: 0;
}
</style>

<div id="layout_members_screen" style="position: absolute; width: 100%; height: calc(100% - 30px); padding: 4px; background-color: #fff;"></div>

<script>
$(function () {
	$().w2destroy('layout_members_screen');
    $('#layout_members_screen').w2layout({
        name: 'layout_members_screen',
        panels: [
			{ 
				type: 'main',
				style: 'border: 1px solid #dfdfdf;',
				overflow: 'hidden',
				content: '<iframe id="layout_members_screen_iframe" src="Legissoft/document/document.members.screen.viewer.php" style="height: 100%;"></iframe>', 
				toolbar: {
					items: [
						{ type: 'html', html: '<div id="members_screen_title" style="height: 26px;"></div>' },
					]
				}
			}
		]
    });
});

function removeScreenMembersDocument(document_code) {
	if(document_code != '') {
		$.ajax({
			url:'Legissoft/document/document.save.php',
			type:'POST',
			data: { 
				code_members : document_code
			},
			success: function(result) {
				$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
				
				if($('input[name=document_code]').val() == document_code) {
					$("button[name=button_show_to_members]").removeClass('w2ui-btn-green');
					$("button[name=button_show_to_members]").text('Show To Members');
				}
			}
		});
	}
}

function removeScreenMembersAction(action_code) {
	if(action_code != '') {
		$.ajax({
			url:'Legissoft/action/action.save.php',
			type:'POST',
			data: { 
				code_members : action_code
			},
			success: function(result) {
				$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
				
				if($('input[name=action_code]').val() == action_code) {
					$("button[name=button_action_show_to_members]").removeClass('w2ui-btn-green');
					$("button[name=button_action_show_to_members]").text('Show To Members');
				}
			}
		});
	}
}

function removeScreenMembersVoting(vote_code) {
	if(vote_code != '') {
		$.ajax({
			url:'Legissoft/voting/voting.save.php',
			type:'POST',
			data: { 
				code_members : vote_code
			},
			success: function(result) {
				$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
				
				if($('input[name=voting_code]').val() == vote_code) {
					$("button[name=button_voting_show_to_members]").removeClass('w2ui-btn-green');
					$("button[name=button_voting_show_to_members]").text('Show Result To Members');
				}
			}
		});
	}
}

function removeScreenMembersImage(image_code) {
	if(image_code != '') {
		$.ajax({
			url:'Legissoft/gallery/save.gallery.image.php',
			type:'POST',
			data: { 
				code_members : image_code
			},
			success: function(result) {
				$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
				$('#image_members_' + image_code).removeClass('w2ui-btn-green');
				$('#image_members_' + image_code).text('Show To Members');				
			}
		});
	}
}

function removeScreenMembersAnnouncement() {
	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_members_show : 0
		},
		success: function(result) {
			$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
			$("button[name=button_announcement_show_to_members]").removeClass('w2ui-btn-green');
			$("button[name=button_announcement_show_to_members]").text('Announce To Members');
			$('input[name=content_members_show]').val(1);
		}
	});
}

function reloadScreenMembers() {
	w2ui['layout_members_screen'].refresh();
}
</script>
