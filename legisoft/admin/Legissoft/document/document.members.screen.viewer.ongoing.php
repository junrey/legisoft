<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();
$rows['code']     = '';
$rows['title']    = '';
$rows['document'] = '';
$rows['filename'] = '';
$rows['source']   = '';
$rows['date_log'] = '';

$stmt = $conn->prepare("SELECT members_view FROM config"); 
$stmt->execute();

if($stmt->rowCount() > 0) {
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$members_view = $row['members_view'];
	
	
	$stmt = $conn->prepare("SELECT * FROM document WHERE code = :members_view");
	$stmt->bindParam(':members_view', $members_view, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$rows['code']     = $row['code'];
		$rows['title']    = $row['title'];
		$rows['document'] = $row['document'];
		$rows['filename'] = $row['filename'];
		$rows['source']   = $row['source'];
		$rows['date_log'] = $row['mod_date'];
	}
}

echo json_encode(array('data' => $rows));
?>