<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

if(isset($_POST['code']) && trim($_POST['code']) != "") {

	$code = trim($_POST['code']);
	
	$stmt = $conn->prepare("SELECT * FROM document WHERE isdelete = '0' AND code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$stmt2 = $conn->prepare("SELECT members_view FROM config WHERE members_view = :members_view");
		$stmt2->bindParam(':members_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['members_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['members_view'] = $row2['members_view'];
		}
		
		$stmt2 = $conn->prepare("SELECT audience_view FROM config WHERE audience_view = :audience_view");
		$stmt2->bindParam(':audience_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['audience_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['audience_view'] = $row2['audience_view'];
		}

		
		$rows['code']          = $row['code'];
		$rows['title']         = $row['title'];
		$rows['document']      = $row['document'];
		$rows['document_type'] = $row['document_type'];
		$rows['document_date'] = date("m/d/Y", strtotime($row['document_date']));
		$rows['filename']      = $row['filename'];
		$rows['source']        = $row['source'];
	}

}

echo json_encode(array('data' => $rows));
?>
