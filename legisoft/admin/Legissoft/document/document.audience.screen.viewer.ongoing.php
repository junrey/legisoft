<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();
$rows['title']    = '';
$rows['document'] = '';
$rows['filename'] = '';
$rows['source']   = '';
$rows['date_log'] = '';

$stmt = $conn->prepare("SELECT audience_view FROM config"); 
$stmt->execute();

if($stmt->rowCount() > 0) {
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$audience_view = $row['audience_view'];
	
	
	$stmt = $conn->prepare("SELECT * FROM document WHERE code = :audience_view");
	$stmt->bindParam(':audience_view', $audience_view, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$rows['code']     = $row['code'];
		$rows['title']    = $row['title'];
		$rows['document'] = $row['document'];
		$rows['filename'] = $row['filename'];
		$rows['source']   = $row['source'];
		$rows['date_log'] = $row['mod_date'];
	}
}

echo json_encode(array('data' => $rows));
?>