<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();
$rows['code']     = '';
$rows['title']    = '';
$rows['image']    = '';
$rows['date_log'] = '';

$stmt = $conn->prepare("SELECT image_audience_view, image_audience_view_date FROM config");
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$image_audience_view = $row['image_audience_view'];
$date_log            = $row['image_audience_view_date'];


if(trim($image_audience_view) != "") {
	$stmt2 = $conn->prepare("SELECT filename FROM image_gallery WHERE code = :image_audience_view AND deleted = '0'"); 
	$stmt2->bindParam(':image_audience_view', $image_audience_view, PDO::PARAM_STR);
	$stmt2->execute();	
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$filename = $row2['filename'];
	
	$rows['code']     = $image_audience_view;
	$rows['title']    = $filename;
	$rows['image']    = '<img src="' . $GENERAL_FUNCTIONS->getSystemPath() . '/admin/Legissoft/image_gallery/' . $filename . '">';
	$rows['date_log'] = $date_log;

} 

echo json_encode(array('data' => $rows));
?>