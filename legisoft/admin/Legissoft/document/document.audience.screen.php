<style>
#audience_screen_title {
	padding: 5px 10px; 
	font-size:14px; 
	font-weight: bold;
	text-transform:capitalize;
}
#audience_screen_title .tools {
    position: absolute;
    top: 2px;
    right: 0;
}
</style>

<div id="layout_audience_screen" style="position: absolute; width: 100%; height: calc(100% - 30px); padding: 4px; background-color: #fff;"></div>

<script>
$(function () {
	$().w2destroy('layout_audience_screen');
    $('#layout_audience_screen').w2layout({
        name: 'layout_audience_screen',
        panels: [
			{
				type: 'main',
				style: 'border: 1px solid #dfdfdf;',
				overflow: 'hidden',
				content: '<iframe id="layout_audience_screen_iframe" src="Legissoft/document/document.audience.screen.viewer.php" style="height:100%;"></iframe>',
				toolbar: {
					items: [
						{ type: 'html', html: '<div id="audience_screen_title" style="height: 26px;"></div>' },
					]
				}
			}
		]
    });
	
});

function removeScreenAudienceDocument(document_code) {
	if(document_code != '') {
		$.ajax({
			url:'Legissoft/document/document.save.php',
			type:'POST',
			data: { 
				code_audience : document_code
			},
			success: function(result) {
				$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
				
				if($('input[name=document_code]').val() == document_code) {
					$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green');
					$("button[name=button_show_to_audience]").text('Show To Audience');
				}
			}
		});
	}
}

function removeScreenAudienceAction(action_code) {	
	if(action_code != '') {
		$.ajax({
			url:'Legissoft/action/action.save.php',
			type:'POST',
			data: { 
				code_audience : action_code
			},
			success: function(result) {
				$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
				
				if($('input[name=action_code]').val() == action_code) {
					$("button[name=button_action_show_to_audience]").removeClass('w2ui-btn-green');
					$("button[name=button_action_show_to_audience").text('Show To Audience');
				}
			}
		});
	}
}

function removeScreenAudienceVoting(vote_code) {
	if(vote_code != '') {
		$.ajax({
			url:'Legissoft/voting/voting.save.php',
			type:'POST',
			data: { 
				code_audience : vote_code
			},
			success: function(result) {
				$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
				
				if($('input[name=voting_code]').val() == vote_code) {
					$("button[name=button_voting_show_to_audience]").removeClass('w2ui-btn-green');
					$("button[name=button_voting_show_to_audience]").text('Show Result To Members');
				}
			}
		});
	}
}

function removeScreenAudienceImage(image_code) {
	if(image_code != '') {
		$.ajax({
			url:'Legissoft/gallery/save.gallery.image.php',
			type:'POST',
			data: { 
				code_audience : image_code
			},
			success: function(result) {
				$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
				$('#image_audience_' + image_code).removeClass('w2ui-btn-green');
				$('#image_audience_' + image_code).text('Show To Audience');				
			}
		});
	}
}

function removeScreenAudienceAnnouncement() {
	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_audience_show : 0
		},
		success: function(result) {
			$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
			$("button[name=button_announcement_show_to_audience]").removeClass('w2ui-btn-green');
			$("button[name=button_announcement_show_to_audience]").text('Announce To Audience');
			$('input[name=content_audience_show]').val(1);
		}
	});
}

function reloadScreenAudience() {
	w2ui['layout_audience_screen'].refresh();
}
</script>
