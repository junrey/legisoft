<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");

$stmt = $conn->prepare("SELECT members_action_view FROM config"); 
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$members_action_view = $row['members_action_view'];

$stmt2 = $conn->prepare("SELECT action, title, status_code, reference FROM action WHERE code = :members_action_view"); 
$stmt2->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

$title       = $row2['title'];
$action      = $row2['action'];
$status_code = $row2['status_code'];
$reference   = $row2['reference'];

$stmt3 = $conn->prepare("SELECT detail FROM action_reference WHERE code = :reference"); 
$stmt3->bindParam(':reference', $reference, PDO::PARAM_STR);
$stmt3->execute();
$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);

if($action != '') {
?>
    
    <input type="hidden" name="screen_action_code" value="<?php echo $members_action_view; ?>">
    <input type="hidden" name="screen_action_title" value="<?php echo $title; ?>">

	<div id="reference">
		<?php 
			if($stmt3->rowCount() > 0) {
				echo trim($row3['detail']);
			} 
		?>
	</div>
	<div id="action_wrapper">
		<div id="action_content">
		<?php
		$stmt4 = $conn->prepare("SELECT detail FROM action_item WHERE code = :status_code");
		$stmt4->bindParam(':status_code', $status_code, PDO::PARAM_STR);
		$stmt4->execute();
		$row4 = $stmt4->fetch(PDO::FETCH_ASSOC);
		?>
		
		<?php if($stmt4->rowCount() > 0) { ?>
			<div id="action_item" style="font-size:35px;">
				<?php echo trim($row4['detail']); ?>
			</div>
		<?php } ?>
		
		<?php echo $action; ?>
		</div>
		
		<?php
		$stmt5 = $conn->prepare("SELECT b.fname, b.lname FROM action_movant AS a INNER JOIN _user AS b ON a.movant_code = b.username WHERE a.action_code = :members_action_view ORDER BY b.lname, b.fname"); 
		$stmt5->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
		$stmt5->execute();
		?>
		
		<?php if($stmt5->rowCount() > 0) { ?>
			<div id="movant">
				<table>
					<tr>
						<td class="title">MOVANT <span>&rsaquo;</span></td>
						<td>
							<?php while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) { ?>
								<div class="label">
									<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>
									<?php echo $row5['fname'] . ' ' . $row5['lname']; ?>
								</div>
							<?php } ?>
						</td>
					</tr>
				</table>
			</div>
		<?php } ?>
		
		<?php
		$stmt6 = $conn->prepare("SELECT b.fname, b.lname FROM action_second AS a INNER JOIN _user AS b ON a.second_code = b.username WHERE a.action_code = :members_action_view ORDER BY b.lname, b.fname"); 
		$stmt6->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
		$stmt6->execute();
		?>
		<?php if($stmt6->rowCount() > 0) { ?>
			<div id="second">
				<table>
					<tr>
						<td class="title">SECOND <span>&rsaquo;</span></td>
						<td>
							<?php while($row6 = $stmt6->fetch(PDO::FETCH_ASSOC)) { ?>
								<div class="label">
									<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>
									<?php echo $row6['fname'] . ' ' . $row6['lname']; ?>
								</div>
							<?php } ?>
						</td>
					</tr>
				</table>
			</div>
		<?php } ?>
		
		<?php
		
		$stmt7 = $conn->prepare("SELECT b.detail FROM action_committee AS a INNER JOIN committee AS b ON a.committee_code = b.code WHERE a.action_code = :members_action_view ORDER BY b.detail"); 
		$stmt7->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
		$stmt7->execute();
		?>
		<?php if($stmt7->rowCount() > 0) { ?>
			<div id="commitee">
				<table>
					<tr>
						<td class="title">COMMITTEE <span>&rsaquo;</span></td>
						<td>
							<?php while($row7 = $stmt7->fetch(PDO::FETCH_ASSOC)) { ?>
								<div class="label">
									<span style="margin-right:10px; color:#fff; font-size:18px;">&bull;</span>
									<?php echo $row7['detail']; ?>
								</div>
							<?php } ?>
						</td>
					</tr>
				</table>
			</div>
		<?php } ?>
	</div>
<?php } ?>