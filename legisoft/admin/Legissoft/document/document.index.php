<div id="layout_editor" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>
"use strict";

$(function () {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	$().w2destroy('layout_editor');
    $('#layout_editor').w2layout({
        name: 'layout_editor',
		padding: 4,
        panels: [
			{ type: 'top', size: 35, overflow: 'hidden',
				toolbar: {
					items: [
						{ type: 'button', id: 'search_document', text: 'Search', icon: 'fa fa-search' },
						{ type: 'button', id: 'new_document', text: 'New', img: 'icon-page' },
						{ type: 'break' },
						{ type: 'html', id: 'toolbar_item_members', html: '<button name="button_show_to_members" onclick="setMembersDocument();" class="w2ui-btn" style="width:135px;" disabled>Show To Members</button>'},
						{ type: 'html', id: 'toolbar_item_audience', html: '<button name="button_show_to_audience" onclick="setAudienceDocument();" class="w2ui-btn" style="width:135px;" disabled>Show To Audience</button>'},
						{ type: 'break' },
						{ type: 'html',  id: 'document_toolbar',
							html: function (item) {
								var html =
									'<div>' +
									'<input type="hidden" name="document_code">' +
									'<input type="hidden" name="document_source">' +
									'<span>Title: </span>' +
									'<input name="document_title" style="width:180px;" class="w2ui-input" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Type: </span>' +
									'<select name="document_type" class="w2ui-input" style="width: 120px;" disabled></select>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Date: </span>' +
									'<input name="document_date" type="us-date" style="width: 80px;" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<button name="document_save" onclick="saveDocument();" class="w2ui-btn w2ui-btn-blue" style="min-width:60px;" disabled>Save</button>' +
									'<button name="document_delete" onclick="deleteDocument();" class="w2ui-btn w2ui-btn-red" style="min-width:60px;" disabled>Delete</button>' +
									'</div>';
								return html;
							}
						},
						{ type: 'spacer' },
						{ type: 'html',  id: 'document_status', html: '<div id="document_status_msg"></div>' }
						
					],
					onClick: function (event) {
						if(event.target == 'new_document') {
							formNewDocument();
						} else if(event.target == 'search_document') {
							w2popup.open({
								title   : 'Search Document',
								width   : 1024,
								height  : 600,
								showMax : true,
								body    : '<div id="layout_search_document" style="position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;"></div>',
								onOpen  : function (event) {
									event.onComplete = function () {
										$().w2destroy('layout_search_document');
										$().w2destroy('sidebar_search_document');
										$('#layout_search_document').w2layout({
											name: 'layout_search_document',
											padding: 4,
											panels: [
												{ type: 'left', size: 300, style: 'border: 1px solid #dfdfdf;', resizable: true, content:
													$().w2sidebar({
														name : 'sidebar_search_document',
														nodes: [],
														onClick: function(event) {
															var file_id = (event.target).substr(1);
															
															if(file_id.search('JDX') != -1) {
																getDocument(file_id);
															}
														}
													})
												},
												{ type: 'main', overflow: 'hidden' }
											]
										});
										
										$().w2destroy('grid_search_document');
										w2ui['layout_search_document'].content('main', 
											$().w2grid({ 
												name   : 'grid_search_document', 
												url    : 'Legissoft/document/json.document.search.php',
												method : 'GET', // need this to avoid 412 error on Safari
												show   : {
													toolbar        : true,
													lineNumbers    : true,
													toolbarColumns : false,
													toolbarEdit    : true
												},
												multiSearch: false,
												searches: [
													{ field: 'search', caption: 'Search here...', type: 'text' }
												],
												columns: [                
													{ field: 'title',         caption: 'Title',         size: '30%' },
													{ field: 'document_type', caption: 'Document Type', size: '20%' },
													{ field: 'created_by',    caption: 'Created By',    size: '13%' },
													{ field: 'created_date',  caption: 'Date Created',  size: '12%' },
													{ field: 'mod_by',        caption: 'Modified By',   size: '13%' },
													{ field: 'mod_date',      caption: 'Last Modified', size: '12%' },
												],
												onEdit: function (event) {
													var sel = w2ui['grid_search_document'].getSelection();
													getDocument(sel[0]);
													w2popup.close();
												},
											}) 
										);
										
										$.ajax({
											url  : 'Legissoft/document/html.document.archive.php',
											type : 'GET',
											success: function(result) {
												var nd = []; 
												for (var i in w2ui['sidebar_search_document'].nodes) nd.push(w2ui['sidebar_search_document'].nodes[i].id);
												w2ui['sidebar_search_document'].remove.apply(w2ui['sidebar_search_document'], nd);
									
												w2ui['sidebar_search_document'].add(eval(result));
											}
										});
									};
								},
								onToggle: function (event) { 
									event.onComplete = function () {
										w2ui['panel_search_document'].resize();
									}
								}
							});
						}
					}
				} 
			},
			{ type: 'left', size: 220, style: 'background-color:#fff;', resizable: true },
            { type: 'main', style: ' background-color: rgba(0,0,0,0.2);', overflow: 'hidden',
				content: 
					'<div id="document_pdf" style="width: 100%; height: 100%; display: none;"></div>' +
					'<div id="document_editor" style="width: 815px; height: 100%; margin:0 auto;">' +
						'<textarea id="content_editor" class="content_editor" style="width: 815px; height: calc(100% - 60px);">Please click the <b>New</b> button for creating a new document.</textarea>' +
					'</div>'
			}
        ]
    });
	
	tinymce.init({
		selector : '.content_editor',
		plugins  : 'autoresize print textcolor colorpicker lists table paste searchreplace image media imagetools responsivefilemanager',
		toolbar1 : 'toolbar_custom_new print | undo redo | cut copy paste | formatselect fontselect fontsizeselect | bold italic underline strikethrough',
		toolbar2 : 'forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat  subscript superscript | image media responsivefilemanager | table | searchreplace',
		font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
		fontsize_formats: '12pt 14pt 18pt 24pt 36pt 48pt 60pt',
		menu: {
			file   : {title: 'File', items: 'menu_custom_new print'},
			edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
			format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		statusbar : false,
		resize    : false,
		branding  : false,
		height    : $('.content_editor').height() + 6,
		autoresize_max_height   : $('.content_editor').height() + 6,
		theme_advanced_resizing : true,
    	theme_advanced_resizing_use_cookie : false,
		setup: function (editor) {
			editor.addMenuItem('menu_custom_new', {
				text: 'New Document',
				icon: 'newdocument',
				onclick: function() {
					formNewDocument();
				}
			});
			editor.addButton('toolbar_custom_new', {
				text: false,
				icon: 'newdocument',
				tooltip: 'New Document',
				onclick: function () {
					formNewDocument();
				}
			});
			editor.on('init', function() {
				editor.execCommand("fontSize", false, "12pt");
				editor.execCommand("fontName", false, "Verdana");
				
			});
		},
		content_style: "body { margin: 0; padding:78px 95px !important; }",
		toolbar_items_size : 'small',
		menubar : false,
		
		image_advtab: true ,
		external_filemanager_path:"/legisoft/admin/framework/filemanager/",
		filemanager_title:"File Manager" ,
		external_plugins: { "filemanager" : "/legisoft/admin/framework/filemanager/plugin.min.js"},
		relative_urls: false

	});
	
	$('input[type=us-date]').w2field('date');
	
	$().w2destroy('layout_document_archive');
	$().w2destroy('sidebar_editor');
	$().w2destroy('grid_document_members');
	w2ui['layout_editor'].content('left',
		$().w2layout({
			name: 'layout_document_archive',
			padding: 4,
			panels: [
				{ type: 'main', style: 'border: 1px solid #dfdfdf;', content:
					$().w2sidebar({
						name : 'sidebar_editor',
						nodes: [],
						onFlat: function (event) {
							if(event.goFlat) {
								w2ui['layout_editor'].set('left', { size: 50 });
							} else {
								w2ui['layout_editor'].set('left', { size: 250 });
							}
						},
						onClick: function(event) {
							var file_id = (event.target).substr(1);
							
							if(file_id.search('JDX') != -1) {
								getDocument(file_id);
							}
						}
					})
				},
				{ type: 'bottom', size: '50%', resizable: true, content: 
					$().w2grid({ 
						name : 'grid_document_members',
						show : {
							toolbar      : true,
							toolbarColumns : false,
						},
						multiSearch: false,
						columns: [
							{ field: 'code', hidden: true },
							{ field: 'fullname', size: '77%', caption: 'Members' },
							{ field: 'button', size: '23%', caption: 'Show' }
						],
						searches : [
							{ field: 'fullname', caption: 'Members', operator : 'contains', type: 'text' }
						]   
					})
				}
			]
		})
	);

	getArchiveDocument('');
	
	
});

function getArchiveDocument(selected) {
	$.ajax({
		url  : 'Legissoft/document/html.document.archive.php',
		type : 'GET',
		success: function(result) {
			var nd = []; 
			for (var i in w2ui['sidebar_editor'].nodes) nd.push(w2ui['sidebar_editor'].nodes[i].id);
			w2ui['sidebar_editor'].remove.apply(w2ui['sidebar_editor'], nd);

			w2ui['sidebar_editor'].add(eval(result));
			
			//if(selected != '') {
			//	w2ui['sidebar_editor'].select(selected);
			//	w2ui['sidebar_editor'].scrollIntoView(selected);
			//}
		}
	});
}

function getDocument(document_id) {
	$().w2destroy('layout_document_pdf');
	
	if(document_id != '') {
		$.ajax({
			url:'Legissoft/document/json.document.get.php',
			type:'POST',
			data: { 
				code : document_id
			},
			success: function(result) {
				if(result.data['source'] == 'pdf') {
					$('#document_editor').hide();
					$('#document_pdf').show();
					
					$('#document_pdf').w2layout({
						name: 'layout_document_pdf',
						panels: [
							{ type: 'top', size: '35',
								toolbar: {
									items: [
										{ type: 'html', html: '<span style="margin-left:10px;">Filename: </span><input type="text" name="pdf_filename" id="pdf_filename" value="' + result.data['filename'] + '" class="w2ui-input" readonly="1" style="margin-right:10px; width: 300px;"/>' },
										{ type: 'button',  id: 'button_select_pdf',  text: 'Select PDF', icon: 'fa fa-file-pdf-o',
											onClick: function(event) {
												w2popup.open({
													title  : 'Filemanager',
													width  : 1024,
													height : 500, 
													body   : '<iframe style="width: 100%; height: 100%;" frameborder="0" src="framework/filemanager/dialog.php?type=2&field_id=pdf_filename&relative_url=1"></iframe>',
													showMax: true,
													style  : 'overflow: hidden'
												});
											}
										}
									]
								},
							},
							{ type: 'main', overflow: 'hidden', content:
								'<div style="width: 815px; height: 100%; margin:0 auto;">' +
									'<embed src="../upload/' + result.data['filename'] + '" width="100%" height="100%" type="application/pdf">' +
								'</div>'
							}
						]
					});
				
				} else {
					/*w2ui['layout_editor'].content('main', 
					'<div style="width: 815px; height: 100%; margin:0 auto;"><textarea id="content_editor" class="content_editor" style="width: 815px; height: calc(100% - 60px);">' + result.data['document'] + '</textarea></div>'
					);
				
					tinyMCE.remove('.content_editor');
					
					tinymce.init({
						selector : '.content_editor',
						plugins  : 'autoresize print textcolor colorpicker lists table paste searchreplace image media imagetools responsivefilemanager',
						toolbar1 : 'toolbar_custom_new print | undo redo | cut copy paste | formatselect fontselect fontsizeselect | bold italic underline strikethrough',
						toolbar2 : 'forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat  subscript superscript | image media responsivefilemanager | table | searchreplace',
						font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
						fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
						menu: {
							file   : {title: 'File', items: 'menu_custom_new print'},
							edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
							format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
							table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
						},
						statusbar : false,
						resize    : false,
						branding  : false,
						height    : $('.content_editor').height() + 6,
						autoresize_max_height   : $('.content_editor').height() + 6,
						theme_advanced_resizing : true,
						theme_advanced_resizing_use_cookie : false,
						setup: function (editor) {
							editor.addMenuItem('menu_custom_new', {
								text: 'New Document',
								icon: 'newdocument',
								onclick: function() {
									formNewDocument();
								}
							});
							editor.addButton('toolbar_custom_new', {
								text: false,
								icon: 'newdocument',
								tooltip: 'New Document',
								onclick: function () {
									formNewDocument();
								}
							});
							editor.on('init', function() {
								this.execCommand("fontName", false, "verdana");
								this.execCommand("fontSize", false, "12pt");
							});
						},
						menubar : false,
						content_style : "body { margin: 0; padding:78px 95px !important; }",
						toolbar_items_size : 'small',
						
						image_advtab: true ,
						external_filemanager_path:"/legisoft/admin/framework/filemanager/",
						filemanager_title:"File Manager" ,
						external_plugins: { "filemanager" : "/legisoft/admin/framework/filemanager/plugin.min.js"},
						relative_urls: false
					});*/
					
					$('#document_pdf').hide();
					$('#document_editor').show();
					
					tinymce.get('content_editor').setContent(result.data['document']);
				
				}
				
				$("input[name=document_title]").attr("disabled", false);
				$("select[name=document_type]").attr("disabled", false);
				$("input[name=document_date]").attr("disabled", false);
				$("button[name=document_save]").attr("disabled", false);
				
				getDocumentType(result.data['document_type']);	
				
				$("input[name=document_title]").val(result.data['title']);
				$("input[name=document_date]").val(result.data['document_date']);
				
				$("input[name=document_title]").removeClass('w2ui-error');
				$("input[name=document_date]").removeClass('w2ui-error');
				
				$('#document_status_msg').html('');
				
				$("input[name=document_code]").val(result.data['code']);
				$("input[name=document_source]").val(result.data['source']);
				
				$("button[name=document_delete]").attr("disabled", false);
				$("button[name=button_show_to_members]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_show_to_members]").text('Show To Members');
				$("button[name=button_show_to_audience]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_show_to_audience]").text('Show To Audience');
				
				if(result.data['members_view'] == document_id) {
					$("button[name=button_show_to_members]").addClass('w2ui-btn-green');
					$("button[name=button_show_to_members]").text('Hide From Members');
				}
				
				if(result.data['audience_view'] == document_id) {
					$("button[name=button_show_to_audience]").addClass('w2ui-btn-green');
					$("button[name=button_show_to_audience]").text('Hide From Audience');
				}
				
				getDocumentMembers('', '');

			}
		});
	}
}

localStorage.removeItem('document_type');
function getDocumentType(selected) {
	if(localStorage.getItem('document_type') === null) {
		$.ajax({
			url:'Legissoft/document_type/json.document.type.php',
			type:'GET',
			dataType: 'json',
			success: function( json ) {
				$('select[name=document_type]').empty();
				
				$.each(json.records, function(i, value) {
					$('select[name=document_type]').append($('<option>').text(value.detail).attr('value', value.recid));
				});
				
				if(selected != '') {
					$('select[name=document_type]').val(selected);
				}
				
				localStorage.setItem('document_type', JSON.stringify(json));
			}
		});
	} else {
		$('select[name=document_type]').empty();
		
		var document_type = JSON.parse(localStorage.getItem('document_type'));
		
		$.each(document_type.records, function(i, value) {
			$('select[name=document_type]').append($('<option>').text(value.detail).attr('value', value.recid));
		});
		
		if(selected != '') {
			$('select[name=document_type]').val(selected);
		}
	}
}
function saveDocument() {
	//var content = tinyMCE.activeEditor.getContent();
	
	var document_source = $("input[name=document_source]").val();
	
	if($.trim($("input[name=document_title]").val()) != '' && $.trim($("input[name=document_date]").val()) != '') {
		
		if(document_source == 'pdf') {
			var pdf_filename = $.trim($('input[name=pdf_filename]').val());
			
			if(pdf_filename != '') {
				$.ajax({
					url:'Legissoft/document/document.save.php',
					type:'POST',
					data: { 
						code     : $("input[name=document_code]").val(),
						content  : '',
						filename : pdf_filename, 
						title    : $("input[name=document_title]").val(),
						type     : $("select[name=document_type").val(),
						date     : $("input[name=document_date").val(),
						source   : 'pdf'
					},
					success: function(result) {
						$("input[name=document_code]").val(result.data['code']);
						$("button[name=button_show_to_members]").attr("disabled", false);
						$("button[name=button_show_to_audience]").attr("disabled", false);
						$("button[name=document_delete]").attr("disabled", false);
						$('#document_status_msg').html('');
						$("input[name=document_title]").removeClass('w2ui-error');
						$("input[name=document_date]").removeClass('w2ui-error');
						
						getArchiveDocument(result.data['code']);
						
						updateMembersScreenDocument();
						updateAudienceScreenDocument();
						
						getDocumentMembers('', '');
						
						ohSnap('Document Saved!', {'color':'green', 'duration':'1000'});
					}
				});
			
			}	else {
				w2alert('Saving blank pdf is not allowed!');
			}
		} else {
			var content = tinyMCE.get('content_editor').getContent();
			if(content.trim() != '') {
				$.ajax({
					url:'Legissoft/document/document.save.php',
					type:'POST',
					data: { 
						code     : $("input[name=document_code]").val(),
						content  : content, 
						filename : '',
						title    : $("input[name=document_title]").val(),
						type     : $("select[name=document_type").val(),
						date     : $("input[name=document_date").val(),
						source   : 'editor'
					},
					success: function(result) {
						$("input[name=document_code]").val(result.data['code']);
						$("button[name=button_show_to_members]").attr("disabled", false);
						$("button[name=button_show_to_audience]").attr("disabled", false);
						$("button[name=document_delete]").attr("disabled", false);
						$('#document_status_msg').html('');
						$("input[name=document_title]").removeClass('w2ui-error');
						$("input[name=document_date]").removeClass('w2ui-error');
						
						getArchiveDocument(result.data['code']);
						
						updateMembersScreenDocument();
						updateAudienceScreenDocument();
						
						getDocumentMembers('', '');
						
						ohSnap('Document Saved!', {'color':'green', 'duration':'1000'});
					}
				});
			} else {
				w2alert('Saving blank document is not allowed!');
			}
		}
	} else {
		if($.trim($("input[name=document_title]").val()) == '') {
			$("input[name=document_title]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Document Title is required!');
			
		} else if($.trim($("input[name=document_date]").val()) == '') {
			$("input[name=document_date]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Document Date is required!');
		}
	}
}

function deleteDocument() {
	var document_code = $.trim($("input[name=document_code]").val());
	if(document_code != '') {
		 w2confirm('Do you want to delete?').yes(function () { 
		 	$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_delete : $.trim($("input[name=document_code]").val())
				},
				success: function(result) {
					w2ui['layout_editor'].content('main','');
					$("input[name=document_code]").val('');
					$("input[name=document_title]").val('').attr("disabled", true);
					$("select[name=document_type]").empty().attr("disabled", true);
					$("input[name=document_date]").val('').attr("disabled", true);
					$("select[name=document_type]").attr("disabled", true);
					$("button[name=document_save]").attr("disabled", true);
					$("input[name=document_title]").removeClass('w2ui-error');
					$("input[name=document_date]").removeClass('w2ui-error');

					
					$("button[name=button_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Members');
					$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Audience');
					
					w2ui['sidebar_editor'].remove('a' + document_code);
					w2ui['sidebar_editor'].remove('b' + document_code);
					
					w2ui['grid_document_members'].clear();
					
					updateMembersScreenDocument();
					updateAudienceScreenDocument();
				}
			});
		 });
	}
}

function setMembersDocument() {
	var document_source = $("input[name=document_source]").val();
	
	if(document_source == 'pdf') {
		var pdf_filename = $("input[name=pdf_filename]").val();
		
		if(pdf_filename.trim() != '') {
			$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_members : $("input[name=document_code]").val()
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_show_to_members]").removeClass('w2ui-btn-green');
						$("button[name=button_show_to_members]").text('Show To Members');
					} else {
						$("button[name=button_show_to_members]").addClass('w2ui-btn-green');
						$("button[name=button_show_to_members]").text('Hide From Members');
					}
					
					updateMembersScreenDocument();
					getArchiveDocument('');
				}
			});
		} else {
			w2alert('Blank pdf will not allow to show to Members!');
		}
		
	} else {
		var content = tinyMCE.get('content_editor').getContent();
		
		if(content.trim() != '') {
			$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_members : $("input[name=document_code]").val()
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_show_to_members]").removeClass('w2ui-btn-green');
						$("button[name=button_show_to_members]").text('Show To Members');
					} else {
						$("button[name=button_show_to_members]").addClass('w2ui-btn-green');
						$("button[name=button_show_to_members]").text('Hide From Members');
					}
					
					updateMembersScreenDocument();
					getArchiveDocument('');
				}
			});
			
		} else {
			w2alert('Blank document will not allow to show to Members!');
		}
	}
}

function setAudienceDocument() {
	var document_source = $("input[name=document_source]").val();
	
	if(document_source == 'pdf') {
		var pdf_filename = $("input[name=pdf_filename]").val();
		
		if(pdf_filename.trim() != '') {
			$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_audience : $("input[name=document_code]").val()
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green');
						$("button[name=button_show_to_audience]").text('Show To Audience');
					} else {
						$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
						$("button[name=button_show_to_audience]").text('Hide From Audience');
					}
					
					updateAudienceScreenDocument();
					getArchiveDocument('');
				}
			});
		} else {
			w2alert('Blank pdf will not allow to show to Audience!');
		}
	} else {
		var content = tinyMCE.get('content_editor').getContent();
		
		if(content.trim() != '') {
			$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_audience : $("input[name=document_code]").val()
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green');
						$("button[name=button_show_to_audience]").text('Show To Audience');
					} else {
						$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
						$("button[name=button_show_to_audience]").text('Hide From Audience');
					}
					
					updateAudienceScreenDocument();
					getArchiveDocument('');
				}
			});
			
		} else {
			w2alert('Blank document will not allow to show to Audience!');
		}
	}
}

function openPopupNewDocument() {
	$().w2destroy('form_new_document');
	$().w2form({
		name  : 'form_new_document',
		style : 'border: 0px; background-color: transparent;',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Title:</label>' +
			'        <div>' +
			'           <input name="pre_document_title" type="text" maxlength="200" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Type:</label>' +
			'        <div>' +
			'            <input name="pre_document_type" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'	<div class="w2ui-field">' +
			'		<label>Date:</label>' +
			'		<div><input name="pre_document_date" type="us-date" style="width: 250px" ></div>' +
			'	</div>' +
			'	<div class="w2ui-field">' +
			'		<label>Source:</label>' +
			'		<div><input name="source" type="list" style="width: 250px" ></div>' +
			'	</div>' +
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Clear</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Create</button>' +
			'</div>',
		fields: [
			{ field: 'pre_document_title',   type: 'text', required: true },
			{ field: 'pre_document_type', type: 'list', required: true,
				options: { 
					url:     'Legissoft/document_type/json.document.type.php', 
					recId:   'recid',
					recText: 'detail', 
					minLength: 0
				} 
			},
			{ field: 'pre_document_date', type: 'date', required: true },
			{ field: 'source', type: 'list', required: true, 
                options: { items: [{ recid: 'editor', text: 'Editor' }, { recid: 'pdf', text: 'PDF' }] }
			},
		],
		actions: {
			"save": function() { 
				var val = this.validate();
				if(val.length == 0){
					$().w2destroy('layout_document_pdf');
					
					if($("input[name=source]").data('selected').recid == 'editor') {
						$("input[name=document_source]").val('editor');
						newDocument();
						w2popup.close();
					} else {
						$("input[name=document_source]").val('pdf');
						newPdfDocument();
						w2popup.close();
					}
				}
			},
			"reset": function() { this.clear(); }
		}
	});
	
	w2ui['form_new_document'].record['pre_document_date'] = new Date(Date.now()).toLocaleDateString();
	w2ui['form_new_document'].refresh(); 
}

function formNewDocument() {
	openPopupNewDocument();
	$().w2popup('open', {
		title   : 'New Document',
		body    : '<div id="popup_form_new_document" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 500,
		height  : 300, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_new_document').w2render('form_new_document');
			}
		},
		onClose: function () {
			w2ui['form_new_document'].clear();
		}
	});
}

function responsive_filemanager_callback(field_id){
	//console.log(field_id);
	//var url=jQuery('#'+field_id).val();
	//alert('update '+field_id+" with "+url);
	w2popup.close();

	if(field_id != '') {
		w2ui['layout_document_pdf'].content('main', 
			'<div style="width: 815px; height: 100%; margin:0 auto;">' +
				'<embed src="../upload/' + $('#pdf_filename').val() + '" width="100%" height="100%" type="application/pdf">' +
			'</div>'
		);
	}
}
function newPdfDocument() {
	$('#document_pdf').show();
	$('#document_editor').hide();
	
	$().w2destroy('layout_document_pdf');
	$('#document_pdf').w2layout({
		name: 'layout_document_pdf',
		panels: [
			{ type: 'top', size: '35',
				toolbar: {
					items: [
						{ type: 'html', html: '<span style="margin-left:10px;">Filename: </span><input type="text" name="pdf_filename" id="pdf_filename" class="w2ui-input" readonly="1" style="margin-right:10px; width: 300px;"/>' },
						{ type: 'button',  id: 'button_select_pdf',  text: 'Select PDF', icon: 'fa fa-file-pdf-o',
							onClick: function(event) {
								w2popup.open({
									title  : 'Filemanager',
									width  : 1024,
									height : 500, 
									body   : '<iframe style="width: 100%; height: 100%;" frameborder="0" src="framework/filemanager/dialog.php?type=2&field_id=pdf_filename&relative_url=1"></iframe>',
									showMax: true,
									style  : 'overflow: hidden'
								});
							}
						}
					]
				},
			},
			{ type: 'main', overflow: 'hidden' }
		]
	});
	
	$("input[name=document_title]").attr("disabled", false);
	$("select[name=document_type]").attr("disabled", false);
	$("input[name=document_date]").attr("disabled", false);
	$("button[name=document_save]").attr("disabled", false);
	
	getDocumentType($("input[name=pre_document_type]").data('selected').recid);	
	
	$("input[name=document_title]").val($("input[name=pre_document_title]").val());
	$("input[name=document_date]").val($("input[name=pre_document_date]").val());
	$('#document_status_msg').html('<div style="background-color:#BB3C3E; color:#fff; padding:4px 5px;"><i class="fa fa-exclamation-triangle"></i> Unsave!</div>');

	$("input[name=document_code]").val('');

	$("button[name=button_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Members');
	$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Audience');
	$("button[name=document_delete]").attr("disabled", true);
	
	$("input[name=document_title]").removeClass('w2ui-error');
	$("input[name=document_date]").removeClass('w2ui-error');
					
	w2ui['grid_document_members'].clear();


}

function newDocument() {
	$('#document_editor').show();
	$('#document_pdf').hide();

	tinymce.get('content_editor').setContent('');
	tinyMCE.get('content_editor').focus();
	/*w2ui['layout_editor'].content('main', 
		'<div style="width: 815px; height: 100%; margin:0 auto;"><textarea id="content_editor" class="content_editor" style="width: 815px; height: calc(100% - 60px);"></textarea></div>'
	);
	
	tinyMCE.remove('.content_editor');
	tinymce.init({
		selector : '.content_editor',
		plugins  : 'autoresize print textcolor colorpicker lists table paste searchreplace image media imagetools responsivefilemanager',
		toolbar1 : 'toolbar_custom_new print | undo redo | cut copy paste | formatselect fontselect fontsizeselect | bold italic underline strikethrough',
		toolbar2 : 'forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat  subscript superscript | image media responsivefilemanager | table | searchreplace',
		font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
		fontsize_formats: '12pt 14pt 18pt 24pt 36pt 48pt 60pt',
		menu: {
			file   : {title: 'File', items: 'menu_custom_new print'},
			edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
			format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		statusbar : false,
		resize    : false,
		branding  : false,
		height    : $('.content_editor').height() + 6,
		autoresize_max_height   : $('.content_editor').height() + 6,
		theme_advanced_resizing : true,
    	theme_advanced_resizing_use_cookie : false,
		setup: function (editor) {
			editor.addMenuItem('menu_custom_new', {
				text: 'New Document',
				icon: 'newdocument',
				onclick: function() {
					formNewDocument();
				}
			});
			editor.addButton('toolbar_custom_new', {
				text: false,
				icon: 'newdocument',
				tooltip: 'New Document',
				onclick: function () {
					formNewDocument();
				}
			});
			editor.on('init', function() {
				editor.execCommand("fontSize", false, "12pt");
				editor.execCommand("fontName", false, "Verdana");
				
			});
		},
		content_style: "body { margin: 0; padding:78px 95px !important; }",
		toolbar_items_size : 'small',
		menubar : false,
		
		image_advtab: true ,
		external_filemanager_path:"/legisoft/admin/framework/filemanager/",
		filemanager_title:"File Manager" ,
		external_plugins: { "filemanager" : "/legisoft/admin/framework/filemanager/plugin.min.js"},
		relative_urls: false

	});*/
	
	$("input[name=document_title]").attr("disabled", false);
	$("select[name=document_type]").attr("disabled", false);
	$("input[name=document_date]").attr("disabled", false);
	$("button[name=document_save]").attr("disabled", false);

	getDocumentType($("input[name=pre_document_type]").data('selected').recid);	
	
	$("input[name=document_title]").val($("input[name=pre_document_title]").val());
	$("input[name=document_date]").val($("input[name=pre_document_date]").val());
	$('#document_status_msg').html('<div style="background-color:#BB3C3E; color:#fff; padding:4px 5px;"><i class="fa fa-exclamation-triangle"></i> Unsave!</div>');

	$("input[name=document_code]").val('');

	$("button[name=button_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Members');
	$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true).text('Show To Audience');
	$("button[name=document_delete]").attr("disabled", true);
	
	$("input[name=document_title]").removeClass('w2ui-error');
	$("input[name=document_date]").removeClass('w2ui-error');
					
	w2ui['grid_document_members'].clear();
}

function getDocumentMembers(member_code, status) {
	// on - off specific viewing
	var document_code = $.trim($("input[name=document_code]").val());
	if(document_code != '') {
		$.ajax({
			url      :'Legissoft/document/json.document.members.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code        : document_code,
				member_code : member_code,
				status      : status
			},
			success: function( json ) {
				w2ui['grid_document_members'].clear();
				
				$.each(json.records, function(i, value) {
					if(value.status == 'hide') {
						w2ui['grid_document_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button, 'w2ui': { 'style': 'background-color: #C2F5B4; font-weight: bold;' } });
					} else {
						w2ui['grid_document_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button });
					}
				});
				
				if(member_code != '') {
					getArchiveDocument('');
				}
			}
		});
	}
}

function updateMembersScreenDocument() {
	$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
}

function updateAudienceScreenDocument() {
	$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
}
</script>
