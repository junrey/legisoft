<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");

$stmt = $conn->prepare("SELECT image_members_view FROM config");
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$image_members_view = $row['image_members_view'];


if(trim($image_members_view) != "") {
	$stmt2 = $conn->prepare("SELECT filename FROM image_gallery WHERE code = :image_members_view AND deleted = '0'"); 
	$stmt2->bindParam(':image_members_view', $image_members_view, PDO::PARAM_STR);
	$stmt2->execute();	
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$filename = $row2['filename'];
	
?>
	<input type="hidden" name="screen_image_code" value="<?php echo $image_members_view; ?>">
	<input type="hidden" name="screen_image_title" value="<?php echo $filename; ?>">

<?php
	echo "<img src=\"../image_gallery/" . $filename . "\">";

} 
?>