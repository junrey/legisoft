<div id="layout_editor" style="position: absolute; width: 100%; height: calc(100% - 58px);"></div>

<script>
$(function () {
	$().w2destroy('layout_editor');
    $('#layout_editor').w2layout({
        name: 'layout_editor',
        panels: [
            { type: 'main', overflow: 'hidden',
				toolbar: {
					items: [
						{ type: 'button', id: 'new_document', text: 'New Document', img: 'icon-page' },
						{ type: 'break' },
						{ type: 'html', id: 'toolbar_item_members', html: '<button name="button_show_to_members" onclick="setMembersDocument();" class="w2ui-btn" style="width:130px;" disabled>Show to Members</button>'},
						{ type: 'html', id: 'toolbar_item_audience', html: '<button name="button_show_to_audience" onclick="setAudienceDocument();" class="w2ui-btn" style="width:130px;" disabled>Show to Audience</button>'},
						{ type: 'break' },
						{ type: 'html',  id: 'document_toolbar',
							html: function (item) {
								var html =
									'<div style="padding: 3px 10px;">' +
									'<input type="hidden" name="document_code">' +
									'<span>Title: </span>' +
									'<input name="document_title" style="width:250px;" class="w2ui-input" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Document Type: </span>' +
									'<select name="document_type" class="w2ui-input" disabled></select>' +
									'<span style="margin-left:10px;"></span>' +
									'<button name="document_save" onclick="saveDocument();" class="w2ui-btn w2ui-btn-blue" disabled>Save</button>' +
									'<button name="document_delete" onclick="deleteDocument();" class="w2ui-btn w2ui-btn-red" disabled>Delete</button>' +
									'</div>';
								return html;
							}
						},
						{ type: 'spacer' },
						{ type: 'html',  id: 'document_status', html: '<div id="document_status_msg"></div>' }
						
					],
					onClick: function (event) {
						if(event.target == 'new_document') {
							formNewDocument();
						}
					}
				} 
			}
        ]
    });
	
});

function getDocumentType(selected) {
	$.ajax({
		url:'Legissoft/document_type/json.document.type.php',
		type:'GET',
		dataType: 'json',
		success: function( json ) {
			$.each(json.records, function(i, value) {
				$('select[name=document_type]').append($('<option>').text(value.detail).attr('value', value.recid));
			});
			
			if(selected != '') {
				$('select[name=document_type]').val(selected);
			}
		}
	});

}
function saveDocument() {
	if($.trim($("input[name=document_title]").val()) != '') {
		$.ajax({
			url:'Legissoft/document/document.save.php',
			type:'POST',
			data: { 
				code   : $("input[name=document_code]").val(),
				content: tinyMCE.activeEditor.getContent(), 
				title  : $("input[name=document_title]").val(),
				type   : $("select[name=document_type").val(),
			},
			success: function(result) {
				$("input[name=document_code]").val(result.data['code']);
				$("button[name=button_show_to_members]").attr("disabled", false);
				$("button[name=button_show_to_audience]").attr("disabled", false);
				$("button[name=document_delete]").attr("disabled", false);
				$('#document_status_msg').html('');
				$("input[name=document_title]").removeClass('w2ui-error');
			}
		});
	
	} else {
		$("input[name=document_title]").removeClass('w2ui-error').addClass('w2ui-error').focus();
		 w2alert('Document Title is required!');
	}
}

function deleteDocument() {
	if($.trim($("input[name=document_code]").val()) != '') {
		 w2confirm('Do you want to delete?').yes(function () { 
		 	$.ajax({
				url:'Legissoft/document/document.save.php',
				type:'POST',
				data: { 
					code_delete : $("input[name=document_code]").val()
				},
				success: function(result) {
					$('#tab_editor').load('Legissoft/document/document.create.php');
				}
			});
		 });
	}
}

function setMembersDocument() {
	$.ajax({
		url:'Legissoft/document/document.save.php',
		type:'POST',
		data: { 
			code_members : $("input[name=document_code]").val()
		},
		success: function(result) {
			if(result.data['status'] == 'show') {
				$("button[name=button_show_to_members]").removeClass('w2ui-btn-green');
				$("button[name=button_show_to_members]").text('Show to Members');
			} else {
				$("button[name=button_show_to_members]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
				$("button[name=button_show_to_members]").text('Hide to Members');
			}
		}
	});
}

function setAudienceDocument() {
	$.ajax({
		url:'Legissoft/document/document.save.php',
		type:'POST',
		data: { 
			code_audience : $("input[name=document_code]").val()
		},
		success: function(result) {
			if(result.data['status'] == 'show') {
				$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green');
				$("button[name=button_show_to_audience]").text('Show to Audience');
			} else {
				$("button[name=button_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
				$("button[name=button_show_to_audience]").text('Hide to Audience');
			}
		}
	});

}


function openPopupNewDocument() {
	$().w2destroy('form_new_document');
	$().w2form({
		name  : 'form_new_document',
		style : 'border: 0px; background-color: transparent;',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Title:</label>' +
			'        <div>' +
			'           <input name="pre_document_title" type="text" maxlength="200" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Type:</label>' +
			'        <div>' +
			'            <input name="pre_document_type" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Clear</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Create</button>' +
			'</div>',
		fields: [
			{ field: 'pre_document_title',   type: 'text', required: true },
			{ field: 'pre_document_type', type: 'list', required: true,
				options: { 
					url:     'Legissoft/document_type/json.document.type.php', 
					recId:   'recid',
					recText: 'detail', 
					minLength: 0
				} 
			},
		],
		actions: {
			"save": function() { 
				var val = this.validate();
				if(val.length == 0){ 
					newDocument();
					w2popup.close();
				}
			},
			"reset": function() { this.clear(); }
		}
	});
}

function formNewDocument() {
	openPopupNewDocument();
	$().w2popup('open', {
		title   : 'New Document',
		body    : '<div id="popup_form_new_document" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 500,
		height  : 200, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_new_document').w2render('form_new_document');
			}
		},
		onClose: function () {
			w2ui['form_new_document'].clear();
		}
	});
}

function newDocument() {
	w2ui['layout_editor'].content('main', 
		'<textarea class="content_editor" style="width: calc(100% - 2px); height: calc(100% - 112px);"></textarea>'
	);
	tinymce.init({
		selector                           : '.content_editor',
		resize                             : false,
		branding                           : false,
		theme_advanced_resizing            : true,
    	theme_advanced_resizing_use_cookie : false,
		plugins                            : 'autoresize',
		autoresize_max_height			   : $('.content_editor').height() + 6
	});
	
	$("input[name=document_title]").attr("disabled", false);
	$("select[name=document_type]").attr("disabled", false);
	$("button[name=document_save]").attr("disabled", false);
	
	getDocumentType($("input[name=pre_document_type]").data('selected').recid);	
	
	$("input[name=document_title]").val($("input[name=pre_document_title]").val());
	
	$('#document_status_msg').html('<div style="background-color:#BB3C3E; color:#fff; padding:4px 5px;"><i class="fa fa-exclamation-triangle"></i> This document is not save!</div>');


}

</script>
