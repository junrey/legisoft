<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$stmt = $conn->prepare("SELECT image_audience_view FROM config");
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$image_audience_view = $row['image_audience_view'];


if(trim($image_audience_view) != "") {
	$stmt2 = $conn->prepare("SELECT filename FROM image_gallery WHERE code = :image_audience_view AND deleted = '0'"); 
	$stmt2->bindParam(':image_audience_view', $image_audience_view, PDO::PARAM_STR);
	$stmt2->execute();	
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$filename = $row2['filename'];
	
?>
	<input type="hidden" name="screen_image_code" value="<?php echo $image_audience_view; ?>">
	<input type="hidden" name="screen_image_title" value="<?php echo $filename; ?>">

<?php
	echo "<img src=\"" . $GENERAL_FUNCTIONS->getSystemPath(). "/admin/Legissoft/image_gallery/" . $filename . "\">";

} 
?>