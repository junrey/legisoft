<style>
.messaging img {
	float:left;
	height:40px;
	width:40px;
	border-radius: 50%;	
	border:2px solid #ccc;
	vertical-align:middle;
}
.messaging .detail {
	float:left;	
	padding:5px 0 5px 5px;
}
.messaging .detail div {
	padding:2px;
}
.messaging .detail div i {
	color:#aaa;	
}

textarea[name=message] {
	width:calc(100% - 100px); 
	height:60px; 
	border-radius: 4px 0 0 4px; 
	vertical-align:top;
}
button[name=message_button] {
	width:100px; 
	height:60px; 
	margin:0; 
	border-radius: 0 4px 4px 0; 
	vertical-align:top;
}
.avatar.chat {
	width:60px;
	height:60px;
	border-radius: 50%;	
}
.chat_content_user_message {
	max-width:250px;
	min-width:120px;
	padding:10px;
	border-radius: 5px;
	
	overflow-wrap: break-word;
	word-wrap: break-word;
	
	/* Adds a hyphen where the word breaks */
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	-moz-hyphens: auto;
	hyphens: auto;
}
.chat_content_user_image.left {
	float:left;
}
.chat_content_user_message.left {
	float:left;
	margin-left:6px;
	background-color:#e8e8e8;
	color:#000;
}
.chat_content_user_message.left .meta {
	font-size: 11px;
	line-height: 1.4;
	color: #999;
}
.chat_content_user_message.left button {
	float:right;
	width:15px; 
	height:15px;
	min-width:15px; 
	min-height:15px; 
	padding:0;
	margin:0;
}

.chat_content_user_image.right {
	float:right;
}
.chat_content_user_message.right {
	float:right;
	margin-right:6px;
	background-color:#73b4f3;
	color:#fff;
}
.chat_content_user_message.right .meta {
	font-size: 11px;
	line-height: 1.4;
	color:#e0e9f1;	
}
.chat_content_user_message.right button {
	float:right;
	width:15px; 
	height:15px;
	min-width:15px; 
	min-height:15px; 
	padding:0;
	margin:0;
}
#chat_selected_user {
	width:400px;	
	height:40px;
}

.messaging_total_count {
	float: right;
	border: 2px solid #6abe68;
	width: 30px;
	height: 30px;
	color: #6abe68;
	border-radius: 50%;
	margin-top: 5px;
	font-size: 14px;
	display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */	
}

.messaging_total_count.new_msg {
	background-color: #6abe68;
	color: #fff;
}
</style>
<div id="layout_messaging" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>
"use strict";
$().w2grid({ 
	name : 'grid_messaging_account',
	recordHeight : 50,
	show : {
		columnHeaders : false,
	},
	columns: [
		{ field: 'accounts', size: '100%', caption: 'Accounts' },
		{ field: 'total', size: '40px', caption: 'Totals', style: 'text-align: center' },
		{ field: 'username', hidden: true }
	],
	onSelect: function(event) {
		event.onComplete = function () {
			loadMessage();
			
			var sel = w2ui['grid_messaging_account'].getSelection();
			$('#chat_selected_user').html(w2ui['grid_messaging_account'].get(sel[0])['accounts']);
		
			$('textarea[name=message').prop('disabled', false);
			$('button[name=message_button').prop('disabled', false);
			
			w2ui['layout_messaging_content_top_toolbar'].enable('menu_chat_option');

		}
	}, 
	onUnselect: function(event) {
		w2ui['layout_messaging_content'].content('main', '');
		$('#chat_selected_user').html('');
		w2ui['layout_messaging_content_top_toolbar'].disable('menu_chat_option');
        $('textarea[name=message').prop('disabled', true);
		$('button[name=message_button').prop('disabled', true);
    }   
});	

$(function () {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px; background-color:#f6f6f6';
	$().w2destroy('layout_messaging');
    $('#layout_messaging').w2layout({
        name: 'layout_messaging',
		padding: 4,
        panels: [
			{ type: 'left', size: 300, style: 'border: 1px solid #dfdfdf;', resizable: true },
            { type: 'main', style: 'background-color:#fff', overflow: 'hidden', content:
				$().w2layout({
					name: 'layout_messaging_content',
					padding: 4,
					panels: [
						{ type: 'top', size: 50,
							toolbar: {
								name: 'toolbar_chat',
								items: [
									{ type: 'html', html : '<div id="chat_selected_user"></div>' },
									{ type: 'spacer' },
									{ type: 'menu', id:'menu_chat_option', text: 'Option', icon: 'fa fa-gear', disabled: true, 
										items: [
											{ id: 'menu_chat_option_delete_chat', text: 'Delete Conversation', icon: 'fa fa-times' }
										]
									},
								],
								onClick: function(event) {
									if(event.target == 'menu_chat_option:menu_chat_option_delete_chat') {
										deleteChat();
									}
								}
							}
						},
						{ type: 'main', style: pstyle },
						{ type: 'bottom', size: 60, overflow: 'hidden',
							content: 
								'<textarea class="w2ui-input" name="message" placeholder="Type your message here..." maxlength="500" disabled></textarea>' +
								'<button class="w2ui-btn" name="message_button" onclick="sendMessage();" disabled>Send</button>'
						}
					]
				})
			},
			{ type: 'right', size: 430, style: 'background-color:#fff;', overflow: 'hidden', resizable: true,
				content: 
					'<div id="tab_announcement" style="width: 100%; height: 29px;"></div>' +
					'<div id="tab_announcement_members" class="announcement_tab" style="width: 100%; height: calc(100% - 8px); overflow:hidden;"></div>' +
    				'<div id="tab_announcement_audience" class="announcement_tab" style="width: 100%; height: calc(100% - 8px); overflow:hidden;"></div>'
			},
        ],
		onShow: function(event) {
			
		} 
    });
	
	w2ui['layout_messaging'].content('left', w2ui['grid_messaging_account']);
		
	$().w2destroy('tab_announcement_members');
	$('#tab_announcement_members').w2layout({
		name: 'tab_announcement_members',
		panels: [
			{ type: 'main', 
				toolbar: {
					items: [
						{ type: 'html', html : '<button name="announcement_members_save" onclick="saveAnnouncementMembers();" class="w2ui-btn w2ui-btn-blue" style="min-width:60px; margin:0;">Save</button>' },
						{ type: 'spacer' },
						{ type: 'html', html: '<button name="button_announcement_show_to_members" onclick="setMembersAnnouncement();" class="w2ui-btn" style="min-width:150px; margin:0;">Announce To Members</button>'},
					]
				},
				overflow: 'hidden',
				content : '<input type="hidden" name="content_members_show" value="1"><textarea id="writer_announcement_members" class="writer_announcement" style="width: calc(100% - 2px); height: calc(100% - 106px);"></textarea>'
			}
		]
	});
	
	$().w2destroy('tab_announcement_audience');
	$('#tab_announcement_audience').w2layout({
		name: 'tab_announcement_audience',
		panels: [
			{ type: 'main', 
				toolbar: {
					items: [
						{ type: 'html', html : '<button name="announcement_audience_save" onclick="saveAnnouncementAudience();" class="w2ui-btn w2ui-btn-blue" style="min-width:60px; margin:0;">Save</button>' },
						{ type: 'spacer' },
						{ type: 'html', html: '<button name="button_announcement_show_to_audience" onclick="setAudienceAnnouncement();" class="w2ui-btn" style="min-width:150px; margin:0;">Announce To Audience</button>'},
					]
				},
				overflow: 'hidden',
				content : '<input type="hidden" name="content_audience_show" value="1"><textarea id="writer_announcement_audience" class="writer_announcement" style="width: calc(100% - 2px); height: calc(100% - 106px);"></textarea>'
			}
		]
	});
	
	$().w2destroy('tab_announcement');
	$('#tab_announcement').w2tabs({
		name: 'tab_announcement',
		active: 'tab_announcement_members',
		tabs: [
			{ id: 'tab_announcement_members', text: 'Members' },
			{ id: 'tab_announcement_audience', text: 'Audience' }
		],
		onClick: function (event) {
			$('.announcement_tab').hide();
			$('#' + event.target).show();
			
			for (var item in w2ui){
				if (w2ui[item].resize){
					w2ui[item].resize();
				}
			}
		}
	});

	getAnnouncement();
	
	tinymce.init({
		selector : '.writer_announcement',
		plugins  : 'autoresize print textcolor colorpicker lists table paste',
		toolbar1 : 'newdocument print | undo redo | cut copy paste | formatselect removeformat  subscript superscript  | fontselect fontsizeselect | bold italic underline strikethrough forecolor backcolor',
		toolbar2 : 'alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | | table',
		font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
		fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
		menu: {
			file   : {title: 'File', items: 'menu_custom_new print'},
			edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
			format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		statusbar : false,
		resize    : false,
		branding  : false,
		height    : $('.writer_announcement').height() - 21,
		autoresize_max_height   : $('.writer_announcement').height() - 21,
		theme_advanced_resizing : true,
		theme_advanced_resizing_use_cookie : false,
		toolbar_items_size : 'small',
		menubar : false,		
	});
	
	loadMessagingAccount();
	
	setInterval(function() { loadMessage(); }, 5000);


	
});
function loadMessagingAccount() {
	$.ajax({
		url    :'Legissoft/messaging/json.messaging.account.php',
		type   :'GET',
		dataType : 'json',
		success: function( json ) {
			 w2ui['grid_messaging_account'].clear();
			
			 $.each(json.records, function(i, value) {
				w2ui['grid_messaging_account'].add({ recid: value.recid, username: value.username, accounts: value.accounts, total: value.total });
			}); 
		}
	});
}

var message_size = 0;
function loadMessage() {
	var sel = w2ui['grid_messaging_account'].getSelection();
	if(sel != '') {
		var record = w2ui['grid_messaging_account'].get(sel[0]);
		
		$.ajax({
			type: "GET",
			url:  'Legissoft/messaging/messaging.content.php?user2=' + record['username'],
			global: false,
			success: function(data){
				w2ui['layout_messaging_content'].content('main', data);
				
				var el = w2ui['layout_messaging_content'].el('main');
				var size = (w2ui['layout_messaging_content'].get('main'));

				if(message_size != el.childNodes.length) {
					message_size = el.childNodes.length;
					$(el).scrollTop(el.scrollHeight);
				}
				
			}
		});
	}
}
function sendMessage() {
	var sel     = w2ui['grid_messaging_account'].getSelection();
	var record  = w2ui['grid_messaging_account'].get(sel[0]);
	var message = $('textarea[name=message]').val().trim();
	
	if(message != '') {
		$.ajax({
			type: "POST",
			url:  'Legissoft/messaging/messaging.send.php',
			data: { 
				user2   : record['username'],
				message : message
			},
			success: function(data){
				w2ui['layout_messaging_content'].load('main', 'Legissoft/messaging/messaging.content.php?user2=' + record['username'], null,
				function() {
					var el = w2ui['layout_messaging_content'].el('main');
					$(el).scrollTop(el.scrollHeight);
					$('textarea[name=message]').val('')
				});
				
			}
		});
	}
}

function getAnnouncement() {
	$.ajax({
		url  : 'Legissoft/messaging/json.messaging.announcement.get.php',
		type : 'GET',
		success: function(result) {
			$('#writer_announcement_members').val(result.data['members_alert_message']);
			$('#writer_announcement_audience').val(result.data['audience_alert_message']);
			
			if(result.data['members_alert_message_show'] == '0') {
				$("button[name=button_announcement_show_to_members]").removeClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_members]").text('Announce To Members');
				
				$('input[name=content_members_show]').val(1);
			} else {
				$("button[name=button_announcement_show_to_members]").addClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_members]").text('Hide From Members');
				
				$('input[name=content_members_show]').val(0);
			}
			
			if(result.data['audience_alert_message_show'] == '0') {
				$("button[name=button_announcement_show_to_audience]").removeClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_audience]").text('Announce To Audience');
				
				$('input[name=content_audience_show]').val(1);
			} else {
				$("button[name=button_announcement_show_to_audience]").addClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_audience]").text('Hide From Audience');
				
				$('input[name=content_audience_show]').val(0);
			}
		}
	});
}

function saveAnnouncementMembers() {
	var content = tinyMCE.get('writer_announcement_members').getContent();

	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_members : content
		},
		success: function(result) {
			updateMembersScreenAnnouncement();
			ohSnap('Announcement Saved!', {'color':'green', 'duration':'1000'});
		}
	});
}

function saveAnnouncementAudience() {
	var content = tinyMCE.get('writer_announcement_audience').getContent();

	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_audience : content
		},
		success: function(result) {
			updateAudienceScreenAnnouncement();	
			ohSnap('Announcement Saved!', {'color':'green', 'duration':'1000'});
		}
	});
}


function setMembersAnnouncement() {
	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_members_show : $('input[name=content_members_show]').val()
		},
		success: function(result) {
			if(result.data['members_alert_message_show'] == '0') {
				$("button[name=button_announcement_show_to_members]").removeClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_members]").text('Announce To Members');
				
				$('input[name=content_members_show]').val(1)
			} else {
				$("button[name=button_announcement_show_to_members]").addClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_members]").text('Hide From Members');
				
				$('input[name=content_members_show]').val(0)
			}
			
			updateMembersScreenAnnouncement();
		}
	});
}

var content_audience_show = 1;
function setAudienceAnnouncement() {
	$.ajax({
		url:'Legissoft/messaging/messaging.announcement.save.php',
		type:'POST',
		data: { 
			content_audience_show : $('input[name=content_audience_show]').val()
		},
		success: function(result) {
			if(result.data['audience_alert_message_show'] == '0') {
				$("button[name=button_announcement_show_to_audience]").removeClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_audience]").text('Announce To Audience');
				
				$('input[name=content_audience_show]').val(1)
			} else {
				$("button[name=button_announcement_show_to_audience]").addClass('w2ui-btn-green');
				$("button[name=button_announcement_show_to_audience]").text('Hide From Audience');
				
				$('input[name=content_audience_show]').val(0)
			}

			updateAudienceScreenAnnouncement();
		}
	});
}

function deleteMessage(code) {
	if(code != '') {
		w2confirm('Do you want to delete?').yes(
			function () { 
				$.ajax({
					url:'Legissoft/messaging/messaging.send.php',
					type:'POST',
					data: { 
						delete_code : code
					},
					success: function(result) {
						$('#chat_message_' + code).fadeOut();
					}
				});
			}
		);
	}
}

function deleteChat() {
	var sel     = w2ui['grid_messaging_account'].getSelection();
	var record  = w2ui['grid_messaging_account'].get(sel[0]);
	
	if(record['username'] != '') {
		$.ajax({
			type: "POST",
			url:  'Legissoft/messaging/messaging.send.php',
			data: { 
				delete_chat_user2 : record['username']
			},
			success: function(data){
				w2ui['layout_messaging_content'].content('main', '');			
			}
		});
	}
}

function updateMembersScreenAnnouncement() {
	$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
}

function updateAudienceScreenAnnouncement() {
	$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
}


</script>
