<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

if(isset($_POST['content_members'])) {

	$content_members = trim($_POST['content_members']);

	$stmt = $conn->prepare("UPDATE config SET members_alert_message = :members_alert_message, members_alert_message_date = NOW()");
	$stmt->bindParam(':members_alert_message', $content_members, PDO::PARAM_STR);
	$stmt->execute();
	
	echo json_encode(array('data' => $rows));
	
} else if(isset($_POST['content_members_show']) && trim($_POST['content_members_show']) != "") {
	$content_members_show = trim($_POST['content_members_show']);

	$stmt = $conn->prepare("UPDATE config SET members_alert_message_show = :members_alert_message_show, members_alert_message_date = NOW()");
	$stmt->bindParam(':members_alert_message_show', $content_members_show, PDO::PARAM_STR);
	$stmt->execute();
	
	$rows['members_alert_message_show'] = $content_members_show;

	echo json_encode(array('data' => $rows));
	
} else if(isset($_POST['content_audience'])) {

	$content_audience = trim($_POST['content_audience']);

	$stmt = $conn->prepare("UPDATE config SET audience_alert_message = :audience_alert_message, audience_alert_message_date = NOW()");
	$stmt->bindParam(':audience_alert_message', $content_audience, PDO::PARAM_STR);
	$stmt->execute();
	
	echo json_encode(array('data' => $rows));
	
} else if(isset($_POST['content_audience_show']) && trim($_POST['content_audience_show']) != "") {
	$content_audience_show = trim($_POST['content_audience_show']);

	$stmt = $conn->prepare("UPDATE config SET audience_alert_message_show = :audience_alert_message_show, audience_alert_message_date = NOW()");
	$stmt->bindParam(':audience_alert_message_show', $content_audience_show, PDO::PARAM_STR);
	$stmt->execute();
	
	$rows['audience_alert_message_show'] = $content_audience_show;

	echo json_encode(array('data' => $rows));
}
?>