<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$user = $GENERAL_FUNCTIONS->getSessionVar('username');

if(isset($_POST['user2']) && trim($_POST['user2']) != "" && trim($_POST['message']) != "") {

	$user2   = $GENERAL_FUNCTIONS->filterInput($_POST['user2']);
	$message = $GENERAL_FUNCTIONS->filterInput($_POST['message']);
	
	$stmt = $conn->prepare(
		"INSERT INTO chat SET posted_by = :posted_by, message_date = NOW(), message_from = :message_from, message_to = :message_to, message = :message, display_to = :user; " .
		"INSERT INTO chat SET posted_by = :posted_by, message_date = NOW(), message_from = :message_from, message_to = :message_to, message = :message, display_to = :user2"
	);
	
	$stmt->bindParam(':posted_by',    $user,    PDO::PARAM_STR);
	$stmt->bindParam(':message_from', $user,    PDO::PARAM_STR);
	$stmt->bindParam(':message_to',   $user2,   PDO::PARAM_STR);
	$stmt->bindParam(':message',      $message, PDO::PARAM_STR);
	$stmt->bindParam(':user',         $user,    PDO::PARAM_STR);
	$stmt->bindParam(':user2',        $user2,   PDO::PARAM_STR);
	$stmt->execute();

} else if(isset($_POST['delete_code']) && trim($_POST['delete_code']) != "") {
	$row_id = $GENERAL_FUNCTIONS->filterInput($_POST['delete_code']);
	
	$stmt = $conn->prepare("UPDATE chat SET deleted = '1', updated_by = :updated_by, updated_date = NOW() WHERE row_id = :row_id");
	$stmt->bindParam(':row_id',     $row_id, PDO::PARAM_STR);
	$stmt->bindParam(':updated_by', $user,   PDO::PARAM_STR);
	$stmt->execute();

} else if(isset($_POST['delete_chat_user2']) && trim($_POST['delete_chat_user2']) != "") {
	$user2 = $GENERAL_FUNCTIONS->filterInput($_POST['delete_chat_user2']);
	$message_from_to = $user . $user2;
	$message_to_from = $user2 . $user;
	
	$stmt = $conn->prepare("UPDATE chat SET deleted = '1', updated_by = :updated_by, updated_date = NOW() WHERE CONCAT(message_from, message_to) IN (:message_from_to, :message_to_from) AND display_to = :display_to");
	$stmt->bindParam(':display_to',      $user,            PDO::PARAM_STR);
	$stmt->bindParam(':message_from_to', $message_from_to, PDO::PARAM_STR);
	$stmt->bindParam(':message_to_from', $message_to_from, PDO::PARAM_STR);
	$stmt->bindParam(':updated_by',      $user,            PDO::PARAM_STR);
	$stmt->execute();

}
?>