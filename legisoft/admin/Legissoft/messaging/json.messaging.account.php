<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$stmt = $conn->prepare("SELECT user_id, username, fname, lname, image_file, title FROM _user WHERE username <> :username AND type <> '3' AND deleted = '0' ORDER BY FIELD(type, '1', '2'), lname, fname");
$stmt->bindValue(':username', $GENERAL_FUNCTIONS->getSessionVar('username'));
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$accounts = '<div class=\"messaging\">' . 
						'<img src=\"Legissoft/user_image/thumbnail/' . $row['image_file'] . '\" onerror=\"if (this.src != \'framework/images/error.png\') this.src = \'framework/images/error.png\';\" />' . 
						'<div class=\"detail\"><div>' . htmlspecialchars($row['lname'] . ", " . $row['fname']) . '</div><div><i>' .  htmlspecialchars($row['title']) . '</i></div></div>' .
					'</div>';
		$cnt++; if($cnt > 1) { echo ","; }
		echo '{ 
			"recid"      : "' . $row['user_id'] . '", 
			"username"   : "' . $row['username'] . '", 
			"accounts"   : "' . $accounts . '" ,
			"total"   : "<span class=\"messaging_total_count\" id=\"messaging_total_count_' . $row['username'] . '\">0</span>" 
			
		}';

   }

echo ']}';

?>
