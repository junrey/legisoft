<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

$stmt = $conn->prepare("SELECT members_alert_message, members_alert_message_show, audience_alert_message, audience_alert_message_show FROM config");
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
$rows['members_alert_message']       = $row['members_alert_message'];
$rows['members_alert_message_show']  = $row['members_alert_message_show'];
$rows['audience_alert_message']      = $row['audience_alert_message'];
$rows['audience_alert_message_show'] = $row['audience_alert_message_show'];

echo json_encode(array('data' => $rows));
?>
