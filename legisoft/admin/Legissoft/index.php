<?php
	require_once("database/pdo.mysql.connection.legissoft.php");
	$stmt = $conn->prepare("SELECT show_action_writer, show_voting, show_image_gallery FROM config");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
?>

<div id="tabs_main" style="width: 100%;"></div>
<div id="tab_members_screen"  class="tabs_main" style="width: 100%;"></div>
<div id="tab_audience_screen" class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_document_writer" class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_action_writer"   class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_voting"          class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_account_members" class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_config"          class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_image_gallery"   class="tabs_main" style="width: 100%; display: none;"></div>
<div id="tab_messaging"       class="tabs_main" style="width: 100%; display: none;"></div>


<script>
$(function () {
	var load_audience_once = 0;
	var load_writer_once   = 0;
	var load_action_once   = 0;
	var load_voting_once   = 0;
	var load_members_once  = 0;
	var load_config_once   = 0;
	var load_image_once    = 0;
	var load_messaging     = 0;
	
	$().w2destroy('tabs_main');
    $('#tabs_main').w2tabs({
        name: 'tabs_main',
        active: 'tab_members_screen',
        tabs: [
			{ id: 'tab_members_screen',  text: 'Members Screen' },
            { id: 'tab_audience_screen', text: 'Audience Screen' },
            { id: 'tab_document_writer', text: 'Document Writer' },
			{ id: 'tab_action_writer',   text: 'Action Writer' <?php if($row['show_action_writer'] == 0) {?>, hidden: true <?php } ?> },
			{ id: 'tab_voting',          text: 'Voting'        <?php if($row['show_voting'] == 0) {?>, hidden: true <?php } ?> },
			{ id: 'tab_image_gallery',   text: 'Image Gallery' <?php if($row['show_image_gallery'] == 0) {?>, hidden: true <?php } ?> },
			{ id: 'tab_messaging',       text: 'Messaging' },
			{ id: 'tab_account_members', text: 'User Account' },
			{ id: 'tab_config',          text: 'Configuration', hidden: true },
        ],
        onClick: function (event) {
			w2uiElementResize();
			
			if(event.target == 'tab_members_screen') {
				$('.tabs_main').hide();
				$('#tab_members_screen').show();
				
			} else if(event.target == 'tab_audience_screen') {
           		$('.tabs_main').hide();
				
				if(load_audience_once == 0) {
					$('#tab_audience_screen').load('Legissoft/document/document.audience.screen.php');
					load_audience_once = 1;
				}
				
				$('#tab_audience_screen').show();
			
			} else if(event.target == 'tab_document_writer') {
				$('.tabs_main').hide();
				
				if(load_writer_once == 0) {
					$('#tab_document_writer').load('Legissoft/document/document.index.php');
					load_writer_once = 1;
				}
				
				$('#tab_document_writer').show();
				
			} else if(event.target == 'tab_action_writer') {
				$('.tabs_main').hide();
				
				if(load_action_once == 0) {
					$('#tab_action_writer').load('Legissoft/action/action.index.php', function() {});
					load_action_once = 1;
				}
				
				$('#tab_action_writer').show();
				
			} else if(event.target == 'tab_voting') {
				$('.tabs_main').hide();
				
				if(load_voting_once == 0) {
					$('#tab_voting').load('Legissoft/voting/voting.index.php', function() {});
					load_voting_once = 1;
				}
				
				$('#tab_voting').show();
				
			} else if(event.target == 'tab_account_members') {
				$('.tabs_main').hide();
				
				if(load_members_once == 0) {
					$('#tab_account_members').load('Legissoft/account/members/account.members.index.php', function() {});
					load_members_once = 1;
				}
				
				$('#tab_account_members').show();
				
			} else if(event.target == 'tab_config') {
				$('.tabs_main').hide();
				
				if(load_config_once == 0) {
					$('#tab_config').load('Legissoft/config/config.index.php', function() {});
					load_config_once = 1;
				}
				
				$('#tab_config').show();
				
			} else if(event.target == 'tab_image_gallery') {
				$('.tabs_main').hide();
				
				if(load_image_once == 0) {
					$('#tab_image_gallery').load('Legissoft/gallery/gallery.image.index.php', function() {});
					load_image_once = 1;
				}
				
				$('#tab_image_gallery').show();
				
			}  else if(event.target == 'tab_messaging') {
				$('.tabs_main').hide();
				
				if(load_messaging == 0) {
					$('#tab_messaging').load('Legissoft/messaging/messaging.index.php', function() {});
					load_messaging = 1;
				}
				
				$('#tab_messaging').show();
			}

			// reset grid
			if (w2ui['grid_messaging_account'] != null) { 
				w2ui['grid_messaging_account'].selectNone();
			}
			
        }
    });
	
	$('#tab_members_screen').load('Legissoft/document/document.members.screen.php');
	
	$(document).ready(function(){
		$(window).resize(function(){
			w2uiElementResize();
		});
	}); 
});

function w2uiElementResize() {
	for (var item in w2ui){
		if (w2ui[item].resize){
			w2ui[item].resize();
		}
	}
}
</script>