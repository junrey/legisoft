<div id="layout_action" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>

"use strict";

$().w2layout({
	name: 'layout_action_main',
	padding: 4,
	panels: [
		{ type: 'main', style: 'border: 1px solid #dfdfdf;' },
		{ type: 'right', size: 300, resizable: true,
			tabs: {
				active: 'tab_committee',
				style: 'background-color:#fff;',
				tabs: [
					{ id: 'tab_committee', caption: 'Committee' },
					{ id: 'tab_movant', caption: 'Movant' },
					{ id: 'tab_second', caption: 'Second' }
				],
				onClick: function (id, data) { 
					if(id == 'tab_committee') {
						w2ui['layout_action_main'].content('right', w2ui['grid_committee']);
						
						if($("input[name=action_code]").val() != '') {
							setGridCommittee();
						}
					
					} else if(id == 'tab_movant') {
						w2ui['layout_action_main'].content('right', w2ui['grid_movant']);
						
						if($("input[name=action_code]").val() != '') {
							setGridMovant();
						}
						
					} else if(id == 'tab_second') {
						w2ui['layout_action_main'].content('right', w2ui['grid_second']);
						
						if($("input[name=action_code]").val() != '') {
							setGridSecond();
						}
						
					}
				}
			}
		},
		{ type: 'preview', style: 'background-color:#fff;', size: 200, resizable: true }
	]
});

$().w2layout({
	name: 'layout_action_main_assign',
	padding: 4,
	panels: [
		{ type: 'left',  style: 'border: 1px solid #dfdfdf;', size: '33%', resizable: true },
		{ type: 'main',  style: 'border: 1px solid #dfdfdf;', size: '33%', resizable: true },
		{ type: 'right', style: 'border: 0px solid #dfdfdf;', size: '33%', resizable: true }
	]
});

$().w2grid({
	name : 'grid_committee',
	//url  : 'Legissoft/action/json.action.committee.php',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
	},
	toolbar: {
		items: [
			{ type: 'button', id: 'assign_committee', caption: 'Assign', icon: 'w2ui-icon-check',  disabled: true }
		],
		onClick: function (target, data) {
			if (target == 'assign_committee') {
				setGridCommittee();
			}
		}
	},
	multiSearch: false,
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'detail', size: '10%', caption: 'Committee' }
	],
	onSelect: function(event) {
		w2ui['grid_committee'].toolbar.enable('assign_committee');
	},
	onUnselect: function(event) {
		w2ui['grid_committee'].toolbar.disable('assign_committee');
	}, 
	searches : [
		{ field: 'detail', caption: 'Committee', operator : 'contains', type: 'text' }
	]   
});

$().w2grid({
	name : 'grid_movant',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
	},
	toolbar: {
		items: [
			{ type: 'button', id: 'assign_movant', caption: 'Assign', icon: 'w2ui-icon-check',  disabled: true }
		],
		onClick: function (target, data) {
			if (target == 'assign_movant') {
				setGridMovant();
			}
		}
	},
	multiSearch: false,
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'fullname', size: '10%', caption: 'Movant' }
	],
	onSelect: function(event) {
		w2ui['grid_movant'].toolbar.enable('assign_movant');
	},
	onUnselect: function(event) {
		w2ui['grid_movant'].toolbar.disable('assign_movant');
	}, 
	searches : [
		{ field: 'fullname', caption: 'Movant', operator : 'contains', type: 'text' }
	]   
});

$().w2grid({
	name : 'grid_second',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
	},
	toolbar: {
		items: [
			{ type: 'button', id: 'assign_second', caption: 'Assign', icon: 'w2ui-icon-check',  disabled: true }
		],
		onClick: function (target, data) {
			if (target == 'assign_second') {
				setGridSecond();
			}
		}
	},
	multiSearch: false,
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'fullname', size: '10%', caption: 'Second' }
	],
	onSelect: function(event) {
		w2ui['grid_second'].toolbar.enable('assign_second');
	},
	onUnselect: function(event) {
		w2ui['grid_second'].toolbar.disable('assign_second');
	}, 
	searches : [
		{ field: 'fullname', caption: 'Second', operator : 'contains', type: 'text' }
	]   
});


// assigned

$().w2grid({ 
	name : 'grid_committee_assign',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
		toolbarSearch  : false,
		toolbarInput   : false,
		searchAll      : false,
		toolbarReload  : false,
		toolbarDelete  : true
	},
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'detail', size: '10%', caption: 'Assiged Committee' }
	],
	buttons: {
		'delete': {
			text: 'Remove',
			tooltip: 'Remove selected records'
		}
	},
	onDelete: function (event) {
		event.preventDefault();
		setGridCommitteeAssigned();
	}
});

$().w2grid({
	name : 'grid_movant_assign',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
		toolbarSearch  : false,
		toolbarInput   : false,
		searchAll      : false,
		toolbarReload  : false,
		toolbarDelete  : true
	},
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'fullname', size: '10%', caption: 'Assiged Movant' }
	],
	buttons: {
		'delete': {
			text: 'Remove',
			tooltip: 'Remove selected records'
		}
	},
	onDelete: function (event) {
		event.preventDefault();
		setGridMovantAssigned();
	}
});

$().w2grid({
	name : 'grid_second_assign',
	show : {
		toolbar        : true,
		selectColumn   : true,
		multiSelect    : true,
		toolbarColumns : false,
		lineNumbers    : true,
		toolbarSearch  : false,
		toolbarInput   : false,
		searchAll      : false,
		toolbarReload  : false,
		toolbarDelete  : true
	},
	columns: [
		{ field: 'code', hidden: true },
		{ field: 'fullname', size: '10%', caption: 'Assiged Second' }
	],
	buttons: {
		'delete': {
			text: 'Remove',
			tooltip: 'Remove selected records'
		}
	},
	onDelete: function (event) {
		event.preventDefault();
		setGridSecondAssigned();
	}
});

$(function () {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	$().w2destroy('layout_action');
    $('#layout_action').w2layout({
        name: 'layout_action',
		padding: 4,
        panels: [
			{ type: 'top', size: 35, overflow: 'hidden',
				toolbar: {
					items: [
						{ type: 'button', id: 'search_action', text: 'Search', icon: 'fa fa-search' },
						{ type: 'button', id: 'new_action', text: 'New', img: 'icon-page' },
						{ type: 'break' },
						{ type: 'html', id: 'toolbar_action_members', html: '<button name="button_action_show_to_members" onclick="setMembersAction();" class="w2ui-btn" style="width:135px;" disabled>Show To Members</button>'},
						{ type: 'html', id: 'toolbar_action_audience', html: '<button name="button_action_show_to_audience" onclick="setAudienceAction();" class="w2ui-btn" style="width:135px;" disabled>Show To Audience</button>'},
						{ type: 'break' },
						{ type: 'html',  id: 'action_toolbar',
							html: function (item) {
								var html =
									'<div>' +
									'<input type="hidden" name="action_status">' +
									'<input type="hidden" name="action_code">' +
									'<span>Title: </span>' +
									'<input name="action_title" style="width:180px;" class="w2ui-input" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Reference: </span>' +
									'<select name="action_reference" class="w2ui-input" style="width:150px;" disabled></select>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Date: </span>' +
									'<input name="action_date" type="us-date" style="width: 80px;" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<button name="action_save" onclick="saveAction();" class="w2ui-btn w2ui-btn-blue" style="min-width:60px;" disabled>Save</button>' +
									'<button name="action_delete" onclick="deleteAction();" class="w2ui-btn w2ui-btn-red" style="min-width:60px;" disabled>Delete</button>' +
									'</div>';
								return html;
							}
						},
						{ type: 'spacer' },
						{ type: 'html',  id: 'action_status', html: '<div id="action_status_msg"></div>' }
						
					],
					onClick: function (event) {
						if(event.target == 'new_action') {
							formNewAction();
						} else if(event.target == 'search_action') {
							w2popup.open({
								title   : 'Search Action',
								width   : 1024,
								height  : 600,
								showMax : true,
								body    : '<div id="layout_search_action" style="position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;"></div>',
								onOpen  : function (event) {
									event.onComplete = function () {
										$().w2destroy('layout_search_action');
										$().w2destroy('sidebar_search_action');
										$('#layout_search_action').w2layout({
											name: 'layout_search_action',
											padding: 4,
											panels: [
												{ type: 'left', size: 300, style: 'border: 1px solid #dfdfdf;', resizable: true, content:
													$().w2sidebar({
														name : 'sidebar_search_action',
														nodes: [],
														onClick: function(event) {
															var file_id = (event.target).substr(1);
															
															if(file_id.search('JDX') != -1) {
																getAction(file_id);
															}
														}
													})
												},
												{ type: 'main', overflow: 'hidden'}
											]
										});
										
										$().w2destroy('grid_search_action');
										w2ui['layout_search_action'].content('main', 
											$().w2grid({ 
												name   : 'grid_search_action', 
												url    : 'Legissoft/action/json.action.search.php',
												method : 'GET', // need this to avoid 412 error on Safari
												show   : {
													toolbar        : true,
													lineNumbers    : true,
													toolbarColumns : false,
													toolbarEdit    : true
												},
												multiSearch: false,
												searches: [
													{ field: 'search', caption: 'Search here...', type: 'text' }
												],
												columns: [                
													{ field: 'title',        caption: 'Title',         size: '30%' },
													{ field: 'status',       caption: 'Status',        size: '15%' },
													{ field: 'reference',    caption: 'Reference',     size: '15%' },
													{ field: 'created_by',   caption: 'Created By',    size: '13%' },
													{ field: 'created_date', caption: 'Date Created',  size: '12%' },
													{ field: 'mod_by',       caption: 'Modified By',   size: '13%' },
													{ field: 'mod_date',     caption: 'Last Modified', size: '12%' },
												],
												onEdit: function (event) {
													var sel = w2ui['grid_search_action'].getSelection();
													getAction(sel[0]);
													w2popup.close();
												},
											}) 
										);
										
										$.ajax({
											url  : 'Legissoft/action/html.action.archive.php',
											type : 'GET',
											success: function(result) {
												var nd = []; 
												for (var i in w2ui['sidebar_search_action'].nodes) nd.push(w2ui['sidebar_search_action'].nodes[i].id);
												w2ui['sidebar_search_action'].remove.apply(w2ui['sidebar_search_action'], nd);
									
												w2ui['sidebar_search_action'].add(eval(result));
											}
										});
									};
								},
								onToggle: function (event) { 
									event.onComplete = function () {
										w2ui['panel_search_action'].resize();
									}
								}
							});
						}
					}
				}
			},
			{ type: 'left', size: 220, style: 'background-color:#fff;', resizable: true },
            { type: 'main', style: 'background-color: #fff;', overflow: 'hidden' }
        ]
		
    });
	
	$('input[type=us-date]').w2field('date');
	
	w2ui['layout_action'].content('left', 
		$().w2layout({
			name: 'layout_action_archive',
			padding: 4,
			panels: [
				{ type: 'main', style: 'border: 1px solid #dfdfdf;', content:
					$().w2sidebar({
						name : 'sidebar_action',
						nodes: [],
						onFlat: function (event) {
							if(event.goFlat) {
								w2ui['layout_action'].set('left', { size: 50 });
							} else {
								w2ui['layout_action'].set('left', { size: 250 });
							}
						},
						onClick: function(event) {
							var file_id = (event.target).substr(1);
							
							if(file_id.search('JDX') != -1) {
								getAction(file_id);
								do_not_call_screen_on_startup = 0;
							}
						}
					})
				},
				{ type: 'bottom', size: '50%', resizable: true, content: 
					$().w2grid({ 
						name : 'grid_action_members',
						show : {
							toolbar      : true,
							toolbarColumns : false,
						},
						multiSearch: false,
						columns: [
							{ field: 'code', hidden: true },
							{ field: 'fullname', size: '77%', caption: 'Members' },
							{ field: 'button', size: '23%', caption: 'Show' }
						],
						searches : [
							{ field: 'fullname', caption: 'Members', operator : 'contains', type: 'text' }
						]   
					})
				}
			]
		})
	);
	
	w2ui['layout_action'].content('main', w2ui['layout_action_main']);
	w2ui['layout_action_main'].content('right', w2ui['grid_committee']);
	w2ui['layout_action_main'].content('preview', w2ui['layout_action_main_assign']);
	w2ui['layout_action_main_assign'].content('left', w2ui['grid_committee_assign']);
	w2ui['layout_action_main_assign'].content('main', w2ui['grid_movant_assign']);
	w2ui['layout_action_main_assign'].content('right', w2ui['grid_second_assign']);
	
	getArchiveAction('');

});

function getArchiveAction(selected) {
	$.ajax({
		url  : 'Legissoft/action/html.action.archive.php',
		type : 'GET',
		success: function(result) {
			var nd = []; 
			for (var i in w2ui['sidebar_action'].nodes) nd.push(w2ui['sidebar_action'].nodes[i].id);
			w2ui['sidebar_action'].remove.apply(w2ui['sidebar_action'], nd);

			w2ui['sidebar_action'].add(eval(result));
			
			//if(selected != '') {
			//	w2ui['sidebar_action'].select(selected);
			//}
		}
	});
}

function setGridCommittee() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url:'Legissoft/action/json.action.committee.php',
			type:'POST',
			dataType: 'json',
			data: { 
				code      : $("input[name=action_code]").val(),
				committee : w2ui['grid_committee'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_committee'].toolbar.disable('assign_committee');
				w2ui['grid_committee'].clear();
				
				$.each(json.records, function(i, value) {
					w2ui['grid_committee'].add({ recid: value.recid, code: value.code, detail: value.detail });
				});
				
				w2ui['grid_committee_assign'].load('Legissoft/action/json.action.committee.assigned.php?code=' + $("input[name=action_code]").val());
			
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	} else {
		  w2alert('Please save you action first!')
	}
}
function setGridMovant() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.movant.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code   : $("input[name=action_code]").val(),
				movant : w2ui['grid_movant'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_movant'].toolbar.disable('assign_movant');
				w2ui['grid_movant'].clear();
				
				$.each(json.records, function(i, value) { 
					w2ui['grid_movant'].add({ recid: value.recid, code: value.code, fullname: value.fullname });
				});
				
				w2ui['grid_movant_assign'].load('Legissoft/action/json.action.movant.assigned.php?code=' + $("input[name=action_code]").val());
			
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	} else {
		  w2alert('Please save you action first!')
	}
}
function setGridSecond() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.second.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code   : $("input[name=action_code]").val(),
				second : w2ui['grid_second'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_second'].toolbar.disable('assign_second');
				w2ui['grid_second'].clear();
				
				$.each(json.records, function(i, value) { 
					w2ui['grid_second'].add({ recid: value.recid, code: value.code, fullname: value.fullname });
				});
				
				w2ui['grid_second_assign'].load('Legissoft/action/json.action.second.assigned.php?code=' + $("input[name=action_code]").val());
			
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	} else {
		  w2alert('Please save you action first!')
	}
}
function setGridCommitteeAssigned() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.committee.assigned.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code      : $("input[name=action_code]").val(),
				committee : w2ui['grid_committee_assign'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_committee_assign'].clear();
				
				$.each(json.records, function(i, value) {
					w2ui['grid_committee_assign'].add({ recid: value.recid, code: value.code, detail: value.detail });
				});
				
				w2ui['grid_committee'].load('Legissoft/action/json.action.committee.php?code=' + $("input[name=action_code]").val());
				
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	
	} else {
		  w2alert('Please save you action first!')
	}
}

function setGridMovantAssigned() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.movant.assigned.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code   : $("input[name=action_code]").val(),
				movant : w2ui['grid_movant_assign'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_movant_assign'].clear();
				
				$.each(json.records, function(i, value) {
					w2ui['grid_movant_assign'].add({ recid: value.recid, code: value.code, fullname: value.fullname });
				});
				
				w2ui['grid_movant'].load('Legissoft/action/json.action.movant.php?code=' + $("input[name=action_code]").val());
				
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	
	} else {
		  w2alert('Please save you action first!')
	}
}

function setGridSecondAssigned() {
	if($("input[name=action_code]").val() != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.second.assigned.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code   : $("input[name=action_code]").val(),
				second : w2ui['grid_second_assign'].getSelection()
			},
			success: function( json ) {
				w2ui['grid_second_assign'].clear();
				
				$.each(json.records, function(i, value) {
					w2ui['grid_second_assign'].add({ recid: value.recid, code: value.code, fullname: value.fullname });
				});
				
				w2ui['grid_second'].load('Legissoft/action/json.action.second.php?code=' + $("input[name=action_code]").val());
				
				if(do_not_call_screen_on_startup >= 3) {
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
				do_not_call_screen_on_startup++;
			}
		});
	
	} else {
		  w2alert('Please save you action first!')
	}
}

function formNewAction() {
	openPopupNewAction();
	$().w2popup('open', {
		title   : 'New Action',
		body    : '<div id="popup_form_new_action" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 500,
		height  : 250, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_new_action').w2render('form_new_action');
			}
		},
		onClose: function () {
			w2ui['form_new_action'].clear();
		}
	});
}
function openPopupNewAction() {
	$().w2destroy('form_new_action');
	$().w2form({
		name  : 'form_new_action',
		style : 'border: 0px; background-color: transparent;',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Title:</label>' +
			'        <div>' +
			'           <input name="pre_action_title" type="text" maxlength="200" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Reference:</label>' +
			'        <div>' +
			'            <input name="pre_action_reference" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'	<div class="w2ui-field">' +
			'		<label>Date:</label>' +
			'		<div><input name="pre_action_date" type="us-date" style="width: 250px" ></div>' +
			'	</div>' +
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Clear</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Create</button>' +
			'</div>',
		fields: [
			{ field: 'pre_action_title', type: 'text', required: true },
			{ field: 'pre_action_reference', type: 'list', required: true,
				options: { 
					url:     'Legissoft/action_reference/json.action.reference.php', 
					recId:   'recid',
					recText: 'detail', 
					minLength: 0
				} 
			},
			{ field: 'pre_action_date', type: 'date', required: true }
		],
		actions: {
			"save": function() { 
				var val = this.validate();
				if(val.length == 0){ 
					newAction();
					w2popup.close();
				}
			},
			"reset": function() { this.clear(); }
		}
	});
	
	w2ui['form_new_action'].record['pre_action_date'] = new Date(Date.now()).toLocaleDateString();
	w2ui['form_new_action'].refresh(); 
}

function newAction() {	
	w2ui['layout_action_main'].content('main', 
		'<div id="panel_status" style="padding:10px 5px;"></div>' +
		'<textarea id="writer_editor" class="writer_editor" style="width: calc(100% - 2px); height: calc(100% - 106px);"></textarea>'
	);
	
	tinyMCE.remove('.writer_editor');
	tinymce.init({
		selector : '.writer_editor',
		plugins  : 'autoresize print textcolor colorpicker lists table paste searchreplace image media imagetools responsivefilemanager',
		toolbar1 : 'toolbar_custom_new_action print | undo redo | cut copy paste | formatselect fontselect fontsizeselect | bold italic underline strikethrough',
		toolbar2 : 'forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat  subscript superscript | image media responsivefilemanager | table | searchreplace',
		font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
		fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
		menu: {
			file   : {title: 'File', items: 'menu_custom_new print'},
			edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
			format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		statusbar : false,
		resize    : false,
		branding  : false,
		height    : $('.writer_editor').height() + 6,
		autoresize_max_height   : $('.writer_editor').height() + 6,
		theme_advanced_resizing : true,
		theme_advanced_resizing_use_cookie : false,
		setup: function (editor) {
			editor.addMenuItem('menu_custom_new_action', {
				text: 'New Action',
				icon: 'newdocument',
				onclick: function() {
					formNewAction();
				}
			});
			editor.addButton('toolbar_custom_new_action', {
				text: false,
				icon: 'newdocument',
				tooltip: 'New Action',
				onclick: function () {
					formNewAction();
				}
			});
			editor.on('init', function() {
				this.execCommand("fontName", false, "times new roman");
				this.execCommand("fontSize", false, "12pt");
			});
		},
		toolbar_items_size : 'small',
		menubar : false,
		
		image_advtab: true ,
		external_filemanager_path:"/legisoft/admin/framework/filemanager/",
		filemanager_title:"File Manager" ,
		external_plugins: { "filemanager" : "/legisoft/admin/framework/filemanager/plugin.min.js"},
		relative_urls: false
	});
	
	$("input[name=action_title]").attr("disabled", false);
	$("select[name=action_reference").attr("disabled", false);
	$("input[name=action_date]").attr("disabled", false);
	$("button[name=action_save]").attr("disabled", false);
	
	getActionReference($("input[name=pre_action_reference]").data('selected').recid);	
	
	$("input[name=action_title]").val($("input[name=pre_action_title]").val());
	$("input[name=action_date]").val($("input[name=pre_action_date]").val());
	$('#action_status_msg').html('<div style="background-color:#BB3C3E; color:#fff; padding:4px 5px;"><i class="fa fa-exclamation-triangle"></i> Unsave!</div>');

	getActionItem('0');
	
	w2ui['grid_committee'].clear();
	w2ui['grid_movant'].clear();
	w2ui['grid_second'].clear();
	w2ui['grid_committee_assign'].clear();
	w2ui['grid_movant_assign'].clear();
	w2ui['grid_second_assign'].clear();
	
	$("input[name=action_code]").val('');
	
	$("input[name=action_code]").val('');
	$("button[name=button_action_show_to_members]").attr("disabled", true).removeClass('w2ui-btn-green').text('Show To Members');;
	$("button[name=button_action_show_to_audience]").attr("disabled", true).removeClass('w2ui-btn-green').text('Show To Audience');;
	$("button[name=action_delete]").attr("disabled", true);
	
	$("input[name=action_title]").removeClass('w2ui-error');
	$("input[name=action_date]").removeClass('w2ui-error');
	
	w2ui['grid_action_members'].clear();
	
}

localStorage.removeItem('action_item');
function getActionItem(status) {
	if(localStorage.getItem('action_item') === null) {
		$.ajax({
			url:'Legissoft/action_item/json.action.item.php',
			type:'GET',
			dataType: 'json',
			success: function( json ) {
				$.each(json.records, function(i, value) {
					$('#panel_status').append(
						'<button id="' + value.recid + '" class="button_action_status w2ui-btn" onclick="setActionStatus(\'' + value.recid + '\');" disabled>' + value.detail + '</button>'
					);
				});
				
				// gin zero ko kay para biskan blank or wala status enable gihapon sa edit
				if(status != '0') {
					$(".button_action_status").attr("disabled", false);
					$('.button_action_status').removeClass('w2ui-btn-green');
					
					if(status != '') {
						$('#' + status).addClass('w2ui-btn-green');
					}
				}
				
				localStorage.setItem('action_item', JSON.stringify(json));
			}
		});
	} else {
		var action_item = JSON.parse(localStorage.getItem('action_item'));
		
		$.each(action_item.records, function(i, value) {
			$('#panel_status').append(
				'<button id="' + value.recid + '" class="button_action_status w2ui-btn" onclick="setActionStatus(\'' + value.recid + '\');" disabled>' + value.detail + '</button>'
			);
		});
		
		// gin zero ko kay para biskan blank or wala status enable gihapon sa edit
		if(status != '0') {
			$(".button_action_status").attr("disabled", false);
			$('.button_action_status').removeClass('w2ui-btn-green');
			
			if(status != '') {
				$('#' + status).addClass('w2ui-btn-green');
			}
		}
	}
}

localStorage.removeItem('action_reference');
function getActionReference(selected) {
	if(localStorage.getItem('action_reference') === null) {
		$.ajax({
			url:'Legissoft/action_reference/json.action.reference.php',
			type:'GET',
			dataType: 'json',
			success: function( json ) {
				$('select[name=action_reference]').empty();
				
				$.each(json.records, function(i, value) {
					$('select[name=action_reference]').append($('<option>').text(value.detail).attr('value', value.recid));
				});
				
				if(selected != '') {
					$('select[name=action_reference]').val(selected);
				}
				
				localStorage.setItem('action_reference', JSON.stringify(json));
			}
		});
		
	} else {
		$('select[name=action_reference]').empty();
		
		var action_reference = JSON.parse(localStorage.getItem('action_reference'));
		
		$.each(action_reference.records, function(i, value) {
			$('select[name=action_reference]').append($('<option>').text(value.detail).attr('value', value.recid));
		});
		
		if(selected != '') {
			$('select[name=action_reference]').val(selected);
		}
	}
}

// dapat mag process anay ang tatlo ka committee, movant kag second na function
// tapos amo na na pag call dayon sng mga screen na function
// dapat ang value sng do_not_call_screen_on_startup is 3 antis amg run ang mga screen function
var do_not_call_screen_on_startup = 0;
function getAction(action_id) {
	if(action_id != '') {
		$.ajax({
			url:'Legissoft/action/json.action.get.php',
			type:'POST',
			data: { 
				code : action_id
			},
			success: function(result) {
				w2ui['layout_action_main'].content('main', 
					'<div id="panel_status" style="padding:10px 5px;"></div>' +
					'<textarea id="writer_editor" class="writer_editor" style="width: calc(100% - 2px); height: calc(100% - 106px);">' + result.data.action + '</textarea>'
				);
				
				tinyMCE.remove('.writer_editor');
				tinymce.init({
					selector : '.writer_editor',
					plugins  : 'autoresize print textcolor colorpicker lists table paste searchreplace image media imagetools responsivefilemanager',
					toolbar1 : 'toolbar_custom_new_action print | undo redo | cut copy paste | formatselect fontselect fontsizeselect | bold italic underline strikethrough',
					toolbar2 : 'forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat  subscript superscript | image media responsivefilemanager | table | searchreplace',
					font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
					fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
					menu: {
						file   : {title: 'File', items: 'menu_custom_new print'},
						edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
						format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
						table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
					},
					statusbar : false,
					resize    : false,
					branding  : false,
					height    : $('.writer_editor').height() + 6,
					autoresize_max_height   : $('.writer_editor').height() + 6,
					theme_advanced_resizing : true,
					theme_advanced_resizing_use_cookie : false,
					setup: function (editor) {
						editor.addMenuItem('menu_custom_new_action', {
							text: 'New Action',
							icon: 'newdocument',
							onclick: function() {
								formNewAction();
							}
						});
						editor.addButton('toolbar_custom_new_action', {
							text: false,
							icon: 'newdocument',
							tooltip: 'New Action',
							onclick: function () {
								formNewAction();
							}
						});
						editor.on('init', function() {
							this.execCommand("fontName", false, "times new roman");
							this.execCommand("fontSize", false, "12pt");
						});
					},
					toolbar_items_size : 'small',
					menubar : false,
					
					image_advtab: true ,
					external_filemanager_path:"/legisoft/admin/framework/filemanager/",
					filemanager_title:"File Manager" ,
					external_plugins: { "filemanager" : "/legisoft/admin/framework/filemanager/plugin.min.js"},
					relative_urls: false
				});
				
				$("input[name=action_title]").attr("disabled", false);
				$("select[name=action_reference").attr("disabled", false);
				$("input[name=action_date]").attr("disabled", false);
				$("button[name=action_save]").attr("disabled", false);
				
				getActionReference(result.data.reference);	
				
				$("input[name=action_title]").val(result.data.title);
				$("input[name=action_date]").val(result.data['action_date']);
						
				getActionItem(result.data.status_code);
				
				w2ui['grid_committee'].clear();
				w2ui['grid_movant'].clear();
				w2ui['grid_second'].clear();
				w2ui['grid_committee_assign'].clear();
				w2ui['grid_movant_assign'].clear();
				w2ui['grid_second_assign'].clear();

			
				$("input[name=action_code]").val(result.data.code);
				$("button[name=button_action_show_to_members]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_action_show_to_members]").text('Show To Members');
				$("button[name=button_action_show_to_audience]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_action_show_to_audience]").text('Show To Audience');
				$("button[name=action_delete]").attr("disabled", false);
				$("#action_status_msg").html('');
				$("input[name=action_title]").removeClass('w2ui-error');
				$("input[name=action_date]").removeClass('w2ui-error');
				
				setGridCommittee();
				setGridMovant();
				setGridSecond();
				
				if(result.data['members_action_view'] == action_id) {
					$("button[name=button_action_show_to_members]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
					$("button[name=button_action_show_to_members]").text('Hide From Members');
				}
				
				if(result.data['audience_action_view'] == action_id) {
					$("button[name=button_action_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
					$("button[name=button_action_show_to_audience]").text('Hide From Audience');
				}
				
				getActionMembers('', '');
			}
		});
	}
}
function saveAction() {
	var code        = $.trim($("input[name=action_code]").val());
	var title       = $.trim($("input[name=action_title]").val());
	var action_date = $.trim($("input[name=action_date]").val());
	//var content     = tinyMCE.activeEditor.getContent();
	var content     = tinyMCE.get('writer_editor').getContent();
	
	if(title != '' && action_date != '') {
		if(content.trim() != '') {
			$.ajax({
				url:'Legissoft/action/action.save.php',
				type:'POST',
				data: { 
					code      : code,
					action    : content, 
					title     : title,
					reference : $("select[name=action_reference").val(),
					date      : $("input[name=action_date").val()
				},
				success: function(result) {
					$("input[name=action_code]").val(result.data['code']);
					$("button[name=button_action_show_to_members]").attr("disabled", false);
					$("button[name=button_action_show_to_audience]").attr("disabled", false);
					$("button[name=action_delete]").attr("disabled", false);
					$("#action_status_msg").html('');
					$("input[name=action_title]").removeClass('w2ui-error');
					$("input[name=action_date]").removeClass('w2ui-error');
					$(".button_action_status").attr("disabled", false);
					
					setGridCommittee();
					setGridMovant();
					setGridSecond();
					
					getArchiveAction(result.data.code);
					
					updateMembersScreenAction();
					updateAudienceScreenAction();
					
					getActionMembers('', '');
					
					ohSnap('Action Saved!', {'color':'green', 'duration':'1000'});
				}
			});
			
		} else {
			w2alert('Saving blank action is not allowed!');
		}
	
	} else {
		if(title == '') {
			$("input[name=action_title]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Action Title is required!');
			
		} else if(action_date == '') {
			$("input[name=action_date]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Action Date is required!');
		}
		
	}
	
}

function setMembersAction() {
	//var content = tinyMCE.activeEditor.getContent();
	var content = tinyMCE.get('writer_editor').getContent();
	
	if(content.trim() != '') {
		$.ajax({
			url:'Legissoft/action/action.save.php',
			type:'POST',
			data: { 
				code_members : $("input[name=action_code]").val()
			},
			success: function(result) {
				if(result.data['status'] == 'show') {
					$("button[name=button_action_show_to_members]").removeClass('w2ui-btn-green');
					$("button[name=button_action_show_to_members]").text('Show To Members');
				} else {
					$("button[name=button_action_show_to_members]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
					$("button[name=button_action_show_to_members]").text('Hide From Members');
				}
				
				updateMembersScreenAction();
				getArchiveAction('');
			}
		});
	} else {
		w2alert('Blank action will not allow to show to Members!');
	}
}

function setAudienceAction() {
	//var content = tinyMCE.activeEditor.getContent();
	var content = tinyMCE.get('writer_editor').getContent();
	
	if(content.trim() != '') {
		$.ajax({
			url:'Legissoft/action/action.save.php',
			type:'POST',
			data: { 
				code_audience : $("input[name=action_code]").val()
			},
			success: function(result) {
				if(result.data['status'] == 'show') {
					$("button[name=button_action_show_to_audience]").removeClass('w2ui-btn-green');
					$("button[name=button_action_show_to_audience]").text('Show To Audience');
				} else {
					$("button[name=button_action_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
					$("button[name=button_action_show_to_audience]").text('Hide From Audience');
				}
				
				updateAudienceScreenAction();
				getArchiveAction('');
			}
		});
		
	} else {
		w2alert('Blank action will not allow to show to Audience!');
	}
}

function setActionStatus(status) {
	if(status != '') {
		var set_status = '0';
		
		if($('#' + status).hasClass('w2ui-btn-green')) {
			set_status = '';
		} else {
			set_status = status;
		}
		
		$.ajax({
			url:'Legissoft/action/action.save.php',
			type:'POST',
			data: { 
				code_status : $("input[name=action_code]").val(),
				status      : set_status
			},
			success: function(result) {
				$('.button_action_status').removeClass('w2ui-btn-green');
				
				if(set_status != '') {
					$('#' + status).addClass('w2ui-btn-green');
				}
				
				updateMembersScreenAction();
				updateAudienceScreenAction();
			}
		});
	}
}

function deleteAction() {
	var action_code = $.trim($("input[name=action_code]").val());
	
	if(action_code != '') {
		w2confirm('Do you want to delete?').yes(function () { 
			$.ajax({
				url:'Legissoft/action/action.save.php',
				type:'POST',
				data: { 
					delete_code : action_code,
					cmd         : 'delete'
				},
				success: function(result) {
					$("input[name=action_code]").val('');
					$("input[name=action_title]").val('').attr("disabled", true);
					$("input[name=action_date]").val('').attr("disabled", true);
					$("select[name=action_reference").empty().attr("disabled", true);
					$("button[name=action_save]").attr("disabled", true);
					$("button[name=action_delete]").attr("disabled", true);
					$("input[name=action_title]").removeClass('w2ui-error');
					$("input[name=action_date]").removeClass('w2ui-error');
					$('#action_status_msg').html('');
					
					$("button[name=button_action_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true);
					$("button[name=button_action_show_to_members]").text('Show To Members');
					$("button[name=button_action_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true);
					$("button[name=button_action_show_to_audience]").text('Show To Audience');
					
					w2ui['grid_committee'].clear();
					w2ui['grid_movant'].clear();
					w2ui['grid_second'].clear();
					w2ui['grid_committee_assign'].clear();
					w2ui['grid_movant_assign'].clear();
					w2ui['grid_second_assign'].clear();
					
					w2ui['layout_action_main'].content('main', '');
					
					w2ui['sidebar_action'].remove('a' + action_code);
					w2ui['sidebar_action'].remove('b' + action_code);
					
					w2ui['grid_action_members'].clear();
					
					updateMembersScreenAction();
					updateAudienceScreenAction();
				}
			});
		});
	}
}

function getActionMembers(member_code, status) {
	// on - off specific viewing
	var action_code = $.trim($("input[name=action_code]").val());
	if(action_code != '') {
		$.ajax({
			url      :'Legissoft/action/json.action.members.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code        : action_code,
				member_code : member_code,
				status      : status
			},
			success: function( json ) {
				w2ui['grid_action_members'].clear();
				
				$.each(json.records, function(i, value) {
					if(value.status == 'hide') {
						w2ui['grid_action_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button, 'w2ui': { 'style': 'background-color: #C2F5B4; font-weight: bold;' } });
					} else {
						w2ui['grid_action_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button });
					}
				});
				
				if(member_code != '') {
					getArchiveAction('');
				}
			}
		});
	}
}

function updateMembersScreenAction() {
	$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
}

function updateAudienceScreenAction() {
	$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
}
</script>