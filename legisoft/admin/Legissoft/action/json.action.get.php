<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

if(isset($_POST['code']) && trim($_POST['code']) != "") {

	$code = trim($_POST['code']);
	
	$stmt = $conn->prepare("SELECT * FROM action WHERE isdelete = '0' AND code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$stmt2 = $conn->prepare("SELECT members_action_view FROM config WHERE members_action_view = :members_action_view");
		$stmt2->bindParam(':members_action_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['members_action_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['members_action_view'] = $row2['members_action_view'];
		}
		
		$stmt2 = $conn->prepare("SELECT audience_action_view FROM config WHERE audience_action_view = :audience_action_view");
		$stmt2->bindParam(':audience_action_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['audience_action_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['audience_action_view'] = $row2['audience_action_view'];
		}


		$rows['code']        = $row['code'];
		$rows['title']       = $row['title'];
		$rows['action']      = $row['action'];
		$rows['status_code'] = $row['status_code'];
		$rows['reference']   = $row['reference'];
		$rows['action_date'] = date("m/d/Y", strtotime($row['action_date']));
	}

}

echo json_encode(array('data' => $rows));
?>
