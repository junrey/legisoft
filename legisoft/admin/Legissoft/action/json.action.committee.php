<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$code = "";
if(isset($_GET['code']) && trim($_GET['code']) != "") {
	$code = trim($_GET['code']);
	
} else if(isset($_POST['code']) && trim($_POST['code']) != "") {
	$code = trim($_POST['code']);
	
}

if(isset($_POST['committee']) && !empty($_POST['committee']) && $code != "") {

	//$stmt = $conn->prepare("DELETE FROM action_committee WHERE action_code = :action_code");
	//$stmt->bindParam(':action_code', $code, PDO::PARAM_STR);
	//$stmt->execute();
	
	foreach($_POST['committee'] as $val) {
		$stmt = $conn->prepare("INSERT INTO action_committee SET committee_code = :committee_code, action_code = :action_code, mod_by = :mod_by, mod_date = NOW()");
		$stmt->bindParam(':committee_code', $val,                                          PDO::PARAM_STR);
		$stmt->bindParam(':action_code',    $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',         $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}
	
	$stmt = $conn->prepare("UPDATE action SET mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code',   $code,                                         PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();

}
///$data = json_decode($_POST['request'], true);
	
//$search = "";
//if(isset($data['search'][0]['value'])){
//    $search = trim($data['search'][0]['value']);
///}

//if($search == "") {
	$stmt = $conn->prepare("SELECT * FROM committee WHERE isdelete = '0' AND enable = '1' ORDER BY detail");
//} else {
//	$stmt = $conn->prepare("SELECT * FROM committee WHERE isdelete = '0' AND enable = '1' AND detail LIKE :detail ORDER BY detail");
//	$search = '%' . $search . '%';
//	$stmt->bindParam(':detail', $search, PDO::PARAM_STR);
//}

$stmt->execute();

echo '{ 
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		
		$stmt2 = $conn->prepare("SELECT committee_code FROM action_committee WHERE committee_code = :committee_code AND action_code = :action_code");
		$stmt2->bindParam(':committee_code', $row['code'], PDO::PARAM_STR);
		$stmt2->bindParam(':action_code',    $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		if($stmt2->rowCount() == 0) {
			$cnt++; if($cnt > 1) { echo ","; }
		 
			echo '{ 
				"recid"     : "' . $row['code'] . '", 
				"code"      : "' . $row['code'] . '", 
				"detail"    : "' . htmlspecialchars($row['detail']) . '"
			}';
		}
		
		

	}

echo '], "status"  : "' . $status . '", 
	     "message" : "' . $message . '", 
	     "total"   : "' . $cnt . '"}';

?>
