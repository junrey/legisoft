<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows        = array();
$active      = array();
$active_node = array();

$stmt2 = $conn->prepare("SELECT members_action_view, audience_action_view FROM config");
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

if($row2['members_action_view'] != "") {
	$active[$row2['members_action_view']] = $row2['members_action_view'];
}

if($row2['audience_action_view'] != "") {
	$active[$row2['audience_action_view']] = $row2['audience_action_view'];
}

$stmt3 = $conn->prepare("SELECT view_action FROM _user WHERE view_action <> '' AND type = '1' AND enable = '1' AND deleted = '0'");
$stmt3->execute();
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
	$active[$row3['view_action']] = $row3['view_action'];
}

$reference = array();
$stmt4 = $conn->prepare("SELECT code, detail FROM action_reference WHERE isdelete = '0'");
$stmt4->execute();
while($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
	$reference[$row4['code']] = $row4['detail'];
}

$stmt = $conn->prepare("SELECT code, title, action_date, DATE(action_date) AS parse_date, reference FROM action WHERE isdelete = '0' ORDER BY YEAR(action_date) DESC, MONTH(action_date) DESC, WEEK(action_date) ASC, title");
$stmt->execute();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$time = strtotime($row['action_date']);
	
	$temp = array();
	$temp['code']  = $row['code'];
	$temp['title'] = $row['title'];
	
	$rows[date('Y', $time)]['label'] = date('Y', $time);
	$rows[date('Y', $time)]['month'][date('F', $time)]['label'] = date('F', $time);
	$rows[date('Y', $time)]['month'][date('F', $time)]['week'][$GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['label'] = $GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday");
	$rows[date('Y', $time)]['month'][date('F', $time)]['week'][$GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['reference'][$row['reference']]['label'] = $row['reference'];
	$rows[date('Y', $time)]['month'][date('F', $time)]['week'][$GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['reference'][$row['reference']]['file'][$row['code']] = $temp;
		
	// gin dugangan ko prefix na 'a' and 'b' para wala conflict sa sidebar,
	// karon kung may duplicate ga console log error
	
	if (in_array($row['code'], $active)) {
		$active_node[$row['code']] = "{ id: 'b" . $row['code'] . "', text: \"" . htmlspecialchars($row['title']) . "\", img: 'icon-page' },";
	}

}
echo "[";

	echo "{ id: 'level-0', text: 'Active', img: 'icon-page', expanded: true, group: true, groupShowHide: false, collapsible: false },";

if(!empty($active_node)) {
	foreach ($active_node as $val) {
		echo $val;
	}
}

echo "{ id: 'level-1', text: 'Archive', img: 'icon-folder', expanded: true, group: true, groupShowHide: false },";
foreach($rows as $val) {
	echo "{ id: '" . $val['label'] . "', text: '" . $val['label'] . "', img: 'icon-folder', nodes: [";
		foreach($val['month'] as $val2) {
			echo "{ id: '" . $val['label'] . $val2['label'] . "', text: '" . $val2['label'] . "', img: 'icon-folder', nodes: [";
				foreach($val2['week'] as $val3) {
					echo "{ id: '" . $val['label'] . $val2['label'] . $val3['label'] . "', text: 'Week " . $val3['label'] . "', img: 'icon-folder', nodes: [";
					foreach($val3['reference'] as $val4) {
						echo "{ id: '" . $val['label'] . $val2['label'] . $val3['label'] . $val4['label'] . "', text: '" . $reference[$val4['label']] . "', img: 'icon-folder', nodes: [";
						foreach($val4['file'] as $val5) {
							echo "{ id: 'a" . $val5['code'] . "', text: \"" . htmlspecialchars($val5['title']) . "\", img: 'icon-page'},";
						}
						echo "]},";
					}
					echo "]},";
				}
			echo "]},";	
		}
	echo "]}, ";
}
echo "]";
?>
