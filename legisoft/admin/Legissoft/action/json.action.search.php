<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$search = "";
if(isset($_GET['request'])) {
	$data = json_decode($_GET['request'], true);
	if(isset($data["search"])) {
		$search = $data["search"][0]["value"];
	}
}

$action_item = array();
$action_item[""] = "";
$stmt2 = $conn->prepare("SELECT code, detail FROM action_item WHERE isdelete = '0'");
$stmt2->bindParam(':status_code', $row['status_code'], PDO::PARAM_STR);
$stmt2->execute();
while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
	$action_item[$row2['code']] = $row2['detail'];
}

$action_reference = array();
$action_reference[""] = "";
$stmt3 = $conn->prepare("SELECT code, detail FROM action_reference WHERE isdelete = '0'");
$stmt3->bindParam(':reference', $row['reference'], PDO::PARAM_STR);
$stmt3->execute();
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
	$action_reference[$row3['code']] = $row3['detail'];
}


$stmt = $conn->prepare("SELECT code, title, status_code, reference, action_date, created_by, mod_by, mod_date FROM action WHERE CONCAT(title, ' ', action) LIKE :search AND isdelete = '0' ORDER BY YEAR(action_date) DESC, MONTH(action_date) DESC, WEEK(action_date) ASC, title");
$stmt->bindValue(':search', '%' . $search . '%', PDO::PARAM_STR);
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		echo '{ 
			"recid"        : "' . $row['code'] . '", 
			"action_code"  : "' . $row['code'] . '", 
			"title"        : "' . $GENERAL_FUNCTIONS->cleanString($row['title']) . '",
			"status"       : "' . $action_item[$row['status_code']] . '", 
			"reference"    : "' . $action_reference[$row['reference']] . '", 
			"created_by"   : "' . $GENERAL_FUNCTIONS->cleanString($row['created_by']) . '",
			"created_date" : "' . date('m/d/Y', strtotime($row['action_date'])) . '", 
			"mod_by"       : "' . $GENERAL_FUNCTIONS->cleanString($row['mod_by']) . '",
			"mod_date"     : "' . date('m/d/Y', strtotime($row['mod_date'])) . '"

		}';

   }

echo ']}';

?>
