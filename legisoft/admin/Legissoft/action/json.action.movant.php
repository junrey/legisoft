<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$code = "";
if(isset($_GET['code']) && trim($_GET['code']) != "") {
	$code = trim($_GET['code']);
	
} else if(isset($_POST['code']) && trim($_POST['code']) != "") {
	$code = trim($_POST['code']);
	
}

if(isset($_POST['movant']) && !empty($_POST['movant']) && $code != "") {
	foreach($_POST['movant'] as $val) {
		$stmt = $conn->prepare("INSERT INTO action_movant SET movant_code = :movant_code, action_code = :action_code, mod_by = :mod_by, mod_date = NOW()");
		$stmt->bindParam(':movant_code', $val,                                          PDO::PARAM_STR);
		$stmt->bindParam(':action_code', $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}
	
	$stmt = $conn->prepare("UPDATE action SET mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code',   $code,                                         PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
}

$stmt = $conn->prepare("SELECT username, fname, lname FROM _user WHERE enable = '1' AND deleted = '0' AND type = '1' ORDER BY lname, fname");
$stmt->execute();

echo '{ 
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		
		$stmt2 = $conn->prepare("SELECT movant_code FROM action_movant WHERE movant_code = :movant_code AND action_code = :action_code");
		$stmt2->bindParam(':movant_code', $row['username'], PDO::PARAM_STR);
		$stmt2->bindParam(':action_code', $code,            PDO::PARAM_STR);
		$stmt2->execute();
		
		if($stmt2->rowCount() == 0) {
			$cnt++; if($cnt > 1) { echo ","; }
		 
			echo '{ 
				"recid"    : "' . $row['username'] . '", 
				"code"     : "' . $row['username'] . '", 
				"fullname" : "' . htmlspecialchars($row['lname']) . ', ' . htmlspecialchars($row['fname']) . '"
			}';
		}
	}

echo '], "status"  : "' . $status . '", 
	     "message" : "' . $message . '", 
	     "total"   : "' . $cnt . '"}';

?>
