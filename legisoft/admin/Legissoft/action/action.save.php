<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();

if(isset($_POST['code']) && trim($_POST['title']) != "") {
	$action    = trim($_POST['action']);
	$title     = trim($_POST['title']);
	$reference = trim($_POST['reference']);
	$action_date = date("Y-m-d", strtotime(trim($_POST['date'])));

	if(trim($_POST['code']) == "") {
		$today = date("mdY") . date("His");
		$code  = "JDX" . rand(0,9) . rand(0,9) . $today; 
		
		$stmt = $conn->prepare("INSERT INTO action SET code = :code, title = :title, action = :action, reference= :reference, action_date = :action_date, created_by = :created_by, created_date = NOW(), mod_by = :mod_by, mod_date = NOW(), save = '1'");
		$stmt->bindParam(':code',        $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',       $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':action',      $action,                                       PDO::PARAM_STR);
		$stmt->bindParam(':reference',   $reference, 								    PDO::PARAM_STR);
		$stmt->bindParam(':action_date', $action_date, 								    PDO::PARAM_STR);
		$stmt->bindParam(':created_by',  $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
				
	} else {
		$code = trim($_POST['code']);
		
		$stmt = $conn->prepare("UPDATE action SET title = :title, action = :action, reference= :reference, action_date = :action_date, mod_by = :mod_by, mod_date = NOW(), save = '1' WHERE code = :code");
		$stmt->bindParam(':code',        $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',       $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':action',      $action,                                       PDO::PARAM_STR);
		$stmt->bindParam(':reference',   $reference, 								    PDO::PARAM_STR);
		$stmt->bindParam(':action_date', $action_date, 								    PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}

	$rows['code'] = $code;
	
} else if(isset($_POST['code_members']) && trim($_POST['code_members']) != "") {
	$members_action_view = trim($_POST['code_members']);
	
	$stmt = $conn->prepare("SELECT members_action_view FROM config WHERE members_action_view = :members_action_view");
	$stmt->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET members_action_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
	
	} else {
		$stmt = $conn->prepare("UPDATE config SET members_action_view = :members_action_view");
		$stmt->bindParam(':members_action_view', $members_action_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}

	$rows['code'] = $members_action_view;
	
} else if(isset($_POST['code_audience']) && trim($_POST['code_audience']) != "") {
	$audience_action_view = trim($_POST['code_audience']);
	
	$stmt = $conn->prepare("SELECT audience_action_view FROM config WHERE audience_action_view = :audience_action_view");
	$stmt->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
	$stmt->execute();

	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET audience_action_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
		
	} else {
		$stmt = $conn->prepare("UPDATE config SET audience_action_view = :audience_action_view");
		$stmt->bindParam(':audience_action_view', $audience_action_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}
	
	$rows['code'] = $audience_action_view;
	
} else if(isset($_POST['code_status'])) {
	$code_status = trim($_POST['code_status']);
	$status      = trim($_POST['status']);
	
	if($code_status != "" && $status != "") {
		$stmt = $conn->prepare("UPDATE action SET status_code = :status_code, mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
		$stmt->bindParam(':code',        $code_status, PDO::PARAM_STR);
		$stmt->bindParam(':status_code', $status,      PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	
	} else if($code_status != "" && $status == "") {
		$stmt = $conn->prepare("UPDATE action SET status_code = '', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
		$stmt->bindParam(':code',   $code_status, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}
	
	$rows['code'] = $code_status;
	
} else if(isset($_POST['delete_code']) && trim($_POST['cmd']) == "delete") {
	$code = trim($_POST['delete_code']);
	
	$stmt = $conn->prepare("UPDATE action SET isdelete = '1', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET members_action_view = '' WHERE members_action_view = :members_action_view");
	$stmt->bindParam(':members_action_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET audience_action_view = '' WHERE audience_action_view = :audience_action_view");
	$stmt->bindParam(':audience_action_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE _user SET view_action = '' WHERE view_action = :view_action");
	$stmt->bindParam(':view_action', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$rows['code'] = "";
	
}



echo json_encode(array('data' => $rows));
?>