<div id="tabs_config_main" style="width: 100%; background-color:#fff;"></div>
<div id="tab_config_system"        class="tabs_config_main" style="width: 100%;"></div>
<div id="tab_config_document_type" class="tabs_config_main" style="width: 100%; display: none;"></div>
<div id="tab_config_committee"     class="tabs_config_main" style="width: 100%; display: none;"></div>
<div id="tab_config_action"        class="tabs_config_main" style="width: 100%; display: none;"></div>
<div id="tab_config_reference"     class="tabs_config_main" style="width: 100%; display: none;"></div>
<div id="tab_config_vote"          class="tabs_config_main" style="width: 100%; display: none;"></div>
<script>
$(function () {
	var load_config_system_once        = 0;
	var load_config_document_type_once = 0;
	var load_config_committee_once     = 0;
	var load_config_action_once        = 0;
	var load_config_reference_once     = 0;
	var load_config_vote_once          = 0;
	
	$().w2destroy('tabs_config_main');
    $('#tabs_config_main').w2tabs({
        name: 'tabs_config_main',
        active: 'tab_config_system',
        tabs: [
			 { id: 'tab_config_system',        text: 'System' },
			 { id: 'tab_config_document_type', text: 'Document Type' },
			 { id: 'tab_config_committee',     text: 'Committee List' },
			 { id: 'tab_config_action',        text: 'Action Choices' },
			 { id: 'tab_config_reference',     text: 'Action Reference' },
			 { id: 'tab_config_vote',          text: 'Vote Choices' }
		],
		onClick: function (event) {
			if(event.target == 'tab_config_system') {
				$('.tabs_config_main').hide();
				$('#tab_config_system').show();
				
			} else if(event.target == 'tab_config_document_type') {
				$('.tabs_config_main').hide();
				$('#tab_config_document_type').show();
				
				if(load_config_document_type_once == 0) {
					$('#tab_config_document_type').load('Legissoft/config/document_type/document_type.index.php');
					load_config_document_type_once = 1;
				}
				
			} else if(event.target == 'tab_config_committee') {
				$('.tabs_config_main').hide();
				$('#tab_config_committee').show();
				
				if(load_config_committee_once == 0) {
					$('#tab_config_committee').load('Legissoft/config/committee/committee.index.php');
					load_config_committee_once = 1;
				}
				
			} else if(event.target == 'tab_config_action') {
				$('.tabs_config_main').hide();
				$('#tab_config_action').show();
				
				if(load_config_action_once == 0) {
					$('#tab_config_action').load('Legissoft/config/action/action.index.php');
					load_config_action_once = 1;
				}
				
			} else if(event.target == 'tab_config_reference') {
				$('.tabs_config_main').hide();
				$('#tab_config_reference').show();
				
				if(load_config_reference_once == 0) {
					$('#tab_config_reference').load('Legissoft/config/reference/reference.index.php');
					load_config_reference_once = 1;
				}
				
			} else if(event.target == 'tab_config_vote') {
				$('.tabs_config_main').hide();
				$('#tab_config_vote').show();
				
				if(load_config_vote_once == 0) {
					$('#tab_config_vote').load('Legissoft/config/vote/vote.index.php');
					load_config_vote_once = 1;
				}
				
			}
			
			w2uiElementResize();

		}
	});

	$('#tab_config_system').load('Legissoft/config/system/system.index.php');
	
	$(document).ready(function(){
		$(window).resize(function(){
			w2uiElementResize();
		});
	}); 
	
});


function w2uiElementResize() {
	for (var item in w2ui){
		if (w2ui[item].resize){
			w2ui[item].resize();
		}
	}
}

</script>