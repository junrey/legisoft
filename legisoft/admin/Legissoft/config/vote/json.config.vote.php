<?php session_start();
require_once("../../database/pdo.mysql.connection.legissoft.php");

$status  = 'success';
$message = '';

$stmt = $conn->prepare("SELECT * FROM vote_item WHERE deleted = '0' ORDER BY detail");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$enable = ($row['enable']) ? "Yes" : "No";
		
		echo '{ 
			"recid"           : "' . $row['code'] . '", 
			"code"            : "' . $row['code'] . '", 
			"detail"          : "' . htmlspecialchars($row['detail']) . '",
			"value"           : "' . $row['value'] . '",
			"place_id"        : "' . $row['place'] . '",
			"place"           : "' . $row['place'] . '",
			"enable_id"       : "' . $row['enable']. '",
			"enable"          : "' . $enable . '"
		}';

   }

echo ']}';

?>
