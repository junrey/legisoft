<div id="layout_vote_choices" style="position: absolute; width: 100%; height: calc(100% - 60px); padding:4px; background-color:#fff;"></div>
<script>
$().w2destroy('grid_vote_choices');
$().w2grid({ 
	name        : 'grid_vote_choices',
	header      : 'Vote Choices',
	multiSelect : false,
	show: {
		header        : true,
		toolbar       : true,
		footer        : true,
		lineNumbers   : true,
		toolbarAdd    : true,
		toolbarDelete : true,
		toolbarEdit   : true
	},      
	columns: [
		{ field: 'detail', size: '60%', caption: 'Vote Choice' },
		{ field: 'value',  size: '60%', caption: 'Value' },
		{ field: 'place',  size: '20%', caption: 'Order' },
		{ field: 'enable', size: '20%', caption: 'Active' },
		{ field: 'enable_id', hidden: true }	
	],
	onAdd: function (event) {
		formAddVoteChoices();
	},
	onEdit: function (event) {
		formEditVoteChoices();
	},
	onDelete: function (event) {
		var code = w2ui['grid_vote_choices'].getSelection()[0];
		event.onComplete = function() {
			formDeleteVoteChoices(code);
		};
	},
	multiSearch: false,
	searches: [
		{ field: 'detail', caption: 'vote Choice', type: 'text', operator : 'contains' }
	]
});
	
$(function () {	
	$().w2destroy('layout_vote_choices');
    $('#layout_vote_choices').w2layout({
        name: 'layout_vote_choices',
        panels: [
            { type: 'main', content: w2ui['grid_vote_choices'] }
        ]
    });
	
	loadVoteChoices();
	
});
function openPopupFormVoteChoices(action) {
	$().w2destroy('form_vote_choices');
	$().w2form({
		name  : 'form_vote_choices',
		style : 'border: 0px; background-color: transparent;',
		url   : 'Legissoft/config/vote/save.vote.php',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Vote Choice:</label>' +
			'        <div>' +
			'           <input name="detail" type="text" maxlength="50" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +	
			'    <div class="w2ui-field">' +
			'        <label>Vote Value:</label>' +
			'        <div>' +
			'           <input name="value" type="number" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +	
			'    <div class="w2ui-field">' +
			'        <label>Place:</label>' +
			'        <div>' +
			'            <input name="place" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +			'    <div class="w2ui-field">' +
			'        <label>Active:</label>' +
			'        <div>' +
			'            <input name="enable" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +					
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Reset</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
			'</div>',
		fields: [
			{ field: 'detail', type: 'text',   required: true },
			{ field: 'value',  type: 'number', required: true },
			{ field: 'place', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: '1' },
						{ id: 2, text: '2' },
						{ id: 3, text: '3' },
						{ id: 4, text: '4' },
						{ id: 5, text: '5' },
						{ id: 6, text: '6' },
						{ id: 7, text: '7' },
						{ id: 8, text: '8' },
						{ id: 9, text: '9' },
						{ id: 10, text: '10' }
					]
				} 
			},
			{ field: 'enable', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					],
					selected: { id: 1, text: 'Yes' }
				} 
			}
		],
		actions: {
			"save": function() { 
				this.save(function(data){ 
					loadVoteChoices();
					w2popup.close();
				}); 
			},
			"reset": function() { this.clear(); }
		},
		onSave: function(event) {}, 
		onError: function(event) {},
		postData: {
			'action' : action
		}
	});
}

function formAddVoteChoices() {
	openPopupFormVoteChoices('add');
	$().w2popup('open', {
		title   : 'Add New Vote Choice',
		body    : '<div id="popup_form_vote_choices" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 280, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_vote_choices').w2render('form_vote_choices');
			}
		},
		onClose: function () {
			w2ui['form_vote_choices'].clear();
		}
	});
}

function formEditVoteChoices() {
	openPopupFormVoteChoices('edit');
	$().w2popup('open', {
		title   : 'Edit Vote Choice',
		body    : '<div id="popup_form_vote_choices" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 280, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_vote_choices').w2render('form_vote_choices');

				var sel    = w2ui['grid_vote_choices'].getSelection();
				var record = w2ui['grid_vote_choices'].get(sel[0]);
				
				w2ui['form_vote_choices'].recid = record['code'];
				w2ui['form_vote_choices'].record['detail'] = record['detail'];
				w2ui['form_vote_choices'].record['value'] = record['value'];
				$('input[name=place]').w2field().set({ id: record['place_id'], text: record['place'] });
				$('input[name=enable]').w2field().set({ id: record['enable_id'], text: record['enable'] });
				$('button[name=reset]').prop('disabled', true);
				w2ui['form_vote_choices'].refresh();	
			}
		},
		onClose: function () {
			w2ui['form_vote_choices'].clear();
		}
	});
}

function loadVoteChoices() {
	w2ui['grid_vote_choices'].clear();
	w2ui['grid_vote_choices'].load('Legissoft/config/vote/json.config.vote.php');
}

function formDeleteVoteChoices(code) {
	if(code != '') {
		$.ajax({
			url:'Legissoft/config/vote/save.vote.php',
			type:'POST',
			data: { 
				code : code,
				cmd  : 'delete'
			},
			success: function(result) {}
		});
	}
}
</script>
