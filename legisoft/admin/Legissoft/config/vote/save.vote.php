<?php session_start();
require_once("../../database/pdo.mysql.connection.legissoft.php");
require_once("../../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$detail = $GENERAL_FUNCTIONS->filterString($data['record']['detail']);
	$value  = $GENERAL_FUNCTIONS->filterString($data['record']['value']);
	$place  = $data['record']['place']['id'];
	$enable = $data['record']['enable']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {
		$code = $GENERAL_FUNCTIONS->generateCode('VOT');
		
		$stmt = $conn->prepare("SELECT code FROM vote_item WHERE detail = :detail AND deleted = '0'");
		$stmt->bindParam(':detail', $detail,  PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO vote_item SET code = :code, detail = :detail, value = :value, place = :place, enable = :enable, mod_date = NOW(), mod_by = :mod_by");
			$stmt->bindParam(':code',   $code,   PDO::PARAM_STR);
			$stmt->bindParam(':detail', $detail, PDO::PARAM_STR);
			$stmt->bindParam(':value',  $value,  PDO::PARAM_INT);
			$stmt->bindParam(':place',  $place,  PDO::PARAM_STR);
			$stmt->bindParam(':enable', $enable, PDO::PARAM_STR);
			$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
			$stmt->execute();
		
		} else {
			$status  = 'error';
			$message = 'Duplicate Choice!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$code = $data['recid'];
		
		$stmt = $conn->prepare("UPDATE vote_item SET detail = :detail, value = :value, place = :place, enable = :enable, mod_date = NOW(), mod_by = :mod_by WHERE code = :code");
		$stmt->bindParam(':code',   $code,   PDO::PARAM_STR);
		$stmt->bindParam(':detail', $detail, PDO::PARAM_STR);
		$stmt->bindParam(':value',  $value,  PDO::PARAM_INT);
		$stmt->bindParam(':place',  $place,  PDO::PARAM_STR);
		$stmt->bindParam(':enable', $enable, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
	
	}
} else if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete") {
	$code = $_POST['code'];
	
	if($code != "") {
		$stmt = $conn->prepare("UPDATE vote_item SET deleted = '1', mod_date = NOW(), mod_by = :mod_by WHERE code = :code");
		$stmt->bindParam(':code', $code, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>