<style>
	#layout_config_system .w2ui-field>label {
		width: 200px;
		margin-right:15px;
	}
	#layout_config_system .w2ui-form {
		height:100% !important;
	}
</style>
<div id="layout_config_system" style="position: absolute; width: 100%; height: calc(100% - 60px); padding:4px; background-color:#fff;"></div>
<script>
$().w2destroy('form_config_system');
$().w2form({
	name  : 'form_config_system',
	style : 'padding:0;',
    focus : -1,
	url   : 'Legissoft/config/system/save.system.index.php',
	formHTML: 
		'<div class="w2ui-page page-0" style="padding-top:25px; width: 500px; margin:0 auto;">' +
		'    <div class="w2ui-field">' +
		'        <label>Members can vote?:</label>' +
		'        <div>' +
		'			<input name="vote_alter" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +
		'    <div class="w2ui-field">' +
		'        <label>Enable messaging?:</label>' +
		'        <div>' +
		'			<input name="messaging" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +
		//'    <div class="w2ui-field">' +
		//'        <label>Enable messaging popup?:</label>' +
		//'        <div>' +
		//'			<input name="messaging_popup" type="list" style="width: 250px"/>' +
		//'        </div>' +
		//'    </div>' +
		'    <div class="w2ui-field">' +
		'        <label>Members can vote only once?:</label>' +
		'        <div>' +
		'			<input name="vote_once" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +		
		'    <div class="w2ui-field">' +
		'        <label>Show Action Writer?:</label>' +
		'        <div>' +
		'			<input name="show_action_writer" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +
		'    <div class="w2ui-field">' +
		'        <label>Show Voting?:</label>' +
		'        <div>' +
		'			<input name="show_voting" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +
		'    <div class="w2ui-field">' +
		'        <label>Show Image Gallery?:</label>' +
		'        <div>' +
		'			<input name="show_image_gallery" type="list" style="width: 250px"/>' +
		'        </div>' +
		'    </div>' +		
		'</div>' +
		'<div class="w2ui-buttons">' +
		'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
		'</div>',
	fields: [	
		{ field: 'vote_alter', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		},
		{ field: 'messaging', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		},
		//{ field: 'messaging_popup', type: 'list', required: true, 
		//	options: {
		//		items: [
		//			{ id: 1, text: 'Yes' },
		//			{ id: 0, text: 'No' }
		//		],
		//		selected: { id: 1, text: 'Yes' }
		//	} 
		//}
		{ field: 'vote_once', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		},
		{ field: 'show_action_writer', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		},
		{ field: 'show_voting', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		},
		{ field: 'show_image_gallery', type: 'list', required: true, 
			options: {
				items: [
					{ id: 1, text: 'Yes' },
					{ id: 0, text: 'No' }
				],
				selected: { id: 1, text: 'Yes' }
			} 
		}
	],
	actions: {
		"save": function() { 
			this.save(function(data){ 
				$('input[name=vote_alter]').w2field().set({ id: data['vote_alter_id'], text: data['vote_alter'] });
				$('input[name=messaging]').w2field().set({ id: data['messaging_id'], text: data['messaging'] });
				//$('input[name=messaging_popup]').w2field().set({ id: data['messaging_popup_id'], text: data['messaging_popup'] });
				$('input[name=vote_once]').w2field().set({ id: data['vote_once_id'], text: data['vote_once'] });
				
				$('input[name=show_action_writer]').w2field().set({ id: data['show_action_writer_id'], text: data['show_action_writer'] });
				$('input[name=show_voting]').w2field().set({ id: data['show_voting_id'], text: data['show_voting'] });
				$('input[name=show_image_gallery]').w2field().set({ id: data['show_image_gallery_id'], text: data['show_image_gallery'] });
				
				w2ui['form_config_system'].refresh();	
				
				ohSnap('System Configuration Saved!', {'color':'green', 'duration':'1000'});
			}); 
		},
	}
});

$(function () {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';

	$().w2destroy('layout_config_system');
    $('#layout_config_system').w2layout({
        name: 'layout_config_system',
        panels: [
            { type: 'main', style: pstyle, content: w2ui['form_config_system'] }
        ]
    });
	

	loadFormConfigSystem();
});

function loadFormConfigSystem() {
	$.ajax({
		url:'Legissoft/config/system/save.system.index.php',
		type:'POST',
		success: function(result) {
			$('input[name=vote_alter]').w2field().set({ id: result['vote_alter_id'], text: result['vote_alter'] });
			$('input[name=messaging]').w2field().set({ id: result['messaging_id'], text: result['messaging'] });
			//$('input[name=messaging_popup]').w2field().set({ id: result['messaging_popup_id'], text: result['messaging_popup'] });
			$('input[name=vote_once]').w2field().set({ id: result['vote_once_id'], text: result['vote_once'] });
			
			$('input[name=show_action_writer]').w2field().set({ id: result['show_action_writer_id'], text: result['show_action_writer'] });
			$('input[name=show_voting]').w2field().set({ id: result['show_voting_id'], text: result['show_voting'] });
			$('input[name=show_image_gallery]').w2field().set({ id: result['show_image_gallery_id'], text: result['show_image_gallery'] });
			
			w2ui['form_config_system'].refresh();	
		}
	});
}
</script>