<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../../database/pdo.mysql.connection.legissoft.php");

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$vote_alter_id      = $data['record']['vote_alter']['id'];
	$messaging_id       = $data['record']['messaging']['id'];
	//$messaging_popup_id = $data['record']['messaging_popup']['id'];
	$vote_once_id       = $data['record']['vote_once']['id'];
	
	$show_action_writer_id = $data['record']['show_action_writer']['id'];
	$show_voting_id        = $data['record']['show_voting']['id'];
	$show_image_gallery_id = $data['record']['show_image_gallery']['id'];

	$stmt = $conn->prepare("UPDATE config SET vote_alter = :vote_alter, messaging = :messaging, vote_once = :vote_once, show_action_writer = :show_action_writer, show_voting = :show_voting, show_image_gallery = :show_image_gallery");
	$stmt->bindParam(':vote_alter',      $vote_alter_id,      PDO::PARAM_STR);
	$stmt->bindParam(':messaging',       $messaging_id,       PDO::PARAM_STR);
	//$stmt->bindParam(':messaging_popup', $messaging_popup_id, PDO::PARAM_STR);
	$stmt->bindParam(':vote_once',       $vote_once_id,       PDO::PARAM_STR);
	
	$stmt->bindParam(':show_action_writer', $show_action_writer_id, PDO::PARAM_STR);
	$stmt->bindParam(':show_voting',        $show_voting_id,        PDO::PARAM_STR);
	$stmt->bindParam(':show_image_gallery', $show_image_gallery_id, PDO::PARAM_STR);
	
	$stmt->execute();
	
	$vote_alter      = ($vote_alter_id) ? "Yes" : "No";
	$messaging       = ($messaging_id) ? "Yes" : "No";
	//$messaging_popup = ($messaging_popup_id) ? "Yes" : "No";
	$vote_once       = ($vote_once_id) ? "Yes" : "No";
	
	$show_action_writer = ($show_action_writer_id) ? "Yes" : "No";
	$show_voting        = ($show_voting_id) ? "Yes" : "No";
	$show_image_gallery = ($show_image_gallery_id) ? "Yes" : "No";

} else {
	$stmt = $conn->prepare("SELECT vote_alter, messaging, vote_once, show_action_writer, show_voting, show_image_gallery FROM config");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$vote_alter_id      = $row['vote_alter'];
	$messaging_id       = $row['messaging'];
	//$messaging_popup_id = $row['messaging_popup'];
	$vote_once_id       = $row['vote_once'];
	
	$show_action_writer_id = $row['show_action_writer'];
	$show_voting_id        = $row['show_voting'];
	$show_image_gallery_id = $row['show_image_gallery'];
	
	$vote_alter      = ($row['vote_alter']) ? "Yes" : "No";
	$messaging       = ($row['messaging']) ? "Yes" : "No";
	//$messaging_popup = ($row['messaging_popup']) ? "Yes" : "No";
	$vote_once       = ($row['vote_once']) ? "Yes" : "No";

	$show_action_writer = ($row['show_action_writer']) ? "Yes" : "No";
	$show_voting        = ($row['show_voting']) ? "Yes" : "No";
	$show_image_gallery = ($row['show_image_gallery']) ? "Yes" : "No";

}


echo '{ 
	"status"             : "' . $status . '", 
	"message" 			 : "' . $message . '",
	"vote_alter_id"      : "' . $vote_alter_id . '",
	"vote_alter"         : "' . $vote_alter . '",
	"messaging_id"       : "' . $messaging_id . '",
	"messaging"          : "' . $messaging . '",
	"vote_once_id"       : "' . $vote_once_id . '",
	"vote_once"          : "' . $vote_once . '",
	
	"show_action_writer_id" : "' . $show_action_writer_id . '",
	"show_action_writer"    : "' . $show_action_writer . '",
	"show_voting_id"        : "' . $show_voting_id . '",
	"show_voting"           : "' . $show_voting . '",
	"show_image_gallery_id" : "' . $show_image_gallery_id . '",
	"show_image_gallery"    : "' . $show_image_gallery . '"

}';

?>