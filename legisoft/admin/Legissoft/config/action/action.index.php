<div id="layout_action_choices" style="position: absolute; width: 100%; height: calc(100% - 60px); padding:4px; background-color:#fff;"></div>
<script>
$().w2destroy('grid_action_choices');
$().w2grid({ 
	name        : 'grid_action_choices',
	header      : 'Action Choices',
	multiSelect : false,
	show: {
		header        : true,
		toolbar       : true,
		footer        : true,
		lineNumbers   : true,
		toolbarAdd    : true,
		toolbarDelete : true,
		toolbarEdit   : true
	},      
	columns: [
		{ field: 'detail', size: '60%', caption: 'Action Choice' },
		{ field: 'place',  size: '20%', caption: 'Order' },
		{ field: 'enable', size: '20%', caption: 'Active' },
		{ field: 'enable_id', hidden: true }	
	],
	onAdd: function (event) {
		formAddActionChoices();
	},
	onEdit: function (event) {
		formEditActionChoices();
	},
	onDelete: function (event) {
		var code = w2ui['grid_action_choices'].getSelection()[0];
		event.onComplete = function() {
			formDeleteActionChoices(code);
		};
	},
	multiSearch: false,
	searches: [
		{ field: 'detail', caption: 'Action Choice', type: 'text', operator : 'contains' }
	]
});
	
$(function () {	
	$().w2destroy('layout_action_choices');
    $('#layout_action_choices').w2layout({
        name: 'layout_action_choices',
        panels: [
            { type: 'main', content: w2ui['grid_action_choices'] }
        ]
    });
	
	loadActionChoices();
	
});
function openPopupFormActionChoices(action) {
	$().w2destroy('form_action_choices');
	$().w2form({
		name  : 'form_action_choices',
		style : 'border: 0px; background-color: transparent;',
		url   : 'Legissoft/config/action/save.action.php',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Action Choice:</label>' +
			'        <div>' +
			'           <input name="detail" type="text" maxlength="50" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +	
			'    <div class="w2ui-field">' +
			'        <label>Place:</label>' +
			'        <div>' +
			'            <input name="place" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +			'    <div class="w2ui-field">' +
			'        <label>Active:</label>' +
			'        <div>' +
			'            <input name="enable" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +					
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Reset</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
			'</div>',
		fields: [
			{ field: 'detail', type: 'text', required: true },
			{ field: 'place', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: '1' },
						{ id: 2, text: '2' },
						{ id: 3, text: '3' },
						{ id: 4, text: '4' },
						{ id: 5, text: '5' },
						{ id: 6, text: '6' },
						{ id: 7, text: '7' },
						{ id: 8, text: '8' },
						{ id: 9, text: '9' },
						{ id: 10, text: '10' }
					]
				} 
			},
			{ field: 'enable', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					],
					selected: { id: 1, text: 'Yes' }
				} 
			}
		],
		actions: {
			"save": function() { 
				this.save(function(data){ 
					loadActionChoices();
					w2popup.close();
				}); 
			},
			"reset": function() { this.clear(); }
		},
		onSave: function(event) {}, 
		onError: function(event) {},
		postData: {
			'action' : action
		}
	});
}

function formAddActionChoices() {
	openPopupFormActionChoices('add');
	$().w2popup('open', {
		title   : 'Add New Action',
		body    : '<div id="popup_form_action_choices" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 250, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_action_choices').w2render('form_action_choices');
			}
		},
		onClose: function () {
			w2ui['form_action_choices'].clear();
		}
	});
}

function formEditActionChoices() {
	openPopupFormActionChoices('edit');
	$().w2popup('open', {
		title   : 'Edit Action',
		body    : '<div id="popup_form_action_choices" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 250, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_action_choices').w2render('form_action_choices');

				var sel    = w2ui['grid_action_choices'].getSelection();
				var record = w2ui['grid_action_choices'].get(sel[0]);
				
				w2ui['form_action_choices'].recid = record['code'];
				w2ui['form_action_choices'].record['detail'] = record['detail'];
				$('input[name=place]').w2field().set({ id: record['place_id'], text: record['place'] });
				$('input[name=enable]').w2field().set({ id: record['enable_id'], text: record['enable'] });
				$('button[name=reset]').prop('disabled', true);
				w2ui['form_action_choices'].refresh();	
			}
		},
		onClose: function () {
			w2ui['form_action_choices'].clear();
		}
	});
}

function loadActionChoices() {
	w2ui['grid_action_choices'].clear();
	w2ui['grid_action_choices'].load('Legissoft/config/action/json.config.action.php');
}

function formDeleteActionChoices(code) {
	if(code != '') {
		$.ajax({
			url:'Legissoft/config/action/save.action.php',
			type:'POST',
			data: { 
				code : code,
				cmd  : 'delete'
			},
			success: function(result) {}
		});
	}
}
</script>
