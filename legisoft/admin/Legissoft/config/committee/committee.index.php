<div id="layout_committee_list" style="position: absolute; width: 100%; height: calc(100% - 60px); padding:4px; background-color:#fff;"></div>
<script>
$().w2destroy('grid_committee_list');
$().w2grid({ 
	name        : 'grid_committee_list',
	header      : 'Committee List',
	multiSelect : false,
	show: {
		header        : true,
		toolbar       : true,
		footer        : true,
		lineNumbers   : true,
		toolbarAdd    : true,
		toolbarDelete : true,
		toolbarEdit   : true
	},      
	columns: [
		{ field: 'detail', size: '50%', caption: 'Committee' },
		{ field: 'enable', size: '10%', caption: 'Active' },
		{ field: 'enable_id', hidden: true }	
	],
	onAdd: function (event) {
		formAddCommitteeList();
	},
	onEdit: function (event) {
		formEditCommitteeList();
	},
	onDelete: function (event) {
		var code = w2ui['grid_committee_list'].getSelection()[0];
		event.onComplete = function() {
			formDeleteCommitteeList(code);
		};
	},
	multiSearch: false,
	searches: [
		{ field: 'detail', caption: 'Committee', type: 'text', operator : 'contains' }
	]
});
	
$(function () {	
	$().w2destroy('layout_committee_list');
    $('#layout_committee_list').w2layout({
        name: 'layout_committee_list',
        panels: [
            { type: 'main', content: w2ui['grid_committee_list'] }
        ]
    });
	
	loadCommitteeList();
	
});
function openPopupFormCommitteeList(action) {
	$().w2destroy('form_committee_list');
	$().w2form({
		name  : 'form_committee_list',
		style : 'border: 0px; background-color: transparent;',
		url   : 'Legissoft/config/committee/save.committee.php',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Committee:</label>' +
			'        <div>' +
			'           <input name="detail" type="text" maxlength="100" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +	
			'    <div class="w2ui-field">' +
			'        <label>Active:</label>' +
			'        <div>' +
			'            <input name="enable" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +					
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Reset</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
			'</div>',
		fields: [
			{ field: 'detail', type: 'text', required: true },
			{ field: 'enable', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					],
					selected: { id: 1, text: 'Yes' }
				} 
			}
		],
		actions: {
			"save": function() { 
				this.save(function(data){ 
					loadCommitteeList();
					w2popup.close();
				}); 
			},
			"reset": function() { this.clear(); }
		},
		onSave: function(event) {}, 
		onError: function(event) {},
		postData: {
			'action' : action
		}
	});
}

function formAddCommitteeList() {
	openPopupFormCommitteeList('add');
	$().w2popup('open', {
		title   : 'Add New Committee',
		body    : '<div id="popup_form_committee_list" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 250, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_committee_list').w2render('form_committee_list');
			}
		},
		onClose: function () {
			w2ui['form_committee_list'].clear();
		}
	});
}

function formEditCommitteeList() {
	openPopupFormCommitteeList('edit');
	$().w2popup('open', {
		title   : 'Edit Committee',
		body    : '<div id="popup_form_committee_list" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 250, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_committee_list').w2render('form_committee_list');

				var sel    = w2ui['grid_committee_list'].getSelection();
				var record = w2ui['grid_committee_list'].get(sel[0]);
				
				w2ui['form_committee_list'].recid = record['code'];
				w2ui['form_committee_list'].record['detail'] = record['detail'];
				$('input[name=enable]').w2field().set({ id: record['enable_id'], text: record['enable'] });
				$('button[name=reset]').prop('disabled', true);
				w2ui['form_committee_list'].refresh();	
			}
		},
		onClose: function () {
			w2ui['form_committee_list'].clear();
		}
	});
}

function loadCommitteeList() {
	w2ui['grid_committee_list'].clear();
	w2ui['grid_committee_list'].load('Legissoft/config/committee/json.config.committee.php');
}

function formDeleteCommitteeList(code) {
	if(code != '') {
		$.ajax({
			url:'Legissoft/config/committee/save.committee.php',
			type:'POST',
			data: { 
				code : code,
				cmd  : 'delete'
			},
			success: function(result) {}
		});
	}
}
</script>
