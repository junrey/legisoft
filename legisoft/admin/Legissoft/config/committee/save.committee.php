<?php session_start();
require_once("../../database/pdo.mysql.connection.legissoft.php");
require_once("../../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$detail = $GENERAL_FUNCTIONS->filterString($data['record']['detail']);
	$enable = $data['record']['enable']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {
		$code = $GENERAL_FUNCTIONS->generateCode('COM');
		
		$stmt = $conn->prepare("SELECT code FROM committee WHERE detail = :detail AND isdelete = '0'");
		$stmt->bindParam(':detail', $detail,  PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO committee SET code = :code, detail = :detail, enable = :enable, mod_date = NOW(), mod_by = :mod_by");
			$stmt->bindParam(':code',   $code,   PDO::PARAM_STR);
			$stmt->bindParam(':detail', $detail, PDO::PARAM_STR);
			$stmt->bindParam(':enable', $enable, PDO::PARAM_STR);
			$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
			$stmt->execute();
		
		} else {
			$status  = 'error';
			$message = 'Duplicate Committee!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$code = $data['recid'];
		
		$stmt = $conn->prepare("UPDATE committee SET detail = :detail, enable = :enable, mod_date = NOW(), mod_by = :mod_by WHERE code = :code");
		$stmt->bindParam(':code',   $code,   PDO::PARAM_STR);
		$stmt->bindParam(':detail', $detail, PDO::PARAM_STR);
		$stmt->bindParam(':enable', $enable, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
	
	}
} else if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete") {
	$code = $_POST['code'];
	
	if($code != "") {
		$stmt = $conn->prepare("UPDATE committee SET isdelete = '1', mod_date = NOW(), mod_by = :mod_by WHERE code = :code");
		$stmt->bindParam(':code', $code, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>