<div id="layout_document_type" style="position: absolute; width: 100%; height: calc(100% - 60px); padding:4px; background-color:#fff;"></div>
<script>
$().w2destroy('grid_document_type');
$().w2grid({ 
	name        : 'grid_document_type',
	header      : 'Document Type',
	multiSelect : false,
	show: {
		header        : true,
		toolbar       : true,
		footer        : true,
		lineNumbers   : true,
		toolbarAdd    : true,
		toolbarDelete : true,
		toolbarEdit   : true
	},      
	columns: [
		{ field: 'detail',       size: '50%', caption: 'Document Type' },
		{ field: 'place',        size: '20%', caption: 'Order' },
		{ field: 'members_show', size: '20%', caption: 'Show To Members' },
		{ field: 'enable',       size: '10%', caption: 'Active' },
		{ field: 'code',            hidden: true },	
		{ field: 'place_id',        hidden: true },		
		{ field: 'enable_id',       hidden: true },		
		{ field: 'members_show_id', hidden: true }
	],
	onAdd: function (event) {
		formAdd();
	},
	onEdit: function (event) {
		formEdit();
	},
	onDelete: function (event) {
		var code = w2ui['grid_document_type'].getSelection()[0];
		event.onComplete = function() {
			formDocumentTypeDelete(code);
		};
	},
	multiSearch: false,
	searches: [
		{ field: 'detail', caption: 'Document Type', type: 'text', operator : 'contains' }
	]
});
	
$(function () {	
	$().w2destroy('layout_document_type');
    $('#layout_document_type').w2layout({
        name: 'layout_document_type',
        panels: [
            { type: 'main', content: w2ui['grid_document_type'] }
        ]
    });
	
	loadDocumentType();
	
});
function openPopupForm(action) {
	$().w2destroy('form_document_type');
	$().w2form({
		name  : 'form_document_type',
		style : 'border: 0px; background-color: transparent;',
		url   : 'Legissoft/config/document_type/save.document_type.php',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Document Type:</label>' +
			'        <div>' +
			'           <input name="detail" type="text" maxlength="100" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Place:</label>' +
			'        <div>' +
			'            <input name="place" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Show To Members:</label>' +
			'        <div>' +
			'            <input name="members_show" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +	
			'    <div class="w2ui-field">' +
			'        <label>Active:</label>' +
			'        <div>' +
			'            <input name="enable" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +					
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Reset</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
			'</div>',
		fields: [
			{ field: 'detail', type: 'text', required: true },
			{ field: 'place', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: '1' },
						{ id: 2, text: '2' },
						{ id: 3, text: '3' },
						{ id: 4, text: '4' },
						{ id: 5, text: '5' },
						{ id: 6, text: '6' },
						{ id: 7, text: '7' },
						{ id: 8, text: '8' },
						{ id: 9, text: '9' },
						{ id: 10, text: '10' }
					]
				} 
			},
			{ field: 'members_show', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					],
					selected: { id: 1, text: 'Yes' }
				} 
			},
			{ field: 'enable', type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					],
					selected: { id: 1, text: 'Yes' }
				} 
			}
		],
		actions: {
			"save": function() { 
				this.save(function(data){ 
					loadDocumentType();
					w2popup.close();
				}); 
			},
			"reset": function() { this.clear(); }
		},
		onSave: function(event) {}, 
		onError: function(event) {},
		postData: {
			'action' : action
		}
	});
}

function formAdd() {
	openPopupForm('add');
	$().w2popup('open', {
		title   : 'Add New Document Type',
		body    : '<div id="popup_form_document_type" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 300, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_document_type').w2render('form_document_type');
			}
		},
		onClose: function () {
			w2ui['form_document_type'].clear();
		}
	});
}

function formEdit() {
	openPopupForm('edit');
	$().w2popup('open', {
		title   : 'Edit Document Type',
		body    : '<div id="popup_form_document_type" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 350, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_document_type').w2render('form_document_type');

				var sel    = w2ui['grid_document_type'].getSelection();
				var record = w2ui['grid_document_type'].get(sel[0]);
				
				w2ui['form_document_type'].recid = record['code'];
				w2ui['form_document_type'].record['code']   = record['code'];
				w2ui['form_document_type'].record['detail'] = record['detail'];
				$('input[name=place]').w2field().set({ id: record['place_id'], text: record['place'] });
				$('input[name=members_show]').w2field().set({ id: record['members_show_id'], text: record['members_show'] });
				$('input[name=enable]').w2field().set({ id: record['enable_id'], text: record['enable'] });
				$('button[name=reset]').prop('disabled', true);
				w2ui['form_document_type'].refresh();	
			}
		},
		onClose: function () {
			w2ui['form_document_type'].clear();
		}
	});
}

function loadDocumentType() {
	w2ui['grid_document_type'].clear();
	w2ui['grid_document_type'].load('Legissoft/config/document_type/json.document_type.php');
}

function formDocumentTypeDelete(code) {
	if(code != '') {
		$.ajax({
			url:'Legissoft/config/document_type/save.document_type.php',
			type:'POST',
			data: { 
				code : code,
				cmd  : 'delete'
			},
			success: function(result) {}
		});
	}
}
</script>
