<?php session_start();
require_once("../../database/pdo.mysql.connection.legissoft.php");

$status  = 'success';
$message = '';

$stmt = $conn->prepare("SELECT * FROM document_type WHERE isdelete = '0' ORDER BY place");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$members_show = ($row['members_show']) ? "Yes" : "No";
		$enable       = ($row['enable']) ? "Yes" : "No";
		
		echo '{ 
			"recid"           : "' . $row['code'] . '", 
			"code"            : "' . $row['code'] . '", 
			"detail"          : "' . htmlspecialchars($row['detail']) . '",
			"place_id"        : "' . $row['place'] . '",
			"place"           : "' . $row['place'] . '",
			"members_show_id" : "' . $row['members_show'] . '",
			"members_show"    : "' . $members_show . '",
			"enable_id"       : "' . $row['enable']. '",
			"enable"          : "' . $enable . '"
			
		
		}';

   }

echo ']}';

?>
