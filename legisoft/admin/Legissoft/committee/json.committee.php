<?php session_start();
require_once("../database/pdo.mysql.connection.php");
require_once("../../library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	$code = $data['selected'];
	
	if($data['cmd'] == 'delete') {
		$code = $code[0] ;
		
		$stmt = $conn->prepare("UPDATE committee SET deleted = '1' WHERE code = :code");
		$stmt->bindParam(':code', $code, PDO::PARAM_STR);
		$stmt->execute();
		
	}
}

$stmt = $conn->prepare("SELECT * FROM committee WHERE deleted = '0' ORDER BY detail");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$enable  = ($row['enable']) ? "Yes" : "No";
		 
		echo '{ 
			"recid"     : "' . $row['code'] . '", 
			"code"      : "' . $row['code'] . '", 
			"detail"    : "' . htmlspecialchars($row['detail']) . '", 
			"enable_id" : "' . $row['enable'] . '",
			"enable"    : "' . $enable . '"
		}';

   }

echo ']}';

?>
