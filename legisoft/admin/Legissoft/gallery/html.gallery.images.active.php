<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows        = array();
$active      = array();
$active_node = array();

$stmt2 = $conn->prepare("SELECT image_members_view, image_audience_view FROM config");
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

if($row2['image_members_view'] != "") {
	$active[$row2['image_members_view']] = $row2['image_members_view'];
}

if($row2['image_audience_view'] != "") {
	$active[$row2['image_audience_view']] = $row2['image_audience_view'];
}

$stmt3 = $conn->prepare("SELECT view_image FROM _user WHERE view_image <> '' AND type = '1' AND enable = '1' AND deleted = '0'");
$stmt3->execute();
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
	$active[$row3['view_image']] = $row3['view_image'];
}

$stmt = $conn->prepare("SELECT code, filename, mod_date FROM image_gallery WHERE deleted = '0' ORDER BY mod_date DESC, filename");
$stmt->execute();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		
	// gin dugangan ko prefix na 'a' and 'b' para wala conflict sa sidebar,
	// karon kung may duplicate ga console log error
	
	if (in_array($row['code'], $active)) {
		$active_node[$row['code']] = "{ id: '" . $row['code'] . "', text: \"" . htmlspecialchars($row['filename']) . "\", img: 'icon-page' },";
	}

}
echo "[";

echo "{ id: 'level-0', text: 'Active', img: 'icon-page', expanded: true, group: true, groupShowHide: false, collapsible: false },";

if(!empty($active_node)) {
	
	// kung the same man lng, isa lng ang e display
	foreach ($active_node as $val) {
		echo $val;
	}
	
}

echo "]";
?>
