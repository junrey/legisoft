<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

$stmt2 = $conn->prepare("SELECT image_members_view, image_audience_view FROM config");
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	
$stmt = $conn->prepare("SELECT * FROM image_gallery WHERE deleted = '0'");
$stmt->execute();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$image_members_view  = "";
	$image_audience_view = "";

	if($row2['image_members_view'] == $row['code']) {
		$image_members_view = "hide";
	}
	
	if($row2['image_audience_view'] == $row['code']) {
		$image_audience_view = "hide";
	}
	$rows[$row['code']]['code']     = $row['code'];
	$rows[$row['code']]['filename'] = $row['filename'];
	$rows[$row['code']]['image_members_view']  = $image_members_view;
	$rows[$row['code']]['image_audience_view'] = $image_audience_view;
	
}

echo json_encode(array('data' => $rows));
?>
