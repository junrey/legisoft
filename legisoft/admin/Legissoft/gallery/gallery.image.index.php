<style>
#layout_image_gallery .w2ui-field-helper {
	height: 27px;
}
#share_image {
	width:300px;
	height:25px !important;
}
#layout_image_gallery .w2ui-form {
	background-color:transparent;
	border:0;
}
#layout_image_gallery .card {
	display:inline-block;
	position:relative;
	vertical-align:top;
	width:200px;
	margin:10px;
	min-width:128px;
	box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);
	transition:0.3s;
	border-radius:5px;
}
#layout_image_gallery .card:hover {
    box-shadow:0 8px 16px 0 rgba(0,0,0,0.2);
}

#layout_image_gallery .card .container {
    padding:5px 0;
}
#layout_image_gallery .card img {
    border-radius:5px 5px 0 0;
	cursor:pointer;

}
#layout_image_gallery .card .tools.maximize {
	position:absolute;
	top:5px;
	right:28px;	
	width:20px;
	min-width:20px;
	padding:2px;
}
#layout_image_gallery .card .tools.delete {
	position:absolute;
	top:5px;
	right:2px;
	width:20px;
	min-width:20px;
	padding:2px;
}
#layout_image_gallery .card .container .title {
	text-align:center;
	font-size:11px;
	margin:5px;
	color:#888;
	text-overflow: ellipsis;
	white-space: nowrap;
  	overflow: hidden;
}

#layout_image_gallery .card.selected {
	 box-shadow:0 8px 20px 0 rgba(0,0,0,.8);
}
.image_members, .image_audience {
	width:85px;	
}

</style>
<div id="layout_image_gallery" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>
"use strict";
$(function () {	
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	$().w2destroy('layout_image_gallery');
    $('#layout_image_gallery').w2layout({
		name: 'layout_image_gallery',
		padding: 4,
		panels: [
			{ type: 'left', size: 220, style: 'background-color:#fff;', resizable: true },
			{ type: 'main', style: 'border: 1px solid #dfdfdf;',
				 toolbar: {
                    items: [
                        { type: 'html', style: 'width: 100%', id: 'item_image_upload', 
							html: 
								'<div id="form_image_gallery" style="width: 700px; height: 50px;">' +
								'	<div class="w2ui-field">' +
								'		<label>Upload Image:&nbsp;</label>' +
								'		<span><input name="share_image" id="share_image" placeholder="Click here to upload image" accept=".jpg, .jpeg, .png"></span>' +
								'		<button class="w2ui-btn" name="save">Upload</button>' +
								'		<button class="w2ui-btn" name="reset">Clear</button>' +
								'	</div>' +
								'</div>' 
						}
                    ]
                },
				content : 
				$().w2layout({
					name: 'layout_image_gallery_content',
					panels: [
						{ type: 'main' }
					]
				})
			}
		]
	});
		
	$('#form_image_gallery').w2form({ 
		name  : 'form_image_gallery',
		url   : 'Legissoft/gallery/save.gallery.image.php',
		fields : [
			 { name: 'share_image', type: 'file', required: true }
		],
		actions: {
			reset: function () {
				this.clear();
			},
			save: function() { 
				if($('input[name=share_image]').w2field().get() == '') {
					$('#form_image_gallery .w2ui-field-helper').addClass('w2ui-error');
					$('input[name=share_image]').w2tag('Required field', { position: 'bottom' })
				} else {
					this.save(function(data){ 
						w2ui['form_image_gallery'].clear();
						loadGalleryImages();
					});
				}
			}
		},
		onError: function(event) {
			event.preventDefault();
			$('#form_image_gallery .w2ui-field-helper').addClass('w2ui-error');
			$('input[name=share_image]').w2tag(event.message, { position: 'bottom' })
		}       
	});
		
	$('input[name=share_image]').w2field('file', { max : 1 });
	
	w2ui['layout_image_gallery'].content('left', 
		$().w2layout({
			name: 'layout_image_gallery_active',
			padding: 4,
			panels: [
				{ type: 'main', style: 'border: 1px solid #dfdfdf;', content:
					$().w2sidebar({
						name : 'sidebar_image',
						nodes: [],
						onClick: function(event) { 
							var file_id = event.target;
							selectGalleryImage(file_id);
						}
					})
				},
				{ type: 'bottom', size: '50%', resizable: true, content: 
					$().w2grid({ 
						name : 'grid_image_members',
						show : {
							toolbar      : true,
							toolbarColumns : false,
						},
						multiSearch: false,
						columns: [
							{ field: 'code', hidden: true },
							{ field: 'fullname', size: '77%', caption: 'Members' },
							{ field: 'button', size: '23%', caption: 'Show' }
						],
						searches : [
							{ field: 'fullname', caption: 'Members', operator : 'contains', type: 'text' }
						]   
					})
				}
			]
		})
	);
	
	loadGalleryImages();
});

function loadGalleryImages() {
	$.ajax({
		url:'Legissoft/gallery/json.gallery.images.php',
		type:'GET',
		success: function(result) {
			var data = '';
			var show_members_button  = "";
			var show_audience_button = "";
			
			$.each(result.data, function(key, val) {
				show_members_button  = '<button class="image_members w2ui-btn" id="image_members_' + val['code'] + '" onclick="showToMembers(\'' + val['code'] + '\');">Show To Members</button>';
				show_audience_button = '<button class="image_audience w2ui-btn" id="image_audience_' + val['code'] + '" onclick="showToAudience(\'' + val['code'] + '\');">Show To Audience</button>';
				
				if(val['image_members_view'] == 'hide') {
					show_members_button = '<button class="image_members w2ui-btn w2ui-btn-green" id="image_members_' + val['code'] + '" onclick="showToMembers(\'' + val['code'] + '\');">Hide From Members</button>';
				}
				
				if(val['image_audience_view'] == 'hide') {
					show_audience_button = '<button class="image_audience w2ui-btn w2ui-btn-green" id="image_audience_' + val['code'] + '" onclick="showToAudience(\'' + val['code'] + '\');">Hide From Audience</button>';
				}
				
			 	data = data +
				'<div id="card_' + val['code'] + '" class="card">' +
				'	<img src="Legissoft/image_gallery/thumbnail/' + val['filename'] + '" data-src="Legissoft/image_gallery/' + val['filename'] + '" onerror="if (this.src != \'Framework/images/error.png\') this.src = \'Framework/images/error.png\';" style="width:100%;" onclick="selectGalleryImage(\'' + val['code'] + '\')" />' +
				'	<button class="tools maximize w2ui-btn" onclick="viewGalleryImage(\'' + val['code'] + '\')"><i class="fa fa-window-maximize" aria-hidden="true"></i></button>' +
				'	<button class="tools delete w2ui-btn" onclick="deleteGalleryImage(\'' + val['code'] + '\');"><i class="fa fa-times" aria-hidden="true"></i></button>' +
				'	<div class="container">' +
				'		<p class="title">' + val['filename'] + '</p>' +
				'		<table style="width: 100%;">' +
				'			<tr>' +
				'				<td>' + show_members_button + '</td>' +
				'				<td>' + show_audience_button + '</td>' +
				'			</tr>' +
				'		</table>' +
				'	</div>' +
				'</div>';				
			});
			
			w2ui['layout_image_gallery_content'].content('main', '<div class="gallery">' + data + '</div>');
			
			getActiveGalleryImage();

		}
	});
}

function showToMembers(id) {
	if(id != '') {
		$.ajax({
			url:'Legissoft/gallery/save.gallery.image.php',
			type:'POST',
			data: { 
				code_members : id
			},
			success: function(result) {
				if(result['show_image_members'] == 'show') {
					$('.image_members').removeClass('w2ui-btn-green');
					$('#image_members_' + id).text('Show To Members');
					
				} else {
					$('.image_members').removeClass('w2ui-btn-green');
					$('.image_members').text('Show To Members');
					$('#image_members_' + id).addClass('w2ui-btn-green').text('Hide From Members');
	
				}
				getActiveGalleryImage();
				updateMembersScreenImage();
			}
		});
	}
}

function showToAudience(id) {
	if(id != '') {
		$.ajax({
			url:'Legissoft/gallery/save.gallery.image.php',
			type:'POST',
			data: { 
				code_audience : id
			},
			success: function(result) {
				if(result['show_image_audience'] == 'show') {
					$('.image_audience').removeClass('w2ui-btn-green');
					$('#image_audience_' + id).text('Show To Audience');
					
				} else {
					$('.image_audience').removeClass('w2ui-btn-green');
					$('.image_audience').text('Show To Audience');
					$('#image_audience_' + id).addClass('w2ui-btn-green').text('Hide From Audience');
	
				}
				
				getActiveGalleryImage();
				updateAudienceScreenImage();
			}
	});
	}
}

function deleteGalleryImage(id) {
	if(id != '') {
		w2confirm('Do you want to delete?').yes(
			function() { 
				$.ajax({
					url:'Legissoft/gallery/save.gallery.image.php',
					type:'POST',
					data: { 
						code_delete : id
					},
					success: function(result) {
						$('#card_' + id).fadeOut('slow');
						
						w2ui['sidebar_image'].remove(id);
						
						w2ui['grid_image_members'].clear();
						
						updateMembersScreenImage();
						updateAudienceScreenImage();
					}
				});
			}
		);
	}
}

function viewGalleryImage(id) {
	 w2popup.open({
        title   : $('#card_' + id + ' .title').text(),
		width   : $(window).width(),
		height  : $(window).height(),
		showMax : true,
        body    : '<img id="image_view" src="' + $('#card_' + id + ' img').data('src') + '" />'
    });	
}

function getActiveGalleryImage() {
	$.ajax({
		url  : 'Legissoft/gallery/html.gallery.images.active.php',
		type : 'GET',
		success: function(result) {
			var nd = []; 
			for (var i in w2ui['sidebar_image'].nodes) nd.push(w2ui['sidebar_image'].nodes[i].id);
			w2ui['sidebar_image'].remove.apply(w2ui['sidebar_image'], nd);

			w2ui['sidebar_image'].add(eval(result));
		}
	});
}

function getGalleryImageMembers(image_code, member_code, status) {
	// on - off specific viewing
	if(image_code != '') {
		$.ajax({
			url      :'Legissoft/gallery/json.gallery.image.show.members.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code        : image_code,
				member_code : member_code,
				status      : status
			},
			success: function( json ) {
				w2ui['grid_image_members'].clear();
				
				$.each(json.records, function(i, value) {
					if(value.status == 'hide') {
						w2ui['grid_image_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button, 'w2ui': { 'style': 'background-color: #C2F5B4; font-weight: bold;' } });
					} else {
						w2ui['grid_image_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button });
					}
				});
				
				if(member_code != '') {
					getActiveGalleryImage();
				}
			}
		});
	}
}

function selectGalleryImage(id) {
	$('#layout_image_gallery .card').removeClass('selected');
	$('#card_' + id).addClass('selected');
	
	w2ui['sidebar_image'].select(id);
	w2ui['sidebar_image'].scrollIntoView(id);
	
	getGalleryImageMembers(id, '', '');
}

function updateMembersScreenImage() {
	$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
}

function updateAudienceScreenImage() {
	$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
}
</script>