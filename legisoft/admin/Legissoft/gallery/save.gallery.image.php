<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");
require_once("../../framework/image-resizer/smart_resize_image.function.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$show_image_members  = '';
$show_image_audience = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$share_image = $data['record']['share_image'];
	
	// get the original filename
	$filename  = "";
	$file_type = "";
	$image     = "";
	if(isset($share_image[0]['name']) && trim($share_image[0]['name']) != "") {
		$code = $GENERAL_FUNCTIONS->generateCode('IMG');
		
		$image_file = $share_image[0]['name'];
		$file_type  = $share_image[0]['type'];
		$image      = $share_image[0]['content'];
		
		$stmt = $conn->prepare("SELECT code FROM image_gallery WHERE filename = :filename AND deleted = '0'");
		$stmt->bindParam(':filename', $image_file, PDO::PARAM_STR);
		$stmt->execute();
	
		if($stmt->rowCount() == 0) {
			$stmt = $conn->prepare("INSERT INTO image_gallery SET code = :code, filename = :filename, file_type = :file_type, mod_by = :mod_by, mod_date = NOW()");
			$stmt->bindParam(':code',      $code,       PDO::PARAM_STR);
			$stmt->bindParam(':filename',  $image_file, PDO::PARAM_STR);
			$stmt->bindParam(':file_type', $file_type,  PDO::PARAM_STR);
			$stmt->bindParam(':mod_by',    $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
			$stmt->execute();

			// image checking if exist or the input field is not empty
			if($image != "") {
				// image storing folder, make sure you indicate the right path
				$folder = "../image_gallery/";
				
				// creating a filename
				$filename   = $folder . $image_file;
				$thumbnail  = $folder . "thumbnail/" . $image_file; 
				$image_data = base64_decode($image);
		
				// creating a filename
				file_put_contents($filename, $image_data);
				smart_resize_image(null , $image_data, 200 , 200 , true , $thumbnail , false , false ,100 );
			}
			
		} else {
			$status = "error";
			$message = "Image already exist!";
		}
	}
	
} else if(isset($_POST['code_members']) && trim($_POST['code_members']) != "") {
	$code = trim($_POST['code_members']);
	
	$stmt = $conn->prepare("SELECT image_members_view FROM config");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($row['image_members_view'] == $code) {
		$image_members_view = "";
		$show_image_members = "show";
	
	} else {
		$image_members_view = $code;
		$show_image_members = "hide";
	}
	
	$stmt = $conn->prepare("UPDATE config SET image_members_view = :image_members_view");
	$stmt->bindParam(':image_members_view', $image_members_view, PDO::PARAM_STR);
	$stmt->execute();
	
} else if(isset($_POST['code_audience']) && trim($_POST['code_audience']) != "") {
	$code = trim($_POST['code_audience']);
	
	$stmt = $conn->prepare("SELECT image_audience_view FROM config");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($row['image_audience_view'] == $code) {
		$image_audience_view = "";
		$show_image_audience = "show";
	
	} else {
		$image_audience_view = $code;
		$show_image_audience = "hide";
	}
	
	$stmt = $conn->prepare("UPDATE config SET image_audience_view = :image_audience_view, image_audience_view_date = NOW()");
	$stmt->bindParam(':image_audience_view', $image_audience_view, PDO::PARAM_STR);
	$stmt->execute();
	
} else if(isset($_POST['code_delete']) && trim($_POST['code_delete']) != "") {
	$code = trim($_POST['code_delete']);
	
	$stmt = $conn->prepare("UPDATE image_gallery SET deleted = '1', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET image_members_view = '' WHERE image_members_view = :image_members_view");
	$stmt->bindParam(':image_members_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET image_audience_view = '' WHERE image_audience_view = :image_audience_view");
	$stmt->bindParam(':image_audience_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE _user SET view_image = '' WHERE view_image = :view_image");
	$stmt->bindParam(':view_image', $code, PDO::PARAM_STR);
	$stmt->execute();

	$rows['code'] = "";
	
}

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '",
	"show_image_members"  : "' . $show_image_members . '",
	"show_image_audience" : "' . $show_image_audience . '"
}';


?>