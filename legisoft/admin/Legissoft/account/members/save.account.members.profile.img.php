<?php session_start();
//header('Content-Type: application/json; charset=UTF-8');
require_once("../../database/pdo.mysql.connection.legissoft.php");
require_once("../../../library/general.functions.php");
require_once("../../../framework/image-resizer/smart_resize_image.function.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$rows = array();

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$member_profile = $data['record']['member_profile'];
	$username       = $data['username'];
	
	// get the original filename
	$image_file = "";
	$image_data = "";
	if(isset($member_profile[0]['name']) && trim($member_profile[0]['name']) != "") {
		$image_file = $member_profile[0]['name'];
		$image_data = $member_profile[0]['content'];
	}
	
	
		
	$stmt = $conn->prepare("UPDATE _user SET image_file = :image_file, mod_by = :mod_by, mod_date = NOW()  WHERE username = :username");
	$stmt->bindParam(':image_file', $image_file, PDO::PARAM_STR);
	$stmt->bindParam(':username',   $username,   PDO::PARAM_STR);
	$stmt->bindParam(':mod_by',     $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
	$stmt->execute();

	// image checking if exist or the input field is not empty
	if($image_data != "") {
		// image storing folder, make sure you indicate the right path
		$folder = "../../user_image/"; 

		// creating a filename
		$filename  = $folder . $image_file;
		$thumbnail = $folder . "thumbnail/" . $image_file; 
		$profile   = $folder . "profile/" . $image_file; 
		$image_data = base64_decode($image_data);
		
		file_put_contents($filename, $image_data);
		smart_resize_image(null, $image_data, 250, 250, false, $thumbnail, false, false, 100 );
		smart_resize_image(null, $image_data, 80, 80, false, $profile, false, false, 100 );
		
		$image_file = $thumbnail;
	}
}

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '",
	"image_file" : "' . $image_file . '"
}';
//echo json_encode(array('data' => $rows));

?>