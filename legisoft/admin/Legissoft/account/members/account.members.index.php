<style>
.panel_contact_profile {
	text-align: center;
	padding-top: 0;
	height:100%;
	padding-top:40px;
}
.panel_contact_profile .contact_header {
	width: 100%;
	height: 165px;
	position: absolute;
	top: 0;
	z-index: -1;
	background-color: #2391dd;
}
.panel_contact_profile p {
	font-size:28px;
}
.panel_contact_profile p span {
	font-size:18px;
	color:#888;
}
.panel_contact_profile .profile_image {
	display:none;
	height: 250px;
	width: 250px;
	border-radius: 50%;
	border: 4px solid white;
	background-color: white;
}
.panel_contact_profile .w2ui-list .w2ui-enum-placeholder {
  	float:left;
   	display:block;
}
.panel_contact_profile .w2ui-multi-items {
	left:0;
}
</style>
<div id="layout_account_members" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>

"use strict";

$().w2grid({ 
	name        : 'grid_account_members',
	header      : 'User Account',
	multiSelect : false,
	multiSearch : false,
	show: {
		header        : true,
		toolbar       : true,
		footer        : true,
		lineNumbers   : true,
		toolbarAdd    : true,
		toolbarDelete : true,
		toolbarEdit   : true
	},      
	columns: [
		{ field: 'username', size: '10%', caption: 'Username' },
		{ field: 'fname',    size: '20%', caption: 'First Name' },
		{ field: 'lname',    size: '20%', caption: 'Last Name' },
		{ field: 'mname',    size: '20%', caption: 'Middle Name' },
		{ field: 'title',    size: '20%', caption: 'Title' },
		{ field: 'type',     size: '10%', caption: 'Type' },
		{ field: 'voter',    size: '10%', caption: 'Voter' },
		{ field: 'enable',   size: '10%', caption: 'Active' },
		{ field: 'enable_id',  hidden: true },
		{ field: 'type_id',    hidden: true },
		{ field: 'voter_id',   hidden: true },
		{ field: 'image_file', hidden: true }
		
	],
	onAdd: function (event) {
		formAdd();
	},
	onEdit: function (event) {
		formEdit();
	},
	onDelete: function (event) {
		var username = w2ui['grid_account_members'].getSelection()[0];
		event.onComplete = function() {
			formDelete(username);
		};
	},
	onSelect: function(event) {
		event.onComplete = function () {
			var sel        = w2ui['grid_account_members'].getSelection();
			var name       = w2ui['grid_account_members'].get(sel[0])['fname'] + ' ' + w2ui['grid_account_members'].get(sel[0])['lname'];
			var title      = w2ui['grid_account_members'].get(sel[0])['title'];
			var username   = w2ui['grid_account_members'].get(sel[0])['username'];
			var image_file = w2ui['grid_account_members'].get(sel[0])['image_file'];
				
			w2ui['layout_account_members'].content('right',
				'<div class="panel_contact_profile">' +
				'	<div class="contact_header"></div>' +
				'	<div>' +
				'		<div style="height:250px;"><img id="profile_image_' + username + '" onload="$(this).show();" class="profile_image" onerror="if (this.src != \'framework/images/error.png\') this.src = \'framework/images/error.png\';" src="Legissoft/user_image/thumbnail/' + image_file + '" /></div>' +
				'		<p class="profile_name">' + name + '<br /><span>' + title + '</span></p>' +
				'		<div id="form_member_profile_pic" style="width: 400px; margin: 0 auto;">' +
				'			<div class="w2ui-page page-0">' +
				'				<div class="w2ui-field">' +
				'					<label>Upload Image:</label>' +
				'					<div><input name="member_profile" id="file" placeholder="Click here to upload image" accept=".jpg, .jpeg, .png" style="width: 200px;"></div>' +
				'				</div>' +
				'			</div>' +
				'			<div class="w2ui-buttons">' +
				'				<button class="w2ui-btn" name="reset">Clear</button>' +
				'				<button class="w2ui-btn" name="save">Upload</button>' +
				'			</div>' +
				'		</div>' +
				'	</div>' +
				'</div>'
			);
			
			$().w2destroy('form_member_profile_pic');
			$('#form_member_profile_pic').w2form({ 
				name  : 'form_member_profile_pic',
				url   : 'Legissoft/account/members/save.account.members.profile.img.php',
				fields : [
					 { name: 'member_profile', type: 'file' }
				],
				actions: {
					reset: function () {
						this.clear();
					},
					save: function() { 
						this.save(function(data){ 
							$('#profile_image_' + username).attr('src', 'Legissoft/user_image/thumbnail/' + data.image_file);
							w2ui['form_member_profile_pic'].clear();
							loadAccountMembers();
						}); 
					}
				}, 
				postData: {
					username : username
				}
			});
			
			$('input[name=member_profile]').w2field('file', { max : 1 });
		};
	}, 
	onUnselect: function(event) {
		$().w2destroy('form_member_profile_pic');
		w2ui['layout_account_members'].content('right', '');
    },   
	searches: [
		{ field: 'username', caption: 'Username',    type: 'text' },
		{ field: 'fname',    caption: 'First Name',  type: 'text' },
		{ field: 'lname',    caption: 'Last Name',   type: 'text' },
		{ field: 'mname',    caption: 'Middle Name', type: 'text' }
	]
});


$(function () {	
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	$().w2destroy('layout_account_members');
    $('#layout_account_members').w2layout({
		name: 'layout_account_members',
		padding: 4,
		panels: [
			{ type: 'main', overflow: 'hidden'},
			{ type: 'right', style: 'border: 1px solid #dfdfdf; background-color: #edf1f6', size: '450px', resizable: true }
		]
	});
	
	w2ui['layout_account_members'].content('main', w2ui['grid_account_members']);
	
	loadAccountMembers();
});

function loadAccountMembers() {
	w2ui['grid_account_members'].clear();
	w2ui['grid_account_members'].load('Legissoft/account/members/json.account.members.php');
}
function openPopupForm(action) {
	$().w2destroy('form_account_members');
	$().w2form({
		name  : 'form_account_members',
		style : 'border: 0px; background-color: transparent;',
		url   : 'Legissoft/account/members/save.account.members.php',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Username:</label>' +
			'        <div>' +
			'           <input name="username" type="text" maxlength="20" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Password:</label>' +
			'        <div>' +
			'           <input name="password" type="password" maxlength="200" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>First Name:</label>' +
			'        <div>' +
			'           <input name="fname" type="text" maxlength="30" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Last Name:</label>' +
			'        <div>' +
			'           <input name="lname" type="text" maxlength="30" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Middle Name:</label>' +
			'        <div>' +
			'           <input name="mname" type="text" maxlength="30" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Title:</label>' +
			'        <div>' +
			'           <input name="title" type="text" maxlength="100" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Type:</label>' +
			'        <div>' +
			'            <input name="type" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Voter:</label>' +
			'        <div>' +
			'            <input name="voter" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'    <div class="w2ui-field">' +
			'        <label>Active:</label>' +
			'        <div>' +
			'            <input name="enable" type="list" style="width: 250px"/>' +
			'        </div>' +
			'    </div>' +
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Reset</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Save</button>' +
			'</div>',
		fields: [	
			{ field: 'username', type: 'text',     required: true },
			{ field: 'password', type: 'password', required: true },
			{ field: 'fname',    type: 'text',     required: true },
			{ field: 'lname',    type: 'text',     required: true },
			{ field: 'mname',    type: 'text' },
			{ field: 'title',    type: 'text' },
			{ field: 'type',     type: 'list',     required: true, 
				options: {
					items: [
						{ id: 1, text: 'Member' },
						{ id: 2, text: 'Admin' },
						{ id: 3, text: 'Audience' }
					]
				} 
			},
			{ field: 'voter',    type: 'list',     required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					]
				} 
			},
			{ field: 'enable',     type: 'list', required: true, 
				options: {
					items: [
						{ id: 1, text: 'Yes' },
						{ id: 0, text: 'No' }
					]
				} 
			}
		],
		actions: {
			"save": function() { 
				this.save(function(data){ 
					loadAccountMembers();
					w2popup.close();
				}); 
			},
			"reset": function() { this.clear(); }
		},
		onSave: function(event) {}, 
		onError: function(event) {},
		onChange: function (event) {
			if(event.target == 'type') {
				if(event.value_new.id == 2 || event.value_new.id == 3) {
					$('input[name=voter]').w2field().set({ id: 0, text: 'No' });
					$('input[name=voter]').prop('disabled', true);
					
				} else {
					$('input[name=voter]').w2field().set({ id: 1, text: 'Yes' });
					$('input[name=voter]').prop('disabled', false);
					
				}
			}
		},
		postData: {
			'action' : action
		}
	});
}

function formAdd() {
	openPopupForm('add');
	$().w2popup('open', {
		title   : 'Add New Member',
		body    : '<div id="popup_form_account_members" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 450, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_account_members').w2render('form_account_members');
				$('input[name=type]').w2field().set({ id: 1, text: 'Member' });
				$('input[name=voter]').w2field().set({ id: 1, text: 'Yes' });
				$('input[name=enable]').w2field().set({ id: 1, text: 'Yes' });
				w2ui['form_account_members'].refresh();
			}
		},
		onClose: function () {
			w2ui['form_account_members'].clear();
		}
	});
}

function formEdit() {
	openPopupForm('edit');
	$().w2popup('open', {
		title   : 'Edit Member',
		body    : '<div id="popup_form_account_members" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 530,
		height  : 450, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_account_members').w2render('form_account_members');

				var sel    = w2ui['grid_account_members'].getSelection();
				var record = w2ui['grid_account_members'].get(sel[0]);
				
				w2ui['form_account_members'].record['username'] = record['username']; 
				$('input[name=password]').prop("placeholder", '*******');
				w2ui['form_account_members'].record['fname'] = record['fname']; 
				w2ui['form_account_members'].record['lname'] = record['lname']; 
				w2ui['form_account_members'].record['mname'] = record['mname'];
				w2ui['form_account_members'].record['title'] = record['title'];
				$('input[name=type]').w2field().set({ id: record['type_id'], text: record['type'] });
				$('input[name=voter]').w2field().set({ id: record['voter_id'], text: record['voter'] });
				$('input[name=enable]').w2field().set({ id: record['enable_id'], text: record['enable'] });
				w2ui['form_account_members'].set('password', { required: false }); 
				$('input[name=username]').prop('disabled', true);
				$('button[name=reset]').prop('disabled', true);
				w2ui['form_account_members'].refresh();	
			}
		},
		onClose: function () {
			w2ui['form_account_members'].clear();
		}
	});
}

function formDelete(username) {
	if(username != '') {
		$.ajax({
			url:'Legissoft/account/members/save.account.members.php',
			type:'POST',
			data: { 
				username : username,
				cmd      : 'delete'
			},
			success: function(result) {
				//loadAccountMembers();
			}
		});
	}
}
</script>
