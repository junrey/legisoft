<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../../database/pdo.mysql.connection.legissoft.php");

$status  = 'success';
$message = '';

$stmt = $conn->prepare("SELECT * FROM _user WHERE deleted = '0' ORDER BY FIELD(type, '1', '2', '3') DESC, lname, fname");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$voter  = ($row['voter']) ? "Yes" : "No";
		$enable = ($row['enable']) ? "Yes" : "No";
		
		if($row['type'] == 1) {
			$type = "Member";
		} else if($row['type'] == 2) {
			$type = "Admin";
		} else if($row['type'] == 3) {
			$type = "Audience";
		}
		 
		echo '{ 
			"recid"      : "' . $row['username'] . '", 
			"username"   : "' . $row['username'] . '", 
			"fname"      : "' . htmlspecialchars($row['fname']) . '", 
			"lname"      : "' . htmlspecialchars($row['lname']) . '", 
			"mname"      : "' . htmlspecialchars($row['mname']) . '", 
			"title"      : "' . htmlspecialchars($row['title']) . '", 
			"type_id"    : "' . $row['type'] . '",
			"type"       : "' . $type . '",
			"voter_id"   : "' . $row['voter'] . '",
			"voter"      : "' . $voter . '",
			"enable_id"  : "' . $row['enable'] . '",
			"enable"     : "' . $enable . '",
			"image_file" : "' . $row['image_file'] . '"
		}';

   }

echo ']}';

?>
