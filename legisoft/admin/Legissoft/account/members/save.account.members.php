<?php session_start();
require_once("../../database/pdo.mysql.connection.legissoft.php");
require_once("../../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

if(isset($_POST['request'])) {
	$data = json_decode($_POST['request'], true);
	
	$username = $GENERAL_FUNCTIONS->filterString($data['record']['username']);
	$password = $GENERAL_FUNCTIONS->filterString($data['record']['password']);
	$fname    = $GENERAL_FUNCTIONS->filterString($data['record']['fname']);
	$lname    = $GENERAL_FUNCTIONS->filterString($data['record']['lname']);
	$mname    = $GENERAL_FUNCTIONS->filterString($data['record']['mname']);
	$mname    = $GENERAL_FUNCTIONS->filterString($data['record']['mname']);
	$title    = $GENERAL_FUNCTIONS->filterString($data['record']['title']);
	$type     = $data['record']['type']['id'];
	$voter    = $data['record']['voter']['id'];
	$enable   = $data['record']['enable']['id'];

	if($data['cmd'] == 'save' && $data['action'] == 'add') {
		$stmt2 = $conn->prepare("SELECT account_limit FROM config");
		$stmt2->execute();
		$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
		
		$stmt3 = $conn->prepare("SELECT COUNT(username) AS total FROM _user WHERE deleted = '0'");
		$stmt3->execute();
		$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);
		
		if($row3['total'] < $row2['account_limit']) {
		
			$user_id  = $GENERAL_FUNCTIONS->generateCode('A');
			$password = $GENERAL_FUNCTIONS->passwordHash($password);
		
			$stmt = $conn->prepare("SELECT username FROM _user WHERE username = :username AND deleted = '0'");
			$stmt->bindParam(':username', $username,  PDO::PARAM_STR);
			$stmt->execute();
		
			if($stmt->rowCount() == 0) {
				$stmt = $conn->prepare("INSERT INTO _user SET user_id = :user_id, username = :username, fname = :fname, lname = :lname, mname = :mname, title = :title, type = :type, voter = :voter, password = :password, enable = :enable, mod_date = NOW(), mod_by = :mod_by");
				$stmt->bindParam(':user_id',    $user_id,    PDO::PARAM_STR);
				$stmt->bindParam(':username',   $username,   PDO::PARAM_STR);
				$stmt->bindParam(':fname',      $fname,      PDO::PARAM_STR);
				$stmt->bindParam(':lname',      $lname,      PDO::PARAM_STR);
				$stmt->bindParam(':mname',      $mname,      PDO::PARAM_STR);
				$stmt->bindParam(':title',      $title,      PDO::PARAM_STR);
				$stmt->bindParam(':type',       $type,       PDO::PARAM_STR);
				$stmt->bindParam(':voter',      $voter,      PDO::PARAM_STR);
				$stmt->bindParam(':password',   $password,   PDO::PARAM_STR);
				$stmt->bindParam(':enable',     $enable,     PDO::PARAM_STR);
				$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
				$stmt->execute();
			
			} else {
				$status  = 'error';
				$message = 'Duplicate Username!';
			}
		} else {
			$status  = 'error';
			$message = 'User Account limit has been exceeded!';
		}
	} else if($data['cmd'] == 'save' && $data['action'] == 'edit') {
		$stmt = $conn->prepare("UPDATE _user SET fname = :fname, lname = :lname, mname = :mname, voter = :voter, title = :title, type = :type, enable = :enable, mod_date = NOW(), mod_by = :mod_by WHERE username = :username");
		$stmt->bindParam(':username',   $username,   PDO::PARAM_STR);
		$stmt->bindParam(':fname',      $fname,      PDO::PARAM_STR);
		$stmt->bindParam(':lname',      $lname,      PDO::PARAM_STR);
		$stmt->bindParam(':mname',      $mname,      PDO::PARAM_STR);
		$stmt->bindParam(':title',      $title,      PDO::PARAM_STR);
		$stmt->bindParam(':type',       $type,       PDO::PARAM_STR);
		$stmt->bindParam(':voter',      $voter,      PDO::PARAM_STR);
		$stmt->bindParam(':enable',     $enable,     PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
		
		if($password != '') {
			$password = $GENERAL_FUNCTIONS->passwordHash($password);
			
			$stmt = $conn->prepare("UPDATE _user SET password = :password WHERE username = :username");
			$stmt->bindParam(':username', $username,   PDO::PARAM_STR);
			$stmt->bindParam(':password', $password, PDO::PARAM_STR);
			$stmt->execute();
		}
	
	}
} else if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete") {
	$username = trim($_POST['username']);
	
	if($username != "") {
		$stmt = $conn->prepare("UPDATE _user SET enable = '0', deleted = '1', mod_date = NOW(), mod_by = :mod_by WHERE username = :username");
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR); 
		$stmt->execute();
	}
}


echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '"
}';

?>