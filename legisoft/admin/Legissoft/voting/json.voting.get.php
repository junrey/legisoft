<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");

$rows = array();

if(isset($_POST['code']) && trim($_POST['code']) != "") {
	$code = trim($_POST['code']);
	
	$stmt = $conn->prepare("SELECT * FROM vote_header WHERE enable = '1' AND code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$stmt2 = $conn->prepare("SELECT vote_members_view FROM config WHERE vote_members_view = :vote_members_view");
		$stmt2->bindParam(':vote_members_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['vote_members_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['vote_members_view'] = $row2['vote_members_view'];
		}
		
		$stmt2 = $conn->prepare("SELECT vote_audience_view FROM config WHERE vote_audience_view = :vote_audience_view");
		$stmt2->bindParam(':vote_audience_view', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['vote_audience_view'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['vote_audience_view'] = $row2['vote_audience_view'];
		}
		
		$stmt2 = $conn->prepare("SELECT vote_code FROM config WHERE vote_code = :vote_code");
		$stmt2->bindParam(':vote_code', $code, PDO::PARAM_STR);
		$stmt2->execute();
		
		$rows['vote_open'] = "";
		if($stmt2->rowCount() > 0) {
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
			$rows['vote_open'] = $row2['vote_code'];
		}
		
		$rows['code']        = $row['code'];
		$rows['title']       = $row['title'];
		$rows['content']     = $row['content'];
		$rows['voting_date'] = date("m/d/Y", strtotime($row['voting_date']));
	}
}

echo json_encode(array('data' => $rows));

?>