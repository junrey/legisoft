<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");

$status  = 'success';
$message = '';

$code = "";
if(isset($_GET['code']) && trim($_GET['code']) != "") {
	$code = trim($_GET['code']);
}

$vote_item = array();
$stmt = $conn->prepare("SELECT detail, value FROM vote_item WHERE enable = '1'");  
$stmt->execute();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$vote_item[$row['value']] = $row['detail'];
}

$stmt = $conn->prepare("SELECT username, fname, lname, title, image_file FROM _user WHERE enable = '1' AND voter = '1' AND deleted = '0' ORDER BY lname, fname");
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$vote        = "";
		$vote_detail = "";
		
		if($code != "") {
			$stmt2 = $conn->prepare("SELECT vote FROM vote WHERE code = :code AND username = :username");
			$stmt2->bindParam(':code',     $code,            PDO::PARAM_STR);
			$stmt2->bindParam(':username', $row['username'], PDO::PARAM_STR);
			$stmt2->execute();
			
			if($stmt2->rowCount() > 0) {
				$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
				$vote = $row2['vote'];
				
				if($vote > 0) {
					$vote_detail = $vote_item[$vote];
				}
			}
		}
		 
		echo '{ 
			"recid"       : "' . $row['username'] . '", 
			"username"    : "' . $row['username'] . '", 
			"fname"       : "' . htmlspecialchars($row['fname']) . '",
			"lname"       : "' . htmlspecialchars($row['lname']) . '",
			"title"       : "' . htmlspecialchars($row['title']) . '",
			"image_file"  : "' . $row['image_file'] . '",
			"vote"        : "' . $vote . '",
			"vote_detail" : "' . $vote_detail . '"
		}';
   }

echo ']}';

?>
