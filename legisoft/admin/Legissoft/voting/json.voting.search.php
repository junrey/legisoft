<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$status  = 'success';
$message = '';

$search = "";
if(isset($_GET['request'])) {
	$data = json_decode($_GET['request'], true);
	if(isset($data["search"])) {
		$search = $data["search"][0]["value"];
	}
}


$stmt = $conn->prepare("SELECT code, title, voting_date FROM vote_header WHERE title LIKE :search AND enable = '1' ORDER BY YEAR(voting_date) DESC, MONTH(voting_date) DESC, WEEK(voting_date) ASC, title");
$stmt->bindValue(':search', '%' . $search . '%', PDO::PARAM_STR);
$stmt->execute();

$total_records = $stmt->rowCount();

echo '{ 
	"status"  : "' . $status . '", 
	"message" : "' . $message . '", 
	"total"   : "' . $total_records . '",
	"records" : [';
	$cnt = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$cnt++; if($cnt > 1) { echo ","; }
		
		$stmt2 = $conn->prepare("SELECT count(username) AS total, vote, mod_date, mod_by FROM vote WHERE code = :vote_code AND deleted = '0' GROUP BY vote");
		$stmt2->bindParam(':vote_code', $row['code'], PDO::PARAM_STR);
		$stmt2->execute();
		
		$vote_yes     = "";
		$vote_no      = "";
		$vote_abstain = "";
		
		while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
			if($row2['vote'] == 1) {
				$vote_yes = $row2['total'];
				
			} else if($row2['vote'] == 2) {
				$vote_no = $row2['total'];
				
			} else if($row2['vote'] == 3) {
				$vote_abstain = $row2['total'];
				
			}
		}

		echo '{ 
			"recid"        : "' . $row['code'] . '", 
			"vote_code"    : "' . $row['code'] . '", 
			"title"        : "' . $GENERAL_FUNCTIONS->cleanString($row['title']) . '",
			"vote_yes"     : "' . $vote_yes . '",
			"vote_no"      : "' . $vote_no . '",
			"vote_abstain" : "' . $vote_abstain . '",
			"mod_date"     : "' . date('m/d/Y', strtotime($row2['mod_date'])) . '",
			"mod_by"       : "' . $GENERAL_FUNCTIONS->cleanString($row2['mod_by']) . '"
		}';

   }

echo ']}';

?>
