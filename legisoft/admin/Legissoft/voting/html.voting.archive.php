<?php session_start();
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows        = array();
$active      = array();
$active_node = array();

$stmt2 = $conn->prepare("SELECT vote_code, vote_members_view, vote_audience_view FROM config");
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

if($row2['vote_code'] != "") {
	$active[$row2['vote_code']] = $row2['vote_code'];
}

if($row2['vote_members_view'] != "") {
	$active[$row2['vote_members_view']] = $row2['vote_members_view'];
}

if($row2['vote_audience_view'] != "") {
	$active[$row2['vote_audience_view']] = $row2['vote_audience_view'];
}

$stmt3 = $conn->prepare("SELECT view_voting FROM _user WHERE view_voting <> '' AND type = '1' AND enable = '1' AND deleted = '0'");
$stmt3->execute();
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
	$active[$row3['view_voting']] = $row3['view_voting'];
}

$stmt = $conn->prepare("SELECT code, title, voting_date, DATE(voting_date) AS parse_date FROM vote_header WHERE enable = '1' ORDER BY YEAR(voting_date) DESC, MONTH(voting_date) DESC, WEEK(voting_date) ASC, title");
$stmt->execute();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$time = strtotime($row['voting_date']);
	
	$temp = array();
	$temp['code']  = $row['code'];
	$temp['title'] = $row['title'];
	
	$rows[date('Y', $time)]['label'] = date('Y', $time);
	$rows[date('Y', $time)]['month'][date('F', $time)]['label'] = date('F', $time);
	$rows[date('Y', $time)]['month'][date('F', $time)]['week'][$GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['label'] = $GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday");
	$rows[date('Y', $time)]['month'][date('F', $time)]['week'][$GENERAL_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['file'][$row['code']] = $temp;
	
	
	// gin dugangan ko prefix na 'a' and 'b' para wala conflict sa sidebar,
	// karon kung may duplicate ga console log error
	
	if (in_array($row['code'], $active)) {
		$active_node[$row['code']] = "{ id: 'b" . $row['code'] . "', text: \"" . htmlspecialchars($row['title']) . "\", img: 'icon-page' },";
	}

}
echo "[";

	echo "{ id: 'level-0', text: 'Active', img: 'icon-page', expanded: true, group: true, groupShowHide: false, collapsible: false },";

if(!empty($active_node)) {
	
	// kung the same man lng, isa lng ang e display
	foreach ($active_node as $val) {
		echo $val;
	}
	
}

echo "{ id: 'level-1', text: 'Archive', img: 'icon-folder', expanded: true, group: true, groupShowHide: false },";
foreach($rows as $val) {
	echo "{ id: '" . $val['label'] . "', text: '" . $val['label'] . "', img: 'icon-folder', nodes: [";
		foreach($val['month'] as $val2) {
			echo "{ id: '" . $val['label'] . $val2['label'] . "', text: '" . $val2['label'] . "', img: 'icon-folder', nodes: [";
				foreach($val2['week'] as $val3) {
					echo "{ id: '" . $val['label'] . $val2['label'] . $val3['label'] . "', text: 'Week " . $val3['label'] . "', img: 'icon-folder', nodes: [";
					foreach($val3['file'] as $val4) {
						echo "{ id: 'a" . $val4['code'] . "', text: \"" . htmlspecialchars($val4['title']) . "\", img: 'icon-page'},";
					}
					echo "]},";
				}
			echo "]},";	
		}
	echo "]}, ";
}
echo "]";
?>
