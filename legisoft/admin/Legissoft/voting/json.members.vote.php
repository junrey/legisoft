<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();

if(isset($_POST['code']) && trim($_POST['username']) != "") {
	$code     = trim($_POST['code']);
	$username = trim($_POST['username']);
	$vote     = trim($_POST['vote']);
	$cmd      = trim($_POST['cmd']);
	
	if($vote != "" && $cmd == "save") {
		$stmt2 = $conn->prepare("UPDATE vote_header SET mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
		$stmt2->bindParam(':code',   $code, PDO::PARAM_STR);
		$stmt2->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt2->execute();

		$stmt = $conn->prepare("SELECT vote FROM vote WHERE code = :code AND username = :username AND enable = '1' AND deleted = '0'");
		$stmt->bindParam(':code',    $code,      PDO::PARAM_STR);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			if($vote == $row['vote']) {
				$vote = "";
			}
			
			$stmt = $conn->prepare("UPDATE vote SET vote = :vote, mod_by = :mod_by, mod_date = NOW() WHERE code = :code AND username = :username");
			$stmt->bindParam(':code',     $code,                                         PDO::PARAM_STR);
			$stmt->bindParam(':username', $username,                                     PDO::PARAM_STR);
			$stmt->bindParam(':vote',     $vote,                                         PDO::PARAM_STR);
			$stmt->bindParam(':mod_by',   $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
			$stmt->execute();
			
		} else {
			$stmt = $conn->prepare("INSERT INTO vote SET code = :code, username = :username, vote = :vote, mod_by = :mod_by, mod_date = NOW()");
			$stmt->bindParam(':code',     $code,                                         PDO::PARAM_STR);
			$stmt->bindParam(':username', $username,                                     PDO::PARAM_STR);
			$stmt->bindParam(':vote',     $vote,                                         PDO::PARAM_STR);
			$stmt->bindParam(':mod_by',   $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
			$stmt->execute();
		
		}	
	}
	
	
	$rows['code'] = $code;
	$rows['vote'] = $vote;
	
	$choices = array();
	$stmt = $conn->prepare("SELECT detail, value, place FROM vote_item WHERE enable = '1' AND deleted = '0' ORDER BY place");  
	$stmt->execute();
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$choices[$row['detail']]['detail'] = $row['detail'];
		$choices[$row['detail']]['value']  = $row['value'];
	}
	
	$rows['choices'] = $choices;
}

echo json_encode(array('data' => $rows));
?>
