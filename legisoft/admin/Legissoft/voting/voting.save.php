<?php session_start();
header('Content-Type: application/json; charset=UTF-8');
require_once("../database/pdo.mysql.connection.legissoft.php");
require_once("../../library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

$rows = array();

if(isset($_POST['code']) && trim($_POST['title']) != "") {
	$content     = trim($_POST['content']);
	$code        = trim($_POST['code']);
	$title       = trim($_POST['title']);
	$voting_date = date("Y-m-d", strtotime(trim($_POST['date'])));
		
	if(trim($_POST['code']) == "") {
		$today = date("mdY") . date("His");
		$code  = "JDX" . rand(0,9) . rand(0,9) . $today; 
		
		$stmt = $conn->prepare("INSERT INTO vote_header SET code = :code, title = :title, content = :content, voting_date = :voting_date, created_by = :created_by, created_date = NOW(), mod_by = :mod_by, mod_date = NOW()");
		$stmt->bindParam(':code',        $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',       $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':content',     $content,                                      PDO::PARAM_STR);
		$stmt->bindParam(':voting_date', $voting_date,                                  PDO::PARAM_STR);
		$stmt->bindParam(':created_by',  $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
				
	} else {
		$code = trim($_POST['code']);
		
		$stmt = $conn->prepare("UPDATE vote_header SET title = :title, content = :content, voting_date = :voting_date, mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
		$stmt->bindParam(':code',        $code,                                         PDO::PARAM_STR);
		$stmt->bindParam(':title',       $title,                                        PDO::PARAM_STR);
		$stmt->bindParam(':content',     $content,                                      PDO::PARAM_STR);
		$stmt->bindParam(':voting_date', $voting_date,                                  PDO::PARAM_STR);
		$stmt->bindParam(':mod_by',      $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
		$stmt->execute();
	}

	$rows['code'] = $code;

} else if(isset($_POST['code_open']) && trim($_POST['code_open']) != "") {
	$code = trim($_POST['code_open']);
	
	$stmt = $conn->prepare("SELECT vote_code FROM config WHERE vote_code = :vote_code");
	$stmt->bindParam(':vote_code', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET vote_code = ''");
		$stmt->execute();
		$rows['status'] = "show";
		
	} else {
		$stmt = $conn->prepare("UPDATE config SET vote_code = :vote_code");
		$stmt->bindParam(':vote_code', $code, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}
	
	$rows['code'] = $code;
	
} else if(isset($_POST['delete_code']) && trim($_POST['delete_code']) != "") {
	$code = trim($_POST['delete_code']);
	
	$stmt = $conn->prepare("UPDATE vote_header SET enable = '0', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE vote SET enable = '0', mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->bindParam(':mod_by', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET vote_members_view = '' WHERE vote_members_view = :vote_members_view");
	$stmt->bindParam(':vote_members_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET vote_audience_view = '' WHERE vote_audience_view = :vote_audience_view");
	$stmt->bindParam(':vote_audience_view', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE config SET vote_code = '' WHERE vote_code = :vote_code");
	$stmt->bindParam(':vote_code', $code, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("UPDATE _user SET view_voting = '' WHERE view_voting = :view_voting");
	$stmt->bindParam(':view_voting', $code, PDO::PARAM_STR);
	$stmt->execute();

	$rows['code'] = "";


} else if(isset($_POST['code_members']) && trim($_POST['code_members']) != "") {
	$vote_members_view = trim($_POST['code_members']);
	
	$stmt = $conn->prepare("SELECT vote_members_view FROM config WHERE vote_members_view = :vote_members_view");
	$stmt->bindParam(':vote_members_view', $vote_members_view, PDO::PARAM_STR);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET vote_members_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
	
	} else {
		$stmt = $conn->prepare("UPDATE config SET vote_members_view = :vote_members_view");
		$stmt->bindParam(':vote_members_view', $vote_members_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}

	$rows['code'] = $vote_members_view;
	
} else if(isset($_POST['code_audience']) && trim($_POST['code_audience']) != "") {
	$vote_audience_view = trim($_POST['code_audience']);
	
	$stmt = $conn->prepare("SELECT vote_audience_view FROM config WHERE vote_audience_view = :vote_audience_view");
	$stmt->bindParam(':vote_audience_view', $vote_audience_view, PDO::PARAM_STR);
	$stmt->execute();

	if($stmt->rowCount() > 0) {
		$stmt = $conn->prepare("UPDATE config SET vote_audience_view = ''");
		$stmt->execute();
		$rows['status'] = "show";
		
	} else {
		$stmt = $conn->prepare("UPDATE config SET vote_audience_view = :vote_audience_view");
		$stmt->bindParam(':vote_audience_view', $vote_audience_view, PDO::PARAM_STR);
		$stmt->execute();
		$rows['status'] = "hide";
	}
	
	$rows['code'] = $vote_audience_view;
}


echo json_encode(array('data' => $rows));
?>