<style>
#panel_vote_profile {
	text-align: center;
	padding-top: 0;
	height:100%;
	padding-top:40px;
}
#panel_vote_profile .vote_profile_header {
	width: 100%;
	height: 165px;
	position: absolute;
	top: 0;
	z-index: -1;
	background-color: #2391dd;
}
#panel_vote_profile p {
	font-size:28px;
}
#panel_vote_profile p span {
	font-size:18px;
	color:#888;
}
#panel_vote_profile .members_profile_image {
	display:none;
	height: 250px;
	width: 250px;
	border-radius: 50%;
	border: 4px solid white;
	background-color: white;
}
</style>
<div id="layout_voting" style="position: absolute; width: 100%; height: calc(100% - 29px); padding:4px; background-color:#fff;"></div>
<script>

"use strict";

$(function() {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
	$().w2destroy('layout_voting');
    $('#layout_voting').w2layout({
		name: 'layout_voting',
		padding: 4,
		panels: [
			{ type: 'top', size: 35, overflow: 'hidden',
				toolbar: {
					items: [
						{ type: 'button', id: 'search_voting', text: 'Search', icon: 'fa fa-search' },
						{ type: 'button', id: 'new_voting', text: 'New', img: 'icon-page' },
						{ type: 'break' },
						{ type: 'html', html: '<button name="button_voting_open" onclick="setVotingOpen();" class="w2ui-btn" style="min-width:60px;" disabled>Open</button>'},
						{ type: 'break' },
						{ type: 'html', html: '<button name="button_voting_show_to_members" onclick="setMembersVoting();" class="w2ui-btn" style="min-width:175px;"  disabled>Show Result To Members</button>'},
						{ type: 'html', html: '<button name="button_voting_show_to_audience" onclick="setAudienceVoting();" class="w2ui-btn" style="min-width:175px;"  disabled>Show Result To Audience</button>'},
						{ type: 'break' },
						{ type: 'html',  id: 'voting_toolbar',
							html: function (item) {
								var html =
									'<div>' +
									'<input type="hidden" name="voting_code">' +
									'<span>Title: </span>' +
									'<input name="voting_title" style="width:180px;" class="w2ui-input" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<span>Date: </span>' +
									'<input name="voting_date" type="us-date" style="width: 80px;" disabled>' +
									'<span style="margin-left:10px;"></span>' +
									'<button name="voting_save" onclick="saveVoting();" class="w2ui-btn w2ui-btn-blue" style="min-width:60px;" disabled>Save</button>' +
									'<button name="voting_delete" onclick="deleteVoting();" class="w2ui-btn w2ui-btn-red" style="min-width:60px;" disabled>Delete</button>' +
									'</div>';
								return html;
							}
						},
						{ type: 'spacer' },
						{ type: 'html',  id: 'voting_status', html: '<div id="voting_status_msg"></div>' }
					],
					onClick: function (event) {
						if(event.target == 'new_voting') {
							formNewVoting();
						} else if(event.target == 'search_voting') {
							w2popup.open({
								title   : 'Search Voting',
								width   : 1024,
								height  : 600,
								showMax : true,
								body    : '<div id="layout_search_voting" style="position: absolute; left: 5px; top: 5px; right: 5px; bottom: 5px;"></div>',
								onOpen  : function (event) {
									event.onComplete = function () {
										$().w2destroy('layout_search_voting');
										$().w2destroy('sidebar_search_voting');
										$('#layout_search_voting').w2layout({
											name: 'layout_search_voting',
											padding: 4,
											panels: [
												{ type: 'left', size: 300, style: 'border: 1px solid #dfdfdf;', resizable: true, content:
													$().w2sidebar({
														name : 'sidebar_search_voting',
														nodes: [],
														onClick: function(event) {
															var file_id = (event.target).substr(1);
															
															if(file_id.search('JDX') != -1) {
																getVoting(file_id);
															}
														}
													})
												},
												{ type: 'main', overflow: 'hidden'}
											]
										});
										
										$().w2destroy('grid_search_voting');
										w2ui['layout_search_voting'].content('main', 
											$().w2grid({ 
												name   : 'grid_search_voting', 
												url    : 'Legissoft/voting/json.voting.search.php',
												method : 'GET', // need this to avoid 412 error on Safari
												show   : {
													toolbar        : true,
													lineNumbers    : true,
													toolbarColumns : false,
													toolbarEdit    : true
												},
												multiSearch: false,
												searches: [
													{ field: 'search', caption: 'Search here...', type: 'text' }
												],
												columns: [                
													{ field: 'title',        caption: 'Title',         size: '30%' },
													{ field: 'vote_yes',     caption: 'Voted Yes',     size: '15%' },
													{ field: 'vote_no',      caption: 'Voted No',      size: '15%' },
													{ field: 'vote_abstain', caption: 'Voted Abstain', size: '15%' },
													{ field: 'mod_by',       caption: 'Modified By',   size: '20%' },
													{ field: 'mod_date',     caption: 'Last Modified', size: '15%' },
												],
												onEdit: function (event) {
													var sel   = w2ui['grid_search_voting'].getSelection();
													getVoting(sel[0]);
													w2popup.close();
												},
											}) 
										);
										
										$.ajax({
											url  : 'Legissoft/voting/html.voting.archive.php',
											type : 'GET',
											success: function(result) {
												var nd = []; 
												for (var i in w2ui['sidebar_search_voting'].nodes) nd.push(w2ui['sidebar_search_voting'].nodes[i].id);
												w2ui['sidebar_search_voting'].remove.apply(w2ui['sidebar_search_voting'], nd);
									
												w2ui['sidebar_search_voting'].add(eval(result));
											}
										});
									};
								},
								onToggle: function (event) { 
									event.onComplete = function () {
										w2ui['panel_search_voting'].resize();
									}
								}
							});
						}
					}
				}
			},
			{ type: 'left', size: 220, style: 'background-color:#fff;', resizable: true },
			{ type: 'main', style: 'background-color:#fff;', overflow: 'hidden' }
		]
	});

	$('input[type=us-date]').w2field('date');
	
	w2ui['layout_voting'].content('left', 
		$().w2layout({
			name: 'layout_voting_archive',
			padding: 4,
			panels: [
				{ type: 'main', style: 'border: 1px solid #dfdfdf;', content:
					$().w2sidebar({
						name : 'sidebar_voting',
						nodes: [],
						onFlat: function (event) {
							if(event.goFlat) {
								w2ui['layout_voting'].set('left', { size: 50 });
							} else {
								w2ui['layout_voting'].set('left', { size: 250 });
							}
						},
						onClick: function(event) { 
							var file_id = (event.target).substr(1);;
	
							if(file_id.search('JDX') != -1) {
								getVoting(file_id);
							}
						}
					})
				},
				{ type: 'bottom', size: '50%', resizable: true, content: 
					$().w2grid({ 
						name : 'grid_voting_members',
						show : {
							toolbar      : true,
							toolbarColumns : false,
						},
						multiSearch: false,
						columns: [
							{ field: 'code', hidden: true },
							{ field: 'fullname', size: '77%', caption: 'Members' },
							{ field: 'button', size: '23%', caption: 'Show' }
						],
						searches : [
							{ field: 'fullname', caption: 'Members', operator : 'contains', type: 'text' }
						]   
					})
				}
			]
		})
	);
	
	getArchiveVoting('');
	
	w2ui['layout_voting'].content('main',
		$().w2layout({
			name: 'layout_voting_main',
			padding: 4,
			panels: [
				{ type: 'main', overflow: 'hidden', },
				{ type: 'preview', size: 350, resizable: true  },
				{ type: 'right', style: 'border: 1px solid #dfdfdf; background-color: #edf1f6', size: '35%', resizable: true }
			]
		})
	);
	
	w2ui['layout_voting_main'].content('preview',
		$().w2grid({ 
			name        : 'grid_voting',
			multiSelect : false,
			show: {
				toolbar        : false,
				lineNumbers    : true,
				toolbarColumns : false
			},      
			columns: [
				{ field: 'fname',       size: '30%', caption: 'First Name' },
				{ field: 'lname',       size: '30%', caption: 'Last Name' },
				{ field: 'title',       size: '20%', caption: 'Title' },
				{ field: 'vote_detail', size: '20%', caption: 'Vote' },
				{ field: 'username',   hidden: true },
				{ field: 'image_file', hidden: true },
				{ field: 'vote',       hidden: true }
				
			],
			onSelect: function(event) {
				event.onComplete = function () {
					var sel        =  w2ui['grid_voting'].getSelection();
					var username   =  w2ui['grid_voting'].get(sel[0])['username'];
					var name       =  w2ui['grid_voting'].get(sel[0])['fname'] + ' ' + w2ui['grid_voting'].get(sel[0])['lname'];
					var title      =  w2ui['grid_voting'].get(sel[0])['title'];
					var image_file =  w2ui['grid_voting'].get(sel[0])['image_file'];
					var vote       =  w2ui['grid_voting'].get(sel[0])['vote'];
					
					var code = $.trim($("input[name=voting_code]").val());
					if(code != '') {
						$.ajax({
							url  : 'Legissoft/voting/json.members.vote.php',
							type : 'POST',
							data: { 
								code     : code,
								vote     : vote,
								username : username,
								cmd      : 'get'
							},
							success: function(result) {
								var button = '';
								
								$.each(result.data['choices'], function(key, val) {
									if(val['value'] == result.data['vote']) {
										button = button + '<button class="members_vote_button w2ui-btn w2ui-btn-green" style="font-size:24px;" onclick="setVote(this, \'' + val['value'] + '\', \'' + username + '\')">' + val['detail'] + '</button>';
									} else {
										button = button + '<button class="members_vote_button w2ui-btn" style="font-size:24px;" onclick="setVote(this, \'' + val['value'] + '\', \'' + username + '\')">' + val['detail'] + '</button>';
									}
									
								});
						
								w2ui['layout_voting_main'].content('right',
									'<div id="panel_vote_profile">' +
									'	<div class="vote_profile_header"></div>' +
									'	<div>' +
									'		<div style="height:250px;"><img class="members_profile_image" onload="$(this).show();" onerror="if (this.src != \'framework/images/error.png\') this.src = \'framework/images/error.png\';" src="Legissoft/user_image/' + image_file + '" /></div>' +
									'		<p class="members_profile_name">' + name + '<br /><span>' + title + '</span></p>' +
									'		<div>' + button + '</div>' +
									'	</div>' +
									'</div>'
								);
							}
						});
					} else {
						w2ui['layout_voting_main'].message('right', {
							width   : 300,
							height  : 150,
							body    : '<div class="w2ui-centered">Please save your voting first!</div>',
							buttons : '<button class="w2ui-btn" onclick="w2ui[\'layout_voting_main\'].message(\'right\')">Ok</button>'
						})
					}
				}
			},
			onUnselect: function(event) {
       			w2ui['layout_voting_main'].content('right', '');
   			}  
		})
	);

});


function getArchiveVoting(selected) {
	$.ajax({
		url  : 'Legissoft/voting/html.voting.archive.php',
		type : 'GET',
		success: function(result) {
			var nd = []; 
			for (var i in w2ui['sidebar_voting'].nodes) nd.push(w2ui['sidebar_voting'].nodes[i].id);
			w2ui['sidebar_voting'].remove.apply(w2ui['sidebar_voting'], nd);

			w2ui['sidebar_voting'].add(eval(result));
			
			//if(selected != '') {
			//	w2ui['sidebar_voting'].select(selected);
			//}
		}
	});
}

function formNewVoting() {
	openPopupNewVoting();
	$().w2popup('open', {
		title   : 'New Voting',
		body    : '<div id="popup_form_new_voting" style="width: 100%; height: 100%;"></div>',
		style   : 'padding: 0px; border-radius:0;',
		width   : 500,
		height  : 230, 
		onOpen: function (event) {
			event.onComplete = function () {
				$('#w2ui-popup #popup_form_new_voting').w2render('form_new_voting');
			}
		},
		onClose: function () {
			w2ui['form_new_voting'].clear();
		}
	});
}

function openPopupNewVoting() {
	$().w2destroy('form_new_voting');
	$().w2form({
		name  : 'form_new_voting',
		style : 'border: 0px; background-color: transparent;',
		formHTML: 
			'<div class="w2ui-page page-0" style="padding-top:25px;">' +
			'    <div class="w2ui-field">' +
			'        <label>Title:</label>' +
			'        <div>' +
			'           <input name="pre_voting_title" type="text" maxlength="200" style="width: 250px" />' +
			'        </div>' +
			'    </div>' +
			'	<div class="w2ui-field">' +
			'		<label>Date:</label>' +
			'		<div><input name="pre_voting_date" type="us-date" style="width: 250px" ></div>' +
			'	</div>' +
			'</div>' +
			'<div class="w2ui-buttons">' +
			'    <button class="w2ui-btn" name="reset">Clear</button>' +
			'    <button class="w2ui-btn w2ui-btn-blue" name="save">Create</button>' +
			'</div>',
		fields: [
			{ field: 'pre_voting_title', type: 'text', required: true },
			{ field: 'pre_voting_date', type: 'date', required: true },
		],
		actions: {
			"save": function() { 
				var val = this.validate();
				if(val.length == 0){ 
					newVoting();
					w2popup.close();
				}
			},
			"reset": function() { this.clear(); }
		}
	});
	
	w2ui['form_new_voting'].record['pre_voting_date'] = new Date(Date.now()).toLocaleDateString();
	w2ui['form_new_voting'].refresh(); 
}

function newVoting() {
	$("input[name=voting_title]").attr("disabled", false);
	$("input[name=voting_date]").attr("disabled", false);
	$("button[name=voting_save]").attr("disabled", false);
	$("button[name=voting_delete]").attr("disabled", true);
	$("input[name=voting_title]").val($("input[name=pre_voting_title]").val());
	$("input[name=voting_date]").val($("input[name=pre_voting_date]").val());
	
	$('#voting_status_msg').html('<div style="background-color:#BB3C3E; color:#fff; padding:4px 5px;"><i class="fa fa-exclamation-triangle"></i> Unsave!</div>');
	
	$("input[name=voting_code]").val('');
	$("input[name=voting_title]").removeClass('w2ui-error');
	$("input[name=voting_date]").removeClass('w2ui-error');
	
	$("button[name=button_voting_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true);
	$("button[name=button_voting_show_to_members]").text('Show Result To Members');
	$("button[name=button_voting_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true);
	$("button[name=button_voting_show_to_audience]").text('Show Result To Audience');
	$("button[name=button_voting_open]").removeClass('w2ui-btn-green').attr("disabled", true);
	$("button[name=button_voting_open]").text('Open');
	
	w2ui['grid_voting_members'].clear();
	
	w2ui['layout_voting_main'].content('right', '');
	w2ui['layout_voting_main'].content('main', '<textarea id="writer_voting" class="writer_voting" style="width: calc(100% - 2px); height: calc(100% - 53px);"></textarea>');
	
	tinyMCE.remove('.writer_voting');
	tinymce.init({
		selector : '.writer_voting',
		plugins  : 'autoresize print textcolor colorpicker lists table paste',
		toolbar1 : 'newdocument print | undo redo | cut copy paste | formatselect removeformat  subscript superscript  | fontselect fontsizeselect',
		toolbar2 : 'bold italic underline strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | | table',
		font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
		fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
		menu: {
			file   : {title: 'File', items: 'menu_custom_new print'},
			edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
			format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		statusbar : false,
		resize    : false,
		branding  : false,
		height    : $('.writer_voting').height(),
		autoresize_max_height   : $('.writer_voting').height(),
		theme_advanced_resizing : true,
		theme_advanced_resizing_use_cookie : false,
		toolbar_items_size : 'small',
		menubar : false,		
	});
	
	
	newVotingLoadMembers();

}

function setVote(button, vote, username) {
	var code = $.trim($("input[name=voting_code]").val());
	$.ajax({
		url  : 'Legissoft/voting/json.members.vote.php',
		type : 'POST',
		data: { 
			code     : code,
			vote     : vote,
			username : username,
			cmd      : 'save'
		},
		success: function(result) {
			newVotingLoadMembers();
			$('.members_vote_button').removeClass('w2ui-btn-green');
			
			if(result.data['vote'] != '') {
				$(button).addClass('w2ui-btn-green');
			}
			
			updateMembersScreenVoting();
			updateAudienceScreenVoting();
		}
	});
}

function newVotingLoadMembers() {
	var code = $.trim($("input[name=voting_code]").val());
	w2ui['grid_voting'].clear();
	w2ui['grid_voting'].load('Legissoft/voting/json.voting.members.php?code=' + code);
	
	
}
function saveVoting() {
	//var content     = tinyMCE.activeEditor.getContent();
	var content     = tinyMCE.get('writer_voting').getContent();
	var code        = $.trim($("input[name=voting_code]").val());
	var title       = $.trim($("input[name=voting_title]").val());
	var voting_date = $.trim($("input[name=voting_date]").val());
	
	if(title != '' && voting_date != '') {
		if(content.trim() != '') {
			$.ajax({
				url:'Legissoft/voting/voting.save.php',
				type:'POST',
				data: { 
					code    : code,
					title   : title,
					content : content,
					date    : voting_date
				},
				success: function(result) {
					$("input[name=voting_code]").val(result.data['code']);
					$("button[name=button_voting_show_to_members]").attr("disabled", false);
					$("button[name=button_voting_show_to_audience]").attr("disabled", false);
					$("button[name=button_voting_open]").attr("disabled", false);
					$("button[name=voting_delete]").attr("disabled", false);
					$("#voting_status_msg").html('');
					$("input[name=voting_title]").removeClass('w2ui-error');
					
					getArchiveVoting(result.data.code);
					updateMembersScreenVoting();
					updateAudienceScreenVoting();
					
					getVotingMembers('', '');
					
					ohSnap('Voting Saved!', {'color':'green', 'duration':'1000'});
				}
			});
		} else {
			w2alert('Saving blank voting is not allowed!');
		}
	
	} else {
		if(title == '') {
			$("input[name=voting_title]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Voting Title is required!');
			
		} else if(voting_date == '') {
			$("input[name=voting_date]").removeClass('w2ui-error').addClass('w2ui-error').focus();
			w2alert('Voting Date is required!');
		}
		
	}
}

function deleteVoting() {
	var code = $.trim($("input[name=voting_code]").val());
	
	if(code != '') {
		w2confirm('Do you want to delete?').yes(function () { 
			$.ajax({
				url:'Legissoft/voting/voting.save.php',
				type:'POST',
				data: { 
					delete_code : code,
					cmd         : 'delete'
				},
				success: function(result) {
					w2ui['grid_voting'].clear();
					w2ui['layout_voting_main'].content('main','');
					w2ui['layout_voting_main'].content('right', '');
					
					$("input[name=voting_code]").val('');
					$("input[name=voting_title]").val('').attr("disabled", true);
					$("input[name=voting_date]").val('').attr("disabled", true);
					$("button[name=voting_save]").attr("disabled", true);
					$("button[name=voting_delete]").attr("disabled", true);
					$("input[name=voting_title]").removeClass('w2ui-error');
					$("input[name=voting_date]").removeClass('w2ui-error');
					$('#voting_status_msg').html('');
					
					$("button[name=button_voting_show_to_members]").removeClass('w2ui-btn-green').attr("disabled", true);
					$("button[name=button_voting_show_to_members]").text('Show Result To Members');
					$("button[name=button_voting_show_to_audience]").removeClass('w2ui-btn-green').attr("disabled", true);
					$("button[name=button_voting_show_to_audience]").text('Show Result To Audience');
					$("button[name=button_voting_open]").removeClass('w2ui-btn-green').attr("disabled", true);
					$("button[name=button_voting_open]").text('Open');
					
					w2ui['sidebar_voting'].remove('a' + code);
					w2ui['sidebar_voting'].remove('b' + code);
					
					w2ui['grid_voting_members'].clear();
					
					updateMembersScreenVoting();
					updateAudienceScreenVoting();
					
				}
			});
		});
	}
}

function getVoting(voting_id) {
	if(voting_id != '') {
		$.ajax({
			url:'Legissoft/voting/json.voting.get.php',
			type:'POST',
			data: { 
				code : voting_id
			},
			success: function(result) {
				$("input[name=voting_title]").attr("disabled", false);
				$("input[name=voting_date]").attr("disabled", false);
				$("button[name=voting_save]").attr("disabled", false);
				
				$('#voting_status_msg').html('');
				
				$("input[name=voting_code]").val(result.data['code']);
				$("input[name=voting_title]").val(result.data['title']);
				$("input[name=voting_date]").val(result.data['voting_date']);
				
				$("button[name=button_voting_show_to_members]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_voting_show_to_members]").text('Show Result To Members');
				$("button[name=button_voting_show_to_audience]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_voting_show_to_audience]").text('Show Result To Audience');
				$("button[name=button_voting_open]").attr("disabled", false).removeClass('w2ui-btn-green');
				$("button[name=button_voting_open]").text('Open');
				$("button[name=voting_delete]").attr("disabled", false);
				$("input[name=voting_title]").removeClass('w2ui-error');
				$("input[name=voting_date]").removeClass('w2ui-error');
				
				if(result.data['vote_members_view'] == voting_id) {
					$("button[name=button_voting_show_to_members]").addClass('w2ui-btn-green');
					$("button[name=button_voting_show_to_members]").text('Hide Result From Members');
				}
				
				if(result.data['vote_audience_view'] == voting_id) {
					$("button[name=button_voting_show_to_audience]").addClass('w2ui-btn-green');
					$("button[name=button_voting_show_to_audience]").text('Hide Result From Audience');
				}
				
				if(result.data['vote_open'] == voting_id) {
					$("button[name=button_voting_open]").addClass('w2ui-btn-green');
					$("button[name=button_voting_open]").text('Close');
				}
				
				w2ui['layout_voting_main'].content('right', '');
				w2ui['layout_voting_main'].content('main', '<textarea id="writer_voting" class="writer_voting" style="width: calc(100% - 2px); height: calc(100% - 53px);">' + result.data['content'] + '</textarea>');
	
				tinyMCE.remove('.writer_voting');
				tinymce.init({
					selector : '.writer_voting',
					plugins  : 'autoresize print textcolor colorpicker lists table paste',
					toolbar1 : 'newdocument print | undo redo | cut copy paste | formatselect removeformat  subscript superscript  | fontselect fontsizeselect',
					toolbar2 : 'bold italic underline strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | | table',
					font_formats: 'Verdana=verdana; Times New Roman=times new roman; Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace',
					fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt',
					menu: {
						file   : {title: 'File', items: 'menu_custom_new print'},
						edit   : {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace'},
						format : {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
						table  : {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
					},
					statusbar : false,
					resize    : false,
					branding  : false,
					height    : $('.writer_voting').height(),
					autoresize_max_height   : $('.writer_voting').height(),
					theme_advanced_resizing : true,
					theme_advanced_resizing_use_cookie : false,
					toolbar_items_size : 'small',
					menubar : false,		
				});

				newVotingLoadMembers();
					
				getVotingMembers('', '');
			}
		});
		
	}
}

function setMembersVoting() {
	var code    = $.trim($("input[name=voting_code]").val());
	//var content = tinyMCE.activeEditor.getContent();
	var content = tinyMCE.get('writer_voting').getContent();
	
	if(content.trim() != '') {
		if(code != '') {
			$.ajax({
				url:'Legissoft/voting/voting.save.php',
				type:'POST',
				data: { 
					code_members : code
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_voting_show_to_members]").removeClass('w2ui-btn-green');
						$("button[name=button_voting_show_to_members]").text('Show Result To Members');
					} else {
						$("button[name=button_voting_show_to_members]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
						$("button[name=button_voting_show_to_members]").text('Hide Result From Members');
					}
					
					getArchiveVoting('');
					updateMembersScreenVoting();
				}
			});
		}
	} else {
		w2alert('Blank voting will not allow to show to Members!');
	}
}

function setAudienceVoting() {
	var code    = $.trim($("input[name=voting_code]").val());
	//var content = tinyMCE.activeEditor.getContent();
	var content = tinyMCE.get('writer_voting').getContent();
	
	if(content.trim() != '') {
		if(code != '') {
			$.ajax({
				url:'Legissoft/voting/voting.save.php',
				type:'POST',
				data: { 
					code_audience : code
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_voting_show_to_audience]").removeClass('w2ui-btn-green');
						$("button[name=button_voting_show_to_audience]").text('Show Result To Audience');
					} else {
						$("button[name=button_voting_show_to_audience]").removeClass('w2ui-btn-green').addClass('w2ui-btn-green');
						$("button[name=button_voting_show_to_audience]").text('Hide Result From Audience');
					}
					
					getArchiveVoting('');
					updateAudienceScreenVoting();
				}
			});
		}
	} else {
		w2alert('Blank voting will not allow to show to Audience!');
	}
}

function setVotingOpen() {
	var code    = $.trim($("input[name=voting_code]").val());
	//var content = tinyMCE.activeEditor.getContent();
	var content = tinyMCE.get('writer_voting').getContent();
	
	if(content.trim() != '') {
		if(code != '') {
			$.ajax({
				url:'Legissoft/voting/voting.save.php',
				type:'POST',
				data: { 
					code_open : code
				},
				success: function(result) {
					if(result.data['status'] == 'show') {
						$("button[name=button_voting_open]").removeClass('w2ui-btn-green');
						$("button[name=button_voting_open]").text('Open');
					
					} else {
						$("button[name=button_voting_open]").addClass('w2ui-btn-green');
						$("button[name=button_voting_open]").text('Close');
					
					}	
					
					getArchiveVoting('');
				}
			});
		}
	} else {
		w2alert('Blank voting will not allow to Open!');
	}
}

function getVotingMembers(member_code, status) {
	// on - off specific viewing
	var voting_code = $.trim($("input[name=voting_code]").val());
	if(voting_code != '') {
		$.ajax({
			url      :'Legissoft/voting/json.voting.show.members.php',
			type     :'POST',
			dataType : 'json',
			data : { 
				code        : voting_code,
				member_code : member_code,
				status      : status
			},
			success: function( json ) {
				w2ui['grid_voting_members'].clear();
				
				$.each(json.records, function(i, value) {
					if(value.status == 'hide') {
						w2ui['grid_voting_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button, 'w2ui': { 'style': 'background-color: #C2F5B4; font-weight: bold;' } });
					} else {
						w2ui['grid_voting_members'].add({ recid: value.recid, code: value.code, fullname: value.fullname, button: value.button });
					}
				});
				
				if(member_code != '') {
					getArchiveVoting('');
				}
			}
		});
	}
}

function updateMembersScreenVoting() {
	$('#layout_members_screen_iframe').attr('src', 'Legissoft/document/document.members.screen.viewer.php');
}

function updateAudienceScreenVoting() {
	$('#layout_audience_screen_iframe').attr('src', 'Legissoft/document/document.audience.screen.viewer.php');
}
</script>