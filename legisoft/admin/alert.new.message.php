<?php session_start();
//header('Content-Type: application/json; charset=UTF-8');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
require_once("Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("library/general.functions.php");

$rows = array();

$GENERAL_FUNCTIONS = new GeneralFunctions();

$stmt = $conn->prepare("SELECT username FROM _user WHERE username <> :username AND type <> '3' AND deleted = '0'");
$stmt->bindParam(':username', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
$stmt->execute();

$total = 0;
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $stmt2 = $conn->prepare("SELECT COUNT(view) AS total_msg FROM chat WHERE display_to = :display_to AND message_from = :message_from AND view = '0'");  
    $stmt2->bindParam(':display_to', $GENERAL_FUNCTIONS->getSessionVar('username'), PDO::PARAM_STR);
    $stmt2->bindParam(':message_from', $row['username'], PDO::PARAM_STR);
    $stmt2->execute();
    $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
    
    $total = $total + $row2['total_msg'];

    $rows['account'][$row['username']] = $row['username'];
    $rows['account'][$row['username']] = $row2['total_msg'];
}

$rows['new_message'] =  $total;

//echo json_encode(array('data' => $rows));

echo "data: " . json_encode(array('data' => $rows)) . "\n\n";

?>