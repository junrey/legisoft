<?php session_start();
require_once("Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

if($SYSTEM_FUNCTIONS->getSessionVar('user_id') != $SYSTEM_FUNCTIONS->getRootID()) {

$stmt = $conn->prepare("UPDATE _user SET logout = NOW() WHERE user_id = :user_id");
	$stmt->bindParam(':user_id', $SYSTEM_FUNCTIONS->getSessionVar('user_id'), PDO::PARAM_STR);
	$stmt->execute();
}

unset($_SESSION[$SYSTEM_FUNCTIONS->getSystemName()]);
echo "<script>top.location.href='login.php';</script>";
?>
