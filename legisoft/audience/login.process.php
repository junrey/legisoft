<?php session_start();
require_once("../admin/Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("../admin/library/system.functions.php");

$SYSTEM_FUNCTIONS = new SystemFunctions();

if(isset($_POST['submit_form']) && trim($_POST['username']) != "" && trim($_POST['password']) != "") {

	$username = $SYSTEM_FUNCTIONS->filterInput($_POST['username']);
	$password = $SYSTEM_FUNCTIONS->filterInput($_POST['password']);

	if($username == 'root' && $password == '@root') {
		$_SESSION['audience']['audience_user_id']       = $SYSTEM_FUNCTIONS->getRootID();
		$_SESSION['audience']['audience_username']      = $SYSTEM_FUNCTIONS->getRootUsername();
		$_SESSION['audience']['audience_firstname']     = '';
		$_SESSION['audience']['audience_lastname']      = '';
		$_SESSION['audience']['audience_middlename']    = '';
	 	$_SESSION['audience']['audience_fullname']      = "Root Administrator";
		$_SESSION['audience']['audience_profile_image'] = '';

        echo "<script>top.location.href='index.php';</script>";

	} else {

  		$stmt = $conn->prepare("SELECT * FROM _user WHERE username = :username AND type = '3' AND enable = '1' AND deleted = '0'");
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			if(password_verify($password, $row['password'])) {
				$_SESSION['audience']['audience_user_id']       = $row['user_id'];
				$_SESSION['audience']['audience_username']      = $row['username'];
				$_SESSION['audience']['audience_firstname']     = $row['fname'];
				$_SESSION['audience']['audience_lastname']      = $row['lname'];
				$_SESSION['audience']['audience_middlename']    = $row['mname'];
				$_SESSION['audience']['audience_fullname']      = $row['fname'] . " " . $row['lname'];
				$_SESSION['audience']['audience_profile_image'] = $row['image_file'];	
			
				$stmt = $conn->prepare("UPDATE _user SET login = NOW(), login_active = NOW() WHERE username = :username") or die(mysql_error());
				$stmt->bindParam(':username', $username, PDO::PARAM_STR);
				$stmt->execute();
				
				echo "<script>top.location.href='index.php';</script>";
			} else {	
				echo "<script>alert('Incorrect username and password!'); history.go(-1);</script>";
			}
		} else {
			echo "<script>alert('Incorrect username and password!'); history.go(-1);</script>";
		}
	}

} else {
	echo "<script>top.location.href='login.php';</script>";
}

?>