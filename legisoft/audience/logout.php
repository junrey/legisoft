<?php session_start();
require_once("../admin/Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("../admin/library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

if($_SESSION['audience']['audience_user_id'] != $GENERAL_FUNCTIONS->getRootID()) {

$stmt = $conn->prepare("UPDATE _user SET logout = NOW() WHERE user_id = :user_id");
	$stmt->bindParam(':user_id', $_SESSION['audience']['audience_user_id'], PDO::PARAM_STR);
	$stmt->execute();
}

unset($_SESSION['audience']);
echo "<script>top.location.href='login.php';</script>";
?>
