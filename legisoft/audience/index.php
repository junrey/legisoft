<?php session_start();
require_once("../admin/library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

if(!isset($_SESSION['audience']['audience_user_id'])) {
	echo "<script>top.location.href='login.php';</script>";
	exit;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Legisoft - Audience Viewer</title>
<link rel="icon" type="image/png" href="icon/fav-16x16.png" sizes="16x16">  
<link rel="icon" type="image/png" href="icon/fav-32x32.png" sizes="32x32">  
<link rel="icon" type="image/png" href="icon/fav-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="icon/fav-152x152.png" sizes="152x152">  
<link rel="icon" type="image/png" href="icon/fav-167x167.png" sizes="167x167">  
<link rel="icon" type="image/png" href="icon/fav-512x512.png" sizes="512x512">  
<link rel="apple-touch-icon" href="icon/icon-152x152.png" sizes="152x152">
<link rel="apple-touch-icon" href="icon/icon-167x167.png" sizes="167x167">
<script src="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/w2ui/w2ui-1.5.rc1.min.js"></script>
<script src="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/tinymce/js/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/w2ui/w2ui-1.5.rc1.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/framework/css/style.css" />

</head>
<body>

<div id="system"></div>

<script>
	$(function () {
		$('#system').w2layout({
			name: 'audience_layout',
			padding: 0,
			panels: [
				{ type: 'main', style: ' background-image: url(\'../images/kabankalan.png\'); background-repeat: no-repeat; background-attachment: fixed; background-position: center;', overflow: 'hidden', toolbar: {
						items: [
							{ type: 'html', html: '<div id="system_name"><span>Legisoft - Kabankalan City</span></div>' },
							{ type: 'spacer' },
							{ type: 'html', 
								html: 
									'<span id="login_fullname"><?php echo $_SESSION['audience']['audience_fullname']; ?></span>' 
							},
							{ type: 'break' },
							{ type: 'html', html: '<i class="fa fa-refresh fa-spin" style="color: #ed2027;"></i>' },
							{ type: 'break' },
							{ type: 'html', html: '<select class="w2ui-input" style="font-size:14px; line-height:110%; margin: 0 5px; height: 29px;" onchange="$(\'#main_viewer\')[0].contentWindow.zoom(this.value);"><option value="scale(1)">Zoom 100%</option><option value="scale(1.2)">Zoom 110%</option><option value="scale(1.4)">Zoom 125%</option><option value="scale(1.6)">Zoom 150%</option><option value="scale(1.8)">Zoom 175%</option><option value="scale(2)">Zoom 200%</option></select>'},
							{ type: 'html', html: '<button onclick="location.reload();" class="w2ui-btn" style="font-size:14px; margin: 0; min-width: 20px;"><i class="fa fa-refresh"></i></button>'},
							{ type: 'html', html: '<button onclick="top.location.href=\'logout.php\';" class="w2ui-btn" style="font-size:14px;"><i class="fa fa-sign-out" style="margin-right:5px;"></i>Log Out</button>'},
						]
					} },
			]
		});
		
		w2ui['audience_layout'].content('main', '<iframe id="main_viewer" src="audience.screen.viewer.php"></iframe>');
		
	});
</script>
</body>
</html>