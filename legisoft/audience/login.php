<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Legisoft - Audience Viewer</title>
<link rel="icon" type="image/png" href="icon/fav-16x16.png" sizes="16x16">  
<link rel="icon" type="image/png" href="icon/fav-32x32.png" sizes="32x32">  
<link rel="icon" type="image/png" href="icon/fav-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="icon/fav-152x152.png" sizes="152x152">  
<link rel="icon" type="image/png" href="icon/fav-167x167.png" sizes="167x167">  
<link rel="icon" type="image/png" href="icon/fav-512x512.png" sizes="512x512">  
<link rel="apple-touch-icon" href="icon/icon-152x152.png" sizes="152x152">
<link rel="apple-touch-icon" href="icon/icon-167x167.png" sizes="167x167">
<link rel="stylesheet" type="text/css" href="../admin/Framework/css/style.login.css" />
</head>

<body>
  <form id="login" method="post" action="login.process.php" class="register">
    <h1 class="register-title">Audience Viewer</h1>
    <input type="text" name="username" class="register-input" placeholder="Username" required autocomplete="off" autofocus>
    <input type="password" name="password" class="register-input" placeholder="Password" required>
    <input type="submit" value="Log In" name="submit_form" class="register-button">
  </form>
</body>
</html>
