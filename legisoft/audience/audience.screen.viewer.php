<?php session_start();
require_once("../admin/Legissoft/database/pdo.mysql.connection.legissoft.php");
require_once("../admin/library/general.functions.php");

$GENERAL_FUNCTIONS = new GeneralFunctions();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
html {
	height:100%; 
	width:100%; 
	overflow:hidden;
	padding:0; 
	margin:0;
}
body { 
	height:100%; 
	width:100%; 
	overflow-y:auto; 
	overflow-x:auto;
	padding:0; 
	margin:0; 
}
.pdf_window {
	display:none;
	height: 100%;
	width: 100%;
	overflow: hidden;
	z-index:1; 
}
#paper_gutter {
	width:615px;
	min-height:11in;
	background-color:#FFFFFF;
	border:2px solid #999999;
	margin:40px auto;
	padding:100px 105px;
	display:none;
}
.action_window {
	display:none;
	z-index:1; 
	width:1024px;
	min-height:11in;
	margin:auto;
	background: #fff;
}
.action_window #action_wrapper {
	height:calc(100% - 75px);
	background-color:#fff;
	overflow-x:hdden;
	overflow-y:auto;
	padding-bottom:40px;
}
.action_window #action_content {
	padding:20px;
}
.action_window #reference {
	background-color:#3e7ab4;
	padding:5px 10px;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center;
	color:#fff;
	font-size:25px;
}
.action_window #action_item {
	background-color:#fbfbfb;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center; 
	padding:10px;
	letter-spacing: 2px;
	border-radius:5px;
}
.action_window #movant {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#08f4ad;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #second {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#ff8000;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#f9ca02;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee .label {
	width:100%;
	float:left;
	font-size:18px;
}
.action_window #movant .label, .action_window #second .label {
	width:370px;
	float:left;
	font-size:18px;
}
.action_window #movant .title, .action_window #second .title, .action_window #commitee .title {
	padding:10px;
	width:150px;
	vertical-align:top;
	font-size:20px;
	text-align:right;
	line-height:25px;
}
.action_window #movant .title span, .action_window #second .title span, .action_window #commitee .title span {
	float:right;
	color:#fff;
	font-size:30px;
	line-height:18px;
	margin-left:10px;
}

.voting_window {
	display:none;
	z-index:1; 
	width:100%;
	height: 100%;
}

.screen_vote_content {
	border-top: 5px solid #3e7ab4;
	width:100%;
	background-color: #fff;
}
.screen_vote_content td {
	padding:0 5px;
}
.screen_vote_panel {
	border-collapse: collapse;
	width:100%;
	height:100%;
	background-color:#f2f2f2;
	border-top: 4px solid #fff;
	border-left: 4px solid #fff;
	border-right: 4px solid #fff;
}

.screen_vote_panel_title {
	padding:5px;
	text-align:center;
	font-family:Verdana,Arial,sans-serif;
	color:#fff;
	font-size:25px;
	width:130px;
}

.screen_vote_panel_content {
	width: calc(100% - 130px);
	padding:5px;
}

.screen_vote_panel_title.yes {
	background-color:#32d296;
}

.screen_vote_panel_title.no {
	background-color:#f0506e;
}

.screen_vote_panel_title.abstain {
	background-color:#aaa;
	
}

.screen_vote_panel_members {
	float: left;
}
.screen_vote_panel_members table {
	width:100%;	
	font-family:Verdana,Arial,sans-serif;
}

.screen_vote_panel_members table th {
	width:60px;
}
.screen_vote_panel_members table td {
	font-size:20px;	
	width:220px;
}
.screen_vote_panel_members table th img {
	border-radius: 50%;
}

.image_window {
	display:none;
	z-index:1; 
	width:100%;
	height:100%;
	background: #fff;
	overflow:auto;
}

.image_window img {
	max-width:100%;
}

.announcement_window {
	display:none;
	z-index:1; 
	width:100%;
	height:100%;
	background: #fff;
	overflow:auto;
}
@media only screen and (max-width: 1024px) {
	.action_window #movant .label, .action_window #second .label {
		width:340px;
	}
	
	.action_window #commitee .label {
		width:100%;
	}
}

@media only screen and (max-width: 800px) {
	#paper_gutter {
		width:auto;
	}
	
	.action_window #commitee .label, .action_window #movant .label, .action_window #second .label {
		width:100%;
	}
}
</style>
</head>
<body>
<div id="page_current">
    <div id="paper_gutter">
        <div id="paper_main"></div>
    </div>
</div>
<div class="pdf_window"></div>
<div class="action_window"></div>
<div class="voting_window"></div>
<div class="image_window"></div>
<div class="announcement_window"></div>
<script>
	var active_window = '';
	
    parent.$(function() {
		audienceActivity();
		//setInterval(function() { audienceActivity(); }, 4000);
    });
	
	function audienceActivity() {
		var source_audience_activity = new EventSource('<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/audience/audience.activity.php');
		source_audience_activity.onmessage = function(event) { 
			var result = parent.$.parseJSON(event.data);
			
			if(result.data != '') {
				if(result.data['type'] == 'announcement') {
					getAudienceAnnouncementAlert();
					
				} else if(result.data['type'] == 'image') {
					getAudienceImageAlert();
					
				} else if(result.data['type'] == 'voting') {
					getAudienceVoteAlert();
					
				} else if(result.data['type'] == 'action') {
					getAudienceActionAlert();
					
				} else if(result.data['type'] == 'document') {
					getContent();
				}
			} else {
				hideWindow('');
			}
		};
		/*parent.$.ajax({
			method: 'GET',
			url: '<?php //echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/audience/audience.activity.php',
			success: function(result){
				if(result.data != '') {
					if(result.data['type'] == 'announcement') {
						getAudienceAnnouncementAlert();
						
					} else if(result.data['type'] == 'image') {
						getAudienceImageAlert();
						
					} else if(result.data['type'] == 'voting') {
						getAudienceVoteAlert();
						
					} else if(result.data['type'] == 'action') {
						getAudienceActionAlert();
						
					} else if(result.data['type'] == 'document') {
						getContent();
					}
				} else {
					hideWindow('');
				}
			}
		});*/
	}
    
	var date_announcement = '';
	function getAudienceAnnouncementAlert() {
        parent.$.ajax({
            method: 'GET',
            url: '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/document/document.audience.screen.viewer.announcement.v2.php', 	
            success: function(result){
				if(result.data['announcement'] != '') {
					hideWindow('announcement');
					
					if(date_announcement != result.data['date_log']) {
						parent.$(document).contents().find('.announcement_window').empty().html(result.data['announcement']);
						
						date_announcement = result.data['date_log'];
					}

					active_window = 'announcement';
				
				} else {
					hideWindow('');
				}
			},
            error: function(error) {

            }
        });
    }
	
	var date_image = '';
	function getAudienceImageAlert() {
        parent.$.ajax({
            method: 'GET',
            url: '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/document/document.audience.screen.viewer.image.v2.php', 	
            success: function(result){
            	if(result.data['image'] != '') {
					hideWindow('image');
					
					if(date_voting != result.data['date_log']) {
                    	parent.$(document).contents().find('.image_window').empty().html(result.data['image']);
					
						date_image = result.data['date_log'];
					}
                    
                	active_window = 'image';
				} else {
					hideWindow('');
                }
            },
            error: function(error) {

            }
        });
    }
	
	var date_voting = '';
	function getAudienceVoteAlert() {
        parent.$.ajax({
            method: 'GET',
            url: '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/document/document.audience.screen.viewer.voting.v2.php', 	
            success: function(result){
                if(result.data['voting'] != '') {
					hideWindow('voting');
						
					if(date_voting != result.data['date_log']) {
						parent.$(document).contents().find('.voting_window').empty().html(result.data['voting']);
						
						date_voting = result.data['date_log'];
					}

                	active_window = 'vote';
				} else {
					hideWindow('');
                }
            },
            error: function(error) {

            }
        });
    }
	
	var date_action = '';
	function getAudienceActionAlert() {
        parent.$.ajax({
            method: 'GET',
            url: '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/document/document.audience.screen.viewer.actions.v2.php', 	
            success: function(result){
                 if(result.data['action'] != '') {
					hideWindow('action');
					
					if(date_action != result.data['date_log']) {
						parent.$(document).contents().find('.action_window').empty().html(result.data['action']);
						
						date_action = result.data['date_log'];
					}
					
					active_window = 'action';
			    } else {
					hideWindow('');
                }
            },
            error: function(error) {

            }
        });
    }
	
	var date_document = '';
    function getContent() {
        parent.$.ajax({
            method: 'GET',
            url: '<?php echo $GENERAL_FUNCTIONS->getSystemPath(); ?>/admin/Legissoft/document/document.audience.screen.viewer.ongoing.php', 	
            success: function(result){
				if(result.data['source'] == 'pdf') {
					if(result.data['filename'] != '') {
						hideWindow('pdf');
						
						if(date_document != result.data['date_log']) {
							parent.$(document).contents().find('.pdf_window').empty().html('<embed src="../upload/' + result.data['filename'] + '" style="height: 100%; width: 100%;" type="application/pdf">');
						
							date_document = result.data['date_log'];
						}
						
						active_window = 'pdf';
						
					} else {
						hideWindow('');
					}
				} else {
					if(result.data['document'] != '') {
						hideWindow('document');
						
						if(date_document != result.data['date_log']) {
							parent.$(document).contents().find('#paper_main').empty().html(result.data['document']);
							
							date_document = result.data['date_log'];
						}
						
						active_window = 'document';
					
					} else {
						hideWindow('');
					}
				}
            },
            error: function(error) {

            }
        });
    }
	
	function hideWindow(id) {
		if(id == 'document') {
			parent.$(document).contents().find('#paper_gutter').show();
		} else {
			parent.$(document).contents().find('#paper_gutter').hide();
		}
		
		if(id == 'pdf') {
			parent.$(document).contents().find('.pdf_window').show();
		} else {
			parent.$(document).contents().find('.pdf_window').hide();
		}
		
		if(id == 'action') {
			parent.$(document).contents().find('.action_window').show();	
		} else {
			parent.$(document).contents().find('.action_window').hide();	
		}
		
		if(id == 'voting') {
			parent.$(document).contents().find('.voting_window').show();	
		} else {
			parent.$(document).contents().find('.voting_window').hide();	
		}
		
		if(id == 'image') {
			parent.$(document).contents().find('.image_window').show();	
		} else {
			parent.$(document).contents().find('.image_window').hide();	
		}
		
		if(id == 'announcement') {
			parent.$(document).contents().find('.announcement_window').show();	
		} else {
			parent.$(document).contents().find('.announcement_window').hide();	
		}
	}
	
	function zoom(val) {
		if(active_window == 'announcement') {
			parent.$(document).contents().find('.announcement_window').css('transform', val);
			parent.$(document).contents().find('.announcement_window').css('transformOrigin', 'left top');
			
		} else if(active_window == 'image') {
			parent.$(document).contents().find('.image_window').css('transform', val);
			parent.$(document).contents().find('.image_window').css('transformOrigin', 'left top');
			
		} else if(active_window == 'vote') {
			parent.$(document).contents().find('.voting_window').css('transform', val);
			parent.$(document).contents().find('.voting_window').css('transformOrigin', 'left top');
		
		} else if(active_window == 'action') {
			parent.$(document).contents().find('.action_window').css('transform', val);
			
			if(val == 'scale(1.4)') {
				parent.$(document).contents().find('.action_window').css('transformOrigin', '400px top');
			} else if(val == 'scale(1.6)') {
				parent.$(document).contents().find('.action_window').css('transformOrigin', '268px top');
			} else if(val == 'scale(1.8)') {
				parent.$(document).contents().find('.action_window').css('transformOrigin', '200px top');
			} else if(val == 'scale(2)') {
				parent.$(document).contents().find('.action_window').css('transformOrigin', '160px top');
			} else {
				parent.$(document).contents().find('.action_window').css('transformOrigin', 'center top');	
			}

		} else if(active_window == 'document') {
			parent.$(document).contents().find('#paper_gutter').css('transform', val);
			
			if(val == 'scale(1.8)') {
				parent.$(document).contents().find('#paper_gutter').css('transformOrigin', '325px top');
			} else if(val == 'scale(2)') {
				parent.$(document).contents().find('#paper_gutter').css('transformOrigin', '260px top');
			} else {
				parent.$(document).contents().find('#paper_gutter').css('transformOrigin', 'center top');
			}
	
		}

	}
    
</script>
</body>
</html>