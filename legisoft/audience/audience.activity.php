<?php session_start();
//header('Content-Type: application/json; charset=UTF-8');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
require_once("../admin/Legissoft/database/pdo.mysql.connection.legissoft.php");

$rows = array();

$stmt = $conn->prepare("SELECT audience_alert_message, audience_alert_message_show, image_audience_view, vote_audience_view, audience_action_view, audience_view FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

if($row['audience_alert_message_show'] == 1 && $row['audience_alert_message'] != "") {
	$rows['type'] = 'announcement';
	$rows['code'] = '';
	
} else if($row['image_audience_view'] != "") {
	$rows['type'] = 'image';
	$rows['code'] = $row['image_audience_view'];
	
} else if($row['vote_audience_view'] != "") {
	$rows['type'] = 'voting';
	$rows['code'] = $row['vote_audience_view'];
	
} else if($row['audience_action_view'] != "") {
	$rows['type'] = 'action';
	$rows['code'] = $row['audience_action_view'];
	
} else if($row['audience_view'] != "") {
	$rows['type'] = 'document';
	$rows['code'] = $row['audience_view'];
}

//echo json_encode(array('data' => $rows));

echo "data: " . json_encode(array('data' => $rows)) . "\n\n";

?>