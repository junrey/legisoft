<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['code'])) {
	exit();
}

$rows = array();
$rows['vote_result'] = '';
$rows['date_log']    = '';

$vote_code = $APP_FUNCTIONS->filterInput($_GET['code']);

if($vote_code != "") {
	$stmt2 = $conn->prepare("SELECT title, content, mod_date FROM vote_header WHERE code = :vote_code");
	$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

	$rows['date_log'] = $row2['mod_date'];		

	$rows['vote_result'] = '<div class="vote_members_title">' . $row2['title'] . '</div>';


	$rows['vote_result'] .= '<div class="uk-card uk-card-default uk-width-1-1">';
		$rows['vote_result'] .= '<div class="uk-card uk-card-body uk-padding-small">';
			$rows['vote_result'] .= $row2['content'];
		$rows['vote_result'] .= '</div>';
	$rows['vote_result'] .= '</div><hr class="uk-margin-remove">';

	$stmt3 = $conn->prepare("SELECT username, lname, fname, image_file FROM _user WHERE voter = '1' AND deleted = '0' AND type = '1' ORDER BY lname, fname"); 
	$stmt3->execute();
	
	$members = '';
	while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) { 
		$stmt4 = $conn->prepare("SELECT b.detail FROM vote a INNER JOIN vote_item b ON a.vote = b.value WHERE a.code = :vote_code AND a.username = :username AND a.enable = '1' AND b.enable = '1'");  
		$stmt4->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
		$stmt4->bindParam(':username', $row3['username'], PDO::PARAM_STR);
		$stmt4->execute();
		$row4 = $stmt4->fetch(PDO::FETCH_ASSOC);

		$members .= '<div class="vote_members uk-card uk-card-default uk-width-1-1">';
			$members .= '<div class="uk-card-header">';
				$members .= '<div class="uk-grid-small uk-flex-middle" uk-grid>';
					$members .= '<div class="uk-width-auto">';
						$members .= '<img class="uk-border-circle" width="60" height="60" src="admin/Legissoft/user_image/profile/' . $row3['image_file'] . '" onerror="if(this.src != \'images/no_profile_pic.gif\') this.src = \'images/no_profile_pic.gif\';">';
					$members .= '</div>';
					$members .= '<div class="uk-width-expand uk-text-left">';
						$members .= '<h3 class="uk-card-title">' . $row3['fname'] . ' ' . $row3['lname'] . '</h3>';
					$members .= '</div>';
					$members .= '<div class="uk-width-auto">';
						$members .= '<div class="view_' . strtolower($row4['detail']) . '">' . $row4['detail']. '</div>';
					$members .= '</div>';
				$members .= '</div>';
			$members .= '</div>';
		$members .= '</div>';
	}
   
  	$rows['vote_result'] .= $members;
  	$rows['vote_result'] .= '<div class="vote_members_title" style="text-align:center">';
	
	$stmt5 = $conn->prepare("SELECT b.detail, COUNT(b.detail) AS total FROM vote a INNER JOIN vote_item b ON a.vote = b.value WHERE a.code = :vote_code AND a.enable = '1' AND b.enable = '1' GROUP BY b.detail ORDER BY b.place");
	$stmt5->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt5->execute();
	
	$total = '';
	while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) {
		$total .= '<span>' . $row5['detail'] . ': <b>' .  $row5['total'] . '</b></span>';
	}
	
	$rows['vote_result'] .= $total;
	$rows['vote_result'] .= '</div>';

} 
 
echo json_encode(array('data' => $rows)); 
?>
