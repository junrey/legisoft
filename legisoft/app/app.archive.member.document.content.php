<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['code'])) {
	exit();
}

$code = $APP_FUNCTIONS->filterInput($_GET['code']);

$data = array();

$stmt = $conn->prepare("SELECT document FROM document WHERE code = :code");
$stmt->bindParam(':code', $code, PDO::PARAM_STR);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
html {
	height:100%; 
	width:100%; 
	overflow:hidden;
	padding:0; 
	margin:0;
}
body { 
	-webkit-overflow-scrolling:touch; 
	height: 100%; 
	width:100%; 
	overflow-y:auto; 
	overflow-x:hidden;
	padding:0; 
	margin:0; 
}
#archive_paper_gutter {
	width:615px;
	min-height:11in;	
	background-color:#FFFFFF;
	border:2px solid #999999;
	margin: 10px auto;
	padding:78px 95px;
}
@media only screen and (max-width: 800px) {
	#archive_paper_gutter {
		width:615px;
		padding-left:65px;
		padding-right:65px;
	}
}
</style>
</head>

<body>
<div id="archive_page_current">
    <div id="archive_paper_gutter">
        <?php echo $row['document']; ?>
    </div>
</div>
</body>
</html>
