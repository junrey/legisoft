<?php

class AppFunctions {

function filterInput($s) {
	return filter_var(trim($s), FILTER_SANITIZE_STRING);
}

function getWeeks($date, $rollover) {
	$cut = substr($date, 0, 8);
	$daylen = 86400;

	$timestamp = strtotime($date);
	$first = strtotime($cut . "00");
	$elapsed = ($timestamp - $first) / $daylen;

	$weeks = 1;

	for ($i = 1; $i <= $elapsed; $i++) {
		$dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
		$daytimestamp = strtotime($dayfind);

		$day = strtolower(date("l", $daytimestamp));

		if($day == strtolower($rollover))  $weeks ++;
	}

	return $weeks;
}


}


?>