<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$stmt = $conn->prepare("SELECT view_document FROM _user WHERE username = :user"); 
$stmt->bindParam(':user', $user, PDO::PARAM_STR);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$members_view = $row['view_document'];

if(trim($members_view) == "") {
	$stmt2 = $conn->prepare("SELECT members_view FROM config"); 
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$members_view = $row2['members_view'];

}

$stmt3 = $conn->prepare("SELECT document FROM document WHERE code = :members_view");
$stmt3->bindParam(':members_view', $members_view, PDO::PARAM_STR);
$stmt3->execute();
$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);

echo $row3['document'];

?>