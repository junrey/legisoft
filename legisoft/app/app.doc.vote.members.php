<?php
require_once("../db/db.mysql.pdo.php");
	
$stmt = $conn->prepare("SELECT vote_members_view FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$vote_code = $row['vote_members_view'];

if($vote_code != "") {
	$stmt2 = $conn->prepare("SELECT title FROM vote_header WHERE code = :vote_code");
	$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

	
?>

<div class="vote_members_title"><?php echo $row2['title']; ?></div>

    <?php
		$stmt3 = $conn->prepare("SELECT username, lname, fname, image_file FROM _user WHERE voter = '1' AND type = '1' ORDER BY lname, fname"); 
		$stmt3->execute();
		while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) { 
			$stmt4 = $conn->prepare("SELECT b.detail FROM vote a INNER JOIN vote_item b ON a.vote = b.value WHERE a.code = :vote_code AND a.username = :username AND a.enable = '1' AND b.enable = '1'");  
			$stmt4->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
			$stmt4->bindParam(':username', $row3['username'], PDO::PARAM_STR);
			$stmt4->execute();
			$row4 = $stmt4->fetch(PDO::FETCH_ASSOC);
	?>
			<div class="vote_members uk-card uk-card-default uk-width-1-1">
				<div class="uk-card-header">
					<div class="uk-grid-small uk-flex-middle" uk-grid>
						<div class="uk-width-auto">
							<img class="uk-border-circle" width="60" height="60" src="admin/Legissoft/user_image/profile/<?php echo $row3['image_file']; ?>" onerror="if(this.src != 'images/no_profile_pic.gif') this.src = 'images/no_profile_pic.gif';">
						</div>
						<div class="uk-width-expand uk-text-left">
							<h3 class="uk-card-title"><?php echo $row3['lname'] . ", " . $row3['fname']; ?></h3>
						</div>
						<div class="uk-width-auto">
							<div class="view_<?php echo strtolower($row4['detail']); ?>"><?php echo $row4['detail']; ?></div>
						</div>
					</div>
				</div>
			</div>
    <?php } ?>
    <div class="vote_members_title" style="text-align:center">
		<?php 
			$stmt5 = $conn->prepare("SELECT b.detail, COUNT(b.detail) AS total FROM vote a INNER JOIN vote_item b ON a.vote = b.value WHERE a.code = :vote_code AND a.enable = '1' AND b.enable = '1' GROUP BY b.detail ORDER BY b.place");
			$stmt5->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
			$stmt5->execute();
		?>
		<?php while($row5 = $stmt5->fetch(PDO::FETCH_ASSOC)) { ?>
			<span><?php echo $row5['detail'] . ': <b>' .  $row5['total'] . '</b>'; ?></span>
		<?php } ?>
	</div>
<?php } ?>
