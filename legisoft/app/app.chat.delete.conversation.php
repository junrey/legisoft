<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user']) || !isset($_GET['user2'])) {
	exit();
}

$user  = $APP_FUNCTIONS->filterInput($_GET['user']);
$user2 = $APP_FUNCTIONS->filterInput($_GET['user2']);

if($user != "" && $user2 != "") {	
	$message_from_to = $user . $user2;
	$message_to_from = $user2 . $user;
	
	$stmt = $conn->prepare("UPDATE chat SET deleted = '1', updated_by = :updated_by, updated_date = NOW() WHERE CONCAT(message_from, message_to) IN (:message_from_to, :message_to_from) AND display_to = :display_to");
	$stmt->bindParam(':display_to',      $user,            PDO::PARAM_STR);
	$stmt->bindParam(':message_from_to', $message_from_to, PDO::PARAM_STR);
	$stmt->bindParam(':message_to_from', $message_to_from, PDO::PARAM_STR);
	$stmt->bindParam(':updated_by',      $user,            PDO::PARAM_STR);
	$stmt->execute();
}
	
?>