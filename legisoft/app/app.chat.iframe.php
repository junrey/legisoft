<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user']) || !isset($_GET['user2'])) {
	exit();
}

$user  = $APP_FUNCTIONS->filterInput($_GET['user']);
$user2 = $APP_FUNCTIONS->filterInput($_GET['user2']);

?>
<link rel="stylesheet" href="../plugins/uikit-3.0.0-beta.35/css/uikit.min.css" />
<style>
	html {
		height:100%; 
		width:100%; 
		overflow:hidden;
		padding:0; 
		margin:0;
	}
	body { 
		-webkit-overflow-scrolling:touch; 
		height:calc(100% - 20px); 
		width:100%; 
		overflow-y:scroll; 
		overflow-x:hidden;
		padding:10px 0; 
		margin:0; 
	}
	.avatar.chat {
		height:30px;
	}
	.chat_iframe_content {
		padding:10px; 
	}
	
	.chat_content_user_message {
		max-width: 250px;
		min-width: 110px;
		padding:2px;
		border-radius: 5px;
		
		overflow-wrap: break-word;
		word-wrap: break-word;
		
		/* Adds a hyphen where the word breaks */
		-webkit-hyphens: auto;
		-ms-hyphens: auto;
		-moz-hyphens: auto;
		hyphens: auto;
	}
	.chat_content_user_message .description {
		padding:0 5px 5px 5px;
	}
	
	.chat_content_user_message .meta {
		font-size: 11px;
		padding:5px;
	}
	.chat_content_user_image.left {
		float:left;
	}
	.chat_content_user_message.left {
		float:left;
		margin-left:6px;
		background-color:#f1f0f0;
		color:#000;
	}
	.chat_content_user_message.left table {
		width:100%;
		border-collapse:collapse;
	}
	.chat_content_user_message.left td {
		padding:0;
	}
	.chat_content_user_message.left .meta {
		color: #999;
	}
	.chat_content_user_message.left .chat_content_close {
		border: 0;
		padding: 5px;
		background: transparent;
		cursor: pointer;
	}
	.chat_content_user_image.right {
		float:right;
	}
	.chat_content_user_message.right {
		float:right;
		margin-right:5px;
		background-color:#73b4f3;
		color:#fff;
	}
	.chat_content_user_message.right table {
		width:100%;
		border-collapse:collapse;
	}
	.chat_content_user_message.right td {
		padding:0;
	}
	.chat_content_user_message.right .meta {
		color:#e0e9f1;
	}
	.chat_content_user_message.right .description {
		color:#fff;
	}
	.chat_content_user_message.right .chat_content_close {
		border: 0;
		padding: 5px;
		background: transparent;
		cursor: pointer;
	}
</style>
<div id="chat_iframe_<?php echo $user2; ?>" class="chat_iframe_content"></div>
<script>

	getChatContent();
	setInterval(function() { getChatContent(); }, 4000);
	
	var temp_size = 0;
	
	function getChatContent() {
		parent.$.ajax({
			method: 'GET',
			url: 'app/app.chat.iframe.content.php?user=<?php echo $user; ?>&user2=<?php echo $user2; ?>', 	
			success: function(result){
				var result_size = result.toString().length;
				if(temp_size != result_size) {
					parent.$(document).contents().find('#chat_iframe_<?php echo $user2; ?>').empty().html(result);
					scrollBottom();
					temp_size = result_size;
				}
				
			},
			error: function(error) {

			}
		});
	}
	
	function scrollBottom() {
		document.getElementById('chat_iframe_<?php echo $user2; ?>').scrollIntoView(false);
	}
	
</script>