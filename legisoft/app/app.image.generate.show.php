<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['code'])) {
	exit();
}

$code = $APP_FUNCTIONS->filterInput($_GET['code']);

$stmt = $conn->prepare("SELECT image, file_type FROM image WHERE code = :code");
$stmt->bindParam(':code', $code, PDO::PARAM_STR);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

if($stmt->rowCount() > 0) {
	$imagebytes = $row["image"];
	$file_type = $row["file_type"];
	
	header("Content-type: $file_type");
	print $imagebytes;
} 
?>