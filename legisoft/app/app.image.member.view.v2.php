<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['code'])) {
	exit();
}

$code = $APP_FUNCTIONS->filterInput($_GET['code']);

$stmt2 = $conn->prepare("SELECT filename FROM image_gallery WHERE code = :image_members_view AND deleted = '0'"); 
$stmt2->bindParam(':image_members_view', $code, PDO::PARAM_STR);
$stmt2->execute();	
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
$filename = $row2['filename'];

$stmt3 = $conn->prepare("UPDATE _user SET image_members_user = :code WHERE username = :user");
$stmt3->bindParam(':code', $image_members_view, PDO::PARAM_STR);
$stmt3->bindParam(':user', $user, PDO::PARAM_STR);

echo "admin/Legissoft/image_gallery/" . $filename;


?>