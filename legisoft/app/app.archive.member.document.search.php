<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['search'])) {
	exit();
}

$search = $APP_FUNCTIONS->filterInput($_GET['search']);

$data = array();

$document_type = array();
$stmt4 = $conn->prepare("SELECT code, detail FROM document_type WHERE isdelete = '0' AND enable = '1' AND members_show = '1'");
$stmt4->execute();
while($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
	$document_type[$row4['code']] = $row4['detail'];
}

$stmt = $conn->prepare("SELECT code, title, document_date, DATE(document_date) AS parse_date, document_type, filename, source FROM document WHERE CONCAT(title, ' ', document) LIKE :search AND isdelete = '0' ORDER BY document_date DESC");
$stmt->bindValue(':search', '%' . $search . '%');
$stmt->execute();


while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	
	$data[$row['code']]['code']     = $row['code'];
	$data[$row['code']]['title']    = htmlspecialchars($row['title']);
	$data[$row['code']]['document'] = $document_type[$row['document_type']];
	$data[$row['code']]['date']     = date('m/d/Y', strtotime($row['document_date'])); 
	$data[$row['code']]['filename'] = $row['filename'];
	$data[$row['code']]['source']   = $row['source'];
	
}

echo json_encode(array('data' => $data));	
?>
