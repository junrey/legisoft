<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
html {
	height:100%; 
	width:100%; 
	overflow:hidden;
	padding:0; 
	margin:0;
}
body { 
	-webkit-overflow-scrolling:touch; 
	height:calc(100% - 20px); 
	width:100%; 
	overflow-y:auto; 
	overflow-x:hidden;
	padding:0; 
	margin:0; 
}
.pdf_window {
	display:none;
	height: 100%;
	width: 100%;
	overflow: hidden;
	z-index:1; 
}
#paper_gutter {
	display:none;
	width:615px;
	min-height:11in;	
	background-color:#FFFFFF;
	border:2px solid #999999;
	margin: 10px auto;
	padding:78px 95px;
}
.action_window {
	display:none;
	position:absolute; 
	z-index:1; 
	left:0; 
	top:0;
	width:100%;
	height:100%;
	background: #fff;
}
.action_window #action_wrapper {
	height:calc(100% - 82px);
	background-color:#fff;
	overflow-x:hdden;
	overflow-y:auto;
	padding-bottom:40px;
}
.action_window #action_content {
	padding:20px;
}
.action_window #reference {
	background-color:#3e7ab4;
	padding:5px 10px;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center;
	color:#fff;
	font-size:25px;
}
.action_window #action_item {
	background-color:#fbfbfb;
	font-family:Verdana, Geneva, sans-serif;
	text-align:center; 
	padding:10px;
	letter-spacing: 2px;
	border-radius:5px;
}
.action_window #movant {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#08f4ad;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #second {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#ff8000;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee {
	font-family:Verdana, Geneva, sans-serif;
	background-color:#f9ca02;
	margin:10px 20px;
	border-radius:5px;
	padding:10px;
}
.action_window #commitee .label {
	width:100%;
	float:left;
	font-size:18px;
}
.action_window #movant .label, .action_window #second .label {
	width:370px;
	float:left;
	font-size:18px;
}
.action_window #movant .title, .action_window #second .title, .action_window #commitee .title {
	padding:10px;
	width:150px;
	vertical-align:top;
	font-size:20px;
	text-align:right;
	line-height:25px;
}
.action_window #movant .title span, .action_window #second .title span, .action_window #commitee .title span {
	float:right;
	color:#fff;
	font-size:30px;
	line-height:18px;
	margin-left:10px;
}
@media only screen and (max-width: 1024px) {
	.action_window #movant .label, .action_window #second .label {
		width:340px;
	}
	
	.action_window #commitee .label {
		width:100%;
	}
}

@media only screen and (max-width: 800px) {
	#paper_gutter {
		width:615px;
		padding-left:65px;
		padding-right:65px;
	}
	
	.action_window #commitee .label, .action_window #movant .label, .action_window #second .label {
		width:100%;
	}
}
</style>
</head>
<body>
<div id="page_current">
    <div id="paper_gutter">
        <div id="paper_main"></div>
    </div>
</div>
<div class="pdf_window"></div>
<div class="action_window"></div>
<script>
    parent.$(function() {
        //getContent();
        //setInterval("getContent()", 5000);
        //getMembersActionAlert();
        //setInterval("getMembersActionAlert()", 5000);
        membersActivity();
       // setInterval(function() { membersActivity(); }, 4000);
    });
    
    function membersActivity() {
		var source_audience_activity = new EventSource('app.members.activity.php?user=<?php echo $user; ?>');
		source_audience_activity.onmessage = function(event) { 
			var result = parent.$.parseJSON(event.data);
			
			if(result.data != '') {
				if(result.data['type'] == 'announcement') {
					parent.moduleMembersAlert();
					
				} else if(result.data['type'] == 'image') {
					parent.moduleImageShared(result.data['code']);
					
				} else if(result.data['type'] == 'voting') {
					parent.moduleMembersVotes(result.data['code']);
				
				} else if(result.data['type'] == 'action') {
					getMembersActionAlert(result.data['code']);
					
				} else if(result.data['type'] == 'document') {
					getContent(result.data['code']);
				}
			} else {
				hideWindow('');
			}
		};
        /*parent.$.ajax({
            method: 'GET',
            url: 'app/app.members.activity.php?user=<?php //echo $user; ?>',
            success: function(result){
                if(result.data != '') {
                    if(result.data['type'] == 'announcement') {
                        parent.moduleMembersAlert();
                        
                    } else if(result.data['type'] == 'image') {
                        parent.moduleImageShared(result.data['code']);
                        
                    } else if(result.data['type'] == 'voting') {
                        parent.moduleMembersVotes(result.data['code']);
                    
                    } else if(result.data['type'] == 'action') {
                        getMembersActionAlert(result.data['code']);
                        
                    } else if(result.data['type'] == 'document') {
                        getContent(result.data['code']);
                    }
                } else {
                	hideWindow('');
                }
            }
        });*/
    }
    
    var date_document = '';
    function getContent(code) {
        parent.$.ajax({
            method: 'GET',
            url: 'app/app.doc.ongoing.v2.php?code=' + code, 	
            success: function(result){
				if(result.data['source'] == 'pdf') {
					if(result.data['filename'] != '') {
						hideWindow('pdf');
						
						if(date_document != result.data['date_log']) {
							parent.$(document).contents().find('.pdf_window').empty().html('<iframe src="../plugins/pdfjs-1.9.426/web/viewer.html?file=../../../upload/' + encodeURIComponent(result.data['filename']) + '#zoom=page-width" style="height: 100%; width: 100%;"></iframe>');
							
							date_document = result.data['date_log'];
						}
					} else {
						hideWindow('');
					}
				} else {
					if(result.data['document'] != '') {
						hideWindow('document');
						
						if(date_document != result.data['date_log']) {
							parent.$(document).contents().find('#paper_main').empty().html(result.data['document']);
							
							date_document = result.data['date_log'];
						}
					} else {
						hideWindow('');
					}
				}
            },
            error: function(error) {

            }
        });
    }
    
    var date_action = '';
    function getMembersActionAlert(code) {
        parent.$.ajax({
            method: 'GET',
            url: 'app/app.doc.action.members.v2.php?code=' + code, 	
            success: function(result){
            	if(result.data['action'] != '') {
					hideWindow('action');
					
                    if(date_action != result.data['date_log']) {
                        parent.$(document).contents().find('.action_window').empty().html(result.data['action']);
                        
                        date_action = result.data['date_log'];
                    }
                } else {
                    hideWindow('');
                }
            },
            error: function(error) {

            }
        });
    }
    
    function hideWindow(id) {
        if(id == 'document') {
            parent.$(document).contents().find('#paper_gutter').show();
        } else {
            parent.$(document).contents().find('#paper_gutter').hide();
        }
		
		if(id == 'pdf') {
			parent.$(document).contents().find('.pdf_window').show();
		} else {
			parent.$(document).contents().find('.pdf_window').hide();
		}
        
        if(id == 'action') {
            parent.$(document).contents().find('.action_window').show();	
        } else {
            parent.$(document).contents().find('.action_window').hide();	
        }
        
        if(id == 'voting') {
			parent.UIkit.modal('#panel_member_votes').show();
        } else {
        	parent.UIkit.modal('#panel_member_votes').hide();
        }
        
        if(id == 'image') {
			parent.UIkit.modal("#panel_image_shared").show();
        } else {
        	parent.UIkit.modal("#panel_image_shared").hide();
        }
        
        if(id == 'announcement') {
        	parent.UIkit.modal("#panel_member_alert").show();
        } else {
        	parent.UIkit.modal("#panel_member_alert").hide();
        }
    }

</script>
</body>
</html>