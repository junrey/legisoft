<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$rows  = array();

$stmt = $conn->prepare("SELECT vote_code, vote_once FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$vote_code = $row['vote_code'];
$vote_once = $row['vote_once'];

if($vote_code != "") {
	if(isset($_GET['vote']) && trim($_GET['vote']) != "") {
		$vote = $APP_FUNCTIONS->filterInput($_GET['vote']);
		
		$stmt2 = $conn->prepare("UPDATE vote_header SET mod_by = :mod_by, mod_date = NOW() WHERE code = :code");
		$stmt2->bindParam(':code',   $vote_code, PDO::PARAM_STR);
		$stmt2->bindParam(':mod_by', $user, PDO::PARAM_STR);
		$stmt2->execute();
			
		$stmt2 = $conn->prepare("SELECT vote FROM vote WHERE code = :vote_code AND username = :user AND enable = '1'");  
		$stmt2->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
		$stmt2->bindParam(':user', $user, PDO::PARAM_STR);
		$stmt2->execute();
		
		if($stmt2->rowCount() > 0) {
			$stmt3 = $conn->prepare("UPDATE vote SET vote = :vote, mod_by = :user, mod_date = NOW() WHERE code = :vote_code AND username = :user");  
			$stmt3->bindParam(':vote', $vote, PDO::PARAM_STR);
			$stmt3->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
			$stmt3->bindParam(':user', $user, PDO::PARAM_STR);
			$stmt3->execute();
		} else {
			$stmt3 = $conn->prepare("INSERT INTO vote SET code = :vote_code, username = :user, vote = :vote, mod_by = :user, mod_date = NOW()");  
			$stmt3->bindParam(':vote', $vote, PDO::PARAM_STR);
			$stmt3->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
			$stmt3->bindParam(':user', $user, PDO::PARAM_STR);
			$stmt3->execute();
		}
	}

	
	$stmt4 = $conn->prepare("SELECT code, title, content FROM vote_header WHERE code = :vote_code AND enable = '1'"); 
	$stmt4->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt4->execute();
	$row4 = $stmt4->fetch(PDO::FETCH_ASSOC);
	
	$rows['title']   = $row4['title'];
	$rows['content'] = $row4['content'];

	$stmt5 = $conn->prepare("SELECT vote FROM vote WHERE code = :vote_code AND username = :user AND enable = '1'");
	$stmt5->bindParam(':vote_code', $vote_code, PDO::PARAM_STR);
	$stmt5->bindParam(':user', $user, PDO::PARAM_STR);
	$stmt5->execute();
	$row5 = $stmt5->fetch(PDO::FETCH_ASSOC);
	
	$disabled = "";
	if($row5['vote'] > 0) {
		$disabled = "disabled";
	}
	
	$button = "";
	$stmt6 = $conn->prepare("SELECT detail, value, place FROM vote_item WHERE enable = '1' ORDER BY place");  
	$stmt6->execute();
	while($row6 = $stmt6->fetch(PDO::FETCH_ASSOC)) {
		$status = "";
		if($row5['vote'] == $row6['value']) {
			$status = "selected";
		}
		
		if($vote_once == 1) {
			if($disabled == "disabled") {
				$button = $button . '<input type="button" disabled class="uk-button uk-button-large uk-border-circle vote ' . $status . '" value="' . $row6['detail'] . '" />';
			} else {
				$button = $button . '<input type="button" onclick="moduleVoteAction(' . $row6['value'] . ');" class="uk-button uk-button-large uk-border-circle vote ' . $status . '" value="' . $row6['detail'] . '" />';
			}
		} else {
			if($status == "selected") {
				$button = $button . '<input type="button" onclick="moduleVoteAction(\'0\');" class="uk-button uk-button-large uk-border-circle vote selected" value="' . $row6['detail'] . '" />';
			} else {
				$button = $button . '<input type="button" onclick="moduleVoteAction(' . $row6['value'] . ');" class="uk-button uk-button-large uk-border-circle vote" value="' . $row6['detail'] . '" />';
			}
		}
	}

	$rows['buttons'] = $button;

} else {

	$rows['title']   = '&nbsp;';
	$rows['content'] = '<p class="uk-text-large uk-text-center">Voting closed!</p>';
	$rows['buttons'] = '&nbsp;';	
}




echo json_encode(array('data' => $rows));

?>