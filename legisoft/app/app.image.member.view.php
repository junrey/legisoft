<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$stmt = $conn->prepare("SELECT image_members_view FROM config");
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$image_members_view = $row['image_members_view'];


if(trim($image_members_view) != "") {
	$stmt2 = $conn->prepare("SELECT filename FROM image_gallery WHERE code = :image_members_view AND deleted = '0'"); 
	$stmt2->bindParam(':image_members_view', $image_members_view, PDO::PARAM_STR);
	$stmt2->execute();	
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	$filename = $row2['filename'];
	
	$stmt3 = $conn->prepare("UPDATE _user SET image_members_user = :code WHERE username = :user");
	$stmt3->bindParam(':code', $image_members_view, PDO::PARAM_STR);
	$stmt3->bindParam(':user', $user, PDO::PARAM_STR);
	
	echo "admin/Legissoft/image_gallery/" . $filename;

} 
?>