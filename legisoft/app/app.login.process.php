<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_POST['username']) || !isset($_POST['password'])) {
	header("HTTP/1.0 404 Not Found");
	header('Content-Type: application/json; charset=UTF-8');
	echo json_encode(array('error' => ['message' => 'Incorrect username and password!']));	

} else {
	$username = $APP_FUNCTIONS->filterInput($_POST['username']);
	$password = $APP_FUNCTIONS->filterInput($_POST['password']);

	$stmt = $conn->prepare("SELECT username, password, lname, fname, type, image_file FROM _user WHERE username = :username AND type IN('1', '2')");
	$stmt->bindParam(':username', $username, PDO::PARAM_STR);
	
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	if(password_verify($password, $row['password'])) {
		$user = array();
		$user['username']  = $row['username']; 
		$user['firstname'] = $row['fname']; 
		$user['lastname']  = $row['lname']; 
		$user['avatar']    = "admin/Legissoft/user_image/thumbnail/" . $row['image_file'];
		$user['type']      = $row['type']; 
		
		$stmt2 = $conn->prepare("UPDATE _user SET login = NOW() WHERE username = :username");
		$stmt2->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt2->execute();
		
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode(array('data' => $user));
		
	} else {
		header("HTTP/1.0 404 Not Found");
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode(array('error' => ['message' => 'Incorrect username and password!']));	
	}
	
} 

?>