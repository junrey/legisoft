<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");

$rows = array();

$stmt = $conn->prepare("SELECT vote_alter, messaging, messaging_popup FROM config"); 
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$rows['messaging']       = $row['messaging'];
$rows['messaging_popup'] = $row['messaging_popup'];
$rows['vote_alter']      = $row['vote_alter'];

echo json_encode(array('data' => $rows));

?>