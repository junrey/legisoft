<?php
//header('Content-Type: application/json; charset=UTF-8');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$rows = array();

$stmt = $conn->prepare("SELECT members_alert_message, members_alert_message_show, image_members_view, vote_members_view, members_action_view, members_view FROM config");  
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$stmt2 = $conn->prepare("SELECT view_image, view_voting, view_action, view_document FROM _user WHERE username = :username"); 
$stmt2->bindParam(':username', $user, PDO::PARAM_STR);
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

if($row['members_alert_message_show'] == 1 && $row['members_alert_message'] != "") {
	$rows['type'] = 'announcement';
	$rows['code'] = '';
	
} else if($row2['view_image'] != "") {
	$rows['type'] = 'image';
	$rows['code'] = $row2['view_image'];

} else if($row2['view_voting'] != "") {
	$rows['type'] = 'voting';
	$rows['code'] = $row2['view_voting'];
	
} else if($row2['view_action'] != "") {
	$rows['type'] = 'action';
	$rows['code'] = $row2['view_action'];
	
} else if($row2['view_document'] != "") {
	$rows['type'] = 'document';
	$rows['code'] = $row2['view_document'];
	
} else if($row['image_members_view'] != "") {
	$rows['type'] = 'image';
	$rows['code'] = $row['image_members_view'];
	
} else if($row['vote_members_view'] != "") {
	$rows['type'] = 'voting';
	$rows['code'] = $row['vote_members_view'];
	
} else if($row['members_action_view'] != "") {
	$rows['type'] = 'action';
	$rows['code'] = $row['members_action_view'];
	
} else if($row['members_view'] != "") {
	$rows['type'] = 'document';
	$rows['code'] = $row['members_view'];
}

//echo json_encode(array('data' => $rows));
echo "data: " . json_encode(array('data' => $rows)) . "\n\n";

?>