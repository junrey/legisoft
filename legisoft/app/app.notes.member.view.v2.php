<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");

$rows = array();
$rows['announcement'] = '';
$rows['date_log']     = '';

$stmt = $conn->prepare("SELECT members_alert_message, members_alert_message_date FROM config WHERE members_alert_message_show = '1'"); 
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);


if($row['members_alert_message'] != "") {
	$rows['announcement'] = $row['members_alert_message'];
	$rows['date_log']     = $row['members_alert_message_date'];
}

echo json_encode(array('data' => $rows));

?>