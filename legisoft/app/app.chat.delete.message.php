<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user']) || !isset($_GET['delete_code'])) {
	exit();
}

$user   = $APP_FUNCTIONS->filterInput($_GET['user']);
$row_id = $APP_FUNCTIONS->filterInput($_GET['delete_code']);

if($row_id != "") {
	$stmt = $conn->prepare("UPDATE chat SET deleted = '1', updated_by = :updated_by, updated_date = NOW() WHERE row_id = :row_id");
	$stmt->bindParam(':row_id',     $row_id, PDO::PARAM_STR);
	$stmt->bindParam(':updated_by', $user,   PDO::PARAM_STR);
	$stmt->execute();

}


?>
