<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();


$data = array();

$document_type = array();
$stmt4 = $conn->prepare("SELECT code, detail FROM document_type WHERE isdelete = '0' AND enable = '1' AND members_show = '1'");
$stmt4->execute();
while($row4 = $stmt4->fetch(PDO::FETCH_ASSOC)) {
	$document_type[$row4['code']] = $row4['detail'];
}

$stmt = $conn->prepare("SELECT code, title, document_date, DATE(document_date) AS parse_date, document_type, filename, source FROM document WHERE isdelete = '0' ORDER BY YEAR(document_date) DESC, MONTH(document_date) DESC, WEEK(document_date) ASC, title ASC");
$stmt->execute();

$display = "";
$label   = "";
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$time = strtotime($row['document_date']);
	
	$temp = array();
	$temp['code']     = $row['code'];
	$temp['title']    = htmlspecialchars($row['title']);
	$temp['filename'] = $row['filename'];
	$temp['source']   = $row['source'];
	
	$show = 0;
	if(date('Y', $time) == date('Y') && date('m', $time) == date('m') && $APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday") == $APP_FUNCTIONS->getWeeks(date('Y-m-d'), "sunday")) {
		$display = date('Y', $time) . date('F', $time) . $APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday");
		$label   = "Documents-" . date('Y', $time) . "-" . date('F', $time) . "-" . "Week " . $APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday");
	}
	
	$data['folder'][date('Y', $time)]['label'] = date('Y', $time);
	$data['folder'][date('Y', $time)]['month'][date('F', $time)]['label'] = date('F', $time);
	$data['folder'][date('Y', $time)]['month'][date('F', $time)]['week'][$APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['label'] = $APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday");
	$data['folder'][date('Y', $time)]['month'][date('F', $time)]['week'][$APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['type'][$row['document_type']]['label'] = $document_type[$row['document_type']];
	$data['folder'][date('Y', $time)]['month'][date('F', $time)]['week'][$APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['type'][$row['document_type']]['file'][$row['code']] = $temp;
	$data['folder'][date('Y', $time)]['month'][date('F', $time)]['week'][$APP_FUNCTIONS->getWeeks($row['parse_date'], "sunday")]['show'] = $show;
	
}

$data['show']['code']  = $display;
$data['show']['label'] = $label;

echo json_encode(array('data' => $data));	
?>
