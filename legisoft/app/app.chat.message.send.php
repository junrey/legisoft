<?php
require_once("../db/db.mysql.pdo.php");

$user = trim($_GET['user']);

if(isset($_POST['user2']) && trim($_POST['user2']) != "" && trim($_POST['chat_message']) != "") {

	$user2   = filter_var(trim($_POST['user2']), FILTER_SANITIZE_STRING);
	$message = filter_var(trim($_POST['chat_message']), FILTER_SANITIZE_STRING);

	/*$stmt = $conn->prepare("UPDATE chat SET view = '1' WHERE posted_to = '" . $user . "' AND username = :user2 AND (DAY(posted) = DAY(CURDATE()) AND MONTH(posted) = MONTH(CURDATE()) AND YEAR(posted) = YEAR(CURDATE()))");
	$stmt->bindParam(':user2', $user2, PDO::PARAM_STR);
	$stmt->execute();
	
	$stmt = $conn->prepare("INSERT INTO chat SET username = '" . $user . "', message = :message, posted_to = :posted_to, code = :code, posted = NOW()");
	$stmt->bindParam(':message', $message, PDO::PARAM_STR);
	$stmt->bindParam(':posted_to', $user2, PDO::PARAM_STR);
	$stmt->bindParam(':code', $code, PDO::PARAM_STR);
	$stmt->execute();*/
	
	$stmt = $conn->prepare(
		"INSERT INTO chat SET posted_by = :posted_by, message_date = NOW(), message_from = :message_from, message_to = :message_to, message = :message, display_to = :user; " .
		"INSERT INTO chat SET posted_by = :posted_by, message_date = NOW(), message_from = :message_from, message_to = :message_to, message = :message, display_to = :user2"
	);
	
	$stmt->bindParam(':posted_by',    $user,    PDO::PARAM_STR);
	$stmt->bindParam(':message_from', $user,    PDO::PARAM_STR);
	$stmt->bindParam(':message_to',   $user2,   PDO::PARAM_STR);
	$stmt->bindParam(':message',      $message, PDO::PARAM_STR);
	$stmt->bindParam(':user',         $user,    PDO::PARAM_STR);
	$stmt->bindParam(':user2',        $user2,   PDO::PARAM_STR);
	$stmt->execute();

}
?>