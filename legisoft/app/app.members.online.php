<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$stmt = $conn->prepare("SELECT username, fname, lname, image_file FROM _user WHERE username <> :user AND type <> '3' AND enable = '1' AND deleted = '0' ORDER BY FIELD(type, '1', '2'), lname, fname");
$stmt->bindParam(':user', $user, PDO::PARAM_STR);
$stmt->execute();

$rows = array();
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$rows[$row['username']] = $row;
	$rows[$row['username']]['image'] = 'admin/Legissoft/user_image/thumbnail/' . $row['image_file'];
	
}

echo json_encode(array('data' => $rows));

?>
