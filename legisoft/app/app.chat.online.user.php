<?php 
//header('Content-Type: application/json; charset=UTF-8');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$rows     = array();
$chat_new = array();

$stmt = $conn->prepare("SELECT username, TIMESTAMPDIFF(MINUTE, login_active, NOW()) AS active FROM _user WHERE enable = '1' AND deleted = '0' ORDER BY FIELD(type, '1', '2'), lname, fname");
$stmt->execute();	

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$rows[$row['username']] = $row['active'];
	
	$stmt2 = $conn->prepare("SELECT COUNT(view) AS total_msg FROM chat WHERE message_from = :message_from AND display_to = :display_to AND view = '0'");  
	$stmt2->bindParam(':display_to',   $user,            PDO::PARAM_STR);
	$stmt2->bindParam(':message_from', $row['username'], PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
	
	if($row2['total_msg'] > 0) {
		$chat_new[$row['username']] = $row2['total_msg'];
	}
}

$data = array();
$data['online'] = $rows;
$data['chat']   = $chat_new;

//echo json_encode(array('data' => $data));	
echo "data: " . json_encode(array('data' => $data)) . "\n\n";

?>