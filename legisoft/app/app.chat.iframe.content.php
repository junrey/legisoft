<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user']) || !isset($_GET['user2'])) {
	exit();
}

$user  = $APP_FUNCTIONS->filterInput($_GET['user']);
$user2 = $APP_FUNCTIONS->filterInput($_GET['user2']);

$message_from_to = $user . $user2;
$message_to_from = $user2 . $user;

$stmt = $conn->prepare("UPDATE chat SET view = '1' WHERE CONCAT(message_from, message_to) IN (:message_from_to, :message_to_from) AND display_to = :display_to");
$stmt->bindParam(':display_to', $user,  PDO::PARAM_STR);
$stmt->bindParam(':message_from_to', $message_from_to, PDO::PARAM_STR);
$stmt->bindParam(':message_to_from', $message_to_from, PDO::PARAM_STR);
$stmt->execute();	

$stmt = $conn->prepare("SELECT row_id, posted_by, message_date, message, message_to FROM chat WHERE CONCAT(message_from, message_to) IN (:message_from_to, :message_to_from) AND display_to = :display_to AND deleted = '0' ORDER BY message_date ASC");
$stmt->bindParam(':display_to', $user,  PDO::PARAM_STR);
$stmt->bindParam(':message_from_to', $message_from_to, PDO::PARAM_STR);
$stmt->bindParam(':message_to_from', $message_to_from, PDO::PARAM_STR);
$stmt->execute();

$stmt2 = $conn->prepare("SELECT image_file FROM _user WHERE username = :username");
$stmt2->bindParam(':username', $user, PDO::PARAM_STR);
$stmt2->execute();
$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

$stmt3 = $conn->prepare("SELECT image_file FROM _user WHERE username = :username");
$stmt3->bindParam(':username', $user2, PDO::PARAM_STR);
$stmt3->execute();
$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	if($row['message_to'] == $user2){ ?>
        <div id="chat_message_<?php echo $row['row_id']; ?>" class="chat_message_panel">
        	<p></p>      
        	<div class="chat_content_user_image right">
       			<img class="avatar chat uk-border-circle" src="../admin/Legissoft/user_image/profile/<?php echo $row2['image_file']; ?>" onerror="if(this.src != '../images/no_profile_pic.gif') this.src = '../images/no_profile_pic.gif';">
        	</div>
        	<div class="chat_content_user_message right">
                <table>
                	<tr>
						<td><div class="meta"><?php echo date('g:i:s a', strtotime($row['message_date'])); ?></div></td>
                    	<td style="width:30px; text-align:right;">
                        	<button class="chat_content_close" onclick="parent.deleteMessage('<?php echo $row['row_id']; ?>', '<?php echo $user2; ?>');">
                           		<img src="../images/close.png"  />
                        	</button>
                    	</td>                    	
                	</tr>
                    <tr>
                    	<td colspan="2">
                        	<div class="description"><?php echo nl2br($row['message']); ?></div>
                        </td>
                    </tr>
                </table>
        	</div>    
        </div>
	<?php } else { ?>
  	<div id="chat_message_<?php echo $row['row_id']; ?>" class="chat_message_panel">
      <p></p>
			<div class="chat_content_user_image left">
				<img class="avatar chat uk-border-circle" src="../admin/Legissoft/user_image/profile/<?php echo $row3['image_file']; ?>" onerror="if(this.src != '../images/no_profile_pic.gif') this.src = '../images/no_profile_pic.gif';">
			</div>
			<div class="chat_content_user_message left">
                <table>
                	<tr>
						<td><div class="meta"><?php echo date('g:i:s a', strtotime($row['message_date'])); ?></div></td>					
                    	<td style="width:30px; text-align:right;">
                        	<button class="chat_content_close" onclick="parent.deleteMessage('<?php echo $row['row_id']; ?>', '<?php echo $user2; ?>');">
                                <img src="../images/close.png" />
                            </button>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="2">
                        	<div class="description"><?php echo nl2br($row['message']); ?></div>
                        </td>
                    </tr>
                </table>
				
			</div>			
		</div>
	<?php } ?>
	<div style="clear: both;"></div>
<?php } ?>