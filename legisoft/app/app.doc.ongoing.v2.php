<?php
header('Content-Type: application/json; charset=UTF-8');
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['code'])) {
	exit();
}

$rows = array();
$rows['document'] = "";
$rows['date_log'] = "";

$code = $APP_FUNCTIONS->filterInput($_GET['code']);

$stmt3 = $conn->prepare("SELECT * FROM document WHERE code = :members_view");
$stmt3->bindParam(':members_view', $code, PDO::PARAM_STR);
$stmt3->execute();

if($stmt3->rowCount() > 0) {
	$row3 = $stmt3->fetch(PDO::FETCH_ASSOC);
	$rows['document'] = $row3['document'];
	$rows['filename'] = $row3['filename'];
	$rows['source']   = $row3['source'];
	$rows['date_log'] = $row3['mod_date'];
}

echo json_encode(array('data' => $rows));

?>