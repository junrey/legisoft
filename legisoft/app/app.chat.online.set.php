<?php
require_once("../db/db.mysql.pdo.php");
require_once("app.functions.php");

$APP_FUNCTIONS = new AppFunctions();

if(!isset($_GET['user'])) {
	exit();
}

$user = $APP_FUNCTIONS->filterInput($_GET['user']);

$stmt = $conn->prepare("UPDATE _user SET login_active = NOW() WHERE username = :user");
$stmt->bindParam(':user', $user, PDO::PARAM_STR);
$stmt->execute();

?>