-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2018 at 10:02 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `legissoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `code` varchar(30) NOT NULL,
  `title` varchar(200) NOT NULL,
  `action` text NOT NULL,
  `action_date` date NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `save` tinyint(1) NOT NULL DEFAULT '0',
  `status_code` varchar(30) NOT NULL,
  `reference` varchar(30) NOT NULL,
  `isdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `action_committee`
--

CREATE TABLE `action_committee` (
  `committee_code` varchar(30) NOT NULL,
  `action_code` varchar(30) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `action_item`
--

CREATE TABLE `action_item` (
  `code` varchar(30) NOT NULL,
  `detail` varchar(50) NOT NULL,
  `place` int(11) NOT NULL DEFAULT '1',
  `color` varchar(10) NOT NULL DEFAULT '#000000',
  `font_size` varchar(10) NOT NULL DEFAULT 'medium',
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `isdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `action_item`
--

INSERT INTO `action_item` (`code`, `detail`, `place`, `color`, `font_size`, `enable`, `mod_by`, `mod_date`, `isdelete`) VALUES
('ACT1611042011004525', 'APPROVED', 1, '#820713', 'xx-large', 1, 'junrey', '2011-11-08 16:36:45', 0),
('ACT2011042011004535', 'DISAPPROVED', 2, '#820713', 'xx-large', 1, 'dbadmin', '2011-11-19 21:32:16', 0),
('ACT3611042011004558', 'DEFERRED', 3, '#820713', 'xx-large', 1, 'dbadmin', '2011-11-19 21:32:21', 0),
('ACT4211042011004650', 'REFER  BACK TO COMMITTEE', 4, '#820713', 'xx-large', 1, 'dbadmin', '2011-11-19 21:32:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `action_movant`
--

CREATE TABLE `action_movant` (
  `movant_code` varchar(30) NOT NULL,
  `action_code` varchar(30) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `action_reference`
--

CREATE TABLE `action_reference` (
  `code` varchar(30) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `button_text` varchar(25) NOT NULL,
  `place` tinyint(4) NOT NULL DEFAULT '1',
  `color` varchar(10) NOT NULL DEFAULT '#000000',
  `font_size` varchar(10) NOT NULL DEFAULT 'medium',
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `isdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `action_reference`
--

INSERT INTO `action_reference` (`code`, `detail`, `button_text`, `place`, `color`, `font_size`, `enable`, `mod_by`, `mod_date`, `isdelete`) VALUES
('REF4011092011114802', 'REFERENCE OF BUSINESS', 'REFERENCE OF BUSINESS', 1, '#000000', '48px', 1, 'junrey', '2011-11-20 15:50:37', 0),
('REF9711092011114823', 'CALENDAR OF BUSINESS', 'CALENDAR OF BUSINESS', 2, '#000000', '48px', 1, 'junrey', '2011-11-20 15:39:48', 0),
('REF9011092011114839', 'CERTIFIED URGENT', 'URGENT MATTERS', 3, '#000000', '48px', 1, 'bryan', '2011-12-07 10:31:11', 0),
('REF5811092011114923', 'OTHER MATTERS', 'OTHER MATTERS', 4, '#000000', '48px', 1, 'junrey', '2011-11-20 15:46:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `action_second`
--

CREATE TABLE `action_second` (
  `second_code` varchar(30) NOT NULL,
  `action_code` varchar(30) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `row_id` bigint(20) NOT NULL,
  `posted_by` varchar(20) NOT NULL,
  `message_date` datetime NOT NULL,
  `message_from` varchar(20) NOT NULL,
  `message_to` varchar(20) NOT NULL,
  `message` varchar(500) NOT NULL,
  `display_to` varchar(20) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`row_id`, `posted_by`, `message_date`, `message_from`, `message_to`, `message`, `display_to`, `view`, `updated_date`, `updated_by`, `deleted`) VALUES
(1, 'cordero', '2017-09-29 22:44:06', 'cordero', 'spexec', 'hello', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(2, 'cordero', '2017-09-29 22:44:06', 'cordero', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(3, 'alonso', '2017-09-29 22:51:55', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(4, 'alonso', '2017-09-29 22:51:55', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(5, 'alonso', '2017-09-29 22:52:23', 'alonso', 'spexec', 'sdfdsf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(6, 'alonso', '2017-09-29 22:52:23', 'alonso', 'spexec', 'sdfdsf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(7, 'cordero', '2017-09-29 23:50:46', 'cordero', 'spexec', 'hello', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(8, 'cordero', '2017-09-29 23:50:46', 'cordero', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(9, 'alonso', '2017-09-30 00:00:23', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(10, 'alonso', '2017-09-30 00:00:23', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(11, 'alonso', '2017-09-30 00:00:45', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(12, 'alonso', '2017-09-30 00:00:45', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(13, 'alonso', '2017-09-30 00:06:46', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(14, 'alonso', '2017-09-30 00:06:46', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(15, 'alonso', '2017-09-30 00:06:49', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(16, 'alonso', '2017-09-30 00:06:49', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(17, 'alonso', '2017-09-30 00:12:37', 'alonso', 'spexec', 'fgfdgfd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(18, 'alonso', '2017-09-30 00:12:37', 'alonso', 'spexec', 'fgfdgfd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(19, 'spexec', '2017-10-02 22:22:27', 'spexec', 'cordova', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(20, 'spexec', '2017-10-02 22:22:27', 'spexec', 'cordova', 'hello', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(21, 'spexec', '2017-10-02 22:23:02', 'spexec', 'cordova', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(22, 'spexec', '2017-10-02 22:23:02', 'spexec', 'cordova', 'hello', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(23, 'spexec', '2017-10-03 16:19:30', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(24, 'spexec', '2017-10-03 16:19:30', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(25, 'alonso', '2017-10-03 16:19:47', 'alonso', 'spexec', 'hrhhhh', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(26, 'alonso', '2017-10-03 16:19:47', 'alonso', 'spexec', 'hrhhhh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(27, 'alonso', '2017-10-03 16:19:52', 'alonso', 'spexec', 'hgghjjjjjhgg', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(28, 'alonso', '2017-10-03 16:19:52', 'alonso', 'spexec', 'hgghjjjjjhgg', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(29, 'alonso', '2017-10-03 16:19:57', 'alonso', 'spexec', 'dfgnjjj', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(30, 'alonso', '2017-10-03 16:19:57', 'alonso', 'spexec', 'dfgnjjj', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(31, 'cordova', '2017-10-05 00:33:22', 'cordova', 'alonso', 'cxzczxcxzc', 'cordova', 1, '2017-10-05 00:34:34', 'cordova', 1),
(32, 'cordova', '2017-10-05 00:33:22', 'cordova', 'alonso', 'cxzczxcxzc', 'alonso', 1, '2017-10-12 20:07:04', 'alonso', 1),
(33, 'cordova', '2017-10-05 00:33:26', 'cordova', 'alonso', 'sadasdasd', 'cordova', 1, '2017-10-05 00:34:34', 'cordova', 1),
(34, 'cordova', '2017-10-05 00:33:26', 'cordova', 'alonso', 'sadasdasd', 'alonso', 1, '2017-10-12 20:07:01', 'alonso', 1),
(35, 'cordova', '2017-10-05 00:34:22', 'cordova', 'alonso', 'sadasd', 'cordova', 1, '2017-10-05 00:34:34', 'cordova', 1),
(36, 'cordova', '2017-10-05 00:34:22', 'cordova', 'alonso', 'sadasd', 'alonso', 1, '2017-10-12 20:07:03', 'alonso', 1),
(37, 'cordova', '2017-10-05 00:34:28', 'cordova', 'alonso', 'dsadsad', 'cordova', 1, '2017-10-05 00:34:34', 'cordova', 1),
(38, 'cordova', '2017-10-05 00:34:28', 'cordova', 'alonso', 'dsadsad', 'alonso', 1, '2017-10-12 20:06:59', 'alonso', 1),
(39, 'spexec', '2017-10-07 15:07:57', 'spexec', 'cordero', 'hi', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(40, 'spexec', '2017-10-07 15:07:57', 'spexec', 'cordero', 'hi', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(41, 'spexec', '2017-10-07 15:08:28', 'spexec', 'cordero', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(42, 'spexec', '2017-10-07 15:08:28', 'spexec', 'cordero', 'hello', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(43, 'cordero', '2017-10-07 15:08:42', 'cordero', 'spexec', 'Hello', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(44, 'cordero', '2017-10-07 15:08:42', 'cordero', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(45, 'cordero', '2017-10-07 15:09:02', 'cordero', 'spexec', 'Aka so darana', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(46, 'cordero', '2017-10-07 15:09:02', 'cordero', 'spexec', 'Aka so darana', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(47, 'spexec', '2017-10-07 15:09:33', 'spexec', 'cordero', 'asdasda', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(48, 'spexec', '2017-10-07 15:09:33', 'spexec', 'cordero', 'asdasda', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(49, 'cordova', '2017-10-07 15:10:04', 'cordova', 'cordero', 'hello', 'cordova', 1, '0000-00-00 00:00:00', '', 0),
(50, 'cordova', '2017-10-07 15:10:04', 'cordova', 'cordero', 'hello', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(51, 'cordero', '2017-10-07 15:10:16', 'cordero', 'cordova', 'Hello', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(52, 'cordero', '2017-10-07 15:10:16', 'cordero', 'cordova', 'Hello', 'cordova', 1, '0000-00-00 00:00:00', '', 0),
(53, 'cordero', '2017-10-07 15:10:57', 'cordero', 'spexec', 'Hello', 'cordero', 1, '2017-10-07 15:11:06', 'cordero', 1),
(54, 'cordero', '2017-10-07 15:10:57', 'cordero', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(55, 'cordero', '2017-10-07 15:11:11', 'cordero', 'spexec', 'Hhghh', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(56, 'cordero', '2017-10-07 15:11:11', 'cordero', 'spexec', 'Hhghh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(57, 'cordero', '2017-10-07 15:24:28', 'cordero', 'bandojo', 'Helllo', 'cordero', 1, '2017-10-07 15:24:58', 'cordero', 1),
(58, 'cordero', '2017-10-07 15:24:28', 'cordero', 'bandojo', 'Helllo', 'bandojo', 0, '0000-00-00 00:00:00', '', 0),
(59, 'cordero', '2017-10-07 15:24:41', 'cordero', 'bandojo', 'Grllo', 'cordero', 1, '2017-10-07 15:24:59', 'cordero', 1),
(60, 'cordero', '2017-10-07 15:24:41', 'cordero', 'bandojo', 'Grllo', 'bandojo', 0, '0000-00-00 00:00:00', '', 0),
(61, 'cordero', '2017-10-07 15:24:50', 'cordero', 'alonso', 'Hrjhgg', 'cordero', 1, '2017-10-07 15:24:54', 'cordero', 1),
(62, 'cordero', '2017-10-07 15:24:50', 'cordero', 'alonso', 'Hrjhgg', 'alonso', 1, '2017-10-12 19:57:57', 'alonso', 1),
(63, 'cordova', '2017-10-07 16:54:26', 'cordova', 'spexec', 'Hello', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(64, 'cordova', '2017-10-07 16:54:26', 'cordova', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(65, 'cordova', '2017-10-07 16:54:31', 'cordova', 'spexec', 'Hello', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(66, 'cordova', '2017-10-07 16:54:31', 'cordova', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(67, 'cordova', '2017-10-07 16:54:35', 'cordova', 'spexec', 'Hello', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(68, 'cordova', '2017-10-07 16:54:35', 'cordova', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(69, 'cordova', '2017-10-07 16:54:40', 'cordova', 'spexec', 'Weert', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(70, 'cordova', '2017-10-07 16:54:40', 'cordova', 'spexec', 'Weert', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(71, 'cordova', '2017-10-07 16:54:43', 'cordova', 'spexec', 'Fghh', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(72, 'cordova', '2017-10-07 16:54:43', 'cordova', 'spexec', 'Fghh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(73, 'cordova', '2017-10-07 16:54:47', 'cordova', 'spexec', 'Tgh', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(74, 'cordova', '2017-10-07 16:54:47', 'cordova', 'spexec', 'Tgh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(75, 'cordova', '2017-10-07 16:54:52', 'cordova', 'spexec', 'Fdjdjd', 'cordova', 1, '2017-10-07 16:55:08', 'cordova', 1),
(76, 'cordova', '2017-10-07 16:54:52', 'cordova', 'spexec', 'Fdjdjd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(77, 'cordova', '2017-10-07 16:55:15', 'cordova', 'spexec', 'Hello', 'cordova', 1, '0000-00-00 00:00:00', '', 0),
(78, 'cordova', '2017-10-07 16:55:15', 'cordova', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(79, 'cordova', '2017-10-08 14:45:24', 'cordova', 'spexec', 'asdsadasdsa', 'cordova', 1, '0000-00-00 00:00:00', '', 0),
(80, 'cordova', '2017-10-08 14:45:24', 'cordova', 'spexec', 'asdsadasdsa', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(81, 'cordova', '2017-10-08 14:48:00', 'cordova', 'spexec', 'asdasdas', 'cordova', 1, '0000-00-00 00:00:00', '', 0),
(82, 'cordova', '2017-10-08 14:48:00', 'cordova', 'spexec', 'asdasdas', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(83, 'alonso', '2017-10-08 14:52:34', 'alonso', 'spexec', 'sdfsdfsdfsd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(84, 'alonso', '2017-10-08 14:52:34', 'alonso', 'spexec', 'sdfsdfsdfsd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(85, 'cordero', '2017-10-08 14:53:17', 'cordero', 'spexec', 'sdfsdfsdf', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(86, 'cordero', '2017-10-08 14:53:17', 'cordero', 'spexec', 'sdfsdfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(87, 'cordero', '2017-10-08 15:09:56', 'cordero', 'spexec', 'sadasdasd', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(88, 'cordero', '2017-10-08 15:09:56', 'cordero', 'spexec', 'sadasdasd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(89, 'cordero', '2017-10-08 15:10:06', 'cordero', 'spexec', 'xcxzc', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(90, 'cordero', '2017-10-08 15:10:06', 'cordero', 'spexec', 'xcxzc', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(91, 'cordero', '2017-10-08 15:10:30', 'cordero', 'spexec', 'xczxcxzc', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(92, 'cordero', '2017-10-08 15:10:30', 'cordero', 'spexec', 'xczxcxzc', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(93, 'alonso', '2017-10-08 23:23:55', 'alonso', 'spexec', 'Hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(94, 'alonso', '2017-10-08 23:23:55', 'alonso', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(95, 'spexec', '2017-10-08 23:24:08', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(96, 'spexec', '2017-10-08 23:24:08', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(97, 'zayco', '2017-10-10 15:41:58', 'zayco', 'spexec', 'Hello', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(98, 'zayco', '2017-10-10 15:41:58', 'zayco', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(99, 'zayco', '2017-10-10 15:42:04', 'zayco', 'spexec', 'Hello', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(100, 'zayco', '2017-10-10 15:42:04', 'zayco', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(101, 'zayco', '2017-10-10 15:43:42', 'zayco', 'spexec', 'Hello', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(102, 'zayco', '2017-10-10 15:43:42', 'zayco', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(103, 'zayco', '2017-10-10 15:43:46', 'zayco', 'spexec', 'Hello', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(104, 'zayco', '2017-10-10 15:43:46', 'zayco', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(105, 'spexec', '2017-10-10 15:44:04', 'spexec', 'zayco', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(106, 'spexec', '2017-10-10 15:44:04', 'spexec', 'zayco', 'hello', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(107, 'spexec', '2017-10-10 15:44:06', 'spexec', 'zayco', 'q', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(108, 'spexec', '2017-10-10 15:44:06', 'spexec', 'zayco', 'q', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(109, 'spexec', '2017-10-10 15:44:08', 'spexec', 'zayco', 'w', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(110, 'spexec', '2017-10-10 15:44:08', 'spexec', 'zayco', 'w', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(111, 'spexec', '2017-10-10 15:44:10', 'spexec', 'zayco', 'r', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(112, 'spexec', '2017-10-10 15:44:10', 'spexec', 'zayco', 'r', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(113, 'spexec', '2017-10-10 15:44:11', 'spexec', 'zayco', 't', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(114, 'spexec', '2017-10-10 15:44:11', 'spexec', 'zayco', 't', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(115, 'spexec', '2017-10-10 15:44:13', 'spexec', 'zayco', 'y', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(116, 'spexec', '2017-10-10 15:44:13', 'spexec', 'zayco', 'y', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(117, 'spexec', '2017-10-10 15:44:14', 'spexec', 'zayco', 'y', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(118, 'spexec', '2017-10-10 15:44:14', 'spexec', 'zayco', 'y', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(119, 'spexec', '2017-10-10 15:44:16', 'spexec', 'zayco', 'y', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(120, 'spexec', '2017-10-10 15:44:16', 'spexec', 'zayco', 'y', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(121, 'spexec', '2017-10-10 15:44:17', 'spexec', 'zayco', 'u', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(122, 'spexec', '2017-10-10 15:44:17', 'spexec', 'zayco', 'u', 'zayco', 1, '2017-10-10 15:47:54', 'zayco', 1),
(123, 'alonso', '2017-10-10 15:49:15', 'alonso', 'spexec', 'xzcz', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(124, 'alonso', '2017-10-10 15:49:15', 'alonso', 'spexec', 'xzcz', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(125, 'alonso', '2017-10-10 15:49:16', 'alonso', 'spexec', 'dsfsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(126, 'alonso', '2017-10-10 15:49:16', 'alonso', 'spexec', 'dsfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(127, 'alonso', '2017-10-10 15:49:18', 'alonso', 'spexec', 'sdfsd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(128, 'alonso', '2017-10-10 15:49:18', 'alonso', 'spexec', 'sdfsd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(129, 'alonso', '2017-10-10 15:49:19', 'alonso', 'spexec', 'dfgsd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(130, 'alonso', '2017-10-10 15:49:19', 'alonso', 'spexec', 'dfgsd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(131, 'alonso', '2017-10-10 15:49:20', 'alonso', 'spexec', 'sdgfsdg', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(132, 'alonso', '2017-10-10 15:49:20', 'alonso', 'spexec', 'sdgfsdg', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(133, 'alonso', '2017-10-10 15:49:29', 'alonso', 'spexec', 'xcxzc', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(134, 'alonso', '2017-10-10 15:49:29', 'alonso', 'spexec', 'xcxzc', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(135, 'alonso', '2017-10-10 15:49:30', 'alonso', 'spexec', 'dsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(136, 'alonso', '2017-10-10 15:49:30', 'alonso', 'spexec', 'dsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(137, 'alonso', '2017-10-10 15:49:31', 'alonso', 'spexec', 'sdgfsdg', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(138, 'alonso', '2017-10-10 15:49:31', 'alonso', 'spexec', 'sdgfsdg', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(139, 'alonso', '2017-10-10 15:49:32', 'alonso', 'spexec', 'sdgsd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(140, 'alonso', '2017-10-10 15:49:32', 'alonso', 'spexec', 'sdgsd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(141, 'alonso', '2017-10-12 20:49:37', 'alonso', 'largado', 'Hello', 'alonso', 1, '2017-10-12 20:49:46', 'alonso', 1),
(142, 'alonso', '2017-10-12 20:49:37', 'alonso', 'largado', 'Hello', 'largado', 0, '0000-00-00 00:00:00', '', 0),
(143, 'alonso', '2017-10-12 20:49:41', 'alonso', 'largado', 'Gello', 'alonso', 1, '2017-10-12 20:49:46', 'alonso', 1),
(144, 'alonso', '2017-10-12 20:49:41', 'alonso', 'largado', 'Gello', 'largado', 0, '0000-00-00 00:00:00', '', 0),
(145, 'alonso', '2017-10-12 21:36:01', 'alonso', 'regalia', 'Hello', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(146, 'alonso', '2017-10-12 21:36:01', 'alonso', 'regalia', 'Hello', 'regalia', 0, '0000-00-00 00:00:00', '', 0),
(147, 'alonso', '2017-10-12 21:36:07', 'alonso', 'rivera', 'Hello', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(148, 'alonso', '2017-10-12 21:36:07', 'alonso', 'rivera', 'Hello', 'rivera', 0, '0000-00-00 00:00:00', '', 0),
(149, 'alonso', '2017-10-14 16:30:54', 'alonso', 'anacan', 'Hello', 'alonso', 1, '2017-10-14 16:33:54', 'alonso', 1),
(150, 'alonso', '2017-10-14 16:30:54', 'alonso', 'anacan', 'Hello', 'anacan', 0, '0000-00-00 00:00:00', '', 0),
(151, 'alonso', '2017-10-24 12:26:38', 'alonso', 'spexec', 'Hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(152, 'alonso', '2017-10-24 12:26:38', 'alonso', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(153, 'spexec', '2017-10-24 12:26:53', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(154, 'spexec', '2017-10-24 12:26:53', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(155, 'spexec', '2017-10-24 12:27:29', 'spexec', 'alonso', 'huhihoi', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(156, 'spexec', '2017-10-24 12:27:29', 'spexec', 'alonso', 'huhihoi', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(157, 'spexec', '2017-10-24 12:27:47', 'spexec', 'alonso', 'ghgfhgf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(158, 'spexec', '2017-10-24 12:27:47', 'spexec', 'alonso', 'ghgfhgf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(159, 'spexec', '2017-10-24 12:28:11', 'spexec', 'alonso', 'gfdgfd', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(160, 'spexec', '2017-10-24 12:28:11', 'spexec', 'alonso', 'gfdgfd', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(161, 'alonso', '2017-10-31 16:03:30', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(162, 'alonso', '2017-10-31 16:03:30', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(163, 'alonso', '2017-10-31 16:07:23', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(164, 'alonso', '2017-10-31 16:07:23', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(165, 'alonso', '2017-10-31 16:12:47', 'alonso', 'spexec', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(166, 'alonso', '2017-10-31 16:12:47', 'alonso', 'spexec', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(167, 'alonso', '2017-10-31 16:13:14', 'alonso', 'spexec', 'hi', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(168, 'alonso', '2017-10-31 16:13:14', 'alonso', 'spexec', 'hi', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(169, 'spexec', '2017-10-31 16:13:29', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(170, 'spexec', '2017-10-31 16:13:29', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(171, 'spexec', '2017-10-31 16:24:24', 'spexec', 'alonso', 'sdfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(172, 'spexec', '2017-10-31 16:24:24', 'spexec', 'alonso', 'sdfsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(173, 'spexec', '2017-11-06 18:40:29', 'spexec', 'alonso', 'gello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(174, 'spexec', '2017-11-06 18:40:29', 'spexec', 'alonso', 'gello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(175, 'spexec', '2017-11-06 18:40:38', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(176, 'spexec', '2017-11-06 18:40:38', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(177, 'spexec', '2017-11-06 18:41:46', 'spexec', 'alonso', 'dsdsfsdfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(178, 'spexec', '2017-11-06 18:41:46', 'spexec', 'alonso', 'dsdsfsdfsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(179, 'alonso', '2017-11-06 18:41:57', 'alonso', 'spexec', 'fhfgh', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(180, 'alonso', '2017-11-06 18:41:57', 'alonso', 'spexec', 'fhfgh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(181, 'spexec', '2017-11-06 18:42:06', 'spexec', 'alonso', 'tryrty', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(182, 'spexec', '2017-11-06 18:42:06', 'spexec', 'alonso', 'tryrty', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(183, 'spexec', '2017-11-06 18:42:46', 'spexec', 'alonso', 'fsdfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(184, 'spexec', '2017-11-06 18:42:46', 'spexec', 'alonso', 'fsdfsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(185, 'alonso', '2017-11-06 18:42:51', 'alonso', 'spexec', 'dfsdf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(186, 'alonso', '2017-11-06 18:42:51', 'alonso', 'spexec', 'dfsdf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(187, 'spexec', '2017-11-06 18:42:59', 'spexec', 'alonso', 'gfhfghfghfhgfh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(188, 'spexec', '2017-11-06 18:42:59', 'spexec', 'alonso', 'gfhfghfghfhgfh', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(189, 'spexec', '2017-11-06 18:56:04', 'spexec', 'alonso', 'sdfdsf', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(190, 'spexec', '2017-11-06 18:56:04', 'spexec', 'alonso', 'sdfdsf', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(191, 'spexec', '2017-11-07 17:00:52', 'spexec', 'alonso', 'hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(192, 'spexec', '2017-11-07 17:00:52', 'spexec', 'alonso', 'hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(193, 'alonso', '2017-11-07 17:01:48', 'alonso', 'spexec', 'Hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(194, 'alonso', '2017-11-07 17:01:48', 'alonso', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(195, 'alonso', '2017-11-07 17:02:10', 'alonso', 'spexec', 'Hello', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(196, 'alonso', '2017-11-07 17:02:10', 'alonso', 'spexec', 'Hello', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(197, 'spexec', '2017-11-07 17:02:17', 'spexec', 'alonso', 'hhhhhhh', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(198, 'spexec', '2017-11-07 17:02:17', 'spexec', 'alonso', 'hhhhhhh', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(199, 'alonso', '2017-11-07 17:02:25', 'alonso', 'spexec', 'Geggg', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(200, 'alonso', '2017-11-07 17:02:25', 'alonso', 'spexec', 'Geggg', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(201, 'alonso', '2017-11-07 17:02:35', 'alonso', 'spexec', 'Gejju', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(202, 'alonso', '2017-11-07 17:02:35', 'alonso', 'spexec', 'Gejju', 'spexec', 1, '0000-00-00 00:00:00', '', 0),
(203, 'alonso', '2017-11-08 22:04:52', 'alonso', 'spexec', 'Geyyyy', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(204, 'alonso', '2017-11-08 22:04:52', 'alonso', 'spexec', 'Geyyyy', 'spexec', 0, '0000-00-00 00:00:00', '', 0),
(205, 'cordero', '2017-11-11 21:57:05', 'cordero', 'alonso', 'Hello', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(206, 'cordero', '2017-11-11 21:57:05', 'cordero', 'alonso', 'Hello', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(207, 'alonso', '2017-11-11 21:57:13', 'alonso', 'cordero', 'hi', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(208, 'alonso', '2017-11-11 21:57:13', 'alonso', 'cordero', 'hi', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(209, 'alonso', '2017-11-11 21:57:22', 'alonso', 'cordero', 'hello', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(210, 'alonso', '2017-11-11 21:57:22', 'alonso', 'cordero', 'hello', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(211, 'cordero', '2017-11-11 21:57:28', 'cordero', 'alonso', 'Hi', 'cordero', 1, '0000-00-00 00:00:00', '', 0),
(212, 'cordero', '2017-11-11 21:57:28', 'cordero', 'alonso', 'Hi', 'alonso', 1, '0000-00-00 00:00:00', '', 0),
(213, 'alonso', '2017-11-13 18:43:30', 'alonso', 'spexec', 'jgkjgj', 'alonso', 1, '2017-11-20 13:32:02', 'alonso', 1),
(214, 'alonso', '2017-11-13 18:43:30', 'alonso', 'spexec', 'jgkjgj', 'spexec', 0, '0000-00-00 00:00:00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `committee`
--

CREATE TABLE `committee` (
  `code` varchar(30) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `isdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `committee`
--

INSERT INTO `committee` (`code`, `detail`, `mod_by`, `mod_date`, `enable`, `isdelete`) VALUES
('COM59CB558B2C818', 'Committee In Finance Appropriations and Ways and Means', 'spexec', '2017-09-29 18:45:01', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `row_id` int(11) NOT NULL,
  `members_view` varchar(30) NOT NULL,
  `audience_view` varchar(30) NOT NULL,
  `autosave` tinyint(1) NOT NULL DEFAULT '0',
  `autosave_delay` tinyint(2) NOT NULL DEFAULT '5',
  `pageview_delay` tinyint(2) NOT NULL DEFAULT '5',
  `vote_code` varchar(30) NOT NULL,
  `vote_alter` tinyint(1) NOT NULL DEFAULT '0',
  `vote_once` tinyint(1) NOT NULL DEFAULT '1',
  `vote_view_delay` tinyint(4) NOT NULL DEFAULT '1',
  `vote_audience_view` varchar(30) NOT NULL,
  `vote_members_view` varchar(30) NOT NULL,
  `editor_width` int(5) NOT NULL DEFAULT '700',
  `editor_height` int(5) NOT NULL DEFAULT '500',
  `members_alert_message` text NOT NULL,
  `members_alert_message_show` tinyint(1) NOT NULL DEFAULT '0',
  `members_alert_message_date` datetime NOT NULL,
  `audience_alert_message` text NOT NULL,
  `audience_alert_message_show` tinyint(1) NOT NULL DEFAULT '0',
  `audience_alert_message_date` datetime NOT NULL,
  `alert_delay` tinyint(2) NOT NULL DEFAULT '3',
  `action_list_font_size` varchar(10) NOT NULL DEFAULT 'medium',
  `members_action_view` varchar(30) NOT NULL,
  `audience_action_view` varchar(30) NOT NULL,
  `image_members_view` varchar(30) NOT NULL,
  `image_audience_view` varchar(30) NOT NULL,
  `image_audience_view_date` datetime NOT NULL,
  `image_audience_current` varchar(30) NOT NULL,
  `default_web_address` varchar(200) NOT NULL,
  `web_viewing_enable` tinyint(1) NOT NULL DEFAULT '0',
  `messaging` tinyint(1) NOT NULL DEFAULT '1',
  `messaging_popup` tinyint(1) NOT NULL DEFAULT '1',
  `account_limit` tinyint(4) NOT NULL DEFAULT '21',
  `show_action_writer` tinyint(1) NOT NULL DEFAULT '1',
  `show_voting` tinyint(1) NOT NULL DEFAULT '1',
  `show_image_gallery` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`row_id`, `members_view`, `audience_view`, `autosave`, `autosave_delay`, `pageview_delay`, `vote_code`, `vote_alter`, `vote_once`, `vote_view_delay`, `vote_audience_view`, `vote_members_view`, `editor_width`, `editor_height`, `members_alert_message`, `members_alert_message_show`, `members_alert_message_date`, `audience_alert_message`, `audience_alert_message_show`, `audience_alert_message_date`, `alert_delay`, `action_list_font_size`, `members_action_view`, `audience_action_view`, `image_members_view`, `image_audience_view`, `image_audience_view_date`, `image_audience_current`, `default_web_address`, `web_viewing_enable`, `messaging`, `messaging_popup`, `account_limit`, `show_action_writer`, `show_voting`, `show_image_gallery`) VALUES
(1, 'JDX1711262017164303', 'JDX9910192017221721', 1, 1, 2, '', 1, 1, 1, '', '', 500, 500, '<p>hello</p>', 0, '2017-11-11 22:00:56', '<p><span style=\"font-size: 60pt;\">dsfsdfsd</span></p>\n<p><span style=\"font-size: 60pt;\">afsfasfasf</span></p>', 0, '2017-10-11 16:32:45', 2, 'x-large', '', '', '', '', '2017-10-07 01:05:21', 'IMG1509052012113050', 'http://www.google.com', 0, 1, 1, 21, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `code` varchar(30) NOT NULL,
  `title` varchar(200) NOT NULL,
  `document` longtext NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `filename` varchar(250) NOT NULL,
  `source` varchar(10) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `save` tinyint(1) NOT NULL DEFAULT '0',
  `document_type` varchar(30) NOT NULL,
  `document_date` date NOT NULL,
  `isdelete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_type`
--

CREATE TABLE `document_type` (
  `code` varchar(30) NOT NULL,
  `detail` varchar(100) NOT NULL,
  `place` tinyint(4) NOT NULL DEFAULT '1',
  `members_show` tinyint(1) NOT NULL DEFAULT '1',
  `mod_by` varchar(20) NOT NULL,
  `mod_date` date NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `isdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_type`
--

INSERT INTO `document_type` (`code`, `detail`, `place`, `members_show`, `mod_by`, `mod_date`, `enable`, `isdelete`) VALUES
('DOC9211192011210909', 'Proposals', 3, 1, 'dbadmin', '2011-11-19', 1, 0),
('DOC6311192011210858', 'Admin Cases', 2, 1, 'dbadmin', '2011-11-19', 1, 0),
('DOC2811192011210747', 'Session Agenda', 1, 1, 'spexec', '2017-11-21', 1, 0),
('DOC6011192011211041', 'Minutes', 4, 1, 'dbadmin', '2011-11-19', 1, 0),
('DOC0312052011150332', 'Committee Report', 5, 1, 'dbadmin', '2011-12-05', 1, 0),
('DOC59CB3723DE1CC', 'Committee Agenda', 6, 1, 'spexec', '2017-09-27', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `image_gallery`
--

CREATE TABLE `image_gallery` (
  `code` varchar(30) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `file_type` varchar(15) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `print_action`
--

CREATE TABLE `print_action` (
  `code` varchar(30) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `row_id` bigint(20) NOT NULL,
  `code` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `vote` tinyint(1) NOT NULL,
  `mod_date` datetime NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vote_header`
--

CREATE TABLE `vote_header` (
  `code` varchar(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `voting_date` date NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vote_item`
--

CREATE TABLE `vote_item` (
  `code` varchar(20) NOT NULL,
  `detail` varchar(10) NOT NULL,
  `value` tinyint(1) NOT NULL,
  `place` tinyint(1) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vote_item`
--

INSERT INTO `vote_item` (`code`, `detail`, `value`, `place`, `enable`, `deleted`, `mod_by`, `mod_date`) VALUES
('VOT59AEBE8F9F4B7', 'Yes', 1, 1, 1, 0, 'admin', '2017-09-06 00:28:59'),
('VOT59AEBE95439BC', 'No', 2, 2, 1, 0, 'admin', '2017-09-06 00:30:19'),
('VOT59AEBE9BD4F1D', 'Abstain', 3, 3, 1, 0, 'spexec', '2017-10-04 23:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `_group`
--

CREATE TABLE `_group` (
  `row_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_group`
--

INSERT INTO `_group` (`row_id`, `code`, `name`, `mod_by`, `mod_date`, `enable`) VALUES
(7, '8072611123525', 'Legislation', 'dbadmin', '2011-07-26 18:35:25', 1),
(3, '2060911185224', 'User', 'admin', '2011-06-09 18:52:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_program`
--

CREATE TABLE `_program` (
  `row_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `menu_name` varchar(30) NOT NULL,
  `group_code` varchar(20) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_program`
--

INSERT INTO `_program` (`row_id`, `code`, `filename`, `menu_name`, `group_code`, `mod_by`, `mod_date`, `enable`) VALUES
(2, '6060911155121', 'module/user/user.entry.php', 'User Entry', '2060911185224', 'dbadmin', '2011-06-09 20:32:40', 1),
(3, '1060911190809', 'module/user/user.access_rights.php', 'Access Rights', '2060911185224', 'admin', '2011-06-09 19:07:45', 1),
(13, '2072611123625', 'module/editor/editor.entry.php', 'Editor', '8072611123525', 'dbadmin', '2011-07-26 18:36:25', 1),
(14, '2072811040239', 'module/editor/editor.viewer.php', 'Viewer', '8072611123525', 'dbadmin', '2011-07-28 10:02:39', 1),
(15, '7072911075815', 'module/vote/vote.voters.entry.php', 'Voters Entry', '8072611123525', 'dbadmin', '2011-07-29 13:58:15', 1),
(17, '8081811162448', 'module/editor/editor.config.php', 'Configuration', '8072611123525', 'dbadmin', '2011-08-18 23:03:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_properties`
--

CREATE TABLE `_properties` (
  `row_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `online_user_delay` tinyint(2) NOT NULL DEFAULT '5',
  `chat_window_delay` tinyint(2) NOT NULL DEFAULT '5',
  `chat_enable` tinyint(1) NOT NULL DEFAULT '0',
  `ischat_popup` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_properties`
--

INSERT INTO `_properties` (`row_id`, `title`, `online_user_delay`, `chat_window_delay`, `chat_enable`, `ischat_popup`) VALUES
(1, 'Legisoft -\r\n\r\n Legislation Software', 5, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_user`
--

CREATE TABLE `_user` (
  `user_id` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `mname` varchar(2) NOT NULL,
  `title` varchar(100) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL,
  `login` datetime NOT NULL,
  `login_active` datetime NOT NULL,
  `logout` datetime NOT NULL,
  `voter` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:member,2:admin, 3:audience',
  `image_file` varchar(200) NOT NULL,
  `view_document` varchar(30) NOT NULL,
  `view_action` varchar(30) NOT NULL,
  `view_voting` varchar(30) NOT NULL,
  `view_image` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `_user`
--

INSERT INTO `_user` (`user_id`, `username`, `password`, `fname`, `lname`, `mname`, `title`, `enable`, `mod_by`, `mod_date`, `login`, `login_active`, `logout`, `voter`, `type`, `image_file`, `view_document`, `view_action`, `view_voting`, `view_image`, `deleted`) VALUES
('A59AEC20E60174', 'tubola', '$2y$10$rInl4WFHWRBpd1TbzRMuHuQS.TorR8TO8UwF.3kT0cA46oVAQr/JO', 'Jeffrey', 'Tubola', 'T', 'City Councilor', 1, 'admin', '2017-09-25 23:48:12', '2017-09-27 14:29:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a1.jpg', '', '', '', '', 0),
('A59AEC27D47B0A', 'regalia', '$2y$10$9aXxldKVzNxpliu1Qr/PfO7NIllZn56p8.7.ZpdTUPJIbDume7YX6', 'Roger', 'Regalia', 'M', 'City Councilor', 1, 'admin', '2017-09-26 00:02:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a2.jpg', '', '', '', '', 0),
('A59AEC2995B089', 'zayco', '$2y$10$h93Xl4D02wi8kYQMRBCZO.1ZycygoHSHv0dnSZmuJJ4ONpfoUDBoO', 'Miguel', 'Zayco', 'M', 'City Councilor', 1, 'spexec', '2017-09-27 14:13:27', '2017-10-11 17:11:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a3.jpg', '', '', '', '', 0),
('A59AEC2C90B5CF', 'genizera', '$2y$10$6ZxwIde6dQOW6nhgkKZEMuPHFX3BqRG83VX/AmZQ.58UuKeJQ9DpK', 'Jeanna', 'Genizera', 'C', 'City Councilor', 1, 'admin', '2017-09-26 20:32:49', '2017-09-27 14:37:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a4.jpg', '', '', '', '', 0),
('A59AEC2DB8617F', 'anacan', '$2y$10$tbsiTUi6NC78Q.qL9JoH0eXLSA9PjEMv8PBeRpnMbcZOTojdkwW76', 'Delia', 'Anacan', 'O', 'City Councilor', 1, 'admin', '2017-09-26 20:32:38', '2017-09-27 14:37:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a5.jpg', '', '', '', '', 0),
('A59AEC2FF10D03', 'alonso', '$2y$10$gTc48k3FEqu9D0ITxjqyLuWJenoUaRRtCfLQJPph8TLnMqQM8alZy', 'Valentino Miguel', 'Alonso', 'J', 'City Councilor', 1, 'spexec', '2017-10-06 14:37:48', '2017-11-26 16:50:00', '2017-09-26 00:46:33', '2017-09-10 13:19:44', 1, 1, 'a6.jpg', '', '', '', '', 0),
('A59AEC3131B173', 'cordero', '$2y$10$z2Ikro5WH5KSOv3ZqQ0IK.B1hEKdnaA7Ep404v6L344VWkzurcDkK', 'Rey', 'Cordero', 'G', 'City Councilor', 1, 'spexec', '2017-10-06 14:54:28', '2017-11-11 21:56:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a7.jpg', '', '', '', '', 0),
('A59AEC3297625D', 'cordova', '$2y$10$wR8nZG6Oa59mxEfo3wB1F.eb5G7DOJ2ju66oiB8sf/DtCO2wgBOwq', 'Ramon', 'Cordova', 'L', 'City Councilor', 1, 'spexec', '2017-10-07 00:24:22', '2017-10-08 14:45:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a8.jpg', '', '', '', '', 0),
('A59AEC346F0558', 'dumaquete', '$2y$10$nzHBpkWNijVzCY8cjdG/Ou8AgrYdEeB5i8V5u5UYHR8staAMPxT3C', 'Celestino', 'Dumaguete', 'M', 'City Councilor', 1, 'spexec', '2017-09-27 13:45:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a9.jpg', '', '', '', '', 0),
('A59AEC38D2E3A8', 'gaspillo', '$2y$10$MGrNXoc1ZOQvhsOn47RskOT3i7uSWaJL.P5Pc/h7YnQepoCb9MHA6', 'Efren', 'Gaspillo, Sr.', 'V', 'City Councilor', 1, 'admin', '2017-09-26 20:30:39', '2017-09-27 14:38:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a10.jpg', '', '', '', '', 0),
('A59AEC3A670424', 'bandojo', '$2y$10$UBMIpsJysoih1ip2p/YTbuJWLnkMRJQnprKdkDLgVhRdyYskCARBy', 'Joestarr', 'Bandojo', 'B', 'City Councilor', 1, 'admin', '2017-09-26 21:41:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a11.jpg', '', '', '', '', 0),
('A59AEC3D945BC5', 'largado', '$2y$10$o6BAUI/V65K8HqRB4uzYrOIVYVrHzdrUTRR.wxtuoVksiMF/Df11C', 'Jeorge', 'Largado', 'M', 'JPM Representative', 1, 'admin', '2017-09-26 20:32:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a12.jpg', '', '', '', '', 0),
('A59AEC41B47F19', 'rivera', '$2y$10$LPo26Knh3/4GR7.PSf726.kn2l0hnV9ujte2Brkob7SsUaIbVE.bG', 'Raul', 'Rivera', 'C', 'Vice Mayor', 1, 'admin', '2017-09-05 23:40:16', '2017-09-27 14:30:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 'a13.jpg', '', '', '', '', 0),
('A59AEC461551CD', 'spsec', '$2y$10$jIot/mGEwpxaElBtDKuZruAWQYb8PkQoPS/IrrAGbqxrwHGEosIIq', 'Ma. Virginia', 'Española', 'G', 'Executive SP Secretary', 1, 'spexec', '2017-10-05 13:01:53', '2018-01-08 15:51:40', '2018-01-08 15:51:40', '2017-11-23 12:11:35', 0, 2, 'a14.jpg', '', '', '', '', 0),
('A59AEC4792BB62', 'tabujara', '$2y$10$YUzVYCGC2q5jtavr2lPeoeGwFd94Xt13XxwIrSrCds0jWfOPM42m2', 'Monalisa', 'Tabujara', 'D', 'Staff', 1, 'admin', '2017-09-10 13:18:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 2, 'a15.jpg', '', '', '', '', 0),
('A59B4CBEC70865', 'public', '$2y$10$NHa5cEsWcjUSTZhVOKNyReQnDzK2hHuexelh7Gsap5.ZxUxFaMK.K', 'Public', 'Viewing', '-', 'Projector', 1, 'spexec', '2017-09-27 13:45:14', '2017-11-23 12:14:25', '2017-11-23 12:14:25', '2017-11-23 12:14:57', 0, 3, 'kabankalan_negros_occidental.png', '', '', '', '', 0),
('A59CB3432C9B8E', 'maepiojo', '$2y$10$hcew8sD7vEFl7fsbatohkeCXoWRgHEr/SiD5qXdv1.Ys8ma.0J4YW', 'Mae', 'Piojo', 'B', 'LLSE II', 1, 'admin', '2017-09-27 13:16:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 2, '', '', '', '', '', 0),
('A59CB338E65AC2', 'aldwin', '$2y$10$rhKfqI8vPQd82tnkUehyzuxsDmCaS24cefpMHl1uCVgFFVaHoIDfa', 'John Alduen', 'Benigay', 'G', 'Utility Worker 1', 1, 'admin', '2017-09-27 13:13:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 2, '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `_userprogram`
--

CREATE TABLE `_userprogram` (
  `row_id` int(11) NOT NULL,
  `user` varchar(20) NOT NULL,
  `program_code` varchar(20) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_userprogram`
--

INSERT INTO `_userprogram` (`row_id`, `user`, `program_code`, `mod_by`, `mod_date`) VALUES
(4, 'junrey', '1060911190809', 'dbadmin', '2011-06-10 18:03:31'),
(5, 'junrey', '6060911155121', 'dbadmin', '2011-06-10 18:03:33'),
(7, 'junrey', '2072811040239', 'dbadmin', '2011-08-21 16:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `_user_delete`
--

CREATE TABLE `_user_delete` (
  `username` varchar(20) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `mname` varchar(2) NOT NULL,
  `mod_by` varchar(20) NOT NULL,
  `mod_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_user_online`
--

CREATE TABLE `_user_online` (
  `session` varchar(100) NOT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `action_committee`
--
ALTER TABLE `action_committee`
  ADD KEY `committee_code` (`committee_code`,`action_code`);

--
-- Indexes for table `action_item`
--
ALTER TABLE `action_item`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `action_movant`
--
ALTER TABLE `action_movant`
  ADD KEY `movant_code` (`movant_code`,`action_code`);

--
-- Indexes for table `action_reference`
--
ALTER TABLE `action_reference`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `action_second`
--
ALTER TABLE `action_second`
  ADD KEY `second_code` (`second_code`,`action_code`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `committee`
--
ALTER TABLE `committee`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `image_gallery`
--
ALTER TABLE `image_gallery`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `print_action`
--
ALTER TABLE `print_action`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `vote_header`
--
ALTER TABLE `vote_header`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `vote_item`
--
ALTER TABLE `vote_item`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `_group`
--
ALTER TABLE `_group`
  ADD PRIMARY KEY (`code`,`row_id`),
  ADD UNIQUE KEY `row_id` (`row_id`);

--
-- Indexes for table `_program`
--
ALTER TABLE `_program`
  ADD PRIMARY KEY (`code`,`row_id`),
  ADD UNIQUE KEY `row_id` (`row_id`);

--
-- Indexes for table `_properties`
--
ALTER TABLE `_properties`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `_user`
--
ALTER TABLE `_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `_userprogram`
--
ALTER TABLE `_userprogram`
  ADD PRIMARY KEY (`row_id`),
  ADD UNIQUE KEY `row_id` (`row_id`);

--
-- Indexes for table `_user_online`
--
ALTER TABLE `_user_online`
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_group`
--
ALTER TABLE `_group`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `_program`
--
ALTER TABLE `_program`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `_properties`
--
ALTER TABLE `_properties`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `_userprogram`
--
ALTER TABLE `_userprogram`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
